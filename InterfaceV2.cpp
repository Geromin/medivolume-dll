#define ITKV3_COMPATIBILITY//Necess�rio pq as classes foram escritas para uma vers�o antiga da biblioteca
#pragma warning(disable:4996)
#pragma warning(disable:4049)
//#include <vld.h>
#ifdef OPENGL_LEGADO
#include <glew.h>
#include <wglew.h>
#endif
#ifndef OPENGL_LEGADO
#include <vtk_glew.h>
#include <vtkglew\include\GL\wglew.h>
//#include <GL\wglew.h> //wglew.h"
#endif
////TESTE////
#include "itkVTKImageIO.h"
#include <itkFlipImageFilter.h>
#include <Windows.h>
#include <itkPermuteAxesImageFilter.h>
#include <itkFlipImageFilter.h>
#include <itkIntensityWindowingImageFilter.h>
#include <itkRigid3DTransform.h>
#include <itkOrientImageFilter.h>
#include <loadVolume.h>
#include <LetrasDeOrientacao.h>

#define STATUS_PARAMETROS_INVALIDOS       0xE0000001
#include <vtkRendererCollection.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPngWriter.h>
#include <vtkWindowToImageFilter.h>
#include <vtkImageMapToWindowLevelColors.h>
#include <vtkWin32OpenGLRenderWindow.h>
#include <vtkWin32RenderWindowInteractor.h>
#include <vtkObjectFactory.h>
#include <vtkResliceCursorLineRepresentation.h>
#include <vtkResliceCursorPolyDataAlgorithm.h>
#include <vtkResliceCursorActor.h>
#include <VideoMemory.h>
#include <array>
#include <vtkWin32ProcessOutputWindow.h>
#include <itkImageSeriesReader.h>
#include <itkGDCMImageIO.h>
#include <itkGDCMSeriesFileNames.h>
#include <UtilitiesV2.h>
#include <iostream>
#include <thread>
#include <Windows.h>
#include <string>
#include <vector>
#include "InterfaceDll.h"
#include "VolumeV2.h"
#include "SimpleTextureProvider.h"
#include "VolumeScreen.h"
#include "CubeScreen.h"
#include "CallbackProgressoDeCarga.h"
#include "CodigosDeOperacaoProvider.h"
#include <vtkFileOutputWindow.h>
#include "LogSystem.h"
#include "tinyxml.h"
#include "ThumbScreen.h"
#include <vtkBMPWriter.h>
#include <vtkWindowToImageFilter.h>
#include <vtkMath.h>
#include "cargaDoVolume.h"
#include "glewinfo.h"
#include <vtkImageFlip.h>
#include "SubsistemaVR.h"
#include <loadedImage.h>
#include <imageSource.h>
#include <vtkMatrix4x4.h>
#include <vtkImageSlabReslice.h>
#include <vtkImageMapToWindowLevelColors.h>
#include <codecvt>
#include <SubsistemaMPR.h>
#include <boost/exception/all.hpp>
#include <itkImageFileWriter.h>
#include <ITKIOVTKExport.h>
#include <ResliceCubico.h>
#include "dicomGenerator.h"

int mprWindowInicial;
int mprLevelInicial;

using namespace vr;


vtkWin32ProcessOutputWindow* ProcessOutput = nullptr;
vtkFileOutputWindow* FileOutput = nullptr;
//A metadata da s�rie - carregada no init volume
//map<string, string> metadataDaImagem;

//Fun��o do delphi que move a barrinha de carga.
callbackDeProgressDaCarga callbackProgressoCarga;

ThumbScreen* thumbScreen;
//ptr pro objeto de tela do volume.
//VolumeScreen *telaVolume;
//ptr pro objeto de teal do cubo.
CubeScreen *telaCubo;
//Container pra textura das fatias
SimpleTextureProvider *textureProvider;
//Lista com os nomes das fatias.
std::vector<std::string> listaDeFatias;
//O objeto de volume
unique_ptr<VolumeV2> volumeObject;
//A pipeline do mpr

//Ponteiro para os dados carregados no initVolume
//itk::Image<short, 3>::Pointer imagemInicializada;
LogSystem *sist;

//unique_ptr<TelaGrandeScreenV2> TelaGrande;
int MapperFlag;
//vtkSmartPointer<ResliceCursorCallback> resliceCursorCallback;
//vtkSmartPointer<ResliceWindowCallback> windowResliceCallback;
HDC dcDaTelaMPR4; HWND handleDaTelaMPR4; HGLRC glrcTelaMPR4;
int larguraTelaMpr4, alturaTelaMpr4;

vector<thread> cinThreadContainer;

unique_ptr<datasource::ImageSource> imageLoader;
//Ao inv�s de uma unica instancia, uma lista de instancias.
vector<shared_ptr<datasource::LoadedImage>> images;
//datasource::SliceImageSource SliceSource;
//podem haver multiplos VRs.
vector<shared_ptr<SubsistemaVR>> SubsistemasVR;
vector<shared_ptr<mpr::Subsistema>> SubsistemasMPR;
vector<shared_ptr<ResliceCubico>> SubsistemasCuboReslice;
///Essa fun��o retorna o handle de um subsistema com o idExame e idSerie iguais ao passados como par�metro.
///Assume-se que T tem o m�todo bool IsThisExam(char*, char*), Se achar o subsistema retorna o handle dele,
///se n�o achar retorna 0.
template <class T> long GetHandleDoSubsistema(vector<shared_ptr<T>>vec, string idExame, string idSerie);
//////////////////////////////////////////////////////////////////////////////////////////////////////
//Invocado quando o windows atacha ou detacha a dll
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
	{
		sist = new LogSystem(LogSystem::TipoDeSaida::Arquivo);
		MapperFlag = 1;
		thumbScreen = nullptr;
		textureProvider = new SimpleTextureProvider();
		imageLoader = nullptr;
		images.clear();
	}
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
#ifdef DEBUG
		delete sist;
#endif
#ifndef DEBUG
		delete sist;
#endif
		delete textureProvider;
		volumeObject = nullptr;
		listaDeFatias.clear();
		if (FileOutput)
			FileOutput->Delete();
		FileOutput = nullptr;
//		SistemaMpr = nullptr;
		images.clear();
		break;
	}
	return TRUE;
}

void PaintWindowAsBlack(HWND handleDaJanela)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(handleDaJanela, &ps);
	// All painting occurs here, between BeginPaint and EndPaint.
	HBRUSH hbrBkgnd;
	hbrBkgnd = CreateSolidBrush(RGB(0, 0, 0));
	FillRect(hdc, &ps.rcPaint, hbrBkgnd);
	EndPaint(handleDaJanela, &ps);
}


static vtkSmartPointer<vtkPlaneSource> MPRDebug_axialPlane = nullptr;
static vtkSmartPointer<vtkPlaneSource> MPRDebug_coronalPlane = nullptr;
static vtkSmartPointer<vtkPlaneSource> MPRDebug_sagittalPlane = nullptr;
static vtkSmartPointer<vtkRenderWindow> MPRDebug_renderWindow = nullptr;

std::string WStringToString(std::wstring str)
{
	//setup converter
	using convert_type = codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;

	//use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
	std::string converted_str = converter.to_bytes(str);
	return converted_str;
}

shared_ptr<datasource::LoadedImage> FindLoadedImage(char* idExame, char* idSerie)
{
	cout << __FUNCTION__ << endl;
	shared_ptr<datasource::LoadedImage> chosenImage = nullptr;
	for (shared_ptr<datasource::LoadedImage> im : images)
	{
		if (im->IsThisExam(idExame, idSerie))
		{
			chosenImage = im;
			break;
		}
	}
	assert(chosenImage);
	return chosenImage;
}

short _stdcall GetValorSomadoAosEscalares(char* idExame, char* idSerie)
{
	cout << __FUNCTION__ << endl;
	return FindLoadedImage(idExame, idSerie)->valueOffset;
}

shared_ptr<datasource::LoadedImage> GetImagemPedida(const char* idExame, const char* idSerie)
{
	cout << __FUNCTION__ << endl;
	string strExame(idExame);
	string strSerie(idSerie);
	for (unsigned int i = 0; i < images.size(); i++)
	{
		shared_ptr<datasource::LoadedImage> img = images[i];
		if (img->GetIdExame() == strExame && img->GetIdSerie() == idSerie)
		{
			return img;
		}
	}
	return nullptr;
}


void _stdcall VR_RenderVolume(long handleDoSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->RenderVolume();
}

void _stdcall VR_RenderCubo(long handleDoSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->RenderCuboDeOrientacao();
}



bool _stdcall HasLoadedImage(const char* idExame, const char* idSerie)
{
	cout << __FUNCTION__ << endl;
	if (GetImagemPedida(idExame, idSerie))
		return true;
	else
		return false;
}


void _stdcall DeleteAllImages()
{
	cout << __FUNCTION__ << endl;
	images.clear();
}


void _stdcall DeleteLoadedImage(const char* idExame, const char* idSerie)
{
	cout << __FUNCTION__ << endl;
	string strExame(idExame);
	string strSerie(idSerie);
	vector<int> idsDasImagensAchadas;
	for (unsigned int i = 0; i < images.size(); i++)
	{
		datasource::LoadedImage* img = images[i].get();
		if (img->GetIdExame() == strExame && img->GetIdSerie() == idSerie)//Achou
		{
			idsDasImagensAchadas.push_back(i);
		}
	}
	if (idsDasImagensAchadas.size() == 0)
		return;
	else
	{
		vector<int> idDosSubsistemasVRAchados;
		//N�o basta deletar as imagens - tem que destruir as pipelines que dependem dela.
		for (unsigned int i = 0; i < SubsistemasVR.size(); i++)
		{
			SubsistemaVR* sub = SubsistemasVR[i].get();
			if (sub->IsThisExam((char*)idExame, (char*)idSerie))
			{
				idDosSubsistemasVRAchados.push_back(i);
			}
		}
		//Deleta os subsistemas de vr
		for (int i : idDosSubsistemasVRAchados)
		{
			SubsistemasVR.erase(SubsistemasVR.begin() + i);
		}
		//Deleta as imagens
		for (int i : idsDasImagensAchadas)
		{
			images.erase(images.begin() + i);
		}
		//Tenho que deletar as pipelines que usam a imagem
		//sert("Deletei as pipelines que usam a imagem?" && false);
	}
}
//-------------------------------------------------------------------------------
int _thumb_x, _thumb_y;
int _stdcall GetQualDllDeAcordoComOpengl()
{
	cout << __FUNCTION__ << endl;
	std::string resultadoDoTeste = glewMainTest(0, nullptr, "");
	vector<string> linhas = SplitString(resultadoDoTeste, "\n");
	int resultado = 0;
	for (string current : linhas)
	{
		//ve se a linha atual tem o q eu quero:GL_VERSION_3_2; GL_VERSION_1_5; OK
		size_t foundGL_VERSION_1_5 = current.find("GL_VERSION_1_5");
		if (foundGL_VERSION_1_5 != string::npos)
		{
			//Ve se tem OK
			size_t foundOK = current.find("OK");
			if (foundOK != string::npos)
			{
				cout << "GL_VERSION_1_5 ENCONTRADO" << endl;
				resultado = 1;
			}
		}
		size_t foundGL_VERSION_3_2 = current.find("GL_VERSION_3_2");
		if (foundGL_VERSION_3_2 != string::npos)
		{
			size_t foundOK = current.find("OK");
			if (foundOK != string::npos)
			{
				cout << "GL_VERSION_3_2 ENCONTRADO" << endl;
				resultado = 2;
			}
		}
	}
	//std::string GetVideoMemory(bool& usouDXGI);
	bool isUsandoDXGI;
	std::wstring strMemReport = GetVideoMemory(isUsandoDXGI);
	std::string smr = WStringToString(strMemReport);
	cout << resultadoDoTeste << endl;
	return resultado;
}

void _stdcall GetVideoMemory(int& dedicated, int& shared)
{
	cout << __FUNCTION__ << endl;
	bool isUsandoDXGI;
	std::wstring strMemReport = GetVideoMemory(isUsandoDXGI);
	if (isUsandoDXGI)
	{
		//O Adapter 0 � sempre a placa de v�deo que est� em uso, rodando o programa. Isso importa pq
		//notebooks tem mais de uma placa de v�deo.
		//1) split por device ('DXGI Adapter:')
		vector<wstring> devices = wSplitString(strMemReport, L"DXGI Adapter:");
		wstring deviceZero = devices[1];
		//2) o que vem depois de 'DedicatedVideoMemory:'
		wstring sDedicatedMem = wtrim( wSplitString(wSplitString(deviceZero, L"DedicatedVideoMemory: ")[1], L"MB")[0] );
		dedicated = std::stoi(sDedicatedMem);
		//3) o que vem depois de 'SharedSystemMemory:'
		wstring sSharedMem = wtrim ( wSplitString(wSplitString(deviceZero, L"SharedSystemMemory: ")[1], L"MB")[0] );
		shared = std::stoi(sSharedMem);
		//Converte o wstring pra string e imprime na saida padr�o (que � o arquivo de log)

		std::string smr = WStringToString(strMemReport);
		cout << smr << endl;
		//vector<string> linhas = SplitString(resultadoDoTeste, "\n");
	}
	else
	{
		cout << "N�o est� usando DXGI, vai falhar" << endl;
		dedicated = -1;
		shared = -1;
	}
}

void _stdcall GetGlCaps(char* path)
{
	cout << __FUNCTION__ << endl;
	std::string spath(path);
	std::string resultadoDoTeste = glewMainTest(0, nullptr, spath);
	vector<string> linhas = SplitString(resultadoDoTeste, "\n");
	for (string current:linhas)
	{
		//ve se a linha atual tem o q eu quero:GL_VERSION_3_2; GL_VERSION_1_5; OK
		size_t foundGL_VERSION_1_5 = current.find("GL_VERSION_1_5");
		if (foundGL_VERSION_1_5 != string::npos)
		{
			//Ve se tem OK
			size_t foundOK = current.find("OK");
			if (foundOK != string::npos)
				cout << "GL_VERSION_1_5 ENCONTRADO" << endl;
		}
		size_t foundGL_VERSION_3_2 = current.find("GL_VERSION_3_2");
		if (foundGL_VERSION_3_2 != string::npos)
		{
			size_t foundOK = current.find("OK");
			if (foundOK != string::npos)
				cout << "GL_VERSION_3_2 ENCONTRADO" << endl;
		}
	}
	cout << resultadoDoTeste << endl;
}

void _stdcall UsarGPU()
{
	cout << __FUNCTION__ << endl;
	MapperFlag = 1;
}
void _stdcall UsarCPU()
{
	cout << __FUNCTION__ << endl;
	MapperFlag = 2;
}


void _stdcall GetWL(int* w, int *l, long handleDoSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	(*w) = sub->GetWL()[0];
	(*l) = sub->GetWL()[1];
}

void _stdcall SetSpecularPower(float spec)
{

}
//----------------------------------------------------MPR -----------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
void _stdcall SetWindow(int w, int l, long handleDoSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetWindow(w, l);
	sub->RenderVolume();
}
void _stdcall SetThumbDimensions(int x, int y, long handleDoSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	_thumb_x = x; _thumb_y = y;
	sub->ResizeThumbScreen(x, y);
}
void _stdcall SwitchOffscreen(bool b)
{
	cout << __FUNCTION__ << endl;
	assert(false);//Obsoleta
}

const double degToRad = 0.0174533;
void ParametricCircleR3(double P[3], const double C[3], const double azi, const double elev, const double r, const double t)
{
	cout << __FUNCTION__ << endl;

	P[0] = r * cos(t)*(-sin(elev)) + r*sin(t)*(cos(azi)*cos(elev)) + C[0];
	P[1] = r * cos(t)*(cos(elev)) + r*sin(t)*(cos(azi)*sin(elev)) + C[1];
	P[2] = r * cos(t)*(0) + r*sin(t)*(-sin(azi)) + C[0];
}


void _stdcall GerarPrintsBMP2(bool isDirecaoHorizontal, bool isSentidoPositivo, int numeroDeQuadros, char* nomeDoArquivo, long handleDoSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	if (numeroDeQuadros <= 0)
		return;
	const int quantidadeDePontos = 360;
	//const int step = 360 / numeroDeQuadros+1;
	double viewUp[3];
	sub->GetRendererDaTelaDoVolume()->GetActiveCamera()->GetViewUp(viewUp);
	double *volumeCenter = sub->GetVolumeCenter();
	double *eye = sub->GetRendererDaTelaDoVolume()->GetActiveCamera()->GetPosition();
	double vecDist[] = { volumeCenter[0] - eye[0], volumeCenter[1] - eye[1], volumeCenter[2] - eye[2] };
	double distance = sqrt(vecDist[0] * vecDist[0] + vecDist[1] * vecDist[1] + vecDist[2] * vecDist[2]);
	vtkSmartPointer<vtkRegularPolygonSource> circleSource;
	vtkSmartPointer<vtkPolyDataMapper> circleMapper;
	vtkSmartPointer<vtkActor> circleActor;
	circleSource = vtkSmartPointer<vtkRegularPolygonSource>::New();
	circleSource->SetNumberOfSides(quantidadeDePontos);// a quantidade de pontos = qtd de prints, vir� aqui.
	//double d = telaVolume->GetRenderer()->GetActiveCamera()->GetDistance();
	circleSource->GeneratePolygonOff();
	circleSource->GeneratePolylineOn();
	circleSource->SetRadius(distance);
	circleSource->SetCenter(sub->GetVolumeCenter());
	//A normal depende se � pra fazer o circulo horizontal ou vertical
	double planeNormal[3];
	if (isDirecaoHorizontal)
	{
		planeNormal[0] = viewUp[0];
		planeNormal[1] = viewUp[1];
		planeNormal[2] = viewUp[2];
	}
	else
	{

		const double *direction = sub->GetRendererDaTelaDoVolume()->GetActiveCamera()->GetDirectionOfProjection();
		vtkMath::Cross(viewUp, direction, planeNormal);
	}
	circleSource->SetNormal(planeNormal);
	circleSource->Update();
	circleMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	circleMapper->SetInputConnection(circleSource->GetOutputPort());
	circleMapper->Update();
	circleActor = vtkSmartPointer<vtkActor>::New();
	circleActor->SetMapper(circleMapper);
	//telaVolume->GetRenderer()->AddActor(circleActor);
	circleActor->VisibilityOff();
	sub->RenderVolume();
	//Qual � o ponto inicial? O ponto inicial � aquele dentre os pontos existentes que est� mais pr�ximo da tela
	vtkPolyData* circlePd = circleSource->GetOutput();
	double minDist = 999999; int minPto = -1;
	for (int i = 0; i < quantidadeDePontos; i = i + 1)
	{
		const double* ptoNoCirculo = circlePd->GetPoints()->GetPoint(i);
		const double* ptoDoOlho = sub->GetRendererDaTelaDoVolume()->GetActiveCamera()->GetPosition();
		double dAtual = vtkMath::Distance2BetweenPoints(ptoDoOlho, ptoNoCirculo);
		if (dAtual < minDist)
		{
			minDist = dAtual;
			minPto = i;
		}
	}
	//agora que eu sei qual � o ponto inicial montar a lista
	struct V3
	{
		double x, y, z;
	};
	std::vector<V3> pontos;
	if (isSentidoPositivo)
	{
		for (int i = 0; i < quantidadeDePontos; i = i + 1)
		{
			int pointToVisit = minPto + i;
			//Se chegou no �ltimo elemento volta pro comeco
			if (pointToVisit >= quantidadeDePontos)
				pointToVisit = pointToVisit - quantidadeDePontos;
			const double* ptoNoCirculo = circlePd->GetPoints()->GetPoint(pointToVisit);
			V3 v = { ptoNoCirculo[0], ptoNoCirculo[1], ptoNoCirculo[2] };
			if (i % (360 / ((numeroDeQuadros - 1)==0?1:(numeroDeQuadros-1)) ) == 0)
				pontos.push_back(v);
		}
	}
	else
	{
		for (int i = 0; i < quantidadeDePontos; i = i + 1)
		{
			int pointToVisit = minPto - i;
			//Se chegou no �ltimo elemento volta pro comeco
			if (pointToVisit < 0)
				pointToVisit = pointToVisit + quantidadeDePontos;
			const double* ptoNoCirculo = circlePd->GetPoints()->GetPoint(pointToVisit);
			V3 v = { ptoNoCirculo[0], ptoNoCirculo[1], ptoNoCirculo[2] };
			if (i % (360 / ((numeroDeQuadros - 1) == 0 ? 1 : (numeroDeQuadros - 1))) == 0)
				pontos.push_back(v);
		}
	}
	const double* last = circlePd->GetPoints()->GetPoint(minPto);
	V3 l = { last[0], last[1], last[2] };
	pontos.push_back(l);
	//int qtd = 0;
	int index = 0;
	for (V3 &e : pontos)
	{
		sub->GetRendererDaTelaDoVolume()->GetActiveCamera()->SetPosition(e.x, e.y, e.z);
		sub->GetRendererDaTelaDoVolume()->GetActiveCamera()->OrthogonalizeViewUp();
		sub->RenderVolume();
		sub->ExecutarRotationListenersDaTelaDoVolume();


		vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
		windowToImageFilter->SetInput(sub->GetVolumeVtkWindow());
		windowToImageFilter->SetMagnification(1); //set the resolution of the output image (3 times the current resolution of vtk render window)
		windowToImageFilter->SetInputBufferTypeToRGB(); //also record the alpha (transparency) channel
		windowToImageFilter->ReadFrontBufferOff(); // read from the back buffer
		windowToImageFilter->Update();

		vtkSmartPointer<vtkBMPWriter> writer = vtkSmartPointer<vtkBMPWriter>::New();
		stringstream ss;
		ss << nomeDoArquivo << index << ".bmp";
		writer->SetFileName(ss.str().c_str());
		index++;
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();
	}

}
//Com esse aqui eu consegui gerar o percurso corretamente.



void _stdcall GenerateCanonicalView2(float* x, float* r, float* g, float* b,
	float* a, float* colorMid, float* colorSharpness, int qtdDePontos,
	float* gx, float* ga, float* gMid, float* gSharpness, int qtdDeGradiente,
	float ambient, float diffuse, float specular, bool shading, bool clamping, bool gradientOn,  long handleSubsistema)
{
	cout << __FUNCTION__ << endl;
	SubsistemaVR* sub = (SubsistemaVR*)handleSubsistema;
	sub->GenerateCanonicalView2(x, r, g, b, a, colorMid, colorSharpness,  qtdDePontos, gx, ga, gMid, gSharpness, qtdDeGradiente, ambient, diffuse, specular, shading, clamping, gradientOn);
}

void _stdcall GetAvailableMemory()
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

}

void _stdcall SetGradientOpacityDoThumb(int* x, float* a, float* midpoint, float* sharpness, int size)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;


}

void _stdcall SetGradientOpacity(int* x, float* a, float* midpoint, float* sharpness, int size)
{
	assert(false && "Obsoleto?");
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	//vtkSmartPointer<vtkPiecewiseFunction> fn = vtkSmartPointer<vtkPiecewiseFunction>::New();
	//for (int i = 0; i < size; i++)
	//{
	//	fn->AddPoint(x[i], a[i], midpoint[i], sharpness[i]);
	//}
	//volumeObject->GetVisualizacao()->SetGradientOpacity(fn);
	//std::cout << GetDateAsString() << " - " << "Fim de " << __FUNCTION__ << std::endl;
}



void _stdcall CriarJanelaDoThumb(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	PaintWindowAsBlack(handleDaJanela);

	SubsistemaVR* subsys = (SubsistemaVR*)handleDoSubsistema;
	subsys->CreateThumbScreen(handleDaJanela, dcDaJanela, glrcDaJanela);
	subsys->ActivateThumbScreen();
	//Old - deve ser removido
	//thumbScreen = new ThumbScreen(handleDaJanela, dcDaJanela, glrcDaJanela);
	//thumbScreen->Ativar();
}

void _stdcall CuboMPR_MatarSistema(long& handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	for (auto i = 0; i < SubsistemasCuboReslice.size(); i++) {
		shared_ptr<ResliceCubico> current = SubsistemasCuboReslice[i];
		if ((long)current.get() == handleDoSubsistema) {
			current->Destroy();//Destruir os objetos do subsistema
			SubsistemasCuboReslice[i] = nullptr;//Remove-lo da lista de subsistemas
		}
	}
	//Zerar o ponteiro
	handleDoSubsistema = 0;
}

void _stdcall CuboMPR_GetResliceV2(int fatia, ImageDataToDelphi& output, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	ImageDataToDelphi img = subsys->GetReslice( fatia );
	//for (int i = 0; i < img.bufferSize / sizeof(short); i++) {
	//	cout << img.bufferData[i] << ", ";
	//}
	//copia o que o reslice cubico me deu pra ref passada pelo delphi.
	memcpy(&output, &img, sizeof(ImageDataToDelphi));
}



ImageDataToDelphi _stdcall CuboMPR_GetReslice(double spacing, double espessura, int fatia, long handleDoSubsistema) {
	assert(false && "OBSOLET");
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	//ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	//ImageDataToDelphi img = subsys->GetFatia(spacing, espessura, fatia);
	return ImageDataToDelphi();
}

void _stdcall CuboMPR_SetWindowLevel(double ww, double wl, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->SetWL(ww, wl);
}

void _stdcall CuboMPR_AtivarSubsistema(char* idExame, char* idSerie, long& returnHandle) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	auto newSys = make_shared<ResliceCubico>(idExame, idSerie);
	SubsistemasCuboReslice.push_back(newSys);
	returnHandle = (long)newSys.get();

}

void _stdcall CuboMPR_CriarJanela(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->CreateRenderer(handleDaJanela);
}

void _stdcall CuboMPR_SetInitialSlice(int numSlice, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ <<" slice n "<<numSlice<< std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	std::array<double, 3> center = subsys->GetCenterBySlice(numSlice);
	subsys->SetPosicaoInicial(center);
	subsys->SetPosicao(center);
}

void _stdcall CuboMPR_ResizeTela(int width, int height, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->Resize(width, height);
}

void _stdcall CuboMPR_ForceRender(long handleDoSubsistema) {
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->Render();
}

void _stdcall CuboMPR_CriarPipeline(const char* idExame, const char* idSerie, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return;
	shared_ptr<datasource::LoadedImage> image = GetImagemPedida(idExame, idSerie);
	if (image) {
		std::array<double, 3> pos = { {0,0,0} };
		vtkSmartPointer<vtkImageImport> imported = CreateVTKImage(image->GetImage());
		subsys->SetImage(imported, image->GetMetadata(), image->valueOffset);
		subsys->SetPosicao();

	}
	//subsistema->CreatePipeline(image->GetImage(), MapperFlag == 1 ? true : false, image->GetMetadata(), image->valueOffset, 1);
}
//==============================================================================================
void _stdcall CuboMPR_AvancarReslice(double espessura, double avanco, ImageDataToDelphi& data, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->TesteOffscreenReslice();
}

void _stdcall CuboMPR_TesteAvanco(long handle) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handle;
	if (!subsys) return;
	subsys->TesteOffscreenReslice();
}

void _stdcall CuboMPR_Ligar(HWND handleDaJanela, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->Ligar(handleDaJanela);
}
//==============================================================================================
void _stdcall CuboMPR_Desligar(ImageDataToDelphi& finalData, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->Desligar();
}
//==============================================================================================
void _stdcall CuboMPR_SetPosicaoInicial(double x, double y, double z, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->SetPosicaoInicial({ {x,y,z} });
}
//==============================================================================================
void _stdcall CuboMPR_SetPosicao(double x, double y, double z, long handleDoSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleDoSubsistema;
	if (!subsys) return;
	subsys->SetPosicao({ {x,y,z} });
}
//==============================================================================================
void _stdcall CuboMPR_SetCallbackDeReslice(FNCallbackDoDicomReslice cbk, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return;
	subsys->SetCallbackDeReslice(cbk);
}
//==============================================================================================
void _stdcall CuboMPR_DeleteAllocatedData(ImageDataToDelphi& d) {
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	delete[] d.bufferData;
}
//==============================================================================================
void _stdcall CuboMPR_SetThickness(double t, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ <<" thickness = "<<t<<"mm"<< std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return;
	subsys->SetSlabThicknessInMM(t);
}
//==============================================================================================
void _stdcall CuboMPR_SetFuncao(int idFuncao, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return;
	switch (idFuncao) {
	case 0:
		subsys->SetFuncao(ResliceCubico::Funcao::MIP);
		break;
	case 1:
		subsys->SetFuncao(ResliceCubico::Funcao::MinP);
		break;
	case 2:
		subsys->SetFuncao(ResliceCubico::Funcao::Composite);
		break;
	}
 }

void _stdcall CuboMPR_Reset(long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return;
	subsys->Reset();
 }
void _stdcall CuboMPR_SetOperacaoDoMouse(int qualBotao, int qualOperacao, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return;
	subsys->SetOperacoesDeMouse(qualBotao, qualOperacao);
 }
int _stdcall CuboMPR_MouseMove(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->MouseMove(wnd, nFlags, X, Y);
 }
int _stdcall CuboMPR_LMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->LMouseDown(wnd, nFlags, X, Y);
 }
int _stdcall CuboMPR_LMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->LMouseUp(wnd, nFlags, X, Y);
 }
int _stdcall CuboMPR_MMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->MMouseDown(wnd, nFlags, X, Y);
 }
int _stdcall CuboMPR_MMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->MMouseUp(wnd, nFlags, X, Y);
 }
int _stdcall CuboMPR_RMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->RMouseDown(wnd, nFlags, X, Y);
 }
int _stdcall CuboMPR_RMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleSubsistema) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handleSubsistema;
	if (!subsys) return 0;
	return subsys->RMouseUp(wnd, nFlags, X, Y);
 }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall VR_AtivarSubsistema(char* idExame, char* idSerie, long& returnHandle)
{
	try{
		std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
		//Cria o subsistema
		shared_ptr<SubsistemaVR> subsys = make_shared<SubsistemaVR>(idExame, idSerie);
		SubsistemasVR.push_back(subsys);
		long rh = (long)subsys.get();
		returnHandle = rh;

	}
	catch (std::exception ex){
		cout << ex.what() << endl;
	}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall CriarJanelaDoCubo2(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	PaintWindowAsBlack(handleDaJanela);

	SubsistemaVR* subsys = (SubsistemaVR*)handleDoSubsistema;
	subsys->CreateCubeScreen(handleDaJanela, dcDaJanela, glrcDaJanela, textureProvider);
	subsys->ActivateCubeScreen();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall ResizeTelaCubo(int width, int height, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->ResizeCubeScreen(width, height);
	//Old - deve ser removido
	//telaCubo->Resize(width, height);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall ResizeTelaVolume(int width, int height, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->ResizeVolumeScreen(width, height);
	//Old - deve ser removido
	//telaVolume->Resize(width, height);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall CriarJanelaDoVolume2(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	PaintWindowAsBlack(handleDaJanela);

	SubsistemaVR* subsys = (SubsistemaVR*)handleDoSubsistema;
	subsys->CreateVolumeScreen(handleDaJanela, dcDaJanela, glrcDaJanela);
	subsys->ActivateVolumeScreen();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//For�a a renderiza��o da cena.
void _stdcall ForceRender(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->RenderAll();
	//telaVolume->ForceRender();
	//telaCubo->ForceRender();
	//thumbScreen->ForceRender();

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Guarda uma fatia na lista de fatias
void _stdcall PushFatia(const char* path)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ <<" recebeu a fatia "<<path<< std::endl;

	std::string str(path);
	imageLoader->PushFile(str);
}

float _stdcall GetGantryTilt(const char* idExame, const char* idSerie)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	shared_ptr<datasource::LoadedImage> chosenImage = GetImagemPedida(idExame, idSerie);
	assert("Pr�-Condi��o: imagem carregada?"&&chosenImage);
	return carga::GetGantryTilt(chosenImage->GetMetadata());
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Inicializa o volume a partir das fatias guardadas via PushFatia().
int _stdcall InitVolume(const char* idExame, const char* idSerie)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	assert("O imageLoader foi inicializado? " && imageLoader);
	if (HasLoadedImage(idExame, idSerie))
	{
		cout << "N�o deveria ter chamado InitVolume pq a imagem j� est� carregada. A carga n�o ser� feita." << endl;
		return 0;
	}
	else
	{
		images.push_back(imageLoader->Load(idExame, idSerie));
		imageLoader->ClearList();
		return imageLoader->GetCodigoDeErro();
	}
}
////////////////////////////////////////////////////////////////////////////////////////////
//Define a func�o que vai tratar o callback da carga
void _stdcall SetCallbackDeProgressoDoLoadDaCarga(void* ptr_funcao)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	if (!imageLoader)
		imageLoader = make_unique<datasource::ImageSource>((callbackDeProgressDaCarga)ptr_funcao);
	else
		imageLoader->SetCallbackDeProgesso((callbackDeProgressDaCarga)ptr_funcao);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall CorrectGantryTilt(const char* idExame, const char* idSerie)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	//A corre��o do gantry passa a imagem carregada pra fun��o que faz esse servi�o.
	//Depois disso o caminho diverge dependendo de ter pipelines criadas ou n�o. Se nenhuma
	//Pipeline tiver sido criada a hist�ria acaba aqui. Se tiver pipelines as pipelines tem que
	//ficar cientes da nova imagem.
	//As pipelines e a imagem carregada s�o o mesmo local na mem�ria - a princ�pio basta eu mudar esse
	//local e informar os filtros que os dados foram modificados via os Modified() deles.
	shared_ptr<datasource::LoadedImage> image = GetImagemPedida(idExame, idSerie);
	carga::CorrectGantryTilt(image->GetImage(), image->GetMetadata());
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//P�e a s�rie j� previamente carregada na tela
void _stdcall LoadUsandoSerieJaCarregada(const char* idExame, const char* idSerie, long handleSubsistema)
{
	cout << __FUNCTION__ << endl;
	//Com qual imagem estou trabalhando?
	shared_ptr<datasource::LoadedImage> image = GetImagemPedida(idExame, idSerie);
	//Com qual subsistema estou trabalhando?
	SubsistemaVR* subsistema = (SubsistemaVR*)handleSubsistema;
	if (MapperFlag == 1)
		cout << "Usando GPU" << endl;
	else
		cout << "Usando CPU" << endl;
	subsistema->CreatePipeline(image->GetImage(), MapperFlag == 1 ? true : false, image->GetMetadata(), image->valueOffset, 1);

}

//////////////////////////////////////////////////////////////////////////////////////////
int _stdcall GetCodigoTipoDeRemocao(char* nome)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	return CodigosDeOperacaoProvider::GetCodigoTipoDeRemocao(nome);
}
/////////////////////////////////////////////////////////////////////////////////////////////
void _stdcall SetTipoDeRemocao(int codShape, bool keepInside, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetTipoDeRemocao(codShape, keepInside);
	//telaVolume->SetTipoDeRemocao(codShape, keepInside);
}
/////////////////////////////////////////////////////////////////////////////////////////////
int _stdcall GetCodigoBotao(char* botao)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	return CodigosDeOperacaoProvider::GetCodigoBotao(botao);
}
int _stdcall GetCodigoOperacao(char* nome)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	return CodigosDeOperacaoProvider::GetCodigoOperacao(nome);
}
//Seta a operacao do mouse dado.
void _stdcall SetOperacao(int codBotao, int codOperacao, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	if (codBotao==GetCodigoBotao("direito"))
	{
		sub->SetMouseDireito(codOperacao);
	}
	if (codBotao == GetCodigoBotao("meio"))
	{
		sub->SetMouseMeio(codOperacao);

	}
	if (codBotao == GetCodigoBotao("esquerdo"))
	{
		sub->SetMouseEsquerdo(codOperacao);
	}
}

void _stdcall AtivarCuboDeCorte(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->AtivarCuboDeCorte();
}
void _stdcall DesativarCuboDeCorte(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->DesativarCuboDeCorte();
}
//Invocado quando o mouse se desloca sobre a tela do volume.
int _stdcall OnVolumeMouseMove(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnMouseMove(wnd, nFlags, X, Y);
	return 1;
}
//Invocado quando o usuario pressiona o btn esquerdo sobre a tela do volume.
int _stdcall OnVolumeLMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnVolumeLMouseDown(wnd, nFlags, X, Y);
	return 1;
}
//Invocado quando o usuario solta o bot�o esquerdo sobre a tela do volu
int _stdcall OnVolumeLMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema){
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnVolumeLMouseUp(wnd, nFlags, X, Y);
	return 1;
}
//Invocado quando o usuario pressiona o bot�o do meio sobre a tela do volume;
int _stdcall OnVolumeMMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema){
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnVolumeMMouseDown(wnd, nFlags, X, Y);
	return 1;
}
//Invocado quando o usuario solta o bot�o do meio sobre a tela do volume.
int _stdcall OnVolumeMMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema){
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnVolumeMMouseUp(wnd, nFlags, X, Y);
	return 1;
}
//Invocado quando o usuario pressiona o bot�o direito sobre a tela do volume.
int _stdcall OnVolumeRMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema){
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnVolumeRMouseDown(wnd, nFlags, X, Y);
	return 1;
}
//Invocado quando o usuario solta o bot�o direito sobre a tela do volume.
int _stdcall OnVolumeRMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema){
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->OnVolumeRMouseUp(wnd, nFlags, X, Y);
	return 1;
}

void _stdcall SwitchGradientDoThumb(bool isToUse)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
}
void _stdcall SetShadingDoThumb(bool shade)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	assert(false && "Obsoleto?");
	//volumeObject->GetVisualizacao()->SetShadingDoThumb(shade);
}
void _stdcall SetTabelaDeCorDoThumb(vr::EstruturaDeCor* e, int qualFormato, long handleSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	//aplica o offset do bug da lut:
	for (int i = 0; i < e->Tamanho; i++)
	{
		e->x[i] = e->x[i] + volumeObject->GetPipeline()->GetValueOffset();
	}
	SubsistemaVR* sub = (SubsistemaVR*)handleSubsistema;
	sub->SetEstruturaDeCorDoThumb(e);
	sub->SetClampingDoThumb(e->clamping);

}
void _stdcall SetDiffuseDoThumb(float v)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	volumeObject->GetVisualizacao()->SetDiffuseDoThumb(v);

}
void _stdcall SetAmbientDoThumb(float v)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	volumeObject->GetVisualizacao()->SetAmbientDoThumb(v);
}
void _stdcall SetSpecularDoThumb(float v)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	volumeObject->GetVisualizacao()->SetSpecularDoThumb(v);
}

//Ativa a janela do volume. Quando ela � criada ela ainda n�o est� ativa, seu sistema de eventos ainda n�o est� funcionando, por exemplo.
void _stdcall AtivarJanelaDoVolume(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->ActivateVolumeScreen();
}
//Encerra a vida do contexto VR
void _stdcall FinalizarContexto(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	int indexDoQueVaiSair = -1;
	for (unsigned int i = 0; i < SubsistemasVR.size(); i++)
	{
		long addr = (long)SubsistemasVR[i].get();
		if (addr == handleDoSubsistema)
		{
			indexDoQueVaiSair = i;
			break;
		}
	}
	SubsistemasVR.erase(SubsistemasVR.begin() + indexDoQueVaiSair);
}

void _stdcall UndoRemocao(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->UndoRemocao();
}
void _stdcall RedoRemocao(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->RedoRemocao();
}

void _stdcall ResetSerie(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->UndoAll();
}



void _stdcall SetTextureRight(unsigned char* buff, int bufferSize)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	textureProvider->SetTexturaRightDoCubo(buff, bufferSize);
}

void _stdcall SetTextureAnterior(unsigned char* buff, int bufferSize)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	textureProvider->SetTexturaAnteriorDoCubo(buff, bufferSize);

}

void _stdcall SetTexturePosterior(unsigned char* buff, int bufferSize)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	textureProvider->SetTexturaPosteriorDoCubo(buff, bufferSize);
}

void _stdcall SetTextureInferior(unsigned char* buff, int bufferSize)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	textureProvider->SetTexturaInferiorDoCubo(buff, bufferSize);
}

void _stdcall SetTextureSuperior(unsigned char* buff, int bufferSize)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	textureProvider->SetTexturaSuperiorDoCubo(buff, bufferSize);

}

void _stdcall SetTextureLeft(unsigned char* buff, int bufferSize)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	textureProvider->SetTexturaLeftDoCubo(buff, bufferSize);
}
//Ativa o Mip. Quando o mip � ativado um monte de coisa � desativada - n�o h� shading e o modo de blending muda pra mip
void _stdcall MipOn(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetMip(true);
}
//Desativa o mip desfazendo as flags que o Mip seta
void _stdcall MipOff(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetMip(false);
}

void _stdcall SerializeFuncaoDeTransferencia(char* out_text, const char* nome_da_funcao, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	TiXmlDocument doc;
	//o root
	TiXmlElement* root_element = new TiXmlElement("TransferFunctions");
	root_element->SetAttribute("Modality", "CT");
	root_element->SetAttribute("Comment", nome_da_funcao);
	root_element->SetAttribute("BlendMode", sub->GetVisualizacaoDoVolume()->GetBlendMode());
	root_element->SetAttribute("RelativeVisibleValueRange", "0 1");
	//o VolumeProperty
	TiXmlElement* volume_property = new TiXmlElement("VolumeProperty");
	volume_property->SetAttribute("Version", "1.13");
	volume_property->SetAttribute("InterpolationType", "1");
	volume_property->SetAttribute("IndependentComponents", "1");
	root_element->LinkEndChild(volume_property);
	//o component
	TiXmlElement* component01 = new TiXmlElement("Component");
	component01->SetAttribute("Index", "0");

	component01->SetAttribute("Shade", sub->GetVisualizacaoDoVolume()->GetShading());
	component01->SetDoubleAttribute("Ambient", sub->GetVisualizacaoDoVolume()->GetAmbient());
	component01->SetDoubleAttribute("Diffuse", sub->GetVisualizacaoDoVolume()->GetDiffuse());
	component01->SetDoubleAttribute("Specular", sub->GetVisualizacaoDoVolume()->GetSpecular());
	component01->SetDoubleAttribute("SpecularPower", sub->GetVisualizacaoDoVolume()->GetMapper()->GetSpecularPower());
	component01->SetAttribute("ColorChannels", "3");
	//ve se � pra usar gradient
	bool isUsandoGradient = sub->GetVisualizacaoDoVolume()->GetMapper()->GetUsandoGradient();
	component01->SetAttribute("DisableGradientOpacity", isUsandoGradient ? "0":"1");
	component01->SetAttribute("ComponentWeight", "1");
	component01->SetAttribute("ScalarOpacityUnitDistance", sub->GetVisualizacaoDoVolume()->GetScalarOpacityUnitRange());
	volume_property->LinkEndChild(component01);
	//a RGBTransferFunction
	TiXmlElement* rgb_transfer_function_01 = new TiXmlElement("RGBTransferFunction");
	component01->LinkEndChild(rgb_transfer_function_01);
	//a color transfer function
	TiXmlElement* color_transfer_function = new TiXmlElement("ColorTransferFunction");
	rgb_transfer_function_01->LinkEndChild(color_transfer_function);
	color_transfer_function->SetAttribute("Version", "1.8");
	//pega o tamanho
	vtkSmartPointer<vtkColorTransferFunction> ctf = sub->GetVisualizacaoDoVolume()->GetFuncaoDeTransferencia();
	color_transfer_function->SetAttribute("Size", ctf->GetSize());
	color_transfer_function->SetAttribute("Clamping", ctf->GetClamping());
	color_transfer_function->SetAttribute("ColorSpace", "1");
	int __sz = ctf->GetSize();
	//a lista de pontos rgb
	for (int i = 0; i < __sz; i++)
	{
		TiXmlElement* ponto = new TiXmlElement("Point");
		color_transfer_function->LinkEndChild(ponto);
		double colorData[6];
		sub->GetVisualizacaoDoVolume()->GetPontoDeCor(i, colorData);
		ponto->SetDoubleAttribute("X", colorData[0]);
		std::stringstream s_v;
		s_v << colorData[1] << " " << colorData[2] << " " << colorData[3];
		std::string _val_data = s_v.str();
		ponto->SetAttribute("Value", _val_data.c_str());
		ponto->SetDoubleAttribute("MidPoint", colorData[4]);
		ponto->SetDoubleAttribute("Sharpness", colorData[5]);
	}
	//a lista das opacities escalares
	TiXmlElement* scalar_opacity_function = new TiXmlElement("ScalarOpacity");
	component01->LinkEndChild(scalar_opacity_function);
	TiXmlElement* piecewise_function = new TiXmlElement("PiecewiseFunction");
	scalar_opacity_function->LinkEndChild(piecewise_function);
	vtkSmartPointer<vtkPiecewiseFunction> opacity_fn = sub->GetVisualizacaoDoVolume()->GetOpacidadeEscalar();
	piecewise_function->SetAttribute("Version", "1.7");
	piecewise_function->SetAttribute("Size", opacity_fn->GetSize());
	piecewise_function->SetAttribute("Clamping", opacity_fn->GetClamping());
	for (int i = 0; i < opacity_fn->GetSize(); i++)
	{
		TiXmlElement* ponto = new TiXmlElement("Point");
		piecewise_function->LinkEndChild(ponto);
		double alphaData[4];
		sub->GetVisualizacaoDoVolume()->GetPontoDeAlpha(i, alphaData);
		ponto->SetDoubleAttribute("X", alphaData[0]);
		ponto->SetDoubleAttribute("Value", alphaData[1]);
		ponto->SetDoubleAttribute("MidPoint", alphaData[2]);
		ponto->SetDoubleAttribute("Sharpness", alphaData[3]);
	}
	//a lista de opacities gradientes
	TiXmlElement* gradient_opacity_function = new TiXmlElement("GradientOpacity");
	component01->LinkEndChild(gradient_opacity_function);
	TiXmlElement* gradient_function = new TiXmlElement("PiecewiseFunction");
	gradient_opacity_function->LinkEndChild(gradient_function);
	vtkSmartPointer<vtkPiecewiseFunction> gradFn = sub->GetVisualizacaoDoVolume()->GetOpacidadeGradiente();
	gradient_function->SetAttribute("Version", "1.7");
	gradient_function->SetAttribute("Size", gradFn->GetSize());
	gradient_function->SetAttribute("Clamping", gradFn->GetClamping());
	for (int i = 0; i < gradFn->GetSize(); i++)
	{
		TiXmlElement* ponto = new TiXmlElement("Point");
		gradient_function->LinkEndChild(ponto);
		double alphaData[4];
		sub->GetVisualizacaoDoVolume()->GetPontoDeOpacidadeGradiente(i, alphaData);
		ponto->SetDoubleAttribute("X", alphaData[0]);
		ponto->SetDoubleAttribute("Value", alphaData[1]);
		ponto->SetDoubleAttribute("MidPoint", alphaData[2]);
		ponto->SetDoubleAttribute("Sharpness", alphaData[3]);
	}
	//<PiecewiseFunction Version = "1.7"
	//	Size = "2"
	//	Clamping = "1">
	//poe no doc
	doc.LinkEndChild(root_element);
	//envia o param pra fora.
	TiXmlPrinter printer;
	printer.SetIndent("    ");
	doc.Accept(&printer);
	std::string xmltext = printer.CStr();
	doc.SaveFile("__tempxml.xml");
	const char* str = xmltext.c_str();
	const int sz = xmltext.size();
	memcpy(out_text, str, sz);
}





void _stdcall SetVisaoToDireita(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	double azimuth = -90;
	double elevation = 0;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetCamera(azimuth, elevation);
}
void _stdcall SetVisaoToEsquerda(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	double azimuth = 90;
	double elevation = 0;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetCamera(azimuth, elevation);
}
void _stdcall SetVisaoToAnterior(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	double azimuth = 0;
	double elevation = 0;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetCamera(azimuth, elevation);
}
void _stdcall SetVisaoToPosterior(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	double azimuth = 180;
	double elevation = 0;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetCamera(azimuth, elevation);
}
void _stdcall SetVisaoToSuperior(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	double azimuth = 0;
	double elevation = 90;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetCamera(azimuth, elevation);
}
void _stdcall SetVisaoToInferior(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	double azimuth = 0;
	double elevation = -90;
	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetCamera(azimuth, elevation);
}


void _stdcall SetShading(bool shade)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	volumeObject->GetVisualizacao()->GetMapper()->SetShading(shade);
}


//Aplica a lut - como os volume mappers n�o aceitam luts, ser� feita uma fun��o de transfer�ncia
//com propriedades apropriadas para simular uma lut.
void _stdcall SetLUT(int* x, float* r, float* g, float* b, float* a, int len)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

}
//Avalia se pode usar a GPU. Esse m�todo � s� uma sugest�o. Ningu�m vai ter impedir de usar
//um gpu volumetric raytracer l� na frente.
bool _stdcall CanUseGPU(int tamanhoDaSerieEmBytes)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	return true;
}

//Seta o espa�o entre cada raio. 1 � um raio por pixel. Quando menor mais raios, quando maior menos raios
//Quantidade de raios � fator determinante para a qualidade (quanto mais melhor) e para velocidade (quanto menos
//melhor). O valor m�nimo � usado quando a imagem est� est�tica, o valor m�ximo quando est� havendo intera��o lenta
//com a imagem. Se n�o quiser que ocorra mudan�a de qualidade quando estiver interagindo com a imagem ponha minimo=maximo.
void _stdcall SetEspacoDoRaytrace(float minimo, float maximo, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleDoSubsistema;
	sub->SetSamplingDistance(minimo, maximo);

}



void _stdcall SetDiffuse(float v)
{
	std::cout << GetDateAsString() << " - " << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	volumeObject->GetVisualizacao()->GetMapper()->SetDiffuse(v);
}

void _stdcall SetAmbient(float v)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	volumeObject->GetVisualizacao()->GetMapper()->SetAmbient(v);
}

void _stdcall SetSpecular(float v)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	volumeObject->GetVisualizacao()->GetMapper()->SetSpecular(v);
}


//Muda a cor e opacidade escalar segundo a tabela informada. O formato pode ser rgb ou hsv.
void _stdcall SetTabelaDeCor(vr::EstruturaDeCor* e, int qualFormato, long handleSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR* sub = (SubsistemaVR*)handleSubsistema;
	if (e->blending != 0 && e->blending != 1)
	{
		e->blending = 0;
	}
	//aplica o offset do bug da lut:
	for (int i = 0; i < e->Tamanho; i++)
	{
		e->x[i] = e->x[i] + sub->GetValueOffset();
	}
	sub->SetFuncaoDeCor(e);
}

void _stdcall Test(double* m)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

}


void _stdcall SEG_SetupSistemaDeSegmentacao(int suavizacao, int metodo, void* SEGPropertyStruct, long handleVR)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleVR;
	vr->SetSegmentacao(suavizacao, metodo, SEGPropertyStruct);
}

void _stdcall SEG_Segmentar(long handleVR)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleVR;
	vr->Segmentar();
}

void _stdcall SEG_SetFastMarchingProperties(int threshold, long handleVR)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleVR;
	vr->SetFastMarchingProperties(threshold);
}

void _stdcall SEG_SwitchWidget(long handleVR)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleVR;
	vr->SwitchWidgetDeSegmentacao();
}

void _stdcall SEG_ManterDentroOuManterFora(bool manterDentro, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleDoSubsistema;
	vr->SetSegmentacaoManterDentro(manterDentro);
}


void _stdcall VR_SetWLFontSize(int sz, long handleVR)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleVR;
	vr->SetFontSize(sz);
}

void _stdcall VR_SetSegmentationRPCProperties(char* executavel, char* porta, char* filebuffer, long handleVR)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	SubsistemaVR *vr = (SubsistemaVR*)handleVR;
	//char* __testeExec = "C:\\medivolume_bin\\rpc_server\\Debug\\rpc_server.exe";
	//char* __testePorta = "4747";
	//char* __fileBuffer = "C:\\tutorial_vr\\rpc_buffer.bin";
	vr->SetRPCProperties(executavel, porta, filebuffer);
}

void _stdcall MPR_AtivarSubsistema(char* idExame, char* idSerie, long& returnHandle)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	//Esse subistema existe? Se existe retorna o addr dele como handle ao inv�s de criar outro
	for (size_t i = 0; i < SubsistemasMPR.size(); i++)
	{
		mpr::Subsistema *curr = SubsistemasMPR[i].get();
		if (curr->IsThisExam(idExame, idSerie))
		{
			returnHandle = (long)curr;
			return;
		}
	}
	//Se chegou at� aqui � pq o subsistema para essa exame-serie n�o existe e tem que ser criado.
	SubsistemasMPR.push_back(make_shared<mpr::Subsistema>(idExame, idSerie));
	returnHandle = GetHandleDoSubsistema<mpr::Subsistema>(SubsistemasMPR, idExame, idSerie);
}



void _stdcall MPR_CriarTela(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, int t, long handleSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleSubsistema;
	if (t != -1)
		subsistema->CreateScreen(dcDaJanela, handleDaJanela, glrcDaJanela, t);
	else
		subsistema->CreateScreen(dcDaJanela, handleDaJanela, glrcDaJanela);
}

void _stdcall MPR_Resize(int w, int h, int t, long handleSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleSubsistema;
	subsistema->Resize(w, h, t);
}

void _stdcall MPR_Render(int t, long handleSubsistema)
{
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleSubsistema;
	if (!subsistema)
		return;
	else
		subsistema->ForceRender(t);
}

void _stdcall MPR_SetTelaGrande(int qual, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetOrigemTelaGrande(qual);
}

void _stdcall MPR_InitPipeline(long handleSubsistema)  //Aqui a janela t� estourada
{
	try{
		std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
		mpr::Subsistema *subsistema = (mpr::Subsistema*)handleSubsistema;
		shared_ptr<datasource::LoadedImage> chosenImage = nullptr;
		for (size_t i = 0; i < images.size(); i++){
			if (images[i]->IsThisExam((char*)subsistema->GetIdExame().c_str(), (char*)subsistema->GetIdSerie().c_str())){
				chosenImage = images[i];
				break;
			}
		}
		std::cout << GetDateAsString() << " - " << "Achou a imagem" << std::endl;
		subsistema->CreatePipeline(chosenImage->GetImage(), chosenImage->GetMetadata(), chosenImage->valueOffset);
		std::cout << GetDateAsString() << " - " << "Criou a pipeline" << std::endl;
		subsistema->InitialSetup();
		std::cout << GetDateAsString() << " - " << "Criou a situa��o inicial" << std::endl;
		subsistema->Rotate(0, 0);
		std::cout << GetDateAsString() << " - " << "Fim do " << __FUNCTION__ << std::endl;
		subsistema->SetCurrentWL({ { 350, subsistema->GetScalarOffset() + 25 } });
	}
	catch (boost::exception &ex)
	{
		std::cout << boost::diagnostic_information(ex) << std::endl;
	}
}

void _stdcall MPR_DesativarSubsistema(long handleSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleSubsistema;
	int indexDoQueVaiSair = -1;
	for (unsigned int i = 0; i < SubsistemasMPR.size(); i++)
	{
		long addr = (long)SubsistemasMPR[i].get();
		if (addr == handleSubsistema)
		{
			indexDoQueVaiSair = i;
			break;
		}
	}
	SubsistemasMPR.erase(SubsistemasMPR.begin() + indexDoQueVaiSair);
}

int _stdcall MPR_MouseMove(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
//	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnMouseMove(wnd, nFlags, X, Y, qualPanel);
	return 1;
}
int _stdcall MPR_LMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnLMouseDown(wnd, nFlags, X, Y, qualPanel);
	return 1;
}
int _stdcall MPR_LMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnLMouseUp(wnd, nFlags, X, Y, qualPanel);
	return 1;
}
int _stdcall MPR_MMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnMMouseDown(wnd, nFlags, X, Y, qualPanel);
	return 1;
}
int _stdcall MPR_MMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnMMouseUp(wnd, nFlags, X, Y, qualPanel);
	return 1;
}
int _stdcall MPR_RMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnRMouseDown(wnd, nFlags, X, Y, qualPanel);
	return 1;
}
int _stdcall MPR_RMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->OnRMouseUp(wnd, nFlags, X, Y, qualPanel);
	return 1;
}

void _stdcall MPR_SetTipo(int tipo, long handleDoSubsistema)//0 = mip; 1 = minp; 2 = sum; 3 = mean
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetFuncao(tipo);
}

void _stdcall MPR_SetEspessura(int numFatias, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetThickness(numFatias);
}

void _stdcall MPR_SetTamanhoDoLado(int tamanho, long handleDoSubsistema)
{
	assert(false && "Isso ainda eh usado?");
	//std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	//mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	//subsistema->SetLado(tamanho);
}
void _stdcall MPR_GetWL(int&w, int&l, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	array<int,2 > wl;
	subsistema->GetCurrentWL(wl);
	w = wl[0];
	l = wl[1];
}
void _stdcall MPR_GetDicomFromScreen(int idTela, MPRV2_DicomData& dicomData, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	MPRV2_DicomData dd = subsistema->GetMPRData(idTela);
	memcpy(&dicomData, &dd, sizeof(MPRV2_DicomData));
}

void _stdcall MPR_TESTE(long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->AddDistanceMeasurement();
	////Teste da cria��o do slice
	//Funcionou pela 1a vez, usando
	//array<int, 2> p0 = { {0,0} };
	//array<int, 2> p1 = { {4, 3} };
	//Em mm na imagem.
	//array<int, 2> p0 = { {109,-118} };
	//array<int, 2> p1 = { {-165, -38} };
	//subsistema->CriarImagem(p0, p1, 1, 0);
}

void _stdcall MPR_SetOperacao(int idBtn, int idOperacao, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetMouseInteraction(idBtn, idOperacao);
}

short _stdcall MPR_GetValorAdicionadoAoEscalar(long handle)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handle;
	return subsistema->GetScalarOffset();
}

void _stdcall MPR_SetWL(int w, int l, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << "wl = "<<w<<", "<<l <<std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	std::array<int, 2> wl = { { w, l } };
	subsistema->SetCurrentWL(wl);
}

void _stdcall MPR_GetPhysicalOrigin(float &x, float &y, float &z, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->GetPhysicalOrigin(x, y, z);
}

void _stdcall MPR_GetSpacing(float &x, float &y, float &z, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->GetSpacing(x, y, z);
}

void _stdcall MPR_MoveCursor(float x, float y, float z, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->Translate(x, y, z);
}

void _stdcall MPR_RotateAround(int aoRedorDeQualEixo, float angulo, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->Rotate(aoRedorDeQualEixo, angulo);
}

void _stdcall MPR_GetDimensions(float &x, float &y, float &z, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->GetDimensions(x, y, z);
}

void _stdcall MPR_SetCallbackDeOnChangeDicom(TOnChangeDicom fn, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetOnChangeDicom(fn);
	cout << "aaa" << endl;
}

void _stdcall MPR_DeleteEstruturaDoCallbackDeOnChangeDicom(void* struc)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;

	free(struc);
	struc = nullptr;
}

void _stdcall MPR_SetDragRect(int left, int top, int width, int height, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetDragRect(left, top, width, height);
}

void _stdcall MPR_SetCorDasLetras(float r, float g, float b, long handleDoSubsistema)
{
	std::cout << GetDateAsString()<< " - "<< __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetCorDasLetras(r, g, b);
}

void _stdcall MPR_SetTamanhoDasLetras(float sz, long handleDoSubsistema)
{
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	mpr::Subsistema *subsistema = (mpr::Subsistema*)handleDoSubsistema;
	subsistema->SetFontSize(sz);

}
datasource::ImageSliceDatabase sliceDatabase;
//Essa fun��o mudou de receber uma unica imagem para receber v�rias imagens.
void _stdcall CriarImagemFromStruct(char *idExame, char *idSerie, MPRV2_DicomData data, float physicalOriginX, float physicalOriginY, float physicalOriginZ)
{
	assert(data.bufferData&&"Se est� null � pq a passagem por valor deu errado.");
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	string strIdExame = idExame;
	string strIdSerie = idSerie;
	sliceDatabase.AddSlice(strIdExame, strIdSerie, data);
}

int _stdcall GetNumberOfSlices(char* idExame, char* idSerie){
	//shared_ptr<ImageSlice> GetImageSlice(string idExame, string idSerie){
	shared_ptr<datasource::ImageSlice> aImage = sliceDatabase.GetImageSlice(idExame, idSerie);
	if (aImage)
		return aImage->dicomData.size();
	else
		return -1;
}

MPRV2_DicomData _stdcall GetSlice(char* idExame, char* idSerie, int slice){
	shared_ptr<datasource::ImageSlice> aImage = sliceDatabase.GetImageSlice(idExame, idSerie);
	if (!aImage)
	{
		string erro;
		cout << __FUNCTION__ << " ESSA IMAGEM N�O EXISTE" << endl;
	}
	if (aImage->dicomData.size() <= slice){
		string erro;
		cout << __FUNCTION__ << " IMAGEM FORA DO INTERVALO" << endl;
	}
	return aImage->dicomData[slice];
}


void _stdcall AssembleImage(char *idExame, char *idSerie){
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	string sIdExame = idExame;
	string sIdSerie = idSerie;
	shared_ptr<datasource::ImageSlice> imageData = sliceDatabase.GetImageSlice(sIdExame, sIdSerie);
	itk::Image<short, 3>::Pointer assembled = imageData->GetAssembledImage();


	itk::OrientImageFilter<itk::Image<short, 3>, itk::Image<short, 3>>::Pointer orienter = itk::OrientImageFilter<itk::Image<short, 3>, itk::Image<short, 3>>::New();
	orienter->UseImageDirectionOn();
	////Consultar https://itk.org/Doxygen/html/namespaceitk_1_1SpatialOrientation.html#a8240a59ae2e7cae9e3bad5a52ea3496eaa5d3197482c4335a63a1ad99d6a9edee
	////para a lista de orienta��es
	orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RIP);
	orienter->SetInput(assembled);
	orienter->Update();
	assembled = orienter->GetOutput();

	map<string, string> emptyMetadata;
	shared_ptr<datasource::LoadedImage> result = make_shared<datasource::LoadedImage>(assembled, emptyMetadata, sIdExame, sIdSerie);
	images.push_back(result);
}

template <class T> long GetHandleDoSubsistema(vector<shared_ptr<T>>vec, string idExame, string idSerie) {
	throw std::exception("OBSOLETO E PERIGOSO");
	for (unsigned int i = 0; i < vec.size(); i++) {
		T* curr = vec[i].get();
		if (curr->IsThisExam(const_cast<char*>(idExame.c_str()), const_cast<char*>(idSerie.c_str())))
		{
			return (long)curr;
		}
	}
	return 0;
}

void _stdcall TesteDicomGenerator() {
	shared_ptr<DicomGenerator> gen = make_shared<DicomGenerator>("exame 1", "serie a");
	for (auto i : images) {
		if (i->IsThisExam(const_cast<char*>(gen->GetExame()), const_cast<char*>(gen->GetSerie()))) {
			gen->SetImage(i->GetImage());
		}
	}
	gen->SetProperties(90, DicomGenerator::AXIAL);
	gen->Execute();

	gen->SetProperties(90, DicomGenerator::CORONAL);
	gen->Execute();

	gen->SetProperties(90, DicomGenerator::SAGITAL);
	gen->Execute();
}

void _stdcall GetImageSpacing(const char* idExame, const char* idSerie, float& x, float& y, float& z) {
	for (auto i : images) {
		if (i->IsThisExam(const_cast<char*>(idExame), const_cast<char*>(idSerie))) {
			auto imp = CreateVTKImage(i->GetImage());
			x = imp->GetOutput()->GetSpacing()[0];
			y = imp->GetOutput()->GetSpacing()[1];
			z = imp->GetOutput()->GetSpacing()[2];
			return;
		}
	}
	x = -1;
	y = -1;
	z = -1;
}

void _stdcall CuboMPR_SetCoresDasLetras(BYTE* orientacao, BYTE* informacao, long handle) {
	std::cout << GetDateAsString() << " - " << __FUNCTION__ << std::endl;
	ResliceCubico* subsys = (ResliceCubico*)handle;
	if (!subsys) return;
	subsys->SetCorDasLetrasDeInformacao(informacao[0], informacao[1], informacao[2]);
	subsys->SetCorDasLetrasDeOrientacao(orientacao[0], orientacao[1], orientacao[2]);
}