#define ITKV3_COMPATIBILITY//Necess�rio pq as classes foram escritas para uma vers�o antiga da biblioteca
#pragma warning(disable:4996)
//#include <vld.h>
#ifndef ___Interface
#define ___Interface
#include <Windows.h>
#include "EstruturaDeCor.h"
#include "EstruturaDeOpacidade.h"

#include "PontoNaTela.h"
#include "Misc.h"
#include "MyExceptions.h"
#include <CallbackDePosicaoMPR.h>
#include <itktypes.h>
#include <MprUtils.h>
#include <stdafx.h>//Do cuboReslice



#define DLL_INTERFACE __declspec(dllexport)
extern "C"
{
	DLL_INTERFACE void _stdcall TesteDicomGenerator();

	DLL_INTERFACE void _stdcall GetImageSpacing(const char* idExame, const char* idSerie, float& x, float& y, float& z);
	///FUNCOES RELATIVAS AO MPR CUBICO
	DLL_INTERFACE void _stdcall CuboMPR_SetCoresDasLetras(BYTE* orientacao, BYTE* informacao,  long handle);
	DLL_INTERFACE void _stdcall CuboMPR_TesteAvanco(long handle);
	DLL_INTERFACE void _stdcall CuboMPR_AvancarReslice(double espessura, double avanco,ImageDataToDelphi& data, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_Desligar(ImageDataToDelphi& finalData, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_Ligar(HWND handleDaJanela, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetInitialSlice(int numSlice, long handleDoSubsistema);

	DLL_INTERFACE void _stdcall CuboMPR_AtivarSubsistema(char* idExame, char* idSerie, long& returnHandle);
	DLL_INTERFACE void _stdcall CuboMPR_CriarJanela(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_ResizeTela(int width, int height, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_ForceRender(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_CriarPipeline(const char* idExame, const char* idSerie, long handleSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetCallbackDeReslice(FNCallbackDoDicomReslice cbk, long handleSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_DeleteAllocatedData(ImageDataToDelphi& d );	
	DLL_INTERFACE void _stdcall CuboMPR_SetThickness(double t, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetFuncao(int idFuncao, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_Reset( long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetOperacaoDoMouse(int qualBotao, int qualOperacao, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_MouseMove(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_LMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_LMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_MMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_MMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_RMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall CuboMPR_RMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_MatarSistema(long& handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetWindowLevel(double ww, double wl, long handleDoSubsistema);
	DLL_INTERFACE ImageDataToDelphi _stdcall CuboMPR_GetReslice(double spacing, double espessura, int fatia, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_GetResliceV2(int fatia, ImageDataToDelphi& output,   long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetPosicaoInicial(double x, double y, double z, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CuboMPR_SetPosicao(double x, double y, double z, long handleDoSubsistema);



	DLL_INTERFACE void _stdcall CriarImagemFromStruct(char *idExame, char *idSerie, MPRV2_DicomData data, float physicalOriginX, float physicalOriginY, float physicalOriginZ);
	DLL_INTERFACE int _stdcall GetNumberOfSlices(char* idExame, char* idSerie);
	DLL_INTERFACE MPRV2_DicomData _stdcall GetSlice(char* idExame, char* idSerie, int slice);
	DLL_INTERFACE void _stdcall AssembleImage(char *idExame, char *idSerie);

	DLL_INTERFACE void _stdcall MPR_SetDragRect(int left, int top, int width, int height, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_AtivarSubsistema(char* idExame, char* idSerie, long& returnHandle);
	DLL_INTERFACE void _stdcall MPR_CriarTela(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, int t, long handleSubsistema);
	DLL_INTERFACE void _stdcall MPR_Resize(int w, int h, int t, long handleSubsistema);
	DLL_INTERFACE void _stdcall MPR_Render(int t, long handleSubsistema);
	DLL_INTERFACE void _stdcall MPR_InitPipeline(long handleSubsistema);
	DLL_INTERFACE void _stdcall MPR_DesativarSubsistema(long handleSubsistema);
	DLL_INTERFACE int _stdcall MPR_MouseMove(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall MPR_LMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall MPR_LMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall MPR_MMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall MPR_MMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall MPR_RMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE int _stdcall MPR_RMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetOperacao(int qualBotao, int qualOperacao, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetTipo(int tipo, long handleDoSubsistema);//0 = mip; 1 = minp; 2 = sum; 3 = mean
	DLL_INTERFACE void _stdcall MPR_SetEspessura(int numFatias, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetTamanhoDoLado(int tamanho, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_TESTE(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_GetDicomFromScreen(int idTela, MPRV2_DicomData& dicomData, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_GetWL(int&w, int&l, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetOperacao(int idBtn, int idOperacao, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetWL(int w, int l, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_GetPhysicalOrigin(float &x, float &y, float &z, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_GetSpacing(float &x, float &y, float &z, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_GetDimensions(float &x, float &y, float &z, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_MoveCursor(float x, float y, float z, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_RotateAround(int aoRedorDeQualEixo, float angulo, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetTelaGrande(int qual, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetCallbackDeOnChangeDicom(TOnChangeDicom fn, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_DeleteEstruturaDoCallbackDeOnChangeDicom(void* struc);
	DLL_INTERFACE void _stdcall MPR_SetCorDasLetras(float r, float g, float b, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MPR_SetTamanhoDasLetras(float sz, long handleDoSubsistema);
	DLL_INTERFACE short _stdcall MPR_GetValorAdicionadoAoEscalar(long handle);


	DLL_INTERFACE short _stdcall GetValorSomadoAosEscalares(char* idExame, char* idSerie);
	DLL_INTERFACE void _stdcall VR_SetSegmentationRPCProperties(char* executavel, char* porta, char* filebuffer, long handleVR);
	DLL_INTERFACE void _stdcall VR_SetWLFontSize(int sz, long handleVR);
	//Suaviza��o: 0 = nenhuma, 1 = Curvature Flow, 2 = GPU Anisotropic
	//Metodo =  0 = Confidence Connected
	DLL_INTERFACE void _stdcall SEG_SetupSistemaDeSegmentacao(int suavizacao, int metodo, void* SEGPropertyStruct, long handleVR);
	DLL_INTERFACE void _stdcall SEG_Segmentar(long handleVR);
	DLL_INTERFACE void _stdcall SEG_SwitchWidget(long handleVR);
	DLL_INTERFACE void _stdcall SEG_SetFastMarchingProperties(int threshold, long handleVR);
	DLL_INTERFACE void _stdcall SEG_ManterDentroOuManterFora(bool manterDentro, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall PortaDeTeste(HWND tela01, HWND tela02, HWND tela03);
	DLL_INTERFACE void _stdcall GetVideoMemory(int& dedicated, int& shared);
	DLL_INTERFACE int _stdcall GetQualDllDeAcordoComOpengl();
	///Cria um novo subsistema VR, pr� condi��o pra volume rendering, retornando o addr do
	//subsistema como handle.
	DLL_INTERFACE void _stdcall VR_AtivarSubsistema(char* idExame, char* idSerie, long& returnHandle);
	DLL_INTERFACE void _stdcall VR_RenderVolume(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall VR_RenderCubo(long handleDoSubsistema);
	DLL_INTERFACE bool _stdcall HasLoadedImage(const char* idExame, const char* idSerie);
	DLL_INTERFACE void _stdcall DeleteLoadedImage(const char* idExame, const char* idSerie);
	DLL_INTERFACE void _stdcall ResizeEditorWindow(int w, int h);
	DLL_INTERFACE void _stdcall RenderEditorWindow();
	DLL_INTERFACE void _stdcall ReleaseEditorWindowResources();
	DLL_INTERFACE void _stdcall CreateEditorWindow(HWND handle);
	DLL_INTERFACE void _stdcall GetGlCaps(char* path);
	DLL_INTERFACE void _stdcall CorrectGantryTilt(const char* idExame, const char* idSerie);
	DLL_INTERFACE float _stdcall GetGantryTilt(const char* idExame, const char* idSerie);
	DLL_INTERFACE void _stdcall UsarGPU();
	DLL_INTERFACE void _stdcall UsarCPU();
	DLL_INTERFACE void _stdcall BackdoorCor();
	DLL_INTERFACE void _stdcall GetWL(int* w, int *l, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MipOn(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall MipOff(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetSegmentationStrategy(int idSegStategy);
	DLL_INTERFACE void _stdcall SegmentByMousePos(int x, int y);
	//Deleta todas as imagens carregadas
	DLL_INTERFACE void _stdcall DeleteAllImages();
	DLL_INTERFACE void _stdcall SetWindow(int w, int l, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall GerarPrintsBMP2(bool isDirecaoHorizontal, bool isSentidoPositivo, int numeroDeQuadros, char* nomeDoArquivo, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall AtivarCuboDeCorte(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall DesativarCuboDeCorte(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall GenerateCanonicalView2(float* x, float* r, float* g, float* b, float* a, float* colorMid, float* colorSharpness, int qtdDePontos,
		float* gx, float* ga, float* gMid, float* gSharpness, int qtdDeGradiente, float ambient, float diffuse, float specular, bool shading, bool clamping, bool gradientOn, long handleSubsistema);
	DLL_INTERFACE void _stdcall SwitchGradientDoThumb(bool isToUse);
	DLL_INTERFACE void _stdcall SetShadingDoThumb(bool shade);
	DLL_INTERFACE void _stdcall SetTabelaDeCorDoThumb(vr::EstruturaDeCor* e, int qualFormato, long handleSubsistema);
	DLL_INTERFACE void _stdcall SetDiffuseDoThumb(float v);
	DLL_INTERFACE void _stdcall SetAmbientDoThumb(float v);
	DLL_INTERFACE void _stdcall SetSpecularDoThumb(float v);
	DLL_INTERFACE void _stdcall CriarJanelaDoThumb(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetGradientOpacityDoThumb(int* x, float* a, float* midpoint, float* sharpness, int size);
	DLL_INTERFACE void _stdcall SwitchOffscreen(bool b);
	DLL_INTERFACE void _stdcall SetThumbDimensions(int x, int y, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetGradientOpacity(int* x, float* a, float* midpoint, float* sharpness, int size);
	DLL_INTERFACE void _stdcall ResizeTelaCubo(int width, int height, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CriarJanelaDoCubo2(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall CriarJanelaDoVolume2(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall RemoverOssos();
	DLL_INTERFACE int _stdcall GetCodigoBotao(char* botao);
	DLL_INTERFACE int _stdcall GetCodigoOperacao(char* nome);
	DLL_INTERFACE int _stdcall GetCodigoTipoDeRemocao(char* nome);
	DLL_INTERFACE void _stdcall SetTipoDeRemocao(int codShape, bool keepInside, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetOperacao(int codBotao, int codOperacao, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetTextureLeft(unsigned char* buff, int bufferSize);
	DLL_INTERFACE void _stdcall SetTextureRight(unsigned char* buff, int bufferSize);
	DLL_INTERFACE void _stdcall SetTextureAnterior(unsigned char* buff, int bufferSize);
	DLL_INTERFACE void _stdcall SetTexturePosterior(unsigned char* buff, int bufferSize);
	DLL_INTERFACE void _stdcall SetTextureInferior(unsigned char* buff, int bufferSize);
	DLL_INTERFACE void _stdcall SetTextureSuperior(unsigned char* buff, int bufferSize);
	DLL_INTERFACE void _stdcall SetNormalizedWindowTextPosition(float x, float y, float w, float h);
	DLL_INTERFACE void _stdcall RedoRemocao(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall UndoRemocao(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall ResizeTelaVolume(int width, int height, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall ResetSerie(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetCallbackDeProgressoDoLoadDaCarga(void* ptr_funcao);
	DLL_INTERFACE void _stdcall SetParamsConfidenceConnected(float multiplier, int iteracoes, int raio_inicial,  int tipo_suavizacao);
	DLL_INTERFACE void _stdcall SegmentVolume();
	//Pega o ponto marcado pelo widget de segmenta��o, usando os params definidos nos m�todos SetParams*
	DLL_INTERFACE void _stdcall SegmentMesh();
	//Guarda os params da segmenta��o por connected threshold em um lugar tempor�rio no Contexto. Esse m�todo gera os dados
	//necess�rios pro SegmentMesh ou SegmentVolume funcionar.
	DLL_INTERFACE void _stdcall SetParamsConnectedThreshold(double add_upper, double add_lower, int tipo_suavizacao);
	//Desativa o widget 3d de segmentacao
	DLL_INTERFACE void _stdcall DeactivateWidgetDeSegmentacao();
	//Ativa o widget 3d de segmenta��o.
	DLL_INTERFACE void _stdcall ActivateWidgetDeSegmentacao();
    DLL_INTERFACE  void _stdcall SerializeCTF(vr::EstruturaDeCor* e, float ambient,
		float diffuse, float specular, int shading, int clamping, char* nome, char* result);
	DLL_INTERFACE void _stdcall SerializeFuncaoDeTransferencia(char* out_text, const char* nome_da_funcao, long handleDoSubsistema);
	DLL_INTERFACE void _stdcall GetColonViewMat(float* mat);
	DLL_INTERFACE void _stdcall SetColonViewMat(const float* mat);
	DLL_INTERFACE int _stdcall OnKeyPress(HWND wnd, UINT nFlags, char key);
	DLL_INTERFACE int _stdcall OnKeyUp(HWND wnd, UINT nFlags, char key);
	DLL_INTERFACE int _stdcall OnKeyDown(HWND wnd, UINT nFlags, char key);
	DLL_INTERFACE void _stdcall UpdateDimensoesDasJanelas();
	DLL_INTERFACE void _stdcall AdicionarColonoscopiaNaTelaVolume();
	//Posiciona a cam pra come�ar a colonoscopia
	DLL_INTERFACE void _stdcall PosicionarCameraParaColonoscopia();
	/*Aplica o marching cubes no volume salvo usando SalvarVolumeAtualParaMarchingCubes();*/
	DLL_INTERFACE void _stdcall CriarMarchingCubes();
	/*Guarda o volume que est� na tela em um buffer para ser usado mais tarde no Marching Cubes.
	Ser� guardado o cubo de corte, as remo��es de regi�o e o janelamento usado.*/
	DLL_INTERFACE void _stdcall SalvarVolumeAtualParaMarchingCubes();
	/*Salva o volume que est� na tela. Como n�o sei fazer a captura dos voxels na placa de v�deo tenho que replicar a renderiza��o.
	Para isso o resultado da pipeline passar� por um filtro para converter os shorts para RGB segundo a fun�ao de transfer�ncia
	e ter� removida as regi�es fora do cubo de corte.*/
	DLL_INTERFACE void _stdcall DumpVolume(const char* dirToSave);
	DLL_INTERFACE short _stdcall PickMatrixElement(int x, int y, int z);
	DLL_INTERFACE void _stdcall LoadUsandoSerieJaCarregada(const char* idExame, const char* idSerie, long handleSubsistema);
	DLL_INTERFACE void _stdcall PushFatia(const char* path);
	/*Cria um itk::image e uma tabela com a metadata nele na mem�ria global. N�o inicializa pipeline alguma. Esse itk::image
	� criado a partir da lista de caminhos de arquivos de fatias. Normalmente essa lista ser� informada via PushFatia().*/
	DLL_INTERFACE int  _stdcall InitVolume(const char* idExame, const char* idSerie);
	DLL_INTERFACE void _stdcall SetVisaoBonitinha();
	DLL_INTERFACE void _stdcall SetVisaoToDireita(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetVisaoToEsquerda(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetVisaoToAnterior(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetVisaoToPosterior(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetVisaoToSuperior(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetVisaoToInferior(long handleDoSubsistema);
	DLL_INTERFACE void _stdcall SetShading(bool shade);
	DLL_INTERFACE void _stdcall GetError(ExceptionStructure* errorData);
	DLL_INTERFACE void _stdcall SetKeepInside(bool ki);
	DLL_INTERFACE void _stdcall BeginDrawContour();
	DLL_INTERFACE void _stdcall EndDrawContour();
	DLL_INTERFACE void _stdcall DrawLacoDeCorte(int* x, int* y, int qtd);
	DLL_INTERFACE void _stdcall DrawCirculoDeCorte(int x0, int y0, int r);
	DLL_INTERFACE void _stdcall DrawRectDeCorte(int x0, int y0, int x1, int y1);
	DLL_INTERFACE void _stdcall Bar();
	DLL_INTERFACE void _stdcall SetOrientacaoDoMprToAxial(int numPlano);
	DLL_INTERFACE void _stdcall SetOrientacaoDoMprToCoronal(int numPlano);
	DLL_INTERFACE void _stdcall SetOrientacaoDoMprToSagital(int numPlano);
	DLL_INTERFACE int _stdcall GetSegmentSeedRadius();
	DLL_INTERFACE void _stdcall GetProjectedSeedsPositions(int* lstX, int* lstY, int* sz);
	DLL_INTERFACE void _stdcall ConfidenceConnectedSegmentation(int iterations, double variance, int initialRadius);
	DLL_INTERFACE void _stdcall SetMoveSeedCallback(void* c);
	DLL_INTERFACE void _stdcall CreateSegmentationSeed();
	DLL_INTERFACE void _stdcall ClearSegmentationSeeds();
	DLL_INTERFACE void _stdcall ConnectedThresholdSegmentation(short lowerThres, short upperThresh);
	DLL_INTERFACE void _stdcall Foobar();
	DLL_INTERFACE bool _stdcall CanUseGPU(int tamanhoDaSerieEmBytes);
	DLL_INTERFACE void _stdcall GetRamDisponivel(int* val);
	DLL_INTERFACE void _stdcall QueryOpenGLCapabilities(HWND hwnd);
	DLL_INTERFACE void _stdcall GerarTestePraDadosDaMemoria();
//================================================================================================================
//===================================Diversas=====================================================================
	//Informa qual fun��o vai tratar o callback de progresso dos filtros. A fun��o dever� ser da forma
	//void fn(double v), com conven��o de chamada cdecl.
	DLL_INTERFACE void _stdcall SetProgressHandler(DelphiProgressCallback callback);
//================================================================================================================
//===================================SHADING========================================================================
	DLL_INTERFACE void _stdcall GetDiffuseAmbientSpecular(float* diffuse, float* ambient, float* specular);
	DLL_INTERFACE void _stdcall SetDiffuse(float v);
	DLL_INTERFACE void _stdcall SetAmbient(float v);
	DLL_INTERFACE void _stdcall SetSpecular(float v);
	DLL_INTERFACE void _stdcall SetRenderizacaoToMIP();
	DLL_INTERFACE void _stdcall SetRenderizacaoToAditivo();
	DLL_INTERFACE void _stdcall SetRenderizacaoToComposite();
//================================================================================================================
//==================================================================================================================
//====================================QUALIDADE=====================================================================
	/**Modifica o espa�o entre cada raio do raytrace: quanto menor o espa�o mais raios. A quantidade de raios governa
		a qualidade e velocidade do raytrace: quanto mais raios mais lento e melhor qualidade. Essa opera��o pode ser
		realizada a qualquer momento.*/
	DLL_INTERFACE void _stdcall SetEspacoDoRaytrace(float minimo, float maximo, long handleDoSubsistema);
	/**Se true � para usar a placa de v�deo, se false, n�o. Essa flag � usada no construtor do objeto de volume para
		determinar se � para criar um herdeiro de vtkVolumeMapper que use a placa de v�deo ou n�o. Essa fun��o s� pode ser usada
		antes do volume ser criado.*/
	DLL_INTERFACE void _stdcall SetUsarPlacaDeVideo(bool isPraUsar);


	DLL_INTERFACE void _stdcall SwitchUseColorFunction(bool flag);
	DLL_INTERFACE void _stdcall SwitchWindow(bool flag);
	//DLL_INTERFACE void _stdcall GetWindowCenterAndWidth(int* center, int* width);


	DLL_INTERFACE void _stdcall RemoverContorno(extrusao::Contorno* contour);

	DLL_INTERFACE bool _stdcall IsVOIAtivo();
	DLL_INTERFACE void _stdcall AtivarVOI();
	DLL_INTERFACE void _stdcall DesativarVOI();

	DLL_INTERFACE void _stdcall SetTabelaDeOpacidadeGradiente(corEOpacidade::EstruturaDeOpacidade* e);
	DLL_INTERFACE void _stdcall GetTabelaDeOpacidadeGradiente(corEOpacidade::EstruturaDeOpacidade* e);


	DLL_INTERFACE void _stdcall GetTabelaDeOpacidadeEscalar(corEOpacidade::EstruturaDeOpacidade* e);
	/**
	1=rgb
	2=hsv
	*/
	DLL_INTERFACE void _stdcall SetTabelaDeCorPointers(int* x, float* r, float* g, float* b, float* a, float* mid, float* sharp, int length);
	DLL_INTERFACE void _stdcall SetLUT(int* x, float* r, float* g, float* b, float* a, int len);
	DLL_INTERFACE void _stdcall SetTabelaDeCor(vr::EstruturaDeCor* e, int qualFormato, long handleSubsistema);
	DLL_INTERFACE void _stdcall GetTabelaDeCor(vr::EstruturaDeCor* e);

	/**
		Cria a s�rie a partir dos dados na estrutura passada como par�metro.
		As dimens�es do extent tem que ser iguais ou menores que o tamanho dos dados
		apontados pela array de arrays sen�o vai dar accessViolation
	*/


	/**
		For�a a renderiza��o de todas as janelas
	*/
	DLL_INTERFACE void _stdcall ForceRender(long handleDoSubsistema);
	/**
		Inicializa o contexto. Tem que ser a primeira opera��o.
	*/
	DLL_INTERFACE void _stdcall InicializarContexto();
	/**
		Desliga.
	*/
	DLL_INTERFACE void _stdcall FinalizarContexto(long handleDoSubsistema);
	/**
		Cria, mas n�o ativa a renderiza��o, da janela do volume, vinculando uma janela do Delphi aGetWL um sist de renderiz��o da vtk.
	*/

	/**
		Ativa a renderiza��o da janela do volume
	*/
	DLL_INTERFACE void _stdcall AtivarJanelaDoVolume(long handleDoSubsistema);

	DLL_INTERFACE void _stdcall CriarJanelaDaColonoscopia(HWND handle);
	DLL_INTERFACE void _stdcall AtivarJanelaDaColonoscopia();

	/**
		Cria, mas n�o ativa a renderiza��o, da janela do cubo de orienta��o, vinculando uma janela do Delphi a um sist de renderiz��o da vtk.
	*/
	DLL_INTERFACE void _stdcall CriarJanelaDoCuboDeOrientacao(HWND handle);
	/**
		Ativa a renderiza��o da janela do cubo de orientacao
	*/
	DLL_INTERFACE void _stdcall AtivarJanelaDoCuboDeOrientacao();

	//DLL_INTERFACE void _stdcall CriarJanelaDoPlano01(HWND handle);
	//DLL_INTERFACE void _stdcall AtivarJanelaDoPlano01();
	//DLL_INTERFACE void _stdcall CriarJanelaDoPlano02(HWND handle);
	//DLL_INTERFACE void _stdcall AtivarJanelaDoPlano02();
	//DLL_INTERFACE void _stdcall CriarJanelaDoPlano03(HWND handle);
	//DLL_INTERFACE void _stdcall AtivarJanelaDoPlano03();
	//DLL_INTERFACE void _stdcall CriarTelaSeccaoVolume(HWND handle);
	//DLL_INTERFACE void _stdcall AtivarTelaSeccaoVolume();


	DLL_INTERFACE void _stdcall GetOperacoesDosBotoes(int& esq, int& mid, int& dir);
	//0 nada 1 zoom 2 pan 3 rotate
	DLL_INTERFACE void _stdcall SetOperacaoBtnEsquerdo(int idOperacao);
	DLL_INTERFACE void _stdcall SetOperacaoBtnMeio(int idOperacao);
	DLL_INTERFACE void _stdcall SetOperacaoBtnDireito(int idOperacao);

	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeMouseMove(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeLMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeLMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeMMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeMMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeRMouseDown(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);
	/**
		Tratador do evento
	*/
	DLL_INTERFACE int _stdcall OnVolumeRMouseUp(HWND wnd, UINT nFlags, int X, int Y, long handleDoSubsistema);

	DLL_INTERFACE void _stdcall GetAvailableMemory();

	DLL_INTERFACE void _stdcall SetSpecularPower(float spec);

}
#endif