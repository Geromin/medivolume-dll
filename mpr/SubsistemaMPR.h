#pragma once
#include <string>
#include <array>
#include <Windows.h>
#include <vtkOpenGL.h>
#include "MPRUtils.h"
#include <itktypes.h>
#include <itkImage.h>
#include <map>
#include "TelaMPR.h"
#include <memory>
#include <itkImage.h>
#include <vtkResliceCursor.h>
#include "mprCallback.h"
#include <vtkCommand.h>
#include <vtkResliceCursorWidget.h>
#include <vtkImageImport.h>
using namespace std;

namespace mpr{
	//O contexto do MPR, ele cont�m tudo relativo ao MPR e existe um para cada objeto TMpr no mundo do delphi.
	class Subsistema{
	private:
		//� proibido copiar esse objeto e ele ser copiado estava criando bugs dificeis de detectar na boost. Ent�o desabilitei o
		//construtor de c�pia e o operador =
		Subsistema(const Subsistema&) = delete;
		void operator=(Subsistema const &x) = delete;

		int idDaTelaEscolhidaParaTelaGrande;
		vtkSmartPointer<vtkImageImport> imagemReduzidaImporter;

		static map<void*, void*> gambiarraTabelaSubsistemaCallbackDoDelphi;
		TOnChangeDicom callbackProDelphi;
		int thickness;
		bool pipelineAlredyUsed;
		vtkSmartPointer<myMPRCallback> sharedMPRCallback;
		itk::Image<short, 3>::Pointer imagem, imagemLowRes;
		short scalarOffset;
		array<shared_ptr<Tela>, 3> telas;
		string idExame, idSerie;
		map<string, string> metadata;
		itk::Image<short, 3>::Pointer CreateLowRes(itk::Image<short, 3>::Pointer imagem, float fator = 3.0f);
		vtkSmartPointer<vtkResliceCursor> sharedCursor;
		//vtkSmartPointer<myMPRCallback> sharedCallback;

		void SetImageInTelas(array<shared_ptr<Tela>, 3> _telas, itk::Image<short, 3>::Pointer iHQ, itk::Image<short, 3>::Pointer iLQ )
		{
			for (shared_ptr<Tela> t : _telas)
				t->SetImage(iHQ, imagemReduzidaImporter);
		}

		void SetSharedCursorInTelas(array<shared_ptr<Tela>, 3> _telas, vtkSmartPointer<vtkResliceCursor> c)
		{
			for (shared_ptr<Tela> t : _telas)
				t->SetUp(sharedCursor);
		}

		void SetSignalsAndObserversInTelas(array<shared_ptr<Tela>, 3> _telas,
			vtkSmartPointer<myMPRCallback> sharedCallback){
			for (shared_ptr<Tela> t : _telas)
			{
				t->GetResliceCursorWidget()->AddObserver(vtkResliceCursorWidget::ResliceAxesChangedEvent, sharedCallback);
				t->GetResliceCursorWidget()->AddObserver(vtkCommand::AnyEvent, sharedCallback);
				sharedCallback->AddRenderSignal<shared_ptr<Tela>>(t);
				sharedCallback->AddQualitySignal<shared_ptr<Tela>>(t);
				sharedCallback->AddResliceSignal<shared_ptr<Tela>>(t);
			}
		}
	public:

		void operator()(vtkResliceCursorWidget* widget);
		Subsistema(string idExame, string idSerie);
		void AddDistanceMeasurement();
		void CreatePipeline(itk::Image<short, 3>::Pointer image, map<string, string> metadata, short valueOffset);
		void CreateScreen(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela);
		void CreateScreen(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, int t);
		void ForceRender(int qual);
		void GetCurrentWL(array<int, 2>&wl);
		void GetDimensions(float &x, float &y, float &z);
		MPRV2_DicomData GetMPRData(int qualTela);
		void GetPhysicalOrigin(float &x, float &y, float &z);
		short GetScalarOffset();
		void GetSpacing(float &x, float &y, float &z);
		void InitialSetup();
		void Resize(int w, int h, int qual);
		void OnMouseMove(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void OnMMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void OnMMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void OnRMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void OnRMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void OnLMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void OnLMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel);
		void Resize(int w, int h);
		void Rotate(int qualEixo, float angulo);
		void SetCorDasLetras(float r, float  g, float  b);
		void SetCurrentWL(array<int, 2>wl);
		void SetDragRect(int left,int top,int width,int height);
		void SetFontSize(float sz);
		void SetFuncao(int qual);
		void SetOnChangeDicom(TOnChangeDicom fn);
		//void SetLado(int tamanho);
		void SetMouseInteraction(int idBtn, int idOperacao);
		void SetThickness(int numFatias);
		void Translate(double x, double y, double z);
		void SetOrigemTelaGrande(int qual);
		void SetOnDicomChange(TOnChangeDicom fn);
		string GetIdExame();
		string GetIdSerie();
		bool IsThisExam(char* idExame, char* idSerie);
	};
}