#include "mprCallback.h"

namespace mpr{
	myMPRCallback::myMPRCallback(){
		areAxesSet = false;
		isUsingLowRes = false;
	}

	void myMPRCallback::PrintVector(string txt, array<double, 3> vec){
		cout << txt << " = " << vec[0] << ", " << vec[1] << ", " << vec[2] << endl;
	}

	void myMPRCallback::Execute(vtkObject *caller, unsigned long ev, void *callData){
		vtkResliceCursorWidget* widget = vtkResliceCursorWidget::SafeDownCast(caller);
		if (!areAxesSet)
		{
			widget->GetResliceCursorRepresentation()->GetResliceCursor()->GetXAxis(xAxis.data());
			widget->GetResliceCursorRepresentation()->GetResliceCursor()->GetYAxis(yAxis.data());
			widget->GetResliceCursorRepresentation()->GetResliceCursor()->GetZAxis(zAxis.data());
			areAxesSet = true;
		}
		array<double, 3> currentX, currentY, currentZ;
		widget->GetResliceCursorRepresentation()->GetResliceCursor()->GetXAxis(currentX.data());
		widget->GetResliceCursorRepresentation()->GetResliceCursor()->GetYAxis(currentY.data());
		widget->GetResliceCursorRepresentation()->GetResliceCursor()->GetZAxis(currentZ.data());

		bool hasChangedAngles = false;
		if ((xAxis[0] != currentX[0]) || (xAxis[1] != currentX[1]) || (xAxis[2] != currentX[2]))
			hasChangedAngles = true;
		if ((yAxis[0] != currentY[0]) || (yAxis[1] != currentY[1]) || (yAxis[2] != currentY[2]))
			hasChangedAngles = true;
		if ((zAxis[0] != currentZ[0]) || (zAxis[1] != currentZ[1]) || (zAxis[2] != currentZ[2]))
			hasChangedAngles = true;

		cout << "CHANGED ANGLES = " << hasChangedAngles << endl;

		xAxis = currentX;
		yAxis = currentY;
		zAxis = currentZ;
		//cout <<__FUNCTION__<<" ln "<<__LINE__<< " EV = " << ev << endl;
		switch (ev)
		{
		case 41: //Inicio do click: reduz a qualidade
			if (hasChangedAngles && !isUsingLowRes)
			{
				sinalChangeQuality(Qualidade::LOW_RES);
				isUsingLowRes = true;
			}
			sinalRender();
			sinalReslice(widget);
			break;
		case 42:
			if (hasChangedAngles && !isUsingLowRes)
			{
				sinalChangeQuality(Qualidade::LOW_RES);
				isUsingLowRes = true;
			}
			sinalRender();
			sinalReslice(widget);
			break;
		case 1056:
			if (hasChangedAngles && !isUsingLowRes)
			{
				sinalChangeQuality(Qualidade::LOW_RES);
				isUsingLowRes = true;
			}
			sinalRender();
			sinalReslice(widget);
			break;

		case 43: //Fim do click: aumenta a qualidade
			sinalChangeQuality(Qualidade::HI_RES);
			isUsingLowRes = false;
			sinalRender();
			sinalReslice(widget);
			break;
		}
	}
}