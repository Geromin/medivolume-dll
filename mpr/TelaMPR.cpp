#include "TelaMPR.h"
using namespace std;

namespace mpr{
	void MyRenderPass::SdlLine(int x0, int y0, int x1, int y1){
		assert(canvasSdlRenderer != nullptr);
		SDL_RenderDrawLine(canvasSdlRenderer, x0, y0, x1, y1);
	}
	void MyRenderPass::Rectangle(int x0, int y0, int x1, int y1)
	{
		SDL_Rect rect;
		rect.x = x0;
		rect.y = y0;
		rect.w = x1 - x0;
		rect.h = y1 - y0;
		SDL_RenderDrawRect(canvasSdlRenderer, &rect);
	}

	MyRenderPass::MyRenderPass(){
		sequencePass = vtkSmartPointer<vtkSequencePass>::New();
		lightsPass = vtkSmartPointer<vtkLightsPass>::New();
		defaultPass = vtkSmartPointer<vtkDefaultPass>::New();
		passes = vtkSmartPointer<vtkRenderPassCollection>::New();
		cameraPass = vtkSmartPointer<vtkCameraPass>::New();
		passes->AddItem(lightsPass);
		passes->AddItem(defaultPass);
		sequencePass->SetPasses(passes);
		cameraPass->SetDelegatePass(sequencePass);
	}
	MyRenderPass::~MyRenderPass()
	{
	}
	void MyRenderPass::Render(const vtkRenderState *s){
		cameraPass->Render(s);//Renderização para a tela
		vtkOpenGLRenderer* glRen = vtkOpenGLRenderer::SafeDownCast(s->GetRenderer());
		vtkRenderWindow* wnd = glRen->GetRenderWindow();
		const int screenWidth = wnd->GetSize()[0];
		const int screenHeight = wnd->GetSize()[1];
		BYTE *screenBuffer = wnd->GetRGBACharPixelData(0, 0, screenWidth - 1, screenHeight - 1, 0);
		this->canvasSdlSurface = SDL_CreateRGBSurfaceFrom(screenBuffer, screenWidth, screenHeight, 32, screenWidth * 4, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
		this->canvasSdlRenderer = SDL_CreateSoftwareRenderer(canvasSdlSurface);
		SDL_SetRenderDrawColor(canvasSdlRenderer, 0, 0, 255, 0);
		SDL_Rect rect;
		rect.x = DragRect[0];
		rect.y = screenHeight - (DragRect[1] + 1);
		rect.w = DragRect[2];
		rect.h = -DragRect[3];
		SDL_RenderDrawRect(canvasSdlRenderer, &rect);
		wnd->SetRGBACharPixelData(0, 0, screenWidth - 1, screenHeight - 1, screenBuffer, 0);
		delete[] screenBuffer;
		SDL_DestroyRenderer(canvasSdlRenderer);
		SDL_FreeSurface(canvasSdlSurface);
	}

	void Tela::ApplyThingsNeededForImageBeFloat(vtkSmartPointer<vtkImageResliceToColors> reslicer){
		reslicer->BypassOn();
		reslicer->SetOutputFormatToLuminance();
		reslicer->Modified();
		reslicer->Update();
	}

	void Tela::SetDragRect(int left, int top, int width, int height)
	{
		dragRect = { { left, top, width, height } };
		renderPass->DragRect = dragRect;
	}

	void Tela::SetFuncao(int idFuncao){
		switch (idFuncao)
		{
		case 0:
			imageMapperHiRes->SetSlabTypeToMax();
			imageMapperLowRes->SetSlabTypeToMax();
			break;
		case 1:
			imageMapperHiRes->SetSlabTypeToMin();
			imageMapperLowRes->SetSlabTypeToMin();
			break;
		case 2:
			imageMapperHiRes->SetSlabTypeToSum();
			imageMapperLowRes->SetSlabTypeToSum();
			break;
		case 3:
			imageMapperHiRes->SetSlabTypeToMean();
			imageMapperLowRes->SetSlabTypeToMean();
			break;
		}
	}

	bool Tela::ContainsThisReslice(vtkResliceCursorWidget* w)
	{
		if ((int)w == (int)resliceCursorWidget.GetPointer())
			return true;
		else
			return false;
	}

	vtkSmartPointer<vtkImageResliceToColors> Tela::GetCurrentReslice(){
		vtkSmartPointer<vtkImageResliceToColors> resultado = nullptr;
		switch (currentQualidade){
		case LOW_RES:
			resultado = imageMapperLowRes->GetImageReslice();
			break;
		case HI_RES:
			resultado = imageMapperHiRes->GetImageReslice();
			break;
		}
		//ApplyThingsNeededForImageBeFloat(resultado);
		return resultado;
	}

	array<int, 2> Tela::GetWL(){
		array<int, 2> resultado = { { imagePropertyHiRes->GetColorWindow(), imagePropertyHiRes->GetColorLevel() } };
		return resultado;
	}

	void Tela::SetWL(array<int, 2> wl) 		{
		imagePropertyHiRes->SetColorWindow(wl[0]);
		imagePropertyHiRes->SetColorLevel(wl[1]);
		imagePropertyLowRes->SetColorWindow(wl[0]);
		imagePropertyLowRes->SetColorLevel(wl[1]);
	}

	Tela::Tela(HWND handle, const int _id) :id(_id)
	{
		this->hasSetZoom = false;
		this->renderer = vtkSmartPointer<vtkOpenGLRenderer>::New();
		renderPass = vtkSmartPointer<MyRenderPass>::New();
		renderer->SetPass(renderPass);
		SetDragRect(1, 1, 16, 16);
		this->renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
		this->renderWindow->AddRenderer(this->renderer);
		renderWindow->SetDesiredUpdateRate(20.0);
		// setup the parent window
		this->renderWindow->SetParentId(handle);
		this->renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
		this->renderWindowInteractor->SetRenderWindow(this->renderWindow);
		renderWindow->SetInteractor(renderWindowInteractor);
		style = vtkSmartPointer<vtkInteractorStyle>::New();
		renderWindowInteractor->SetInteractorStyle(style);
		imageMapperHiRes = nullptr;
		imagePropertyHiRes = nullptr;
		letras = nullptr;
		isFlipped = false;
	}

	void Tela::Resize(int w, int h)
	{
		renderWindow->SetSize(w, h);
		if (letras)
			letras->Posicionar(renderer);
		renderWindow->Render();
	}

	void Tela::operator()(Qualidade q){
		currentQualidade = q;
		switch (q)
		{
		case LOW_RES:
			//cout << "Reduziu qualidade" << endl;
			lodProp->SetSelectedLODID(idLodLow);

			break;
		case HI_RES:
			//cout << "Aumentou qualidade" << endl;
			lodProp->SetSelectedLODID(idLodHi);
			break;
		}
	}


	void Tela::operator()(vtkResliceCursorWidget* widget){
		//if (!hasSetZoom)
		//{
		//	this->fixedCameraDistance = this->renderer->GetActiveCamera()->GetDistance();
		//	hasSetZoom = true;
		//}
		vtkImageResliceToColors* reslice = nullptr;
		vtkAlgorithmOutput* saida = nullptr;
		switch (currentQualidade){
		case LOW_RES:
		{
			double *directionCosinesMat;
			directionCosinesMat = this->imageMapperLowRes->GetImageReslice()->GetResliceAxesDirectionCosines();
			cout << "---Tela " << this->id << " direction cosines (low res)" << endl;
			cout << directionCosinesMat[0] << ", " << directionCosinesMat[1] << ", " << directionCosinesMat[2] << endl;
			cout << directionCosinesMat[3] << ", " << directionCosinesMat[4] << ", " << directionCosinesMat[5] << endl;
			cout << directionCosinesMat[6] << ", " << directionCosinesMat[7] << ", " << directionCosinesMat[8] << endl;
			double v1[] = { directionCosinesMat[0], directionCosinesMat[3], directionCosinesMat[6] };
			double v2[] = { directionCosinesMat[1], directionCosinesMat[4], directionCosinesMat[7] };
			double v3[] = { directionCosinesMat[2], directionCosinesMat[5], directionCosinesMat[8] };
			double d0 = vtkMath::Dot(v1, v2);
			double d1 = vtkMath::Dot(v1, v3);
			double d2 = vtkMath::Dot(v2, v3);

			const bool d0ok = (d0 < 0.000001);
			const bool d1ok = (d1 < 0.000001);
			const bool d2ok = (d2 < 0.000001);
			double mag0 = sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]);
			double mag1 = sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2]);
			double mag2 = sqrt(v3[0] * v3[0] + v3[1] * v3[1] + v3[2] * v3[2]);
			const double mag0Ok = (mag0 > 0.999999 && mag0 < 1.000001);
			const double mag1Ok = (mag1 > 0.999999 && mag1 < 1.000001);
			const double mag2Ok = (mag2 > 0.999999 && mag2 < 1.000001);
			if (!d0ok){
				cout << "Bug no direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(d0ok);
				throw std::exception("bug no direction cossine");
			}
			if (!d1ok){
				cout << "Bug no direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(d1ok);
				throw std::exception("bug no direction cossine");
			}
			if (!d2ok){
				cout << "Bug no direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(d2ok);
				throw std::exception("bug no direction cossine");
			}
			if (!mag0Ok){
				cout << "Bug na magnitude do direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(mag0Ok);
				throw std::exception("bug no direction cosine");
			}
			if (!mag1Ok){
				cout << "Bug na magnitude do direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(mag1Ok);
				throw std::exception("bug no direction cosine");
			}
			if (!mag2Ok){
				cout << "Bug na magnitude do direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(mag2Ok);
				throw std::exception("bug no direction cosine");
			}
		}
		break;
		case HI_RES:
		{
			double *directionCosinesMat;
			directionCosinesMat = this->imageMapperLowRes->GetImageReslice()->GetResliceAxesDirectionCosines();
			//cout << "---Tela " << this->id << " direction cosines (hi res)" << endl;
			//cout << directionCosinesMat[0] << ", " << directionCosinesMat[1] << ", " << directionCosinesMat[2] << endl;
			//cout << directionCosinesMat[3] << ", " << directionCosinesMat[4] << ", " << directionCosinesMat[5] << endl;
			//cout << directionCosinesMat[6] << ", " << directionCosinesMat[7] << ", " << directionCosinesMat[8] << endl;
			double v1[] = { directionCosinesMat[0], directionCosinesMat[3], directionCosinesMat[6] };
			double v2[] = { directionCosinesMat[1], directionCosinesMat[4], directionCosinesMat[7] };
			double v3[] = { directionCosinesMat[2], directionCosinesMat[5], directionCosinesMat[8] };
			double d0 = vtkMath::Dot(v1, v2);
			double d1 = vtkMath::Dot(v1, v3);
			double d2 = vtkMath::Dot(v2, v3);
			const bool d0ok = (d0 < 0.000001);
			const bool d1ok = (d1 < 0.000001);
			const bool d2ok = (d2 < 0.000001);
			double mag0 = sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]);
			double mag1 = sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2]);
			double mag2 = sqrt(v3[0] * v3[0] + v3[1] * v3[1] + v3[2] * v3[2]);
			const double mag0Ok = (mag0 > 0.999999 && mag0 < 1.000001);
			const double mag1Ok = (mag1 > 0.999999 && mag1 < 1.000001);
			const double mag2Ok = (mag2 > 0.999999 && mag2 < 1.000001);
			if (!d0ok){
				cout << "Bug no direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(d0ok);
				throw std::exception("bug no direction cossine");
			}
			if (!d1ok){
				cout << "Bug no direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(d1ok);
				throw std::exception("bug no direction cossine");
			}
			if (!d2ok){
				cout << "Bug no direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(d2ok);
				throw std::exception("bug no direction cossine");
			}
			if (!mag0Ok){
				cout << "Bug na magnitude do direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(mag0Ok);
				throw std::exception("bug no direction cosine");
			}
			if (!mag1Ok){
				cout << "Bug na magnitude do direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(mag1Ok);
				throw std::exception("bug no direction cosine");
			}
			if (!mag2Ok){
				cout << "Bug na magnitude do direction cosine " << __FUNCTION__ << " line " << __LINE__ << endl;
				assert(mag2Ok);
				throw std::exception("bug no direction cosine");
			}
		}
		break;
		}
		vtkCamera* cam = this->renderer->GetActiveCamera();
		//cam->SetDistance(fixedCameraDistance);
		cout << "Camera distance da tela "<<this->id<<" = " << cam->GetDistance() << endl;
		cout << "Paralel projection da tela " << this->id << " = " << cam->GetParallelScale() << endl;
	}

	void Tela::SetUp(vtkSmartPointer<vtkResliceCursor> cursor)
	{
		sharedCursor = cursor;
		resliceCursorWidget = vtkSmartPointer<vtkResliceCursorWidget>::New();
		resliceCursorWidget->ManageWindowLevelOff();
		resliceCursorWidget->SetInteractor(renderWindowInteractor);
		resliceCursorWidget->SetPriority(0.1);
		resliceCursorRepresentation = vtkSmartPointer<vtkResliceCursorLineRepresentation>::New();
		resliceCursorWidget->SetRepresentation(resliceCursorRepresentation);
		resliceCursorRepresentation->GetResliceCursorActor()->GetCursorAlgorithm()->SetResliceCursor(sharedCursor);
		switch (id)
		{
		case 0:
			resliceCursorRepresentation->GetResliceCursorActor()->GetCursorAlgorithm()->SetReslicePlaneNormalToZAxis();
			break;
		case 1:
			resliceCursorRepresentation->GetResliceCursorActor()->GetCursorAlgorithm()->SetReslicePlaneNormalToYAxis();
			break;
		case 2:
			resliceCursorRepresentation->GetResliceCursorActor()->GetCursorAlgorithm()->SetReslicePlaneNormalToXAxis();
			break;
		}
		resliceCursorWidget->SetDefaultRenderer(renderer);
		resliceCursorWidget->EnabledOn();
		resliceCursorRepresentation->ShowReslicedImageOff();
		renderer->ResetCamera();
		currentQualidade = HI_RES;
		letras = make_shared<Letras>(renderer);
	}
	void Tela::SetThickness(int thickness)
	{
		imageMapperLowRes->SetSlabSampleFactor(1);
		imageMapperLowRes->ResampleToScreenPixelsOff();
		imageMapperLowRes->SeparateWindowLevelOperationOn();
		imageMapperLowRes->SetSlabThickness(thickness);
		imageMapperLowRes->AutoAdjustImageQualityOn();

		imageMapperHiRes->SetSlabSampleFactor(1);
		imageMapperHiRes->ResampleToScreenPixelsOff();
		imageMapperHiRes->SeparateWindowLevelOperationOn();
		imageMapperHiRes->SetSlabThickness(thickness);
		imageMapperHiRes->AutoAdjustImageQualityOn();

		renderWindow->Render();
	}

	void  Tela::SetImage(itk::Image<short, 3>::Pointer hiRes, vtkSmartPointer<vtkImageImport> lowResImporter) // itk::Image<short, 3>::Pointer lowRes)
	{

		importedImageLowRes = lowResImporter;// CreateVTKImage(lowRes);
		imageMapperLowRes = vtkSmartPointer<vtkImageResliceMapper>::New();
		imageMapperLowRes->ResampleToScreenPixelsOff();
		imageMapperLowRes->SetInputData(importedImageLowRes->GetOutput());
		imageMapperLowRes->GetImageReslice()->BypassOn();
		imageMapperLowRes->SliceFacesCameraOn();
		imageMapperLowRes->SliceAtFocalPointOn();
		imageMapperLowRes->BorderOn();
		imagePropertyLowRes = vtkSmartPointer<vtkImageProperty>::New();
		imagePropertyLowRes->SetColorWindow(350);
		imagePropertyLowRes->SetColorLevel(100);
		imagePropertyLowRes->SetAmbient(0);
		imagePropertyLowRes->SetDiffuse(1);
		imagePropertyLowRes->SetOpacity(1);
		imagePropertyLowRes->SetInterpolationTypeToNearest();
		imageMapperLowRes->BorderOff();
		imageMapperLowRes->BackgroundOn();

		importedImageHiRes = CreateVTKImage(hiRes);
		imageMapperHiRes = vtkSmartPointer<vtkImageResliceMapper>::New();
		imageMapperHiRes->ResampleToScreenPixelsOff();
		imageMapperHiRes->SetInputData(importedImageHiRes->GetOutput());
		imageMapperHiRes->GetImageReslice()->BypassOn();
		imageMapperHiRes->SliceFacesCameraOn();
		imageMapperHiRes->SliceAtFocalPointOn();
		imageMapperHiRes->BorderOn();
		imagePropertyHiRes = vtkSmartPointer<vtkImageProperty>::New();
		imagePropertyHiRes->SetColorWindow(350);
		imagePropertyHiRes->SetColorLevel(100);
		imagePropertyHiRes->SetAmbient(0);
		imagePropertyHiRes->SetDiffuse(1);
		imagePropertyHiRes->SetOpacity(1);
		imagePropertyHiRes->SetInterpolationTypeToLinear(); //Linear();


		imageMapperHiRes->GetImageReslice()->BorderOff();
		imageMapperHiRes->BackgroundOn();
		imageMapperHiRes->GetImageReslice()->SetBackgroundLevel(-3000);

		imageMapperLowRes->GetImageReslice()->BorderOff();
		imageMapperLowRes->BackgroundOn();
		imageMapperLowRes->GetImageReslice()->SetBackgroundLevel(-3000);



		lodProp = vtkSmartPointer<vtkLODProp3D>::New();
		lodProp->AutomaticLODSelectionOff();
		idLodHi = lodProp->AddLOD(imageMapperHiRes, imagePropertyHiRes, 0.0);
		idLodLow = lodProp->AddLOD(imageMapperLowRes, imagePropertyLowRes, 0.0);

		lodProp->SetSelectedLODID(idLodHi);

		renderer->AddViewProp(lodProp);
		renderer->ResetCamera();

		//renderWindow->Render();
		//ApplyThingsNeededForImageBeFloat(imageMapperHiRes->GetImageReslice());
		//ApplyThingsNeededForImageBeFloat(imageMapperLowRes->GetImageReslice());
		//renderWindow->Render();
	}
}