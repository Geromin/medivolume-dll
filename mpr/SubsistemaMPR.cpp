#include "SubsistemaMPR.h"
#include <assert.h>
#include <itkIdentityTransform.h>
#include <itkResampleImageFilter.h>
#include <vtkMatrix4x4.h>
#include <vtkImageResliceToColors.h>
#include <vtkFloatArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkImageFlip.h>
#include <vtkXMLImageDataWriter.h>
#include <fstream>

#include <boost/exception/all.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <vtkImageResample.h>
namespace mpr{
	map<void*, void*> Subsistema::gambiarraTabelaSubsistemaCallbackDoDelphi;

	static TOnChangeDicom testeCbk;
	//1a opera��o - O construtor.
	Subsistema::Subsistema(string idExame, string idSerie){
		//std::cout << __FUNCTION__ << std::endl;
		this->idExame = idExame;
		this->idSerie = idSerie;
		for (shared_ptr<Tela> t : telas)
			t = nullptr;
		scalarOffset = 0;
		imagem = nullptr;
		sharedCursor = nullptr;
		sharedMPRCallback = nullptr;
		pipelineAlredyUsed = false;
		//callbackProDelphi = nullptr;
		thickness = 1;
		idDaTelaEscolhidaParaTelaGrande = 0;
	}
	//2a opera��o - Cria a tela dada.
	void Subsistema::CreateScreen(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela, int t){
		//std::cout << __FUNCTION__ << std::endl;
		assert((t >= 0 && t <= 2) && "Est� dentro do intervalo correto?");
		telas[t] = make_shared<Tela>(handleDaJanela, t);
	}
	//3a coisa a ser invocada.
	void Subsistema::ForceRender(int qual){
		//std::cout << __FUNCTION__ << std::endl;
		assert((telas[0] && telas[1] && telas[2]) && "Telas criadas?");
		telas[qual]->Render();
	}
	//4a opera��o a ser invocada
	void Subsistema::Resize(int w, int h, int qual){
		//std::cout << __FUNCTION__ << std::endl;
		assert(telas[qual] && "Tela pedida existe?");
		telas[qual]->Resize(w, h);
	}
	//5a opera��o a ser invocada.
	void Subsistema::CreatePipeline(itk::Image<short, 3>::Pointer image, map<string, string> metadata, short valueOffset){
		//std::cout << __FUNCTION__ << std::endl;
		if (!pipelineAlredyUsed)//Ainda n�o tem uma pipeline
		{
			this->imagem = image;
			this->scalarOffset = valueOffset;
			this->metadata = metadata;
			imagemLowRes = CreateLowRes(imagem);
			imagemReduzidaImporter = CreateVTKImage(imagemLowRes);
			SetImageInTelas(telas, image, imagemLowRes);
			sharedCursor = vtkSmartPointer<vtkResliceCursor>::New();
			sharedCursor->SetImage(telas[0]->GetImportedImageHiRes()->GetOutput());
			sharedCursor->SetCenter(telas[0]->GetImportedImageHiRes()->GetOutput()->GetCenter());
			sharedCursor->SetThickMode(1);
			SetSharedCursorInTelas(telas, sharedCursor);
			sharedMPRCallback = vtkSmartPointer<myMPRCallback>::New();
			SetSignalsAndObserversInTelas(telas, sharedMPRCallback);
			pipelineAlredyUsed = true;
			sharedMPRCallback->AddResliceSignal(this);
		}
		else//Tem uma pipeline = j� houve um TMpr ligado a esse subsistema no mundo do delphi e ele foi
		{//destruido qdo o usu�rio fechou o MPR e agora que est� sendo criado um novo MPR eu devo aproveitar
			//o que o subsistema j� tem pronto da ultima vez que foi usado.
			SetImageInTelas(telas, image, imagemLowRes);
			SetSharedCursorInTelas(telas, sharedCursor);
			SetSignalsAndObserversInTelas(telas, sharedMPRCallback);
			pipelineAlredyUsed = true;
		}
	}

	void Subsistema::SetCurrentWL(array<int, 2>wl){
		wl[1] = wl[1] + scalarOffset;
		for (shared_ptr<Tela> t : telas)
			t->SetWL(wl);
		ForceRender(0);
		ForceRender(1);
		ForceRender(2);
	}

	//6a opera��o a ser invocada.
	void Subsistema::InitialSetup(){
		//cout << __FUNCTION__ << " NAO FAZ NADA POR ENQUANTO..." << endl;
	}
	//7a opera��o a ser invocada
	void Subsistema::Rotate(int qualEixo, float angulo){
		//cout << __FUNCTION__ << " NA� FAZ NADA POR ENQUANTO..." << endl;
	}

	void Subsistema::SetThickness(int numFatias){
		this->thickness = numFatias;
		for (shared_ptr<Tela> t : telas)
		{
			t->SetThickness(numFatias);
			t->Render();
		}

	}

	void Subsistema::SetOnChangeDicom(TOnChangeDicom fn){
		callbackProDelphi = fn;
		//gambiarraTabelaSubsistemaCallbackDoDelphi.insert(make_pair<void*, void*>((void*)this, (void*)fn));
		//testeCbk = callbackProDelphi;
		//cout << (int)callbackProDelphi << endl;
	}

	itk::Image<short, 3>::Pointer Subsistema::CreateLowRes(itk::Image<short, 3>::Pointer imagem, float fator)
	{
		//std::cout << __FUNCTION__ << std::endl;
		itk::Image<short, 3>::SizeType inputSize = imagem->GetLargestPossibleRegion().GetSize();
		itk::Image<short, 3>::SizeType outputSize;
		outputSize[0] = inputSize[0] / fator;
		outputSize[1] = inputSize[1] / fator;
		outputSize[2] = inputSize[2] / fator;
		itk::Image<short, 3>::SpacingType outputSpacing;
		outputSpacing[0] = imagem->GetSpacing()[0] * fator;
		outputSpacing[1] = imagem->GetSpacing()[1] * fator;
		outputSpacing[2] = imagem->GetSpacing()[2] * fator;
		typedef itk::IdentityTransform<double, 3> TransformType;
		typedef itk::ResampleImageFilter<ImageType, ImageType> ResampleImageFilterType;
		ResampleImageFilterType::Pointer resample = ResampleImageFilterType::New();
		resample->SetOutputParametersFromImage(imagem);
		resample->SetInput(imagem);
		resample->SetSize(outputSize);
		resample->SetOutputSpacing(outputSpacing);
		resample->SetTransform(TransformType::New());
		resample->UpdateLargestPossibleRegion();
		itk::Image<short, 3>::Pointer output = resample->GetOutput();
		return output;
	}



	void Subsistema::AddDistanceMeasurement(){
		assert(false && "nao implementado");
	}

	void Subsistema::CreateScreen(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela){
		assert(false && "nao implementado");
	}


	void Subsistema::GetCurrentWL(array<int, 2>&wl){
		wl[0] = telas[0]->GetWL()[0] ;
		wl[1] = telas[0]->GetWL()[1] - scalarOffset;
	}

	void Subsistema::GetDimensions(float &x, float &y, float &z){
		assert(false && "nao implementado");
	}
	//Pega a imagem e manda pro delphi.
	void Subsistema::operator()(vtkResliceCursorWidget* widget){
		////Isso aqui � tempor�rio e o certo � o usu�rio escolher a tela.
		//int qualTela = -1;
		//if (telas[0]->ContainsThisReslice(widget))
		//	qualTela = 0;
		//if (telas[1]->ContainsThisReslice(widget))
		//	qualTela = 1;
		//if (telas[2]->ContainsThisReslice(widget))
		//	qualTela = 2;
		//assert(qualTela != -1);
		int qualTela = idDaTelaEscolhidaParaTelaGrande;
		MPRV2_DicomData dicomData = GetMPRData(qualTela);
		MPRV2_DicomData *pointerToDelphi = (MPRV2_DicomData*)malloc(sizeof(MPRV2_DicomData));//new MPRV2_DicomData();
		memcpy(pointerToDelphi, &dicomData, sizeof(MPRV2_DicomData));
		callbackProDelphi(idDaTelaEscolhidaParaTelaGrande, (long)this, pointerToDelphi);
		//cout << "Passou do cbk" << endl;
	}

	MPRV2_DicomData Subsistema::GetMPRData(int qualTela){
		//cout << __FUNCTION__ << endl;
		assert((qualTela >= 0) && (qualTela <= 2) && "O qualTela em [0,2]?");
		shared_ptr<Tela> t = telas[qualTela];
		MPRV2_DicomData dicomData;
		vtkSmartPointer<vtkImageResliceToColors> reslicedImage = t->GetCurrentReslice();
		vtkSmartPointer<vtkImageData> img = reslicedImage->GetOutput();
		vtkSmartPointer<vtkImageResample> resampleToUnit = vtkSmartPointer<vtkImageResample>::New();
		resampleToUnit->SetInputData(img);
		resampleToUnit->SetAxisOutputSpacing(0, 1);
		resampleToUnit->SetAxisOutputSpacing(1, 1);
		resampleToUnit->SetAxisOutputSpacing(2, 1);
		resampleToUnit->Update();
		vtkSmartPointer<vtkImageData> foo = resampleToUnit->GetOutput();

		img = resampleToUnit->GetOutput();
		////Grava��o no disco pra debug
#ifndef NDEBUG
		static int debugId;

		vtkSmartPointer<vtkXMLImageDataWriter> debugsave = vtkSmartPointer<vtkXMLImageDataWriter>::New();
		stringstream ss;
		ss << "c:\\debug_image_tela_" << idDaTelaEscolhidaParaTelaGrande<<"_numero_" << debugId << ".vti";
		debugsave->SetFileName(ss.str().c_str());
		debugsave->SetInputData(img);
		debugsave->Update();
#endif
		//Fim da grava��o no disco
		if (img->GetExtent()[1] == -1)
			assert(false && "Imagem Bugada");
		float valuesRange[2];
		/*cout << "Direction cosines" << endl;
		cout << "   " << reslicedImage->GetResliceAxesDirectionCosines()[0] << ", " << reslicedImage->GetResliceAxesDirectionCosines()[1] << ", " << reslicedImage->GetResliceAxesDirectionCosines()[2] << endl;
		cout << "   " << reslicedImage->GetResliceAxesDirectionCosines()[3] << ", " << reslicedImage->GetResliceAxesDirectionCosines()[4] << ", " << reslicedImage->GetResliceAxesDirectionCosines()[5] << endl;
		cout << "   " << reslicedImage->GetResliceAxesDirectionCosines()[6] << ", " << reslicedImage->GetResliceAxesDirectionCosines()[7] << ", " << reslicedImage->GetResliceAxesDirectionCosines()[8] << endl;
		cout << "Extent da imagem = " << img->GetExtent()[0] << ", " << img->GetExtent()[1] << ", " << img->GetExtent()[2] << ", " << img->GetExtent()[3] << ", " << img->GetExtent()[4] << ", " << img->GetExtent()[5] << endl;
		cout << "Spacing da imagem = " << img->GetSpacing()[0] << ", " << img->GetSpacing()[1] << ", " << img->GetSpacing()[2] << endl;*/
		vtkDataArray *id = img->GetPointData()->GetArray("ImageScalars");
		vtkSmartPointer<vtkImageFlip> flipper = vtkSmartPointer<vtkImageFlip>::New();
		flipper->SetInputData(img);	 //Flipper recebe a imagem j� resampleada
		flipper->SetFilteredAxis(1);
		flipper->Update();
		vtkFloatArray *_data = vtkFloatArray::SafeDownCast(flipper->GetOutput()->GetPointData()->GetArray("ImageScalars"));
		if (!_data)
			assert("ERRO! os dados n�o est�o float mas sim byte");
		_data->GetValueRange(valuesRange);
		int _data_sz = _data->GetSize();
		vector<short> convertedFromShortToChar(_data_sz);//Aqui s�o os dados da imagem resampleada
		short reslopeIntercept = 0;
		double slopeRescale = 1;
		if (metadata.count("0028|1054") == 1)//Reslope type - se tem reslope type vamos pegar o resto das infos
		{
			string strSlopeIntercept = metadata.at("0028|1052");
			string strSlopeRescale = metadata.at("0028|1053");
			boost::algorithm::trim(strSlopeIntercept);
			boost::algorithm::trim(strSlopeRescale);
			reslopeIntercept = boost::lexical_cast<short>(strSlopeIntercept);
			slopeRescale = boost::lexical_cast<double>(slopeRescale);
		}

		for (int i = 0; i < _data_sz; i++)
		{
			float val = _data->GetTuple(i)[0];
			if (reslopeIntercept)
			{
				val = val - reslopeIntercept - scalarOffset;
				convertedFromShortToChar[i] = val;
			}
			else
			{
				convertedFromShortToChar[i] = val;
			}
		}
		assert((img->GetExtent()[1] != -1) && "imagem est� valida?");
		array<double, 9> cosineX, cosineY, cosineZ;
		reslicedImage->GetResliceAxesDirectionCosines(cosineX.data(), cosineY.data(), cosineZ.data());
		array<double, 3> S, U, dimensions;
		S[0] = cosineX[0];
		S[1] = cosineX[1];
		S[2] = cosineX[2];
		U[0] = cosineY[0];
		U[1] = cosineY[1];
		U[2] = cosineY[2];
		array<double, 3> axesOrigin;
		reslicedImage->GetResliceAxesOrigin(axesOrigin.data());
		dicomData.sliceOrigin[0] = axesOrigin[0];
		dicomData.sliceOrigin[1] = axesOrigin[1];
		dicomData.sliceOrigin[2] = axesOrigin[2];
		dicomData.voxelSpacing[0] = reslicedImage->GetOutput()->GetSpacing()[0];
		dicomData.voxelSpacing[1] = reslicedImage->GetOutput()->GetSpacing()[1];
		dicomData.voxelSpacing[2] = reslicedImage->GetOutput()->GetSpacing()[2];
		dicomData.slabThickness = this->thickness;
		dicomData.directionCosines[0] = S[0];
		dicomData.directionCosines[1] = S[1];
		dicomData.directionCosines[2] = S[2];
		dicomData.directionCosines[3] = U[0];
		dicomData.directionCosines[4] = U[1];
		dicomData.directionCosines[5] = U[2];
		dicomData.voxelDimensions[0] = flipper->GetOutput()->GetDimensions()[0];
		dicomData.voxelDimensions[1] = flipper->GetOutput()->GetDimensions()[1];
		dicomData.voxelDimensions[2] = flipper->GetOutput()->GetDimensions()[2];
		short* srcBuffer = convertedFromShortToChar.data(); //<short*>(reslicedImage->GetOutput()->GetScalarPointer());
		size_t bufferSz = convertedFromShortToChar.size();//reslicedImage->GetOutput()->GetDimensions()[0] * reslicedImage->GetOutput()->GetDimensions()[1] * reslicedImage->GetOutput()->GetDimensions()[2];
		short *destBuffer = new short[bufferSz];
		memcpy(destBuffer, srcBuffer, bufferSz*sizeof(short));
		dicomData.bufferSize = bufferSz;
		dicomData.bufferData = destBuffer;
		/////salva a struct no disco para propositos de depuracao
#ifndef NDEBUG
		stringstream ssNome;
		ssNome << "c:\\debug_image_tela_" << idDaTelaEscolhidaParaTelaGrande << "_numero_" << debugId << ".txt";
		ofstream fout(ssNome.str());
		fout << "Arquivo = " << ss.str() << endl;
		fout << "Spacing = [" << dicomData.voxelSpacing[0] << ", " << dicomData.voxelSpacing[1] << ", " << dicomData.voxelSpacing[2] << "]" << endl;
		fout << "Center = [" << dicomData.sliceOrigin[0] << ", " << dicomData.sliceOrigin[1] << ", " << dicomData.sliceOrigin[2] << "]" << endl;
		fout << "Dimensions = [" << dicomData.voxelDimensions[0] << ", " << dicomData.voxelDimensions[1] << ", " << dicomData.voxelDimensions[2] << "]" << endl;
		fout << "Direction cosine 0 = [" << dicomData.directionCosines[0] << ", " << dicomData.directionCosines[1] << ", " << dicomData.directionCosines[2] << "]" << endl;
		fout << "Direction cosine 1 = [" << dicomData.directionCosines[3] << ", " << dicomData.directionCosines[4] << ", " << dicomData.directionCosines[5] << "]" << endl;
		fout.close();
		/////
		debugId++;
#endif
		return dicomData;
	}
	void Subsistema::GetPhysicalOrigin(float &x, float &y, float &z){
		assert(false && "nao implementado");
	}
	short Subsistema::GetScalarOffset(){
		return this->scalarOffset;

	}
	void Subsistema::GetSpacing(float &x, float &y, float &z){
		assert(false && "nao implementado");
	}


	void Subsistema::OnLMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::OnLMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::OnMMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::OnMMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::OnMouseMove(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::OnRMouseDown(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::OnRMouseUp(HWND wnd, UINT nFlags, int X, int Y, int qualPanel){
		assert(false && "nao implementado");
	}
	void Subsistema::Resize(int w, int h){
		assert(false && "nao implementado");
	}

	void Subsistema::SetCorDasLetras(float r, float  g, float  b){
		assert(false && "nao implementado");
	}

	void Subsistema::SetDragRect(int left, int top, int width, int height){
		for (shared_ptr<Tela> t : telas)
			t->SetDragRect(left, top, width, height);
	}
	void Subsistema::SetFontSize(float sz){
		assert(false && "nao implementado");
	}

	bool Subsistema::IsThisExam(char* idExame, char* idSerie){
		string e = idExame; string s = idSerie;
		if (this->idExame == e && this->idSerie == s)
			return true;
		else
			return false;
	}
	string Subsistema::GetIdSerie(){
		return idSerie;
	}
	string Subsistema::GetIdExame(){
		return idExame;
	}
	//0 = mip; 1 = minp; 2 = sum; 3 = mean
	void Subsistema::SetFuncao(int qual){
		for (shared_ptr<Tela> t : telas){
			t->SetFuncao(qual);
			t->Render();
		}
	}

	/*void Subsistema::SetLado(int tamanho){
		assert(false && "nao implementado");
	}*/

	void Subsistema::SetMouseInteraction(int idBtn, int idOperacao){
		assert(false && "nao implementado");
	}

	void Subsistema::SetOnDicomChange(TOnChangeDicom fn){
		assert(false && "nao implementado");
	}

	void Subsistema::SetOrigemTelaGrande(int qual){
		assert((qual >= 0) && (qual <= 2) && "valor coerente?");
		idDaTelaEscolhidaParaTelaGrande = qual;
	}



	void Subsistema::Translate(double x, double y, double z){
		assert(false && "nao implementado");
	}

}