#pragma once
#include <memory>
#include "mprLetraDeOrientacao.h"
#include <vtkRenderWindowInteractor.h>
#include <vtkImageResliceMapper.h>
#include <vtkImageProperty.h>
#include <vtkImageSlice.h>
#include <vtkResliceCursor.h>
#include <vtkResliceCursorWidget.h>
#include <vtkResliceCursorLineRepresentation.h>
#include <vtkLODProp3D.h>
#include "MPRUtils.h"
#include <vtkSmartPointer.h>
#include <vtkCallbackCommand.h>
#include <vtkCommand.h>
#include <vtkImageResliceToColors.h>
#include <vtkRenderWindow.h>
#include <vtkResliceCursorActor.h>
#include <vtkImageData.h>
#include <vtkResliceCursorPolyDataAlgorithm.h>
#include <vtkPNGWriter.h>

#include <vtkOpenGLRenderer.h>
#include <vtkRenderPass.h>
#include <vtkSequencePass.h>
#include <vtkCameraPass.h>
#include <vtkLightsPass.h>
#include <vtkDefaultPass.h>
#include <vtkRenderPassCollection.h>
#include <vtkRenderState.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyle.h>
#undef main
#include <SDL2/SDL.h>
#undef main

using namespace std;
namespace mpr{

	class MyRenderPass : public vtkRenderPass{
	private:


		SDL_Surface* canvasSdlSurface;
		SDL_Renderer* canvasSdlRenderer;
		vtkRenderWindow* storedWindowPointer;
		vtkSmartPointer<vtkCameraPass> cameraPass;
		vtkSmartPointer<vtkLightsPass> lightsPass;
		vtkSmartPointer<vtkDefaultPass> defaultPass;
		vtkSmartPointer<vtkRenderPassCollection> passes;
		vtkSmartPointer<vtkSequencePass> sequencePass;
		void SdlLine(int x0, int y0, int x1, int y1);
		void Rectangle(int x0, int y0, int x1, int y1);
		MyRenderPass();
		~MyRenderPass();
	public:
		array<int, 4> DragRect;
		static MyRenderPass* New(){
			return new MyRenderPass();
		}
		void Render(const vtkRenderState *s);
	};

	//class MyRenderWindowInteractorStyle :public vtkInteractorStyle {
	//public:
	//	static MyRenderWindowInteractorStyle* New(){
	//		return new MyRenderWindowInteractorStyle();
	//	}

	//};

	class Tela
	{
	private:
		double fixedCameraDistance;
		bool hasSetZoom;
		bool isFlipped;
		shared_ptr<Letras> letras;
		const int id;
		vtkSmartPointer<vtkInteractorStyle> style;
		vtkSmartPointer<vtkOpenGLRenderer> renderer;
		vtkSmartPointer<vtkRenderWindow> renderWindow;
		vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor;
		vtkSmartPointer<vtkImageResliceMapper> imageMapperHiRes, imageMapperLowRes;
		vtkSmartPointer<vtkImageProperty> imagePropertyHiRes, imagePropertyLowRes;
		vtkSmartPointer<vtkImageImport> importedImageHiRes, importedImageLowRes;
		vtkSmartPointer<vtkResliceCursor> sharedCursor;
		vtkSmartPointer<vtkResliceCursorWidget> resliceCursorWidget;
		vtkSmartPointer<vtkResliceCursorLineRepresentation> resliceCursorRepresentation;
		vtkSmartPointer<vtkLODProp3D> lodProp;
		int idLodHi, idLodLow;
		Qualidade currentQualidade;
		array<double, 3> center, xAxis, yAxis, zAxis;
		array<int, 4> dragRect;
		vtkSmartPointer<MyRenderPass> renderPass;

		void ApplyThingsNeededForImageBeFloat(vtkSmartPointer<vtkImageResliceToColors> reslicer);
	public:
		void SetDragRect(int left, int top, int width, int height);

		//0 = mip; 1 = minp; 2 = sum; 3 = mean
		void SetFuncao(int idFuncao);

		bool ContainsThisReslice(vtkResliceCursorWidget* w);


		vtkSmartPointer<vtkImageResliceToColors> GetCurrentReslice();


		array<int, 2> GetWL();

		void SetWL(array<int, 2> wl);

		void Render(){
			renderWindow->Render();
		}

		vtkSmartPointer<vtkResliceCursorWidget> GetResliceCursorWidget() { return resliceCursorWidget; }

		Tela(HWND handle, const int _id);

		void Resize(int w, int h);

		void SetImage(itk::Image<short, 3>::Pointer hiRes, vtkSmartPointer<vtkImageImport> lowResImporter);

		void SetThickness(int thickness);

		vtkSmartPointer<vtkImageImport> GetImportedImageHiRes(){ return importedImageHiRes; }

		void SetUp(vtkSmartPointer<vtkResliceCursor> cursor);
		//Slot para r eslicing - Quando mexe nas retas cai aqui.
		void operator()(vtkResliceCursorWidget* widget);

		void operator()(Qualidade q);

		//O slot de renderização
		void operator()(){
			renderWindow->Render();
		}
	};
}