#pragma once
#include <CameraRotationListener.h>

#include <vtkSmartPointer.h>
#include <vtkGeneralTransform.h>
#include <vtkTextActor.h>
#include <vtkRenderer.h>
#include <vtkPlane.h>
#include <string>
#include <vector>
#include <array>
#include <map>
#include <assert.h>

using namespace std;
namespace mpr
{

	class Letras :public tela::VolumeCameraRotationListener
	{
	private:
		//string EvaluateIntersection(vtkPlane* plano, double* origin, double* _newLeft, double* _newRight, double* _newTop, double* _newBottom, double* _newAnterior, double* _newPosterior);
		vtkSmartPointer<vtkTextActor> letraEsquerda, letraDireita, letraSuperior, letraInferior;
	public:
		string LetraCima, LetraBaixo, LetraDireita, LetraEsquerda;

		enum CantoDaTela{ LEFT, RIGHT, TOP, BOTTOM };

		Letras(vtkRenderer* targetRenderer);
		void Posicionar(vtkRenderer* targetRenderer);
		void PosicionarNoMPR(vtkRenderer* targetRenderer);
		void Execute(vtkSmartPointer<vtkCamera> cameraDoVolume, float offsetDoPlano = 0.5) override;
		void Calculate(std::array<double, 3> vec);
		void CalculateLetras(double* vec);
		void Desligar();
		void Ligar();
		void ExecuteCameraRotation() override{
			assert("nao implementado - existe por problemas nas interfaces" && false);
		};

		static map<CantoDaTela, string> Calculate(double *vec)
		{
			map<CantoDaTela, string> resultado;
			array<double, 3> normalizedAxis = { vec[0], vec[1], vec[2] };
			//Gera os direction cosines pra fazer a matrix.
			int majorAxis;
			std::array<double, 3> c0;
			std::array<double, 3> c1;
			if (abs(normalizedAxis[0]) > abs(normalizedAxis[1]) && abs(normalizedAxis[0]) > abs(normalizedAxis[2]))
				majorAxis = 0;
			if (abs(normalizedAxis[1]) > abs(normalizedAxis[0]) && abs(normalizedAxis[1]) > abs(normalizedAxis[2]))
				majorAxis = 1;
			if (abs(normalizedAxis[2]) > abs(normalizedAxis[0]) && abs(normalizedAxis[2]) > abs(normalizedAxis[1]))
				majorAxis = 2;
			if (majorAxis == 0)
			{
				c0 = { { 0, 1, 0 } };
				c1 = { { 0, 0, 1 } };
			}
			if (majorAxis == 1)
			{
				c0 = { { 1, 0, 0 } };
				c1 = { { 0, 0, 1 } };
			}
			if (majorAxis == 2)
			{
				c0 = { { 1, 0, 0 } };
				c1 = { { 0, 1, 0 } };
			}

			//Aqui eu tenho os vetores apropriados para as contas que ser�o feitas. Agora vou calcular os
			//novos vetores. c0' = c1 x axis; c1' = c0 x axis
			array<double, 3> c0new, c1new;
			vtkMath::Cross(c1.data(), normalizedAxis.data(), c0new.data());
			vtkMath::Cross(c0.data(), normalizedAxis.data(), c1new.data());
			vtkMath::Normalize(c0new.data());
			vtkMath::Normalize(c1new.data());
			//Com os direction cosines transformados via produtos vetoriais talvez eu j� possa fazer o teste
			//O c0 � o da horizontal da tela e o c1 � o da vertical da tela, enquanto axis � o de dentro da tela.
			//Na vers�o original do m�todo eu rotacionava uma s�rie de vetores basicos usando o quaternion
			//da orienta��o da c�mera.
			const double origin[] = { 0, 0, 0 }; // A origem do sistema|
			const float tam = 0.5;
			{
				vtkSmartPointer<vtkPlane> planoEsquerdo = vtkSmartPointer<vtkPlane>::New();
				planoEsquerdo->SetOrigin(-tam, 0, 0);
				planoEsquerdo->SetNormal(1, 0, 0);
				double p;
				double x[3];
				std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
				int iL = planoEsquerdo->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
				int iR = planoEsquerdo->IntersectWithLine((double*)origin, c0new.data(), p, x);
				//int iT = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
				//int iB = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
				//int iA = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
				//int iP = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
				//v� o que est� acontecendo
				string letraLadoEsquerdo = "";
				if (iL) letraLadoEsquerdo = letraLadoEsquerdo + "L";
				if (iR) letraLadoEsquerdo = letraLadoEsquerdo + "R";
				//if (iT) letraLadoEsquerdo = letraLadoEsquerdo + "S";
				//if (iB) letraLadoEsquerdo = letraLadoEsquerdo + "I";
				//if (iA) letraLadoEsquerdo = letraLadoEsquerdo + "A";
				//if (iP) letraLadoEsquerdo = letraLadoEsquerdo + "P";
				//cout << "Lado esquerdo = " << letraLadoEsquerdo << endl;
				resultado[LEFT] = letraLadoEsquerdo;
			}
		{
			vtkSmartPointer<vtkPlane> planoDireito = vtkSmartPointer<vtkPlane>::New();
			planoDireito->SetOrigin(tam, 0, 0);
			planoDireito->SetNormal(-1, 0, 0);
			double p;
			double x[3];
			std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
			int iL = planoDireito->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
			int iR = planoDireito->IntersectWithLine((double*)origin, c0new.data(), p, x);
			//int iT = planoDireito->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
			//int iB = planoDireito->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
			//int iA = planoDireito->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
			//int iP = planoDireito->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
			////v� o que est� acontecendo
			string letra = "";
			if (iL) letra = letra + "L";
			if (iR) letra = letra + "R";
			//if (iT) letra = letra + "S";
			//if (iB) letra = letra + "I";
			//if (iA) letra = letra + "A";
			//if (iP) letra = letra + "P";
			//cout << "Lado direito = " << letra << endl;
			resultado[RIGHT] = letra;
		}
		{
			vtkSmartPointer<vtkPlane> planoSuperior = vtkSmartPointer<vtkPlane>::New();
			planoSuperior->SetOrigin(0, tam, 0);
			planoSuperior->SetNormal(0, -1, 0);
			double p;
			double x[3];
			std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
			int iL = planoSuperior->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
			int iR = planoSuperior->IntersectWithLine((double*)origin, c0new.data(), p, x);
			//int iT = planoSuperior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
			//int iB = planoSuperior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
			//int iA = planoSuperior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
			//int iP = planoSuperior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
			////v� o que est� acontecendo
			string letra = "";
			if (iL) letra = letra + "L";
			if (iR) letra = letra + "R";
			//if (iT) letra = letra + "S";
			//if (iB) letra = letra + "I";
			//if (iA) letra = letra + "A";
			//if (iP) letra = letra + "P";
			//cout << "Lado SUPERIOR = " << letra << endl;
			resultado[TOP] = letra;
		}
		{
			vtkSmartPointer<vtkPlane> planoInferior = vtkSmartPointer<vtkPlane>::New();
			planoInferior->SetOrigin(0, -tam, 0);
			planoInferior->SetNormal(0, 1, 0);
			double p;
			double x[3];
			std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
			int iL = planoInferior->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
			int iR = planoInferior->IntersectWithLine((double*)origin, c0new.data(), p, x);
			//int iT = planoInferior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
			//int iB = planoInferior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
			//int iA = planoInferior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
			//int iP = planoInferior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
			////v� o que est� acontecendo
			string letra = "";
			if (iL) letra = letra + "L";
			if (iR) letra = letra + "R";
			//if (iT) letra = letra + "S";
			//if (iB) letra = letra + "I";
			//if (iA) letra = letra + "A";
			//if (iP) letra = letra + "P";
			//cout << "Lado inferior = " << letra << endl;
			resultado[BOTTOM] = letra;
		}
		return resultado;
		}
	};
}