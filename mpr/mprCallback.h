#pragma once
#include <boost\signals2.hpp>
#include <vtkSmartPointer.h>
#include <vtkCommand.h>
#include <string>
#include <array>
#include "MPRutils.h"
#include <vtkResliceCursorWidget.h>
#include <vtkResliceCursorRepresentation.h>
#include <vtkResliceCursor.h>
#include <iostream>
#include <boost/core/ref.hpp>
using namespace std;
namespace mpr{
	//A troca de qualidade s� deve ocorrer se realmente estiver fazendo alguma opera��o.
	class myMPRCallback : public vtkCommand
	{
	private:
		array<double, 3> xAxis, yAxis, zAxis;
		bool isUsingLowRes;
		bool areAxesSet;
		boost::signals2::signal<void(Qualidade)> sinalChangeQuality;
		boost::signals2::signal<void()> sinalRender;
		boost::signals2::signal<void(vtkResliceCursorWidget* widget)> sinalReslice;
		myMPRCallback();
		void PrintVector(string txt, array<double, 3> vec);
	public:
		static myMPRCallback *New(){ return new myMPRCallback(); }
		template <typename T> void AddRenderSignal(T s)
		{
			sinalRender.connect(boost::ref(*s));
		}
		template <typename T> void AddQualitySignal(T s)
		{
			sinalChangeQuality.connect(boost::ref(*s));
		}
		template <typename T> void AddResliceSignal(T s)
		{
			sinalReslice.connect(boost::ref(*s));
		}
		void Execute(vtkObject *caller, unsigned long ev, void *callData);
	};
}