#pragma once
#include <string>
#include <array>
#include <itktypes.h>
using namespace std;
typedef void(__stdcall *TOnChangeDicom)(int qualTela,long handleDoSubsistema, MPRV2_DicomData *dicomData);

enum Qualidade{ LOW_RES, HI_RES };

namespace mpr
{
	class IMPRMaster
	{
	public:
		virtual void SetQualidadeToLow() = 0;
		virtual void SetQualidadeToHigh() = 0;
		virtual int GetTelaApontadaPelaTelaGrande() = 0;
		virtual void Rotate(int idTela, double anguloEmGraus) = 0;
		virtual void SetVectors(array<double, 3> v0, array<double,3> v1, array<double,3> v2, int idTela) = 0;
		virtual void Translate(double x, double y, double z) = 0;
		virtual void GetCurrentWL(array<int, 2>& wl) = 0;
		virtual void SetCurrentWL(array<int, 2> wl) = 0;
		virtual void InformaMousePos(array<int, 2> pos) = 0;
		virtual array<double, 3> GetCurrentPlaneNormal() = 0;
		virtual array<double, 3> GetCursorPosition() = 0;

		virtual void SaveParalelScale(double v, int qualTela) = 0;
		virtual int GetIdDaTelaAtual() = 0;

		array<double, 3> staticX;
		array<double, 3> staticY;
		array<double, 3> staticZ;

		IMPRMaster()
		{
			staticX = { { 0, 0, 0 } };
			staticY = { { 0, 0, 0 } };
			staticZ = { { 0, 0, 0 } };
		}
	};
}
