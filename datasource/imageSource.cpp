#include "imageSource.h"
#include <itkMetaDataObject.h>
#include <itkImageSeriesReader.h>
#include <itkGDCMImageIO.h>
#include <itkGDCMSeriesFileNames.h>
#include <map>
#include "cargaDoVolume.h"
#include <itkImageFileWriter.h>
#include <array>
#include <UtilitiesV2.h>
#include <itkOrientImageFilter.h>
#include <itkImportImageFilter.h>
#include <array>
#include <boost/algorithm/string.hpp>


datasource::ImageSource::ImageSource(callbackDeProgressDaCarga callbackExternoDeCarga)
{
	SetCallbackDeProgesso(callbackExternoDeCarga);
}

void datasource::ImageSource::SetCallbackDeProgesso(callbackDeProgressDaCarga cbk)
{
	//Inicializa o callback de carga
	void *ptr = cbk;
	if (ptr)
	{
		handleDoCallbackDeCarga = CallbackProgressoDeCarga::New();
		handleDoCallbackDeCarga->SetCallbackDoDelphi(ptr);
	}
	else
	{
		handleDoCallbackDeCarga = nullptr;
	}
}

void datasource::ImageSource::PushFile(string f)
{
	filePaths.push_back(f);
}

void datasource::ImageSource::ClearList()
{
	filePaths.clear();
}

void Desenhar3Planos(itk::Image<short, 3>::Pointer ptrImg)
{
	//Gmabiarra para testar o alinhamento
	const int sizeX = ptrImg->GetLargestPossibleRegion().GetSize()[0];
	const int sizeY = ptrImg->GetLargestPossibleRegion().GetSize()[1];
	const int sizeZ = ptrImg->GetLargestPossibleRegion().GetSize()[2];
	//Desenhando 3 planos no volume//
	//Plano chapado na tela 2 (coronal)
	for (int x = 0; x < sizeX; x++){
		for (int z = 0; z < sizeZ; z++){
			itk::Image<short, 3>::IndexType index;
			index[0] = x;
			index[1] = sizeY / 3 * 2 - 2;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = sizeY / 3 * 2 - 1;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = sizeY / 3 * 2 - 0;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = sizeY / 3 * 2 + 1;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = sizeY / 3 * 2 + 2;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
		}
	}
	//Chapado na tela 3 - Sagital
	for (int y = 0; y < sizeY; y++){
		for (int z = 0; z < sizeZ; z++){
			itk::Image<short, 3>::IndexType index;
			index[0] = sizeX / 3 * 2 - 2;
			index[1] = y;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = sizeX / 3 * 2 - 1;
			index[1] = y;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = sizeX / 3 * 2 - 0;
			index[1] = y;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = sizeX / 3 * 2 + 1;
			index[1] = y;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
			index[0] = sizeX / 3 * 2 + 2;
			index[1] = y;
			index[2] = z;
			ptrImg->SetPixel(index, 2000);
		}
	}
	//Chapado no axial
	for (int x = 0; x < sizeX; x++){
		for (int y = 0; y < sizeY; y++){
			itk::Image<short, 3>::IndexType index;
			index[0] = x;
			index[1] = y;
			index[2] = sizeZ / 3 * 2 - 2;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = y;
			index[2] = sizeZ / 3 * 2 - 1;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = y;
			index[2] = sizeZ / 3 * 2 - 0;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = y;
			index[2] = sizeZ / 3 * 2 + 1;
			ptrImg->SetPixel(index, 2000);
			index[0] = x;
			index[1] = y;
			index[2] = sizeZ / 3 * 2 + 2;
			ptrImg->SetPixel(index, 2000);
		}
	}
}

void DesenharVariosSagitais(itk::Image<short, 3>::Pointer ptrImg)
{
	//Gmabiarra para testar o alinhamento
	const int sizeX = ptrImg->GetLargestPossibleRegion().GetSize()[0];
	const int sizeY = ptrImg->GetLargestPossibleRegion().GetSize()[1];
	const int sizeZ = ptrImg->GetLargestPossibleRegion().GetSize()[2];
	const int step = 20;
	for (auto ln = 0; ln < 10; ln++)
	{
		for (int y = 0; y < sizeY - (ln * 10); y++){
			for (int z = 0; z < sizeZ - (ln * 10); z++){
				itk::Image<short, 3>::IndexType index;
				index[0] = sizeX / 2 - 2 + (step * ln);
				index[1] = y;
				index[2] = z;
				ptrImg->SetPixel(index, 2000);
				index[0] = sizeX / 2 - 1 + (step * ln);
				index[1] = y;
				index[2] = z;
				ptrImg->SetPixel(index, 2000);
				index[0] = sizeX / 2 - 0 + (step * ln);
				index[1] = y;
				index[2] = z;
				ptrImg->SetPixel(index, 2000);
				index[0] = sizeX / 2 + 1 + (step * ln);
				index[1] = y;
				index[2] = z;
				ptrImg->SetPixel(index, 2000);
				index[0] = sizeX / 2 + 2 + (step * ln);
				index[1] = y;
				index[2] = z;
				ptrImg->SetPixel(index, 2000);
			}
		}
	}

}

shared_ptr<datasource::LoadedImage>  datasource::ImageSource::Load(string idExame, string idSerie)
{
	assert("TEM FATIAS???"&&filePaths.size() > 0);
	map<string, string> metadata;
	codigoDeErro = SEM_ERRO_DE_CARGA;
	typedef itk::MetaDataDictionary DictionaryType;
	typedef itk::MetaDataObject< std::string > MetaDataStringType;
	typedef short    PixelType;
	const unsigned int      Dimension = 3;
	typedef itk::Image< PixelType, Dimension >         ImageType;
	typedef itk::ImageSeriesReader< ImageType >        ReaderType;
	ReaderType::Pointer reader = ReaderType::New();
	typedef itk::GDCMImageIO       ImageIOType;
	ImageIOType::Pointer dicomIO = ImageIOType::New();
	reader->SetImageIO(dicomIO);
	typedef itk::GDCMSeriesFileNames NamesGeneratorType;
	NamesGeneratorType::Pointer nameGenerator = NamesGeneratorType::New();
	//antes de sair abrindo tudo e tomar exce��o se algo n�o for encontrado testar para cada arquivo fornecido
	////se ele existe. Se n�o existe, tira da lista
	vector<int> indices_zuados;
	for (unsigned int i = 0; i < filePaths.size(); i++)
	{
		string path = filePaths[i];
		WIN32_FIND_DATA FindFileData;
		HANDLE handle = FindFirstFileA(path.c_str(), &FindFileData);
		int found = handle != INVALID_HANDLE_VALUE;
		if (found)
		{
			FindClose(handle);
		}
		else
		{
			indices_zuados.push_back(i);
		}
	}
	if (indices_zuados.size() > 0)
	{
		codigoDeErro = ERROR_CODE_INDICES_ZUADOS;
		return nullptr;
	}

	for (unsigned int i = 0; i < indices_zuados.size(); i++)
	{
		filePaths.erase(filePaths.begin() + indices_zuados[i]);
	}
	//Pega o tamanho em bytes da s�rie a partir das propriedades da 1a fatia. Esse tamanho ser� usado para ver
	//se a s�rie caber� na mem�ria. Como o sistema est� limitado a 4gb devido a ser 32 bits e uma s�rie sendo
	//renderizada na tela ocupa 2.7 seu tamanho original o sistema se recusar� a cerregar algo maior que 900mb
	ReaderType::Pointer singleFileReader = ReaderType::New();
	singleFileReader->SetFileName(filePaths[0]);
	singleFileReader->Update();
	//const DictionaryType& singleImageDictionary = singleFileReader->GetMetaDataDictionary();
	ImageType::Pointer singleImage = singleFileReader->GetOutput();
	int x = singleImage->GetLargestPossibleRegion().GetSize()[0];
	int y = singleImage->GetLargestPossibleRegion().GetSize()[1];
	long sizeInBytes = x * y * filePaths.size() * sizeof(short);
	const long maxMemSizeInBytes = 900 * 1024 * 1024;
	if (sizeInBytes > maxMemSizeInBytes)
	{
		codigoDeErro = ERROR_CODE_OOM;
		return nullptr;
	}
	//L� a s�rie
	reader->SetFileNames(filePaths);
	try
	{
		//So devo adicionar um listener de progresso se tenho listener e s� tenho listener
		//se um callback para a fun��o que trata o progresso for imformada.
		if (handleDoCallbackDeCarga)
		{
			reader->AddObserver(itk::ProgressEvent(), handleDoCallbackDeCarga);
		}
		reader->Update();
	}
	catch (itk::ExceptionObject& ex)
	{
		string erro = ex.GetDescription();
		if (erro == "Cannot read requested file")
		{
			codigoDeErro = ERROR_FALHA_NA_LEITURA;
			return nullptr;
		}
	}
	//ptrImg � o que ser� retornado.
	itk::Image<short, 3>::Pointer ptrImg = reader->GetOutput();
	const DictionaryType& dictionary = dicomIO->GetMetaDataDictionary();
	DictionaryType::ConstIterator metadataDictionaryIterator = dictionary.Begin();
	DictionaryType::ConstIterator metadataDictionaryEnd = dictionary.End();
	std::cout << "tags dicom" << std::endl;
	typedef itk::MetaDataObject< std::string > MetaDataStringType;

	while (metadataDictionaryIterator != metadataDictionaryEnd)
	{
		itk::MetaDataObjectBase::Pointer entry = metadataDictionaryIterator->second;
		MetaDataStringType::Pointer entryValue = dynamic_cast<MetaDataStringType*>(entry.GetPointer());
		if (entryValue)
		{
			string tagkey = metadataDictionaryIterator->first;
			string labelId;
			string tagvalue = entryValue->GetMetaDataObjectValue();
			std::cout << tagkey << " = " << tagvalue << endl;
			metadata.insert(make_pair(tagkey, tagvalue));
		}
		++metadataDictionaryIterator;
	}
	//DesenharVariosSagitais(ptrImg);
	double gantryTilt = carga::GetGantryTilt(metadata);
	cout << "Tem gantry tilt = " << gantryTilt << endl;

	//0018|5100 = FFS
	std::string sOrientation = metadata.at("0018|5100");
	boost::trim(sOrientation);
	//ffs - feet first supine - RAI?
	//hfs - head first supine - RAS?


	////Reorienta a imagem
	//itk::OrientImageFilter<itk::Image<short, 3>, itk::Image<short, 3>>::Pointer orienter = itk::OrientImageFilter<itk::Image<short, 3>, itk::Image<short, 3>>::New();
	//orienter->UseImageDirectionOn();
	////////Consultar https://itk.org/Doxygen/html/namespaceitk_1_1SpatialOrientation.html#a8240a59ae2e7cae9e3bad5a52ea3496eaa5d3197482c4335a63a1ad99d6a9edee
	////////para a lista de orienta��es
	//if (sOrientation == "FFS")
	//	orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAS);
	//else if (sOrientation == "HFS")
	//	orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAS);
	//orienter->SetInput(ptrImg);
	//orienter->Update();
	//ptrImg = orienter->GetOutput();	  //Aqui t� ok pro MORENO
	return make_shared<LoadedImage>(ptrImg, metadata, idExame, idSerie);
}
