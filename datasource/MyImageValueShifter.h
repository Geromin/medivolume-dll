#ifndef __my_image_value_shifter_h
#define __my_image_value_shifter_h
#include <itkInPlaceImageFilter.h>
#include <itkSmartPointer.h>
#include <thread>
#include <vector>
#include <algorithm>

using namespace std;

namespace vr
{
	template <class TImage> class MyValueShifter:public itk::InPlaceImageFilter<TImage>
	{
	private:
		short Offset;
	public:
		typedef MyValueShifter Self;
		typedef itk::InPlaceImageFilter<TImage> Superclass;
		typedef itk::SmartPointer<Self> Pointer;
		itkNewMacro(Self);
		itkTypeMacro(MyValueShifter, InPlaceImageFilter);
		short GetOffset()
		{
			return Offset;
		}
	protected:
		MyValueShifter()
		{
			Offset = SHRT_MIN;

		}
		~MyValueShifter()
		{

		}
		virtual void GenerateData()override
		{
			//preambulo
			long t0 = GetTickCount();
			typename TImage::ConstPointer input = this->GetInput();
			typename TImage::Pointer output = this->GetOutput();
			this->AllocateOutputs();
			if (!GetRunningInPlace())
				assert(false);//TEM QUE SER INPLACE!!!!!!
			this->UpdateProgress(0.0);
			unsigned int nthreads = thread::hardware_concurrency();
			//1o percurso - saber qual � o m�nimo
			const int szX = output->GetLargestPossibleRegion().GetSize()[0];
			const int szY = output->GetLargestPossibleRegion().GetSize()[1];
			const int szZ = output->GetLargestPossibleRegion().GetSize()[2];
			short* minimosLocais = new short[nthreads];
			for (unsigned int i = 0; i < nthreads; i++)
			{
				minimosLocais[i] = 0;
			}
			const int numeroDeElementos = output->GetLargestPossibleRegion().GetNumberOfPixels();
			const int szParticao = numeroDeElementos / nthreads;
			vector<thread> workers1;
			short* ptr = output->GetBufferPointer();
			for (unsigned int i = 0; i < nthreads; i++)
			{
				const int inicio = szParticao * i;
				int fim = inicio + szParticao;
				if (i == nthreads - 1)
					fim = fim + szParticao%nthreads;
				const int qualBucket = i;
				//preciso da regi�o do espa�o e do balde pra guardar o m�nimo local
				workers1.push_back(thread([inicio, fim, qualBucket, minimosLocais, ptr]()
				{
					for (int p = inicio; p < fim; p++)
					{
						if (ptr[p] < minimosLocais[qualBucket] && ptr[p] < 0)
							minimosLocais[qualBucket] = ptr[p];
					}
				}));
			}
			for_each(workers1.begin(), workers1.end(), [](std::thread& t)
			{
				t.join();
			});
			short minimoGlobal = 0;
			for (unsigned int i = 0; i < nthreads; i++)
			{
				if (minimoGlobal > minimosLocais[i])
				{
					minimoGlobal = minimosLocais[i];
				}
			}
			minimoGlobal = abs(minimoGlobal);
			//2o percurso - deslocar os valores.
			vector<thread> workers2;
			for (unsigned int i = 0; i < nthreads; i++)
			{
				const int inicio = szParticao * i;
				int fim = inicio + szParticao;
				if (i == nthreads - 1)
					fim = fim + szParticao%nthreads;
				workers2.push_back(thread([inicio, fim, ptr, minimoGlobal]()
				{
					for (int p = inicio; p < fim; p++)
					{
						ptr[p] = ptr[p] + minimoGlobal;
					}
				}));
			}
			for_each(workers2.begin(), workers2.end(), [](std::thread& t)
			{
				t.join();
			});
			//ep�logo
			this->Offset = minimoGlobal;
			delete[] minimosLocais;
			long t = GetTickCount();

		}

	};
}
#endif