#pragma warning(disable:4996)
#pragma warning(disable:4049)
#include "cargaDoVolume.h"
#include <itkMetaDataObject.h>
#include <itkImageSeriesReader.h>
#include <itkGDCMImageIO.h>
#include <itkGDCMSeriesFileNames.h>
#include <itkResampleImageFilter.h>
#include <itkAffineTransform.h>
#include "itktypes.h"

using namespace std;
namespace carga
{
	//Carrega a imagem a partir da lista de arquivos. A imagem � retornada pela fun��o. a metadata sai pelo parametro de saida
	 itk::Image<short, 3>::Pointer LoadImageFromFileList(std::vector<std::string> filelist,
		std::map<std::string, std::string> *metadata,
		callbackDeProgressDaCarga fnDeCallbackDaCarga)
	{
		errorCode = SEM_ERRO_DE_CARGA;
		typedef itk::MetaDataDictionary DictionaryType;
		typedef itk::MetaDataObject< std::string > MetaDataStringType;
		typedef short    PixelType;
		const unsigned int      Dimension = 3;
		typedef itk::Image< PixelType, Dimension >         ImageType;
		typedef itk::ImageSeriesReader< ImageType >        ReaderType;
		ReaderType::Pointer reader = ReaderType::New();
		typedef itk::GDCMImageIO       ImageIOType;
		ImageIOType::Pointer dicomIO = ImageIOType::New();
		reader->SetImageIO(dicomIO);
		typedef itk::GDCMSeriesFileNames NamesGeneratorType;
		NamesGeneratorType::Pointer nameGenerator = NamesGeneratorType::New();
		//antes de sair abrindo tudo e tomar exce��o se algo n�o for encontrado testar para cada arquivo fornecido
		////se ele existe. Se n�o existe, tira da lista
		vector<int> indices_zuados;
		for (unsigned int i = 0; i < filelist.size(); i++)
		{
			string path = filelist[i];
			WIN32_FIND_DATA FindFileData;
			HANDLE handle = FindFirstFileA(path.c_str(), &FindFileData);
			int found = handle != INVALID_HANDLE_VALUE;
			if (found)
			{
				FindClose(handle);
			}
			else
			{
				indices_zuados.push_back(i);
			}
		}
		if (indices_zuados.size() > 0)
		{
			errorCode = ERROR_CODE_INDICES_ZUADOS;
			return nullptr;
		}

		for (unsigned int i = 0; i < indices_zuados.size(); i++)
		{
			filelist.erase(filelist.begin() + indices_zuados[i]);
		}
		//Pega o tamanho em bytes da s�rie a partir das propriedades da 1a fatia. Esse tamanho ser� usado para ver
		//se a s�rie caber� na mem�ria. Como o sistema est� limitado a 4gb devido a ser 32 bits e uma s�rie sendo
		//renderizada na tela ocupa 2.7 seu tamanho original o sistema se recusar� a cerregar algo maior que 900mb
		ReaderType::Pointer singleFileReader = ReaderType::New();
		singleFileReader->SetFileName(filelist[0]);
		singleFileReader->Update();
		//const DictionaryType& singleImageDictionary = singleFileReader->GetMetaDataDictionary();
		ImageType::Pointer singleImage = singleFileReader->GetOutput();
		int x = singleImage->GetLargestPossibleRegion().GetSize()[0];
		int y = singleImage->GetLargestPossibleRegion().GetSize()[1];
		long sizeInBytes = x * y * filelist.size() * sizeof(short);
		const long maxMemSizeInBytes = 900 * 1024 * 1024;
		if (sizeInBytes > maxMemSizeInBytes)
		{
			errorCode = ERROR_CODE_OOM;
			return nullptr;
		}
		//L� a s�rie
		reader->SetFileNames(filelist);
		try
		{
			CallbackProgressoDeCarga::Pointer cb = CallbackProgressoDeCarga::New();
			void* ptr = fnDeCallbackDaCarga;
			if (ptr){
				cb->SetCallbackDoDelphi(ptr);
				reader->AddObserver(itk::ProgressEvent(), cb);
			}
			reader->Update();
		}
		catch (itk::ExceptionObject& ex)
		{
			string erro = ex.GetDescription();
			if (erro == "Cannot read requested file")
			{
				errorCode = ERROR_FALHA_NA_LEITURA;
				return nullptr;
			}
		}
		//ptrImg � o que ser� retornado.
		itk::Image<short, 3>::Pointer ptrImg = reader->GetOutput();
		const DictionaryType& dictionary = dicomIO->GetMetaDataDictionary();
		DictionaryType::ConstIterator metadataDictionaryIterator = dictionary.Begin();
		DictionaryType::ConstIterator metadataDictionaryEnd = dictionary.End();
		std::cout << "tags dicom" << std::endl;
		typedef itk::MetaDataObject< std::string > MetaDataStringType;

		while (metadataDictionaryIterator != metadataDictionaryEnd)
		{
			itk::MetaDataObjectBase::Pointer entry = metadataDictionaryIterator->second;
			MetaDataStringType::Pointer entryValue = dynamic_cast<MetaDataStringType*>(entry.GetPointer());
			if (entryValue)
			{
				string tagkey = metadataDictionaryIterator->first;
				string labelId;
				string tagvalue = entryValue->GetMetaDataObjectValue();
				std::cout << tagkey << " = " << tagvalue << endl;
				metadata->insert(make_pair(tagkey, tagvalue));
			}
			++metadataDictionaryIterator;
		}
		return ptrImg;
	}
	//Retorna o gantry tilt, a partir das informa��es da metadata.
	double GetGantryTilt(std::map<std::string, std::string> metadata)
	{
		//H� casos onde n�o h� metadata. retornar nesses casos com zero.
		if (metadata.size() == 0)
			return 0.0;
		int i = metadata.count("0018|1120");
		if (i != 1)
			return 0.0;
		else
		{
			string _gt = metadata.at("0018|1120");
			return stof(_gt);
		}
	}
	//Corrige o gantry tilt da imagem, retornando a vers�o modificada.
	void CorrectGantryTilt(itk::Image<short, 3>::Pointer source, std::map<std::string, std::string> metadata)
	{
		//Pega o angulo;
		const double tilt = GetGantryTilt(metadata);
		if (tilt <= 0.05)
			return;
		//O filtro n�o � inplace - isso vai gerar uma inevit�vel duplicacao de mem�ria. Os dados da sa�da ser�o copiados pra Source via
		//minha fun��o de deep copy.
		typedef itk::ResampleImageFilter<itk::Image<short, 3>, itk::Image<short, 3>> FilterType;
		FilterType::Pointer filter = FilterType::New();
		typedef itk::AffineTransform<double, 3> TransformType;
		TransformType::Pointer transform = TransformType::New();
		typedef itk::LinearInterpolateImageFunction<itk::Image<short, 3>, double> InterpolatorType;
		InterpolatorType::Pointer interpolator = InterpolatorType::New();
		filter->SetInterpolator(interpolator);
		filter->SetDefaultPixelValue(-3000);
		//--Os parametros da saida sao obtidos da entrada
		const itk::Image<short, 3>::SpacingType &spacing = source->GetSpacing();
		const itk::Image<short, 3>::PointType  &origin = source->GetOrigin();
		itk::Image<short, 3>::SizeType size = source->GetLargestPossibleRegion().GetSize();
		filter->SetOutputOrigin(origin);
		filter->SetOutputSpacing(spacing);
		filter->SetOutputDirection(source->GetDirection());
		filter->SetSize(size);
		filter->SetInput(source);

		////Teste: faz uma rotacao 3d
		//TransformType::OutputVectorType rotation01;
		//rotation01[0] = 0;
		//rotation01[1] = 1;
		//rotation01[2] = 0;
		//transform->Rotate3D(rotation01, 0.78539816);
		//filter->SetTransform(transform);
		transform->Shear(1, 2, sin(18 * 0.01745329));
		filter->SetTransform(transform);
		//Executa o filtro.
		filter->Update();
		//Agora eu tenho que copiar o resultado pra s�rie que serviu de input
		itk::Image<short, 3>::Pointer _inp = filter->GetOutput();
		DeepCopy<itk::Image<short, 3>>(_inp, source);
		//return 0;
	}
}