#include "loadedImage.h"
#include <itkOrientImageFilter.h>
#include "MyImageValueShifter.h"
#include  <itktypes.h>
#include <itkOrientImageFilter.h>

#include <boost/exception/all.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <itkMinimumMaximumImageCalculator.h>
datasource::LoadedImage::LoadedImage(ImageType::Pointer _image, map<string, string> metadata,  string idExame, string idSerie)
{
	this->idExame = idExame;
	this->idSerie = idSerie;
	this->image = _image;
	this->metadataDictionary = metadata;
	this->rawDictionary = rawDictionary;

	long t0 = GetCurrentTime();
	typedef itk::MinimumMaximumImageCalculator <ImageType> ImageCalculatorFilterType;
	ImageCalculatorFilterType::Pointer imageCalculatorFilter = ImageCalculatorFilterType::New();
	imageCalculatorFilter->SetImage(_image );
	imageCalculatorFilter->ComputeMinimum();
	short min = imageCalculatorFilter->GetMinimum();
	valueOffset = 0;//(-1)*min;
	image = _image;
	long t1 = GetCurrentTime();
	cout << __FUNCTION__ << " gastou " << (t1 - t0) << " ms" << endl;

	typedef vr::MyValueShifter<ImageType> TShifter;
	/////n�o preciso mais do importer - faz o shift
	TShifter::Pointer valueShifter = vr::MyValueShifter<ImageType>::New();
	valueShifter->SetInput(image);
	valueShifter->Modified();
	valueShifter->Update();	///T� travando aqui;
	valueOffset = (valueShifter)->GetOffset();
	image = valueShifter->GetOutput();
}
