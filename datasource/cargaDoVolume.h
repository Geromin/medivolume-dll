#define ITKV3_COMPATIBILITY//Necess�rio pq as classes foram escritas para uma vers�o antiga da biblioteca
#ifndef __cargaDoVolume_h
#define __cargaDoVolume_h
#include <itkImage.h>
#include <string>
#include <vector>
#include <map>
#include "CallbackProgressoDeCarga.h"
#include <fstream>
namespace carga
{
	const int SEM_ERRO_DE_CARGA = 0;
	const int ERROR_CODE_INDICES_ZUADOS = 10;
	const int ERROR_CODE_OOM = 2;
	const int ERROR_FALHA_NA_LEITURA = 1;
	static int errorCode;
	//Carrega a imagem a partir da lista de arquivos. A imagem � retornada pela fun��o. a metadata sai pelo parametro de saida
	itk::Image<short, 3>::Pointer LoadImageFromFileList(std::vector<std::string> filelist, 
											   std::map<std::string, std::string> *metadata,
											   callbackDeProgressDaCarga fnDeCallbackDaCarga
											   );
	//Retorna o gantry tilt, a partir das informa��es da metadata. A tag 0018,1120 � lida e seu angulo retornado.
	double GetGantryTilt(std::map<std::string, std::string> metadata);
	//Corrige o gantry tilt da imagem, retornando a vers�o modificada. 
	void CorrectGantryTilt(itk::Image<short, 3>::Pointer source, std::map<std::string, std::string> metadata);

	static std::vector<std::string> GetListFromFile(std::string f)
	{
		std::vector<std::string> result;
		std::ifstream infile;
		infile.open(f);
		std::string ln;
		while (std::getline(infile, ln))
		{
			result.push_back(ln);
		}
		infile.close();
		return result;
	}
}
#endif