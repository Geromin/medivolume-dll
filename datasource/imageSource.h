#pragma warning(disable:4996)
#pragma warning(disable:4049)
#ifndef __image_source_h
#define __image_source_h
#include <vector>
#include <string>
#include "loadedImage.h"
#include <itktypes.h>
#include <CallbackProgressoDeCarga.h>
#include <itkImportImageFilter.h>
#include <array>
#include <vector>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>
#include <memory>
#include <algorithm>
#include <itkImageFileWriter.h>

#include <itkImportImageFilter.h>
#include <itkImageFileWriter.h>
#include <itkTileImageFilter.h>
#include <boost/lexical_cast.hpp>
using namespace std;
namespace datasource
{
	class ImageSlice{
	public:
		string idExame;
		string idSerie;
		vector<MPRV2_DicomData> dicomData;
		vector<short> buffer3d;
		itk::ImportImageFilter<short, 3>::Pointer image3d;

		ImageSlice(){
			image3d = nullptr;
		}

		itk::Image<short, 3>::Pointer GetAssembledImage(){
			if (image3d == nullptr)
			{
				//1) preenche o buffer3d
				const auto singleImageNumberOfPixels = dicomData[0].voxelDimensions[0] * dicomData[0].voxelDimensions[1];
				buffer3d = vector<short>(singleImageNumberOfPixels * dicomData.size());
				//2) determina as propriedades do itkImageImport
				//para cada fatia, copia-la para o buffer
				short* currentBuffer3dPos = buffer3d.data();
				for (size_t i = 0; i < dicomData.size(); i++){
					memcpy(currentBuffer3dPos, dicomData[i].bufferData, dicomData[i].bufferSize);
					currentBuffer3dPos = currentBuffer3dPos + singleImageNumberOfPixels;
				}
				typedef itk::Image<short, 3> ImgType;
				typedef itk::ImportImageFilter<short, 3> ImporterType;
				image3d = itk::ImportImageFilter<short, 3>::New();
				ImporterType::SizeType size;
				size[0] = dicomData[0].voxelDimensions[0];
				size[1] = dicomData[0].voxelDimensions[1];
				size[2] = dicomData.size();
				ImporterType::IndexType start;
				start.Fill(0);
				ImporterType::RegionType region;
				region.SetIndex(start);
				region.SetSize(size);
				image3d->SetRegion(region);
				double origin[3];
				origin[0] = dicomData[0].sliceOrigin[0];
				origin[1] = dicomData[0].sliceOrigin[1];
				origin[2] = dicomData[0].sliceOrigin[2];
				image3d->SetOrigin(origin);
				double spacing[3];
				spacing[0] = dicomData[0].voxelSpacing[0];
				spacing[1] = dicomData[0].voxelSpacing[1];
				spacing[2] = abs( dicomData[1].sliceOrigin[2] - dicomData[0].sliceOrigin[2]);
				assert(spacing[0] != 0);
				assert(spacing[1] != 0);
				assert(spacing[2] != 0);
				itk::ImportImageFilter<short, 3>::DirectionType direction;

				direction(0, 0) = dicomData[0].directionCosines[0];
				direction(1, 0) = dicomData[0].directionCosines[1];
				direction(2, 0) = dicomData[0].directionCosines[2];

				direction(0, 1) = dicomData[0].directionCosines[3];
				direction(1, 1) = dicomData[0].directionCosines[4];
				direction(2, 1) = dicomData[0].directionCosines[5];

				std::array<double, 3> primeiroVetor = { { dicomData[0].directionCosines[0], dicomData[0].directionCosines[1], dicomData[0].directionCosines[2] } };
				std::array<double, 3> segundoVetor = { { dicomData[0].directionCosines[3], dicomData[0].directionCosines[4], dicomData[0].directionCosines[5] } };
				std::array<double, 3> terceiroVetor;

				vtkMath::Cross(primeiroVetor.data(), segundoVetor.data(), terceiroVetor.data());
				direction(0, 2) = terceiroVetor[0];
				direction(1, 2) = terceiroVetor[1];
				direction(2, 2) = terceiroVetor[2];

				//image3d->SetDirection(direction);
				image3d->SetSpacing(spacing);
				image3d->SetImportPointer(buffer3d.data(), buffer3d.size()*sizeof(short), false);
				image3d->Update();

				cout << endl;
				//typedef itk::ImageFileWriter< itk::Image<short, 3> > WriterType;
				//WriterType::Pointer writer = WriterType::New();
				//std::string outFileName = "c:\\meus dicoms\\teste_import_3d.mha";
				//writer->SetFileName(outFileName);
				//writer->SetInput(image3d->GetOutput());
				//writer->Update();
			}
			return image3d->GetOutput();
		}
	};

	class ImageSliceDatabase
	{
	private:
		vector<shared_ptr<ImageSlice>> images;
	public:
		void AddSlice(string idExame, string idSerie, MPRV2_DicomData data){
			shared_ptr<ImageSlice> foundSlice = nullptr;
			for (size_t i = 0; i < images.size(); i++){
				if ((images[i]->idExame == idExame) && (images[i]->idSerie == idSerie))
				{
					foundSlice = images[i];
					break;
				}
			}
			if (!foundSlice){
				foundSlice = make_shared<ImageSlice>();
				foundSlice->idExame = idExame;
				foundSlice->idSerie = idSerie;
				foundSlice->dicomData.push_back(data);
				images.push_back(foundSlice);
			}
			else{
				foundSlice->dicomData.push_back(data);
			}
		}
		shared_ptr<ImageSlice> GetImageSlice(string idExame, string idSerie){
			shared_ptr<ImageSlice> foundSlice = nullptr;
			for (size_t i = 0; i < images.size(); i++){
				if ((images[i]->idExame == idExame) && (images[i]->idSerie == idSerie))
				{
					foundSlice = images[i];
					break;
				}
			}
			return foundSlice;
		}
	};



	class ImageSource
	{
	private:
		//A lista de arquivos fontes.
		vector<string> filePaths;
		//O callback pra barrinha de carga
		CallbackProgressoDeCarga::Pointer handleDoCallbackDeCarga;

		int codigoDeErro;
	public:
		const int SEM_ERRO_DE_CARGA = 0;
		const int ERROR_CODE_INDICES_ZUADOS = 10;
		const int ERROR_CODE_OOM = 2;
		const int ERROR_FALHA_NA_LEITURA = 1;

		ImageSource(callbackDeProgressDaCarga callbackExternoDeCarga);

		void SetCallbackDeProgesso(callbackDeProgressDaCarga cbk);
		//Adiciona um arquivo ao fim da lista.
		void PushFile(string f);
		//Limpa a lista de fatias.
		void ClearList();
		shared_ptr<LoadedImage> Load(string idExame, string idSerie);
		int GetCodigoDeErro(){ return codigoDeErro; }
	};

	class ImageSourceFromMemory{
	private:
		itk::ImportImageFilter<short, 3>::Pointer importer;
	public:
		ImageSourceFromMemory(MPRV2_DicomData data, float physicalOriginX, float physicalOriginY, float physicalOriginZ)
		{
			cout << "Comešando a importacao..." << endl;
			importer = itk::ImportImageFilter<short, 3>::New();
			itk::ImportImageFilter<short, 3>::SizeType  size;
			size[0] = data.voxelDimensions[0];  // size along X
			size[1] = data.voxelDimensions[1];  // size along Y
			size[2] = data.voxelDimensions[2];  // size along Z
			itk::ImportImageFilter<short, 3>::IndexType start;
			start.Fill(0);
			itk::ImportImageFilter<short, 3>::RegionType region;
			region.SetIndex(start);
			region.SetSize(size);
			importer->SetRegion(region);
			std::array<double, 3> origin = { { physicalOriginX, physicalOriginY, physicalOriginZ } };
			importer->SetOrigin(origin.data());
			std::array<double, 3> spacing = { { data.voxelSpacing[0], data.voxelSpacing[1], data.voxelSpacing[2] } };
			importer->SetSpacing(spacing.data());
			importer->SetImportPointer(data.bufferData, size[0] * size[1] * size[2], false);
			importer->Update();
			importer->GetOutput()->Print(cout);
		}
		shared_ptr<LoadedImage> Load(string idExame, string idSerie){
			itk::Image<short, 3>::Pointer ptrImg = importer->GetOutput();
			ptrImg->DisconnectPipeline();
			map<string, string> emptyDictionary;
			typedef itk::MetaDataDictionary DictionaryType;
			return make_shared<LoadedImage>(ptrImg, emptyDictionary, idExame, idSerie);
		}

	};
}
#endif