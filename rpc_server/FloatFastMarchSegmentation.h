#pragma once
#include <SistemaDeSegmentacaoInterface.h>
#include <array>
#include <itkImage.h>
using namespace std;
class FloatFastMarchSegmentation :public SistemaDeSegmentacaoInterface
{
private:
	itk::Image<short,3>::Pointer sigmoidResult;
	itk::Image<unsigned short, 3>::Pointer fastMarchResult;
	itk::Image<unsigned short, 3>::Pointer thresholdResult;
	itk::Image<unsigned short, 3>::Pointer dilatateResult;
	float sigmoidAlpha;
	array<double, 3> seed;
	int distanciaDoFastMarch;
	itk::Image<short, 3>::Pointer input;
	itk::Image<short, 3>::Pointer output;
	short GetScalarNoInput(int x, int y, int z);
public:
	itk::Image<short, 3>::Pointer GetITKOutput();
	vtkSmartPointer<vtkImageStencilData> GetOutput();
	void Segmentar();
	void SetDentroOuFora(ManterDentroOuFora f);
	void SetDistanciaDoFastMarch(int dist);
	void SetInput(itk::Image<short, 3>::Pointer in);
	void SetSeed(std::array<double, 3> pos);
	void SetSigmoidAlpha(float a);
};