#include "FloatFastMarchSegmentation.h"
#include <itkImage.h>
#include <itkCastImageFilter.h>
#include <itkSigmoidImageFilter.h>
#include <itkFastMarchingImageFilter.h>
#include <itkThresholdImageFilter.h>
#include <vtkSmartPointer.h>
#include <itktypes.h>
#include <vtkImageImport.h>
#include <vtkImageDilateErode3D.h>
#include <vtkImageData.h>
#include <vtkXMLImageDataWriter.h>
#include <itkTimeProbe.h>
#include <string>

typedef itk::Image<short, 3> ShortImage;
typedef itk::Image<float, 3> FloatImage;

typedef itk::CastImageFilter<ShortImage, FloatImage> ShortToFloatFilter;
typedef itk::SigmoidImageFilter<FloatImage, FloatImage> SigmoidFilter;
typedef itk::FastMarchingImageFilter< FloatImage, FloatImage > FastMarchingFilterType;
typedef itk::ThresholdImageFilter<FloatImage> ThresholdFilter;

vtkSmartPointer<vtkImageStencilData> FloatFastMarchSegmentation::GetOutput()
{
	assert(FALSE&&"n�o implementado");
	return nullptr;
}

itk::Image<short, 3>::Pointer FloatFastMarchSegmentation::GetITKOutput()
{
	return output;
}

short FloatFastMarchSegmentation::GetScalarNoInput(int x, int y, int z)
{
	itk::Image<float, 3>::IndexType posNaInput;
	posNaInput[0] = x;
	posNaInput[1] = y;
	posNaInput[2] = z;
	short myScalar = input->GetPixel(posNaInput);
	return myScalar;
}
#define VERSAO_FLOAT
void FloatFastMarchSegmentation::Segmentar()
{
#ifdef VERSAO_SHORT
	itk::TimeProbe totalTime;
	totalTime.Start();
	cout << "Iniciando segmenta��o..." << endl;
	FloatImage::PixelType myScalar = GetScalarNoInput(seed[0], seed[1], seed[2]);
	float alpha = sigmoidAlpha;
	float beta = myScalar;
	//1)Sigm�ide
	itk::TimeProbe sigmoidProbe;
	sigmoidProbe.Start();
	typedef itk::SigmoidImageFilter<ShortImage, ShortImage> SigmoidFilter;
	SigmoidFilter::Pointer mySigmoid;
	//mySigmoid->AddObserver(itk::ProgressEvent(), progressObserver);
	mySigmoid = SigmoidFilter::New();
	mySigmoid->SetInput(input);
	mySigmoid->SetAlpha(alpha);
	mySigmoid->SetBeta(beta);
	mySigmoid->SetOutputMinimum(0);
	mySigmoid->SetOutputMaximum(50);
	cout << "Fazendo o sigmoide" << endl;
	mySigmoid->Update();//duplica
	sigmoidResult = mySigmoid->GetOutput();
	sigmoidProbe.Stop();
	cout << "dt sigmoide = " << sigmoidProbe.GetMean() << endl;
	itk::TimeProbe fastMarchTimeProbe;
	fastMarchTimeProbe.Start();
	typedef  itk::FastMarchingImageFilter< itk::Image<unsigned short, 3>, ShortImage >    FastMarchingFilterType;
	typedef FastMarchingFilterType::NodeContainer           NodeContainer;
	typedef FastMarchingFilterType::NodeType                NodeType;
	FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();
	//fastMarching->AddObserver(itk::ProgressEvent(), progressObserver);
	fastMarching->SetNormalizationFactor(50);
	fastMarching->SetInput(sigmoidResult);
	NodeContainer::Pointer seeds = NodeContainer::New();
	FloatImage::IndexType  seedPosition;
	seedPosition[0] = seed[0];
	seedPosition[1] = seed[1];
	seedPosition[2] = seed[2];
	NodeType node;
	const double seedValue = 0.0;
	node.SetValue(seedValue);
	node.SetIndex(seedPosition);
	seeds->Initialize();
	seeds->InsertElement(0, node);
	fastMarching->SetOutputSize(sigmoidResult->GetBufferedRegion().GetSize());
	fastMarching->SetTrialPoints(seeds);
	fastMarching->SetStoppingValue(this->distanciaDoFastMarch + 5);
	fastMarching->SetNormalizationFactor(50);
	fastMarching->Update();//dobra
	fastMarchResult = fastMarching->GetOutput();
	fastMarchTimeProbe.Stop();
	sigmoidResult = nullptr;
	mySigmoid = nullptr;
	cout << "dt fastMarch = " << fastMarchTimeProbe.GetMean() << endl;
	//3) Thresholder, pq eu quero uma imagem bin�ria
	itk::TimeProbe thresholdTimer;
	thresholdTimer.Start();
	typedef itk::ThresholdImageFilter<itk::Image<unsigned short, 3>> ThresholdFilterType;
	ThresholdFilterType::Pointer thresholder1 = ThresholdFilterType::New();
	//thresholder1->AddObserver(itk::ProgressEvent(), progressObserver);
	thresholder1->SetInput(fastMarchResult);
	thresholder1->InPlaceOn();
	thresholder1->ThresholdAbove(this->distanciaDoFastMarch + 1);
	thresholder1->SetOutsideValue(0);
	thresholder1->Update();
	//Aqui as regi�es de valor infinito, que o fast march n�o cobriu, foram zeradas
	ThresholdFilterType::Pointer thresholder2 = ThresholdFilterType::New();
	//thresholder2->AddObserver(itk::ProgressEvent(), progressObserver);
	thresholder2->SetInput(thresholder1->GetOutput());
	thresholder2->InPlaceOn();
	thresholder2->ThresholdAbove(0.1);
	thresholder2->SetOutsideValue(1);
	thresholder2->Update();
	thresholdResult = thresholder2->GetOutput();
	thresholdTimer.Stop();
	///Descarta o fast march
	//fastMarchResult = nullptr;
	fastMarching = nullptr;
	fastMarchResult = nullptr;
	cout << "dt threshold = " << thresholdTimer.GetMean() << endl;
	////4)Dilata��o (na vtk)
	itk::TimeProbe dilatacaoTimer;
	dilatacaoTimer.Start();
	vtkSmartPointer<vtkImageImport> thresholdPassadoPraVTK = CreateVTKImage(thresholdResult);
	vtkSmartPointer<vtkImageDilateErode3D> dilateVTK = vtkSmartPointer<vtkImageDilateErode3D>::New();
	dilateVTK->SetInputConnection(thresholdPassadoPraVTK->GetOutputPort());
	dilateVTK->SetDilateValue(4);
	dilateVTK->Update();
	vtkSmartPointer<vtkImageData> dilatateResultVTK = dilateVTK->GetOutput();
	unsigned short* sourceBuffer = (unsigned short*)dilatateResultVTK->GetScalarPointer();
	dilatateResult = thresholdResult;//
	unsigned short* destBuffer = dilatateResult->GetBufferPointer();
	memcpy(destBuffer, sourceBuffer, dilatateResultVTK->GetDimensions()[0] * dilatateResultVTK->GetDimensions()[1] * dilatateResultVTK->GetDimensions()[2] * sizeof(unsigned short));
	//Se tudo deu certo, o bagulho est� copiado pra dilatateResult.
	//dilatateResult = nullptr;
	dilateVTK = nullptr;
	dilatacaoTimer.Stop();
	//descarta o thresholder - o resultado t� tem dilatateResult
	thresholder1 = nullptr;
	thresholder2 = nullptr;
	thresholdResult = nullptr;
	cout << "dt dilatacao = " << dilatacaoTimer.GetMean() << endl;
	//fim:
	totalTime.Stop();
	cout << "Terminando segmenta��o... dt = " << totalTime.GetMean() << endl;
	itk::CastImageFilter<itk::Image<unsigned short, 3>, itk::Image<short, 3>>::Pointer caster =
		itk::CastImageFilter<itk::Image<unsigned short, 3>, itk::Image<short, 3>>::New();
	caster->SetInput(dilatateResult);
	caster->Update();
	output = caster->GetOutput();
	if (false)
	{
		/*
	 //Persiste o sigm�ide
	 itk::TimeProbe sigmoidPersistTimer;
	 sigmoidPersistTimer.Start();
	 vtkSmartPointer<vtkImageImport> sigVTK = CreateVTKImage(sigmoidResult);
	 vtkSmartPointer<vtkXMLImageDataWriter> sigPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	 stringstream ssSig;
	 ssSig << "sigmoide_" << contadorDeDebugDaSegmentacao << ".vti";
	 sigPersist->SetInputConnection(sigVTK->GetOutputPort());
	 sigPersist->SetFileName(ssSig.str().c_str());
	 sigPersist->Write();
	 sigmoidPersistTimer.Stop();
	 cout << "DEBUG - Persistencia do sigmoide dt = " << sigmoidPersistTimer.GetMeanTime() << endl;
	 ////Persiste o fast march
	 itk::TimeProbe fmPersistenceTimeProbe;
	 fmPersistenceTimeProbe.Start();
	 vtkSmartPointer<vtkImageImport> fmVTK = CreateVTKImage(fastMarchResult);
	 vtkSmartPointer<vtkXMLImageDataWriter> fmPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	 stringstream ssFMM;
	 ssFMM << "FM_" << contadorDeDebugDaSegmentacao << ".vti";
	 fmPersist->SetInputConnection(fmVTK->GetOutputPort());
	 fmPersist->SetFileName(ssFMM.str().c_str());
	 fmPersist->Write();
	 fmPersistenceTimeProbe.Stop();
	 cout << "DEBUG - Persistencia do fm dt = " << fmPersistenceTimeProbe.GetMean() << endl;


	 itk::TimeProbe thresPersistenceTimeProbe;
	 thresPersistenceTimeProbe.Start();
	 vtkSmartPointer<vtkImageImport> threshVTK = CreateVTKImage(thresholdResult);
	 vtkSmartPointer<vtkXMLImageDataWriter> threshPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	 stringstream ssFM;
	 ssFM << "threshold_" << contadorDeDebugDaSegmentacao << ".vti";
	 threshPersist->SetInputConnection(threshVTK->GetOutputPort());
	 threshPersist->SetFileName(ssFM.str().c_str());
	 threshPersist->Write();
	 thresPersistenceTimeProbe.Stop();
	 cout << "DEBUG - Persistencia do thresh dt = " << thresPersistenceTimeProbe.GetMean() << endl;
	 */
		itk::TimeProbe dilatePersistenceTimeProbe;
		dilatePersistenceTimeProbe.Start();
		vtkSmartPointer<vtkImageImport> dilateteVTK = CreateVTKImage(dilatateResult);
		vtkSmartPointer<vtkXMLImageDataWriter> dilatatePersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
		stringstream dFM;
		dFM << "dilatate.vti";
		dilatatePersist->SetInputConnection(dilateteVTK->GetOutputPort());
		dilatatePersist->SetFileName(dFM.str().c_str());
		dilatatePersist->Write();
		dilatePersistenceTimeProbe.Stop();
		cout << "DEBUG - Persistencia do dilatate dt = " << dilatePersistenceTimeProbe.GetMean() << endl;
	}
#endif
#ifdef VERSAO_FLOAT
	output = input;
	//Pega o valor do escalar e params do sigm�ide
	itk::Image<float, 3>::IndexType posNaInput;
	posNaInput[0] = seed[0];
	posNaInput[1] = seed[1];
	posNaInput[2] = seed[2];
	ShortImage::PixelType myScalar = input->GetPixel(posNaInput);
	float alpha = sigmoidAlpha;
	float beta = myScalar;
	//Cast de short pra float
	ShortToFloatFilter::Pointer shortToFloat = ShortToFloatFilter::New();
	shortToFloat->SetInput(input);
	//Sigmoide [0,1]
	SigmoidFilter::Pointer sigmoid = SigmoidFilter::New();
	sigmoid->SetInput(shortToFloat->GetOutput());
	sigmoid->SetAlpha(alpha);
	sigmoid->SetBeta(myScalar);
	sigmoid->SetOutputMinimum(0);
	sigmoid->SetOutputMaximum(10);
	sigmoid->Update();
	//Fast March
	typedef FastMarchingFilterType::NodeContainer           NodeContainer;
	typedef FastMarchingFilterType::NodeType                NodeType;
	FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();
	fastMarching->SetInput(sigmoid->GetOutput());
	NodeContainer::Pointer seeds = NodeContainer::New();
	FloatImage::IndexType  seedPosition;
	seedPosition[0] = seed[0];
	seedPosition[1] = seed[1];
	seedPosition[2] = seed[2];
	NodeType node;
	const double seedValue = 0.0;
	node.SetValue(seedValue);
	node.SetIndex(seedPosition);
	seeds->Initialize();
	seeds->InsertElement(0, node);
	fastMarching->SetOutputSize(sigmoid->GetOutput()->GetBufferedRegion().GetSize());
	fastMarching->SetTrialPoints(seeds);
	fastMarching->SetNormalizationFactor(10);
	fastMarching->SetNumberOfThreads(4);
	fastMarching->SetStoppingValue(this->distanciaDoFastMarch+2);
	fastMarching->Update();
	//Thresholds
	ThresholdFilter::Pointer threshold1 = ThresholdFilter::New();
	threshold1->SetInput(fastMarching->GetOutput());
	threshold1->InPlaceOn();
	threshold1->ThresholdAbove(this->distanciaDoFastMarch+1);
	threshold1->SetOutsideValue(0);
	threshold1->Update();
	ThresholdFilter::Pointer thresholder2 = ThresholdFilter::New();
	thresholder2->SetInput(threshold1->GetOutput());
	thresholder2->InPlaceOn();
	thresholder2->ThresholdAbove(0.5);
	thresholder2->SetOutsideValue(1);
	thresholder2->Update();
	//Dilata��o
	vtkSmartPointer<vtkImageImport> thresholdPassadoPraVTK = CreateVTKImage(thresholder2->GetOutput());
	vtkSmartPointer<vtkImageDilateErode3D> dilateVTK = vtkSmartPointer<vtkImageDilateErode3D>::New();
	dilateVTK->SetInputConnection(thresholdPassadoPraVTK->GetOutputPort());
	dilateVTK->SetDilateValue(4);
	dilateVTK->Update();
	//o dilatate � na vtk, tenho que voltar pra itk com o resultado.
	FloatImage::Pointer lastFloatImage = thresholder2->GetOutput();

	float *sourceBuffer = (float*)dilateVTK->GetOutput()->GetScalarPointer();
	float *destBuffer = lastFloatImage->GetBufferPointer();
	std::memcpy(destBuffer, sourceBuffer, dilateVTK->GetOutput()->GetDimensions()[0] *
									 dilateVTK->GetOutput()->GetDimensions()[1] *
									 dilateVTK->GetOutput()->GetDimensions()[2] *
									 sizeof(float));
	//Cast de float pra short
	itk::ImageRegionConstIterator<FloatImage> inputIterator(lastFloatImage, lastFloatImage->GetLargestPossibleRegion());
	itk::ImageRegionIterator<ShortImage> outputIterator(output, output->GetLargestPossibleRegion());
	while (!inputIterator.IsAtEnd())
	{
		outputIterator.Set(inputIterator.Get());
		++inputIterator;
		++outputIterator;
	}
	//Salva pra debug
	vtkSmartPointer<vtkXMLImageDataWriter> writer = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	writer->SetInputData(dilateVTK->GetOutput());
	writer->SetFileName("C:\\meus dicoms\\debug.vti");
	writer->Write();
#endif
}
void FloatFastMarchSegmentation::SetDentroOuFora(ManterDentroOuFora f)
{
	assert(FALSE&&"n�o implementado");
}
void FloatFastMarchSegmentation::SetDistanciaDoFastMarch(int dist)
{
	this->distanciaDoFastMarch = dist;
}
void FloatFastMarchSegmentation::SetInput(itk::Image<short, 3>::Pointer in)
{
	this->input = in;
}
void FloatFastMarchSegmentation::SetSeed(std::array<double, 3> pos)
{
	this->seed = pos;
}
void FloatFastMarchSegmentation::SetSigmoidAlpha(float a)
{
	this->sigmoidAlpha = a;
}