#include <iostream>
#include "FastMarchingSegmentation.h"
#include <memory>
#include <SistemaDeSegmentacaoInterface.h>
#include "FloatFastMarchSegmentation.h"
#include <string>
#include <windows.h>
#include <fstream>
#include <UtilitiesV2.h>

using namespace std;

unique_ptr<SistemaDeSegmentacaoInterface> segmentador;
array<int, 3> dimensoesDaImagem = { -1, -1, -1 };
array<float, 3> spacingDaImagem = { -1, -1, -1 };
string buffer_path;
itk::Image<short, 3>::Pointer image = nullptr;

void Die(/* [in] */ handle_t hBinding)
{
	std::cout <<GetDateAsString()<<" "<< __FUNCTION__ << std::endl;
	//deleta o segmentador
	segmentador = nullptr;
	image = nullptr;
	//fecha a conex�o
	RpcMgmtStopServerListening(NULL);
	RpcServerUnregisterIf(NULL, NULL, FALSE);
	//desliga o programa
	EXIT_SUCCESS;
}

void LazyInitDoSegmentador();

void SetSegmentationAlpha(/* [in] */ handle_t hBinding,float alpha)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	LazyInitDoSegmentador();
	segmentador->SetSigmoidAlpha(alpha);
	cout << GetDateAsString() << " " << "Alpha do sigmoide = " << alpha << endl;
}

void SetDimensions(/* [in] */ handle_t hBinding,/* [in] */ int x,/* [in] */ int y,/* [in] */ int z)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	LazyInitDoSegmentador();
	dimensoesDaImagem = { { x, y, z } };
	cout << GetDateAsString() << " " << "Dimensoes da imagem = " << dimensoesDaImagem[0] << ", " << dimensoesDaImagem[1] << ", " << dimensoesDaImagem[2] << endl;
}

void SetSpacing(/* [in] */ handle_t hBinding,/* [in] */ float x,/* [in] */ float y,/* [in] */ float z)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	LazyInitDoSegmentador();
	spacingDaImagem = { { x, y, z } };
	cout << GetDateAsString() << " " << "spacing da imagem = " << spacingDaImagem[0] << ", " << spacingDaImagem[1] << ", " << spacingDaImagem[2] << endl;
}

void SetMarchingDistance(/* [in] */ handle_t hBinding,/* [in] */ int dist)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	LazyInitDoSegmentador();
	segmentador->SetDistanciaDoFastMarch(dist);
	cout << GetDateAsString() << " " << "marching distance = " << dist << endl;
}

void AddSeed(/* [in] */ handle_t hBinding,/* [in] */ int x,/* [in] */ int y,/* [in] */ int z)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	LazyInitDoSegmentador();
	segmentador->SetSeed({ { x, y, z } });
}

void LoadBufferFromTempFile(
	/* [in] */ handle_t hBinding,
	UINT nSize,
	/* [size_is][string][in] */ char *filename)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	std::cout << GetDateAsString() << " " << "File Buffer = " << filename << std::endl;
	std::fstream bufferFile;
	//ALOCA A IMAGEM AQUI NO SERVER
	itk::Image<short,3>::RegionType region;
	itk::Image<short, 3>::IndexType start;
	start[0] = 0;
	start[1] = 0;
	start[2] = 0;
	itk::Image<short, 3>::SizeType size;
	size[0] = dimensoesDaImagem[0];
	size[1] = dimensoesDaImagem[1];
	size[2] = dimensoesDaImagem[2];
	region.SetSize(size);
	image = itk::Image<short, 3>::New();
	image->SetSpacing(spacingDaImagem.data());
	image->SetRegions(region);
	image->Allocate();
	//PASSA OS DADOS DO BUFFER PRA IMAGEM
	buffer_path = filename;
	bufferFile.open(buffer_path.c_str(), fstream::in | fstream::binary | fstream::out);
	bufferFile.seekg(0);
	bufferFile.read((char*)image->GetBufferPointer(), image->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
	bufferFile.close();
	//A PARTIR DAQUI, T� TUDO PRONTO
	segmentador->SetInput(image);
}

void Execute(/* [in] */ handle_t hBinding)
{
	std::cout << GetDateAsString() << " " << __FUNCTION__ << std::endl;
	segmentador->Segmentar();
	std::fstream bufferFile;
	bufferFile.open(buffer_path.c_str(), fstream::in | fstream::out | fstream::binary | fstream::trunc);
	bufferFile.seekg(0);
	short *dataBuffer = segmentador->GetITKOutput()->GetBufferPointer();
	size_t buffSize = segmentador->GetITKOutput()->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
	bufferFile.seekg(0, ios::beg);
	bufferFile.write(reinterpret_cast<const char*>(dataBuffer), buffSize);
	bufferFile.close();
}



// Naive security callback.
RPC_STATUS CALLBACK SecurityCallback(RPC_IF_HANDLE /*hInterface*/, void* /*pBindingHandle*/)
{
	//std::cout << __FUNCTION__ << std::endl;
	return RPC_S_OK; // Always allow anyone.
}

// Memory allocation function for RPC.
// The runtime uses these two functions for allocating/deallocating
// enough memory to pass the string to the server.
void* __RPC_USER midl_user_allocate(size_t size)
{
	return malloc(size);
}
// Memory deallocation function for RPC.
void __RPC_USER midl_user_free(void* p)
{
	free(p);
}

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
	)
//int main(int argc, char** argv)
{
	//Pra fazer o cout enviar para um arquivo
	std::fstream fs;
	fs.open("log.txt", std::fstream::in | std::fstream::app);
	std::streambuf *coutbuff = std::cout.rdbuf();
	std::cout.rdbuf(fs.rdbuf());
	//Agora come�a a fazer a parada
	RPC_STATUS status;
	// Uses the protocol combined with the endpoint for receiving
	// remote procedure calls.
	status = RpcServerUseProtseqEpA(
		reinterpret_cast<unsigned char*>("ncacn_ip_tcp"), // Use TCP/IP protocol.
		RPC_C_PROTSEQ_MAX_REQS_DEFAULT, // Backlog queue length for TCP/IP.
		reinterpret_cast<unsigned char*>("4747"), // TCP/IP port to use.
		NULL); // No security.
	if (status){
		std::cout << GetDateAsString() << " " << "Erro em RpcServerUseProtseqEpA codigo " << status << endl;
		string debugErr;
		exit(status);
	}
	else
		std::cout << GetDateAsString() <<" "<<"RpcServerUseProtseqEpA OK" << std::endl;
	// Registers the Example1 interface.
	status = RpcServerRegisterIf2(
		FastMarchingSegmentation_v0_1_s_ifspec, // Interface to register.
		NULL, // Use the MIDL generated entry-point vector.
		NULL, // Use the MIDL generated entry-point vector.
		RPC_IF_ALLOW_CALLBACKS_WITH_NO_AUTH, // Forces use of security callback.
		RPC_C_LISTEN_MAX_CALLS_DEFAULT, // Use default number of concurrent calls.
		(unsigned)-1, // Infinite max size of incoming data blocks.
		SecurityCallback); // Naive security callback.
	if (status){
		std::cout << GetDateAsString() << " " << "Erro em RpcServerRegisterIf2 codigo " << status << endl;
		string debugErr;
		exit(status);
	}
	else
		std::cout << GetDateAsString() << " " << "RpcServerRegisterIf2 OK" << std::endl;
	// Start to listen for remote procedure
	// calls for all registered interfaces.
	// This call will not return until
	// RpcMgmtStopServerListening is called.
	status = RpcServerListen(
		1, // Recommended minimum number of threads.
		RPC_C_LISTEN_MAX_CALLS_DEFAULT, // Recommended maximum number of threads.
		FALSE); // Start listening now.
	if (status){
		std::cout << GetDateAsString() << " " << "Erro em RpcServerListen codigo " << status << endl;
		string debugErr;
		exit(status);
	}
	else
		std::cout << GetDateAsString() << " " << "RpcServerListen OK" << std::endl;
}

void LazyInitDoSegmentador()
{
	if (!segmentador)
	{
		std::cout << __FUNCTION__ << std::endl;
		segmentador = make_unique<FloatFastMarchSegmentation>();
		cout << GetDateAsString() << " " << "criou o segmentador" << endl;
	}
}