#pragma once
#include <string>
#include <array>
#include <itkImage.h>
#include <itktypes.h>
#include <vtkImageImport.h>

using namespace std;
class DicomGenerator {
public:
	enum TipoOrientacao{AXIAL, CORONAL, SAGITAL};
private:
	string idExame, idSerie;
	itk::Image<short, 3>::Pointer imagem;
	vtkSmartPointer<vtkImageImport> imagemImportadaPraVTK;
	array<double, 3> sliceCenter;
	array<double, 3> CalculateSliceCenter(int qualFatia);
	TipoOrientacao orientacao;
public:
	DicomGenerator(string idExame, string idSerie);
	void SetProperties(int qualFatia, TipoOrientacao o);
	void SetImage(itk::Image<short, 3>::Pointer imagem);
	void Execute();
	const char* GetExame() { return idExame.c_str(); }
	const char* GetSerie() { return idSerie.c_str(); }
};