#include <sstream>
#include <vtkImageData.h>
#include "SistemaDeSegmentacao.h"
#include <itkTimeProbe.h>
#include <itkSigmoidImageFilter.h>
#include <itkFastMarchingImageFilter.h>
#include <vtkImageImport.h>
#include "../base/itktypes.h"
#include <vtkXMLImageDataWriter.h>
#include <itkThresholdImageFilter.h>
#include <vtkImageDilateErode3D.h>
#include <vtkImageToImageStencil.h>
#include <vtkImageStencil.h>
#include <vtkMarchingCubes.h>
#include <vtkDecimatePro.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkFillHolesFilter.h>
#include <vtkSmoothPolyDataFilter.h>
#include <itkCommand.h>
class vtkImageToImageStencil;
class vtkImageDilateErode3D;
static int contadorDeDebugDaSegmentacao = 0;



class MyCommand : public itk::Command
{
public:
	itkNewMacro(MyCommand);

	void Execute(itk::Object * caller, const itk::EventObject & event)
	{
		Execute((const itk::Object *)caller, event);
	}

	void Execute(const itk::Object * caller, const itk::EventObject & event)
	{
		if (!itk::ProgressEvent().CheckEvent(&event))
		{
			return;
		}
		const itk::ProcessObject * processObject =
			dynamic_cast< const itk::ProcessObject * >(caller);
		if (!processObject)
		{
			return;
		}
		std::cout << "Progress: " << processObject->GetProgress() << std::endl;
	}
};

SistemaDeSegmentacao::SistemaDeSegmentacao()
{
	this->fastMarchDistance = 0;
	this->seed = { -1, -1, -1 };
	this->inOrOut = ManterDentroOuFora::DENTRO;
	this->imageInput = nullptr;
	debugMode = false;
	sigmoidAlpha = 5.0;
	sigmoidResult = nullptr;
	sigmoidRange = 50;
}

FloatImage::PixelType SistemaDeSegmentacao::GetScalarNoInput(int x, int y, int z)
{
	itk::Image<float, 3>::IndexType posNaInput;
	posNaInput[0] = x;      
	posNaInput[1] = y;      
	posNaInput[2] = z;      
	FloatImage::PixelType myScalar = imageInput->GetPixel(posNaInput);
	return myScalar;
}

void SistemaDeSegmentacao::SetDebugMode(bool debugOn)
{
	this->debugMode = debugOn;
}

void SistemaDeSegmentacao::Segmentar()
{
	itk::TimeProbe totalTime;
	totalTime.Start();
	cout << "Iniciando segmenta��o..." << endl;
	//MyCommand::Pointer progressObserver = MyCommand::New();

	FloatImage::PixelType myScalar = GetScalarNoInput(seed[0], seed[1], seed[2]);
	float alpha = sigmoidAlpha;
	float beta = myScalar;
	//1)Sigm�ide
	itk::TimeProbe sigmoidProbe;
	sigmoidProbe.Start();
	typedef itk::SigmoidImageFilter<ShortImage, ShortImage> SigmoidFilter;
	SigmoidFilter::Pointer mySigmoid;
	//mySigmoid->AddObserver(itk::ProgressEvent(), progressObserver);
	mySigmoid = SigmoidFilter::New();
	mySigmoid->SetInput(imageInput);
	mySigmoid->SetAlpha(alpha);
	mySigmoid->SetBeta(beta);
	mySigmoid->SetOutputMinimum(0);
	mySigmoid->SetOutputMaximum(sigmoidRange);
	cout << "Fazendo o sigmoide" << endl;
	mySigmoid->Update();//duplica
	sigmoidResult = mySigmoid->GetOutput();
	sigmoidProbe.Stop();
	cout << "dt sigmoide = " << sigmoidProbe.GetMean() << endl;
	//1.1)Grava a imagem original no disco e deleta o buffer dela
	//std::fstream bufferFileWrite;
	//bufferFileWrite.open("oldbuffer.dat", fstream::in | fstream::out | fstream::binary | fstream::trunc);
	//bufferFileWrite.seekg(0);
	//short *dataBuffer = imageInput->GetBufferPointer();
	//size_t buffSize = imageInput->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
	//bufferFileWrite.seekg(0, ios::beg);
	//bufferFileWrite.write(reinterpret_cast<const char*>(dataBuffer), buffSize);
	//bufferFileWrite.close();
	//imageInput->ReleaseData();
	////2)Fast Marching
	//cout << "Fazendo o fast march" << endl;
	
	////Teceira etapa: fast march a partir da seed
	itk::TimeProbe fastMarchTimeProbe;
	fastMarchTimeProbe.Start();

	typedef  itk::FastMarchingImageFilter< itk::Image<unsigned short, 3>, ShortImage >    FastMarchingFilterType;
	typedef FastMarchingFilterType::NodeContainer           NodeContainer;
	typedef FastMarchingFilterType::NodeType                NodeType;
	FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();
	//fastMarching->AddObserver(itk::ProgressEvent(), progressObserver);
	fastMarching->SetNormalizationFactor(sigmoidRange);
	fastMarching->SetInput(sigmoidResult);
	NodeContainer::Pointer seeds = NodeContainer::New();
	FloatImage::IndexType  seedPosition;
	seedPosition[0] = seed[0];
	seedPosition[1] = seed[1];
	seedPosition[2] = seed[2];
	NodeType node;
	const double seedValue = 0.0;
	node.SetValue(seedValue);
	node.SetIndex(seedPosition);
	seeds->Initialize();
	seeds->InsertElement(0, node);
	fastMarching->SetOutputSize(sigmoidResult->GetBufferedRegion().GetSize());
	fastMarching->SetTrialPoints(seeds);
	fastMarching->SetStoppingValue(this->fastMarchDistance+5);
	fastMarching->SetNormalizationFactor(sigmoidRange);
	fastMarching->Update();//dobra
	fastMarchResult = fastMarching->GetOutput();
	fastMarchTimeProbe.Stop();
	sigmoidResult = nullptr;
	mySigmoid = nullptr;
	cout << "dt fastMarch = " << fastMarchTimeProbe.GetMean() << endl;
	//Descarta o sigmoide
	//sigmoidResult = nullptr;
	//mySigmoid = nullptr;
	//fastMarching = nullptr;
	
	//3) Thresholder, pq eu quero uma imagem bin�ria

	itk::TimeProbe thresholdTimer;
	thresholdTimer.Start();
	typedef itk::ThresholdImageFilter<itk::Image<unsigned short, 3>> ThresholdFilterType;
	ThresholdFilterType::Pointer thresholder1 = ThresholdFilterType::New();
	//thresholder1->AddObserver(itk::ProgressEvent(), progressObserver);
	thresholder1->SetInput(fastMarchResult);
	thresholder1->InPlaceOn();
	thresholder1->ThresholdAbove(this->fastMarchDistance+1);
	thresholder1->SetOutsideValue(0);
	thresholder1->Update();
	//Aqui as regi�es de valor infinito, que o fast march n�o cobriu, foram zeradas

	ThresholdFilterType::Pointer thresholder2 = ThresholdFilterType::New();
	//thresholder2->AddObserver(itk::ProgressEvent(), progressObserver);
	thresholder2->SetInput(thresholder1->GetOutput());
	thresholder2->InPlaceOn();
	thresholder2->ThresholdAbove(0.1);
	thresholder2->SetOutsideValue(1);
	thresholder2->Update();

	thresholdResult = thresholder2->GetOutput();
	thresholdTimer.Stop();
	///Descarta o fast march
	//fastMarchResult = nullptr;
	fastMarching = nullptr;
	fastMarchResult = nullptr;
	cout << "dt threshold = " << thresholdTimer.GetMean() << endl;
	
	////4)Dilata��o (na vtk)
	itk::TimeProbe dilatacaoTimer;
	dilatacaoTimer.Start();
	vtkSmartPointer<vtkImageImport> thresholdPassadoPraVTK = CreateVTKImage(thresholdResult);
	vtkSmartPointer<vtkImageDilateErode3D> dilateVTK = vtkSmartPointer<vtkImageDilateErode3D>::New();
	dilateVTK->SetInputConnection(thresholdPassadoPraVTK->GetOutputPort());
	dilateVTK->SetDilateValue(4);
	dilateVTK->Update();
	vtkSmartPointer<vtkImageData> dilatateResultVTK = dilateVTK->GetOutput();
	unsigned short* sourceBuffer = (unsigned short*)dilatateResultVTK->GetScalarPointer();
	dilatateResult = thresholdResult;//
	unsigned short* destBuffer = dilatateResult->GetBufferPointer();
	memcpy(destBuffer, sourceBuffer, dilatateResultVTK->GetDimensions()[0] * dilatateResultVTK->GetDimensions()[1] * dilatateResultVTK->GetDimensions()[2] * sizeof(unsigned short));
	//Se tudo deu certo, o bagulho est� copiado pra dilatateResult.
	//dilatateResult = nullptr;
	dilateVTK = nullptr;
	dilatacaoTimer.Stop();
	//descarta o thresholder - o resultado t� tem dilatateResult
	thresholder1 = nullptr;
	thresholder2 = nullptr;
	thresholdResult = nullptr;
	//Final do processo: restaura o que foi gravado
	//std::fstream bufferFileRead;
	//bufferFileRead.open("oldbuffer.dat", fstream::in | fstream::binary | fstream::out);
	//imageInput->SetLargestPossibleRegion(dilatateResult->GetLargestPossibleRegion());
	//short *ptr = imageInput->GetBufferPointer();
	//size_t sz = imageInput->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
	//imageInput->Allocate();
	//ptr = imageInput->GetBufferPointer();
	//sz = imageInput->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
	//bufferFileRead.seekg(0);
	//try
	//{
	//	bufferFileRead.read(reinterpret_cast<char*>(ptr), sz);
	//	bufferFileRead.close();
	//}
	//catch (std::exception &ex)
	//{
	//	cout << ex.what() << endl;
	//}
	

	cout << "dt dilatacao = " << dilatacaoTimer.GetMean() << endl;
	//fim:
	totalTime.Stop();
	cout << "Terminando segmenta��o... dt = " << totalTime.GetMean() << endl;
	
	if (debugMode)
	{/*
		//Persiste o sigm�ide
		itk::TimeProbe sigmoidPersistTimer;
		sigmoidPersistTimer.Start();
		vtkSmartPointer<vtkImageImport> sigVTK = CreateVTKImage(sigmoidResult);
		vtkSmartPointer<vtkXMLImageDataWriter> sigPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
		stringstream ssSig;
		ssSig << "sigmoide_" << contadorDeDebugDaSegmentacao << ".vti";
		sigPersist->SetInputConnection(sigVTK->GetOutputPort());
		sigPersist->SetFileName(ssSig.str().c_str());
		sigPersist->Write();
		sigmoidPersistTimer.Stop();
		cout << "DEBUG - Persistencia do sigmoide dt = " << sigmoidPersistTimer.GetMeanTime() << endl;
		////Persiste o fast march
		itk::TimeProbe fmPersistenceTimeProbe;
		fmPersistenceTimeProbe.Start();
		vtkSmartPointer<vtkImageImport> fmVTK = CreateVTKImage(fastMarchResult);
		vtkSmartPointer<vtkXMLImageDataWriter> fmPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
		stringstream ssFMM;
		ssFMM << "FM_" << contadorDeDebugDaSegmentacao << ".vti";
		fmPersist->SetInputConnection(fmVTK->GetOutputPort());
		fmPersist->SetFileName(ssFMM.str().c_str());
		fmPersist->Write();
		fmPersistenceTimeProbe.Stop();
		cout << "DEBUG - Persistencia do fm dt = " << fmPersistenceTimeProbe.GetMean() << endl;
		

		itk::TimeProbe thresPersistenceTimeProbe;
		thresPersistenceTimeProbe.Start();
		vtkSmartPointer<vtkImageImport> threshVTK = CreateVTKImage(thresholdResult);
		vtkSmartPointer<vtkXMLImageDataWriter> threshPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
		stringstream ssFM;
		ssFM << "threshold_" << contadorDeDebugDaSegmentacao << ".vti";
		threshPersist->SetInputConnection(threshVTK->GetOutputPort());
		threshPersist->SetFileName(ssFM.str().c_str());
		threshPersist->Write();
		thresPersistenceTimeProbe.Stop();
		cout << "DEBUG - Persistencia do thresh dt = " << thresPersistenceTimeProbe.GetMean() << endl;		
		*/
		itk::TimeProbe dilatePersistenceTimeProbe;
		dilatePersistenceTimeProbe.Start();
		vtkSmartPointer<vtkImageImport> dilateteVTK = CreateVTKImage(dilatateResult);
		vtkSmartPointer<vtkXMLImageDataWriter> dilatatePersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
		stringstream dFM;
		dFM << "dilatate_" << contadorDeDebugDaSegmentacao << ".vti";
		dilatatePersist->SetInputConnection(dilateteVTK->GetOutputPort());
		dilatatePersist->SetFileName(dFM.str().c_str());
		dilatatePersist->Write();
		dilatePersistenceTimeProbe.Stop();
		cout << "DEBUG - Persistencia do dilatate dt = " << dilatePersistenceTimeProbe.GetMean() << endl;
		
		contadorDeDebugDaSegmentacao++;
	}
}

void SistemaDeSegmentacao::SetDistanciaDoFastMarch(int dist)
{
	this->fastMarchDistance = dist;
}

void SistemaDeSegmentacao::SetSeed(array<double, 3> pos)
{
	this->seed = pos;
}

void SistemaDeSegmentacao::SetInput(itk::Image<short, 3>::Pointer in)
{
	this->imageInput = in;
}

void SistemaDeSegmentacao::SetDentroOuFora(ManterDentroOuFora f)
{
	this->inOrOut = f;
}

vtkSmartPointer<vtkImageStencilData> SistemaDeSegmentacao::GetOutput()
{

	vtkSmartPointer<vtkImageImport> versaoVTK = CreateVTKImage(dilatateResult);
	vtkSmartPointer<vtkImageToImageStencil> imageToStencilFilter = vtkSmartPointer<vtkImageToImageStencil>::New();
	imageToStencilFilter->SetInputConnection(versaoVTK->GetOutputPort());
	imageToStencilFilter->ThresholdByUpper(0.5);
	imageToStencilFilter->Update();
	/*
	itk::TimeProbe mcTimer;
	mcTimer.Start();
	vtkSmartPointer<vtkMarchingCubes> marchingCubes = vtkSmartPointer<vtkMarchingCubes>::New();
	marchingCubes->SetValue(0, 1);
	marchingCubes->SetInputData(versaoVTK->GetOutput());
	marchingCubes->ComputeNormalsOn();
	marchingCubes->ComputeScalarsOn();
	//marchingCubes->ComputeGradientsOn();
	marchingCubes->Update();
	mcTimer.Stop();
	cout << "tempo mc " << mcTimer.GetMean() << endl;

	itk::TimeProbe decimateTimer;
	decimateTimer.Start();
	vtkSmartPointer<vtkDecimatePro> decimator = vtkSmartPointer<vtkDecimatePro>::New();
	decimator->SetInputData(marchingCubes->GetOutput());
	decimator->SetTargetReduction(0.1);
	decimator->Update();
	decimateTimer.Stop();
	cout << "tempo decimate " << decimateTimer.GetMean() << endl;

	itk::TimeProbe connectTimer;
	connectTimer.Start();
	vtkSmartPointer<vtkPolyDataConnectivityFilter> connect = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	connect->SetInputData(decimator->GetOutput());
	connect->SetExtractionModeToLargestRegion();
	connect->Update();
	connectTimer.Stop();
	cout << "tempo connect " << connectTimer.GetMean() << endl;

	itk::TimeProbe fillHoleTimer;
	fillHoleTimer.Start();
	vtkSmartPointer<vtkFillHolesFilter> filler = vtkSmartPointer<vtkFillHolesFilter>::New();
	filler->SetInputData(connect->GetOutput());
	filler->SetHoleSize(500);
	filler->Update();
	fillHoleTimer.Stop();
	cout << "tempo fill hole " << fillHoleTimer.GetMean() << endl;

	itk::TimeProbe meshsmoothTimer;
	meshsmoothTimer.Start();
	vtkSmartPointer<vtkSmoothPolyDataFilter> meshsmooth = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
	meshsmooth->SetInputData(filler->GetOutput());
	meshsmooth->SetNumberOfIterations(40);
	meshsmooth->Update();
	meshsmoothTimer.Stop();
	cout << "tempo mesh smooth " << meshsmoothTimer.GetMean() << endl;

	mesh = meshsmooth->GetOutput();
	*/

	vtkSmartPointer<vtkImageStencilData> sd = imageToStencilFilter->GetOutput();

	if (inOrOut == DENTRO) //Mant�m s� o q est� dentro.
	{
		vtkImageData* propSrc = versaoVTK->GetOutput();
		vtkSmartPointer<vtkImageData> tempImage = vtkSmartPointer<vtkImageData>::New();
		tempImage->SetDimensions(propSrc->GetDimensions());
		tempImage->SetSpacing(propSrc->GetSpacing());
		tempImage->SetOrigin(propSrc->GetOrigin());
		tempImage->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
		memset(tempImage->GetScalarPointer(), 0xFF , 
			tempImage->GetDimensions()[0] * tempImage->GetDimensions()[1] * tempImage->GetDimensions()[2]);
		vtkSmartPointer<vtkImageStencil> sten = vtkSmartPointer<vtkImageStencil>::New();
		sten->SetInputData(tempImage);
		sten->SetStencilData(sd);
		sten->ReverseStencilOn();
		sten->SetBackgroundValue(0x00);
		sten->Update();
		//Aqui tenho o stencil para conservar somente o interior do contorno com um vtkImageData 
		//preciso transformar o imageData em um vtkImageStencilData
		vtkSmartPointer<vtkImageToImageStencil> imgToSten = vtkSmartPointer<vtkImageToImageStencil>::New();
		imgToSten->SetInputData(sten->GetOutput());
		imgToSten->ThresholdByUpper(254);
		imgToSten->Update();
		sd = imgToSten->GetOutput();
	}
	return sd;
}

void SistemaDeSegmentacao::SetSigmoidAlpha(float a)
{
	this->sigmoidAlpha = a;
}
