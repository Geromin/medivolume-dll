#pragma once
#include "SistemaDeSegmentacaoInterface.h"
#include <array>
#include <string>
using namespace std;
class SegmentacaoRPC : public SistemaDeSegmentacaoInterface
{
private:
	float sigmoidAlpha;
	int fastMarchDistance;
	itk::Image<short, 3>::Pointer imageInput;
	array<double, 3> seed;
	ManterDentroOuFora inOrOut;
	short sigmoidRange;
	bool debugMode;
	string serverName, port, bufferName;
public:
	void SetPropriedadesDeConexao(string exec, string port, string buffer)
	{
		this->serverName = exec;
		this->port = port;
		this->bufferName = buffer;
	}

	itk::Image<short, 3>::Pointer GetITKOutput()
	{
		assert(false && "n�o implementado");
		return nullptr;
	}
	SegmentacaoRPC();
	vtkSmartPointer<vtkImageStencilData> GetOutput();
	void Segmentar();
	void SetDentroOuFora(ManterDentroOuFora f);
	void SetDistanciaDoFastMarch(int dist);
	void SetInput(itk::Image<short, 3>::Pointer in);
	void SetSeed(std::array<double, 3> pos);
	void SetSigmoidAlpha(float a);
};