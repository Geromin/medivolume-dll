#pragma once

#include <itkImage.h>
#include <vtkSmartPointer.h>
#include <vtkImageStencilData.h>
#include <array>
#include <vtkPolyData.h>
#include "SistemaDeSegmentacaoInterface.h"

//#include <itktypes.h>
using namespace std;

typedef itk::Image<float, 3> FloatImage;
typedef itk::Image<short, 3> ShortImage;
class SistemaDeSegmentacao : public SistemaDeSegmentacaoInterface
{
private:
	ShortImage::Pointer sigmoidResult;
	itk::Image<unsigned short, 3>::Pointer fastMarchResult;
	itk::Image<unsigned short, 3>::Pointer thresholdResult;
	itk::Image<unsigned short, 3>::Pointer dilatateResult;

	vtkSmartPointer<vtkPolyData> mesh;

	float sigmoidAlpha;
	int fastMarchDistance;
	itk::Image<short, 3>::Pointer imageInput;
	array<double, 3> seed;
	ManterDentroOuFora inOrOut;
	short sigmoidRange;
	bool debugMode;
public:
	itk::Image<short, 3>::Pointer GetITKOutput()
	{
		assert(false && "n�o implementado");
		return nullptr;
	}
	vtkSmartPointer<vtkPolyData> GetMesh(){ return mesh; }

	SistemaDeSegmentacao();
	FloatImage::PixelType GetScalarNoInput(int x, int y, int z);
	void SetDebugMode(bool debugOn);
	void Segmentar() override;
	void SetDistanciaDoFastMarch(int dist) override;
	void SetSeed(array<double, 3> pos) override;
	void SetInput(itk::Image<short, 3>::Pointer in) override;
	void SetDentroOuFora(ManterDentroOuFora f) override;
	vtkSmartPointer<vtkImageStencilData> GetOutput() override;
	void SetSigmoidAlpha(float a) override;
	void ResetData()
	{
		sigmoidResult = nullptr;
		fastMarchResult = nullptr;
		thresholdResult = nullptr;
		dilatateResult = nullptr;
	}
};