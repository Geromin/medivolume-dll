

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Tue Aug 29 14:48:47 2017
 */
/* Compiler settings for C:\medivolume_src\rpc_idl\FastMarchingSegmentation.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, app_config, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __FastMarchingSegmentation_h__
#define __FastMarchingSegmentation_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __FastMarchingSegmentation_INTERFACE_DEFINED__
#define __FastMarchingSegmentation_INTERFACE_DEFINED__

/* interface FastMarchingSegmentation */
/* [version][uuid] */ 

void SetDimensions( 
    /* [in] */ handle_t hBinding,
    /* [in] */ int x,
    /* [in] */ int y,
    /* [in] */ int z);

void SetSpacing( 
    /* [in] */ handle_t hBinding,
    /* [in] */ float x,
    /* [in] */ float y,
    /* [in] */ float z);

void LoadBufferFromTempFile( 
    /* [in] */ handle_t hBinding,
    UINT nSize,
    /* [size_is][string][in] */ char *filename);

void SetMarchingDistance( 
    /* [in] */ handle_t hBinding,
    /* [in] */ int dist);

void AddSeed( 
    /* [in] */ handle_t hBinding,
    /* [in] */ int x,
    /* [in] */ int y,
    /* [in] */ int z);

void SetSegmentationAlpha( 
    /* [in] */ handle_t hBinding,
    float alpha);

void Execute( 
    /* [in] */ handle_t hBinding);

void Die( 
    /* [in] */ handle_t hBinding);



extern RPC_IF_HANDLE FastMarchingSegmentation_v0_1_c_ifspec;
extern RPC_IF_HANDLE FastMarchingSegmentation_v0_1_s_ifspec;
#endif /* __FastMarchingSegmentation_INTERFACE_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


