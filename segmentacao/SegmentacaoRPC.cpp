#include "SegmentacaoRPC.h"
#include "FastMarchingSegmentation.h"
#include <fstream>
#include <vtkSmartPointer.h>
#include <vtkImageImport.h>
#include <itktypes.h>
#include <vtkImageToImageStencil.h>
#include <vtkImageData.h>
#include <vtkImageStencil.h>

SegmentacaoRPC::SegmentacaoRPC()
{
	this->fastMarchDistance = 0;
	this->seed = { -1, -1, -1 };
	this->inOrOut = ManterDentroOuFora::DENTRO;
	this->imageInput = nullptr;
	debugMode = false;
	sigmoidAlpha = 5.0;
	sigmoidRange = 50;
}

vtkSmartPointer<vtkImageStencilData> SegmentacaoRPC::GetOutput()
{
	vtkSmartPointer<vtkImageImport> versaoVTK = CreateVTKImage(imageInput);
	vtkSmartPointer<vtkImageToImageStencil> imageToStencilFilter = vtkSmartPointer<vtkImageToImageStencil>::New();
	imageToStencilFilter->SetInputConnection(versaoVTK->GetOutputPort());
	imageToStencilFilter->ThresholdByUpper(0.5);
	imageToStencilFilter->Update();
	vtkSmartPointer<vtkImageStencilData> sd = imageToStencilFilter->GetOutput();
	if (inOrOut == DENTRO) //Mant�m s� o q est� dentro.
	{
		vtkImageData* propSrc = versaoVTK->GetOutput();
		vtkSmartPointer<vtkImageData> tempImage = vtkSmartPointer<vtkImageData>::New();
		tempImage->SetDimensions(propSrc->GetDimensions());
		tempImage->SetSpacing(propSrc->GetSpacing());
		tempImage->SetOrigin(propSrc->GetOrigin());
		tempImage->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
		memset(tempImage->GetScalarPointer(), 0xFF,
			tempImage->GetDimensions()[0] * tempImage->GetDimensions()[1] * tempImage->GetDimensions()[2]);
		vtkSmartPointer<vtkImageStencil> sten = vtkSmartPointer<vtkImageStencil>::New();
		sten->SetInputData(tempImage);
		sten->SetStencilData(sd);
		sten->ReverseStencilOn();
		sten->SetBackgroundValue(0x00);
		sten->Update();
		//Aqui tenho o stencil para conservar somente o interior do contorno com um vtkImageData
		//preciso transformar o imageData em um vtkImageStencilData
		vtkSmartPointer<vtkImageToImageStencil> imgToSten = vtkSmartPointer<vtkImageToImageStencil>::New();
		imgToSten->SetInputData(sten->GetOutput());
		imgToSten->ThresholdByUpper(254);
		imgToSten->Update();
		sd = imgToSten->GetOutput();
	}
	return sd;
}
//Esse segmentar passa o trabalho pro servidor. Ele marshaliza os dados, grava o buffer no disco, ordena a
//execu��o, recupera o resultado e desliga o server.
void SegmentacaoRPC::Segmentar()
{
	// additional information
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	// set the size of the structures
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	//Inicia o servidor (C:\medivolume_bin\rpc_server\Release\rpc_server.exe)
	// start the program up
	stringstream cmdss;
	cmdss << "\"" << const_cast<char*>(this->port.c_str()) << "\" ";
	CreateProcessA
		(
		const_cast<char*>(this->serverName.c_str()), //"C:\\medivolume_bin\\rpc_server\\Release\\rpc_server.exe",   // the path
		const_cast<char*>(cmdss.str().c_str()),                // Command line
		NULL,                   // Process handle not inheritable
		NULL,                   // Thread handle not inheritable
		FALSE,                  // Set handle inheritance to FALSE
		CREATE_NEW_CONSOLE,     // Opens file in a separate console
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory
		&si,            // Pointer to STARTUPINFO structure
		&pi           // Pointer to PROCESS_INFORMATION structure
		);

	const std::string buffer_path = this->bufferName;// "C:\\meus dicoms\\_rpcbuffer.bin";
	//Conecta ao servidor
	RPC_STATUS status;
	unsigned char* szStringBinding = NULL;
	status = RpcStringBindingCompose(
		NULL, // UUID to bind to.
		reinterpret_cast<unsigned char*>("ncacn_ip_tcp"), // Use TCP/IP protocol.
		reinterpret_cast<unsigned char*>("localhost"), // TCP/IP network address to use.
		reinterpret_cast<unsigned char*>( const_cast<char*>( this->port.c_str())), // TCP/IP port to use.//reinterpret_cast<unsigned char*>("4747"), // TCP/IP port to use.
		NULL, // Protocol dependent network options to use.
		&szStringBinding); // String binding output.
	//Passa os dados
	if (status)
	{
		std::cout << "erro de RPC em RpcStringBindingCompose = " << status << std::endl;
		assert(false);
	}
	handle_t hExample1ExplicitBinding = NULL;
	// Validates the format of the string binding handle and converts it to a binding handle. Connection is not done here either.
	status = RpcBindingFromStringBinding(
		szStringBinding, // The string binding to validate.
		&hExample1ExplicitBinding); // Put the result in the explicit binding handle.
	if (status)
	{
		std::cout << "erro de RPC em RpcStringBindingCompose = " << status << std::endl;
		assert(false);
	}
	//Passagem dos dados
	SetDimensions(hExample1ExplicitBinding, imageInput->GetLargestPossibleRegion().GetSize()[0],
				    imageInput->GetLargestPossibleRegion().GetSize()[1],
					imageInput->GetLargestPossibleRegion().GetSize()[2]);
	SetSpacing(hExample1ExplicitBinding, imageInput->GetSpacing()[0], imageInput->GetSpacing()[1], imageInput->GetSpacing()[2]);
	SetMarchingDistance(hExample1ExplicitBinding, this->fastMarchDistance);
	AddSeed(hExample1ExplicitBinding, this->seed[0], this->seed[1], this->seed[2]);
	SetSegmentationAlpha(hExample1ExplicitBinding, this->sigmoidAlpha);
	//grava a imagem no buffer
	std::fstream bufferFile;
	bufferFile.open(buffer_path.c_str(), fstream::in | fstream::out | fstream::binary | fstream::trunc);
	bufferFile.seekg(0);
	short *dataBuffer = imageInput->GetBufferPointer();
	size_t buffSize = imageInput->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
	bufferFile.seekg(0, ios::beg);
	bufferFile.write(reinterpret_cast<const char*>(dataBuffer), buffSize);
	bufferFile.close();
	const char* _ccs = buffer_path.c_str();
	char* _cs = const_cast<char*>(_ccs);
	LoadBufferFromTempFile(hExample1ExplicitBinding, buffer_path.size()+1, _cs);
	//LoadBufferFromTempFile(hExample1ExplicitBinding,0, 0/*(unsigned char*)(buffer_path.c_str())*/);//t� dando exe��o externa aqui - Erro de protocolo de chamada de procedimento remoto (RPC).
	//Execu��o
	Execute(hExample1ExplicitBinding);
	//Recupera os dados do buffer
	bufferFile.open(buffer_path.c_str(), fstream::in | fstream::binary | fstream::out);
	bufferFile.seekg(0);
	bufferFile.read((char*)imageInput->GetBufferPointer(), imageInput->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
	bufferFile.close();
	//Mata o servidor
	Die(hExample1ExplicitBinding);

}

void SegmentacaoRPC::SetDentroOuFora(ManterDentroOuFora f)
{
	this->inOrOut = f;
}

void SegmentacaoRPC::SetDistanciaDoFastMarch(int dist)
{
	this->fastMarchDistance = dist;
}

void SegmentacaoRPC::SetInput(itk::Image<short, 3>::Pointer in)
{
	this->imageInput = in;
}

void SegmentacaoRPC::SetSeed(std::array<double, 3> pos)
{
	this->seed = pos;
}

void SegmentacaoRPC::SetSigmoidAlpha(float a)
{
	this->sigmoidAlpha = a;
}
// Memory allocation function for RPC.
// The runtime uses these two functions for allocating/deallocating
// enough memory to pass the string to the server.
void* __RPC_USER midl_user_allocate(size_t size)
{
	return malloc(size);
}

// Memory deallocation function for RPC.
void __RPC_USER midl_user_free(void* p)
{
	free(p);
}