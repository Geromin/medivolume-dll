#pragma once
#include <itkImage.h>
#include <vtkSmartPointer.h>
#include <vtkImageStencilData.h>
#include <array>
/**
 * Se DENTRO, indica que � para manter o que est� dentro da regi�o segmentada.
 * Se FORA, indica que � para manter o que est� fora da regi�o segmentada.
 */
enum ManterDentroOuFora{ DENTRO, FORA };
/**
 * Serve para segmentar uma imagem. Ele usa o m�todo de fast march e retorna o resultado
 * como um stencil pronto para ser aplicado sobre um vtkImageData.
 */
class SistemaDeSegmentacaoInterface
{
public:
	virtual itk::Image<short, 3>::Pointer GetITKOutput() = 0;
	/**
	 * Destrutor virtual.
	 */
	virtual ~SistemaDeSegmentacaoInterface() = default;
	/**
	 * Pega o resultado gerado em Segmentar().
	 */
	virtual vtkSmartPointer<vtkImageStencilData> GetOutput() = 0;
	/**
	 * Segmenta a imagem fornecida em SetInput, usando os par�metros determinados
	 * em SetDentroOuFora(), SetSeed(), SetDistanciaDoFastMarch() e SetSigmoidAlpha().
	 */
	virtual void Segmentar() = 0;
	/**
	 * Mant�m dentro ou mant�m fora?
	 */
	virtual void SetDentroOuFora(ManterDentroOuFora f) = 0;
	/**
	 * Qual � a dist�ncia que ser� percorrida at� o fast march parar?
	 * Quanto mais longe, maior a regi�o que ser� potencialmente escolhida.
	 * Para tubos como vasos sangu�neos, o melhor s�o valores altos (na casa
	 * dos milhares em alguns casos). Para estruturas como rins e f�gados, 
	 * numeros menores s�o menores (na casa das dezenas).
	 */
	virtual void SetDistanciaDoFastMarch(int dist) = 0;
	/**
	 * Qual � a imagem que ser� segmentada?
	 */
	virtual void SetInput(itk::Image<short, 3>::Pointer in) = 0;
	/**
	 * Qual � a posi��o no espa�o onde a segmenta��o come�ar�?
	 */
	virtual void SetSeed(std::array<double, 3> pos) = 0;
	/**
	 * Qual � o alpha do sigm�ide (consulte a fun��o sigm�ide na 
	 * internet para saber o significado).
	 */
	virtual void SetSigmoidAlpha(float a) = 0;
	
};