#ifndef _i_renderpasscontrol_h
#define _i_renderpasscontrol_h
namespace tela{
	class IRenderPassControl{
	public:
		virtual void UseDefaultPasses() = 0;
		virtual void UseSeedPass() = 0;
	};
}
#endif