#include "CpuAddScalarToImage.h"
#include <itkImage.h>
#include <itkImageFileReader.h>
using namespace itk;
void cpuAddExample()
{
	//1a parte - Um filtro de adi��o de valor � uma imagem, na CPU.
	typedef Image<unsigned short, 2> TImage;
	typedef ImageFileReader<TImage> TReader;
	TReader::Pointer reader = TReader::New();
	//Carrega 
	//reader->SetFileName("C://meus dicoms//Abd-Pel w-c  3.0  B30f//IM-0001-0052.dcm");
	reader->SetFileName("C://meus dicoms//arterial//MR.1.3.46.670589.11.72124.5.0.4948.2015112611573240600.D7533582200005010109");
	reader->Update();
	//Aplica filtro.
	MyCpuAddScalarToImage<TImage, unsigned  short>::Pointer CpuAddScalars = MyCpuAddScalarToImage<TImage, unsigned  short>::New();
	CpuAddScalars->SetInput(reader->GetOutput());
	CpuAddScalars->SetScalar(100);
	CpuAddScalars->Update();
	//todos os pixels tem que estar iguais
	unsigned short* input = reader->GetOutput()->GetBufferPointer();
	unsigned short* output = CpuAddScalars->GetOutput()->GetBufferPointer();
	for (int i = 0; i < 1000; i++)
	{
		if (input[i] + 100 != output[i])
			throw "diferente!";
	}
	//Tenta salvar

}

int main()
{	
	cpuAddExample();
	
	return EXIT_SUCCESS;
}