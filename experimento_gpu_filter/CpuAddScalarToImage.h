#pragma warning(disable:4996)
#ifndef _cpu_add_scalar_h
#define _cpu_add_scalar_h
#include <iostream>
#include "itkImage.h"
#include <thread>
#include <vector>
#include <algorithm>
#include "itkImageToImageFilter.h"
using namespace std;
namespace itk
{
	template< class TImage, class ScalarType>
	class MyCpuAddScalarToImage :public ImageToImageFilter< TImage, TImage>
	{
	private:
		ScalarType scalarVal;
		MyCpuAddScalarToImage(const Self &); //purposely not implemented
		void operator=(const Self &);  //purposely not implemented

	public:
		/** Standard class typedefs. */
		typedef MyCpuAddScalarToImage             Self;
		typedef ImageToImageFilter< TImage, TImage > Superclass;
		typedef SmartPointer< Self >        Pointer;

		/** Method for creation through the object factory. */
		itkNewMacro(Self);

		/** Run-time type information (and related methods). */
		itkTypeMacro(MyCpuAddScalarToImage, ImageToImageFilter);
		
		void SetScalar(ScalarType v){ scalarVal = v; }
	protected:
		MyCpuAddScalarToImage(){}
		~MyCpuAddScalarToImage(){}

		/** Does the real work. */
		virtual void GenerateData()
		{
			typename TImage::ConstPointer input = this->GetInput();
			typename TImage::Pointer output = this->GetOutput();
			this->AllocateOutputs();
			long t = GetCurrentTime();//O tempo
			vector<thread> workers;//os threads
			TImage::PixelType *inputPixels = const_cast<TImage::PixelType*>( input->GetBufferPointer() );
			TImage::PixelType *outputPixels = const_cast<TImage::PixelType*>(output->GetBufferPointer());
			TImage::RegionType region = input->GetLargestPossibleRegion();
			const int size = region.GetSize()[0] * region.GetSize()[1]; //* region.GetSize()[2];
			const int numThreads = 1;
			for (int i = 0; i < numThreads; i++)//cria os threads
			{
				const int inicio = i * (size / numThreads);
				const int fim = i * (size / numThreads) + (size / numThreads);
				const ScalarType val = scalarVal;
				workers.push_back(thread([outputPixels,inputPixels, inicio, fim, val](){
					for (int i = inicio; i < fim; i++)
						outputPixels[i] = inputPixels[i] + val;
				}));
			}
			for_each(workers.begin(), workers.end(), [](thread &t)
			{
				t.join();
			});
			long tf = GetCurrentTime();
			cout << "Tempo gasto em " << __FUNCTION__ << " = " << (tf - t) << endl;
		}


	};
} //namespace ITK
#endif