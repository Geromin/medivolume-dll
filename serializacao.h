#ifndef __serializacao
#define __serializacao
#include "tinyxml.h"
#include "EstruturaDeCor.h"
#include <string>
namespace serializao_vvt
{
	TiXmlElement* create_root_element(std::string nome);
	TiXmlElement* create_volume_property();
	TiXmlElement* create_component(int numero_componente, int shade, float ambient, float diffuse, float specular, float specular_power);
	TiXmlElement* create_rgb_transfer_function(vr::EstruturaDeCor* e);
	TiXmlElement* create_scalar_opacity_transfer_function(vr::EstruturaDeCor* e);
	TiXmlElement* create_gradient_opacity_transfer_function(vr::EstruturaDeCor* e);
}
#endif
