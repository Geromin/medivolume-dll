#include <vtkSmartPointer.h>
#include <vtkCubeSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkAnnotatedCubeActor.h>

#include <vtkPoints.h>
#include <vtkHexahedron.h>
#include <vtkCellArray.h>
#include <vtkPlaneSource.h>
#include <vtkAssembly.h>
#include "TextureDataProvider.h"

#include <vtkRegularPolygonSource.h>
#include <memory>
#pragma once
using namespace std;
/**
	Representa o cubinho de orientação dentro da tela do cubo.
*/
namespace tela{
	class CuboDeOrientacao{
	private:
		class Face
		{
		private: 
			//A imagem da textura
			vtkSmartPointer<vtkImageData> imageSource;
			//a fonte do plano
			vtkSmartPointer<vtkPlaneSource> plane;
			//o objeto de textura, criado a partir da imagem da textura.
			vtkSmartPointer<vtkTexture> texture;
			//O mapper do plano
			vtkSmartPointer<vtkPolyDataMapper> planeMapper;
			//o plano
			vtkSmartPointer<vtkActor> texturedPlane;
		public:
			Face(std::string filepath, double nx, double ny, double nz);
			Face(unsigned char* buffer, int sz, double nx, double ny, double nz);
			Face(const Face & f);
			vtkSmartPointer<vtkActor> getActor()
			{
				return texturedPlane;
			}
		};
		std::vector<Face> faces;
		vtkSmartPointer<vtkAssembly> assembly;
		
		vtkSmartPointer<vtkRegularPolygonSource> verticalCircleSource;
		vtkSmartPointer<vtkPolyDataMapper> verticalCircleMapper;
		vtkSmartPointer<vtkActor> verticalCircleActor;

		vtkSmartPointer<vtkRegularPolygonSource> horizontalCircleSource;
		vtkSmartPointer<vtkPolyDataMapper> horizontalCircleMapper;
		vtkSmartPointer<vtkActor> horizontalCircleActor;

	public:
		CuboDeOrientacao(TextureDataProvider* textureData);
		vtkSmartPointer<vtkAssembly> GetAtorEx();
		~CuboDeOrientacao()
		{
			
		}

		void SetTexturaLeft(unsigned char* buff, int buffer_size);
		vtkProp3D* GetVerticalCircleActor();
		vtkProp3D* GetHorizontalCircleActor();
		vtkPolyData* GetHorizontalPolydata();
		vtkMatrix4x4* GetHorizontalCircleMatrix();
	};
}