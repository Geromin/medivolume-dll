#ifndef __my_canvas_render_pass_h
#define __my_canvas_render_pass_h

#include <vtkRenderPass.h>
#undef main
#include <SDL2/SDL.h>
#undef main
#include <vtkRenderState.h>
#include <vtkRenderWindow.h>
namespace tela
{
	static const int NumeroDeCanais = 4;
	//Uma classe para desenho - desenha na tela usando um renderpass. Criada originalmente para lidar com o
	//problema de desenhar os contornos de remo��o. No futuro ter� tamb�m o desenho da seed de segmenta��o.
	class MyCanvasRenderPass : public vtkRenderPass
	{
	private:
		int width;
		int height;
		MyCanvasRenderPass();
		~MyCanvasRenderPass();
		unsigned char* canvasBuffer;
		SDL_Surface* canvasSdlSurface;
		SDL_Renderer* canvasSdlRenderer;
		vtkRenderWindow* storedWindowPointer;

		void PurgeCanvasBuffer();
		void BresenhamLine(int x0, int y0, int x1, int y1);
		void NaiveLine(int x0, int y0, int x1, int y1);
		void SdlLine(int x0, int y0, int x1, int y1);

	public:
		void Invalidate()
		{

//			free(canvasBuffer);
//			canvasBuffer = nullptr;
////			free ( canvasSdlSurface );
//			width = -1;
//			height = -1;
//			SDL_DestroyRenderer(canvasSdlRenderer);
		}

		void SetColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

		static MyCanvasRenderPass* New();
		void InitCanvas(vtkRenderWindow* wnd);

		void Clear();
		//Tem que ser rgba sen�o d� ruim
		void Clear(unsigned char* background, int __width=0, int __height=0);

		void Line(int x0, int y0, int x1, int y1);
		void Rectangle(int x0, int y0, int x1, int y1);
		void SetPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b);

		void Render(const vtkRenderState* s);
	};
}
#endif
