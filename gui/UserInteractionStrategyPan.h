#ifndef  __uisp
#define  __uisp
#include "UserInteractionStrategy.h"

class UserInteractionStrategyPan :public UserInteractionStrategy
{
private:
	bool isPressionado;

public:
	void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) override
	{
		if (isPressionado)
		{
			callbackOnMouseMove();
		}
	}
	UserInteractionStrategyPan(const vtkSmartPointer<vtkInteractorStyle>& interactorStyle, callbackOnMouseMove cb)
		: UserInteractionStrategy(interactorStyle, cb)
	{
		nome = "pan";
		isPressionado = false;
	}
	void Start() override
	{
		isPressionado = true;
		interactorStyle->StartPan();
	}

	void End() override
	{
		isPressionado = false;
		interactorStyle->EndPan();
	}
};

#endif
