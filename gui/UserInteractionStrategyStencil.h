#ifndef  __uisstencil
#define  __uisstencil
#include "UserInteractionStrategy.h"
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include "ContourInterface.h"
#include "PontoNaTela.h"

class UserInteractionStrategyStencil :public UserInteractionStrategy
{
private:
	bool isPressionado;
	vector<int> lstX;
	vector<int> lstY;
	ContourInterface* InterfaceDeDesenho;
	vr::VolumeV2* volumeObject;
public:

	//Tenho que pegar o ponto na tela, guard�-lo em uma lista, processar essa lista de acordo com o tipo de contorno e 
	//fazer o que tiver que ser feito pra desenhar.
	void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) override
	{
		this->volumeObject = vol;
		interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetSize();
		//int* windowSize = interactor->GetRenderWindow()->GetSize(); //GetRenderers()->G
		if (isPressionado)
		{
			int* mousePosition = interactor->GetEventPosition();
			mousePosition[1] = mousePosition[1] - 10;
			if (mousePosition[0] < 0)mousePosition[0] = 0;
			if (mousePosition[1] < 0)mousePosition[1] = 0;
			std::cout << "pto " << mousePosition[0] << ", " << mousePosition[1] << std::endl;
			lstX.push_back(mousePosition[0]);
			lstY.push_back( mousePosition[1]);

			InterfaceDeDesenho->DrawLasso(lstX, lstY);
			//if (codTipoRemocao == CodigosDeOperacaoProvider::GetCodigoTipoDeRemocao("laco"))
			//{
			//	InterfaceDeDesenho->DrawLasso(lstX, lstY);
			//}
			//if (codTipoRemocao == CodigosDeOperacaoProvider::GetCodigoTipoDeRemocao("circulo"))
			//{
			//	//Calcula o raio e o centro
			//	int qX0 = lstX[0];
			//	int qY0 = lstY[0];
			//	int qX1 = lstX[lstX.size() - 1];
			//	int qY1 = lstY[lstY.size() - 1];
			//	int radius = trunc(sqrt((qX1 - qX0) * (qX1 - qX0) + (qY1 - qY0)*(qY1 - qY0)));

			//	int wndY = this->interactorStyle->GetInteractor()->GetRenderWindow()->GetSize()[1];
			//	vector<int> ptsX;
			//	vector<int> ptsY;
			//	//pontos y em fun��o de x
			//	//y = sqrt(r2 - (x-h)2) + k com dominio [h-r, h+r]
			//	int x0 = qX0 - radius;
			//	int xf = qX0 + radius;
			//	int h = x0;
			//	int k = qY0;
			//	int _r = pow(radius, 2);
			//	for (int i = x0; i <= xf; i++)
			//	{
			//		int __y = sqrt(radius * radius - (i - h)*(i - h));
			//		ptsX.push_back(i);
			//		ptsY.push_back(wndY - k + __y);
			//	}
			//	for (int i = xf; i >= x0; i--)
			//	{
			//		int __y = sqrt(radius * radius - (i - h)*(i - h));
			//		ptsX.push_back(i);
			//		ptsY.push_back(wndY - k - __y);
			//	}
			//	InterfaceDeDesenho->DrawLasso(ptsX, ptsY);
			//}
			//if (codTipoRemocao == CodigosDeOperacaoProvider::GetCodigoTipoDeRemocao("retangulo"))
			//{
			//	int qX0 = lstX[0];
			//	int qY0 = lstY[0];
			//	int qX1 = lstX[lstX.size() - 1];
			//	int qY1 = lstY[lstY.size() - 1];
			//	vector<int> ptsX;
			//	vector<int> ptsY;
			//	ptsX.push_back(qX0); ptsY.push_back(qY0);
			//	ptsX.push_back(qX1); ptsY.push_back(qY0);
			//	ptsX.push_back(qX1); ptsY.push_back(qY1);
			//	ptsX.push_back(qX0); ptsY.push_back(qY1);
			//	InterfaceDeDesenho->DrawLasso(ptsX, ptsY);
			//}
		}




		//	//lasso vai toda a lista pro drawLa�o
		//	if (codTipoRemocao == GetCodigoTipoDeRemocao("laco"))
		//	{
		//		int* px = new int[lstX.size()];
		//		for (int i = 0; i < lstX.size(); i++)
		//		{
		//			px[i] = lstX[i];
		//		}
		//		int* py = new int[lstY.size()];
		//		for (int i = 0; i < lstY.size(); i++)
		//		{
		//			py[i] = lstY[i];
		//		}
		//		DrawLacoDeCorte(px, py, lstX.size());
		//	}
		//	if (codTipoRemocao == GetCodigoTipoDeRemocao("circulo"))
		//	{
		//		int qX0 = lstX[0];
		//		int qY0 = lstY[0];
		//		int qX1 = lstX[lstX.size() - 1];
		//		int qY1 = lstY[lstY.size() - 1];
		//		int radius = trunc(sqrt((qX1 - qX0) * (qX1 - qX0) + (qY1 - qY0)*(qY1 - qY0)));
		//		DrawCirculoDeCorte(qX0, qY0, radius);
		//	}
		//	if (codTipoRemocao == GetCodigoTipoDeRemocao("retangulo"))
		//	{
		//		int qX0 = lstX[0];
		//		int qY0 = lstY[0];
		//		int qX1 = lstX[lstX.size() - 1];
		//		int qY1 = lstY[lstY.size() - 1];
		//		DrawRectDeCorte(qX0, qY0, qX1, qY1);
		//	}
		//}
	}
	int codTipoRemocao;
	bool keepInside;
	UserInteractionStrategyStencil(const vtkSmartPointer<vtkInteractorStyle>& interactorStyle, callbackOnMouseMove cb,
		int codTipoRemocao, bool keepInside, ContourInterface* interfaceDeDesenho)
		: UserInteractionStrategy(interactorStyle, cb)
	{
		nome = "stencil";
		this->volumeObject = nullptr;
		this->InterfaceDeDesenho = interfaceDeDesenho;
		this->codTipoRemocao = codTipoRemocao;
		this->keepInside = keepInside;
		isPressionado = false;
	}

	void Start() override
	{
		lstX.clear();
		lstY.clear();
		InterfaceDeDesenho->BeginDraw();
		isPressionado = true;
	}

	void End() override
	{
		InterfaceDeDesenho->EndDraw();
		isPressionado = false;
		Contorno contorno;
		memset(&contorno, 0, sizeof(Contorno));
		contorno.KeepInside = this->keepInside;
		contorno.Tamanho = lstX.size();
		if (contorno.Tamanho > 3)
		{
			contorno.X = new int[contorno.Tamanho];
			contorno.Y = new int[contorno.Tamanho];
			for (int i = 0; i < contorno.Tamanho; i++)
			{
				contorno.X[i] = lstX[i];
				contorno.Y[i] = lstY[i];
			}
			volumeObject->RemoverRegiao(&contorno);
		}
		else
		{
			std::cout << "WARNING - Numero de pontos insuficientes para definir um contorno." << std::endl;
		}
		if (contorno.X)
			delete[] contorno.X;
		if (contorno.Y)
			delete[] contorno.Y;
		lstX.clear();
		lstY.clear();
	}
};

#endif
