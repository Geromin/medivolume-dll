#ifndef  __uisr
#define  __uisr
#include "UserInteractionStrategy.h"
#include "CameraRotationListener.h"
#include "vtkRendererCollection.h"
#include "vtkRenderer.h"
#include <vector>
class UserInteractionStrategyRotate :public UserInteractionStrategy
{
	vector< tela::VolumeCameraRotationListener* >  RotationListeners;
	
public:
	bool isPressionado;
	void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) override
	{
		if (isPressionado)
		{
			// ReSharper disable once CppExpressionStatementsWithoudSideEffects
			onMouseMoveCallback;
			for (unsigned int i = 0; i < RotationListeners.size(); i++)
			{
				RotationListeners[i]->Execute(interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera());
			}
		}
	}
	UserInteractionStrategyRotate(const vtkSmartPointer<vtkInteractorStyle>& interactorStyle, callbackOnMouseMove cb,
		vector< tela::VolumeCameraRotationListener* > CameraRotationListeners)
		: UserInteractionStrategy(interactorStyle, cb)
	{
		this->RotationListeners = CameraRotationListeners;
		nome = "rotate";
		isPressionado = false;
	}

	void Start() override
	{
		isPressionado = true;
		interactorStyle->StartRotate();
	}

	void End() override
	{
		isPressionado = false;
		interactorStyle->EndRotate();
	}
};

#endif
