#ifndef  __uisw
#define  __uisw
#include "UserInteractionStrategy.h"

class UserInteractionStrategyWindow :public UserInteractionStrategy
{
	bool isMousePressed;
public:
	UserInteractionStrategyWindow(const vtkSmartPointer<vtkInteractorStyle>& interactorStyle, callbackOnMouseMove cb)
		: UserInteractionStrategy(interactorStyle, cb)
	{
		nome = "window";
		isMousePressed = false;
	}

	void Start() override
	{
		isMousePressed = true;
	}

	//Aqui, se o mouse estiver pressionado eu devo mudar a janela da imagem de acordo com o 
	//deslocamento do mouse.
	void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) override
	{
		if (isMousePressed)
		{
			int* m1 = interactor->GetEventPosition();
			int* m0 = interactor->GetLastEventPosition();

			float dx = 0;
			dx = (m1[0] - m0[0])*0.75;
			float dy = 0;
			dy = (m1[1] - m0[1])*0.75;

			int w = vol->GetVisualizacao()->GetWindowWidth() + dx;
			if (w <= 0)
				w = 1;
			int l = vol->GetVisualizacao()->GetWindowLevel() + dy;
			vol->GetVisualizacao()->SetWindow(w,l);
			interactor->GetRenderWindow()->Render();
		}
	}
	void End() override
	{
		isMousePressed = false;
	}
};

#endif
