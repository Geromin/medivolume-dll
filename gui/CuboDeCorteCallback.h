#include <vtkSmartPointer.h>
#include <vtkCommand.h>
#include <vtkObject.h>
#include "CuboDeCorteChangeListener.h"
#include <vector>
#include <memory>
#pragma once

using namespace std;
namespace extrusao{
	class CuboDeCorteCallback : public vtkCommand{
	private:
		vector<CuboDeCorteChangeListener* > CuboChangeListeners;
	public:
		static CuboDeCorteCallback *New(){ return new CuboDeCorteCallback(); }
		void Execute(vtkObject* caller, unsigned long ev, void* data);
		void AddCuboChangeListener(CuboDeCorteChangeListener* c);
	};
}