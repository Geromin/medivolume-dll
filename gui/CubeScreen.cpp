#include "CubeScreen.h"
#include "UtilitiesV2.h"
#include <vtkCamera.h>

void CubeScreen::Execute(vtkSmartPointer<vtkCamera> cameraDoVolume,  float offsetDoPlano)
{
	double* sceneAngle = cameraDoVolume->GetOrientation();
	
	cuboDeOrientacao->GetAtorEx()->SetOrientation(sceneAngle);
	cuboDeOrientacao->GetVerticalCircleActor()->SetOrientation(sceneAngle);
	cuboDeOrientacao->GetHorizontalCircleActor()->SetOrientation(sceneAngle);
	renderWindow->Render();
	
}

CubeScreen::CubeScreen(HWND handle, HDC dc, HGLRC glrc, TextureDataProvider* texProvider)
{
	canRender = false;
	renderer = vtkRenderer::New();
	renderer->SetBackground(0, 0, 0);
	renderWindow = vtkWin32OpenGLRenderWindow::New();
	renderWindow->InitializeFromCurrentContext();
	renderWindow->AddRenderer(renderer);
	interactor = vtkWin32RenderWindowInteractor::New();
	interactor->SetRenderWindow(renderWindow);
	interactor->SetInstallMessageProc(false);//sem isso trava no Ativar();
	interactor->Initialize();

	cuboDeOrientacao = make_unique<tela::CuboDeOrientacao>(texProvider);
	renderer->AddViewProp(cuboDeOrientacao->GetAtorEx());
	//renderer->AddViewProp(cuboDeOrientacao->GetVerticalCircleActor());
	//renderer->AddViewProp(cuboDeOrientacao->GetHorizontalCircleActor());

	renderer->ResetCamera();
	renderer->GetActiveCamera()->Zoom(0.5);
	renderer->GetActiveCamera()->ParallelProjectionOn();
}

CubeScreen::CubeScreen(HWND handle)
{
	assert(false);//inutil
}

CubeScreen::~CubeScreen()
{
	FunctionIn;
	interactor->ExitCallback();
	interactor->Delete();
	renderWindow->MakeCurrent();
	renderWindow->Clean();
	renderWindow->Finalize();
	renderer->Delete();
	renderWindow->Delete();
	FunctionOut;
}

void CubeScreen::Ativar()
{
	FunctionIn;
	canRender = true;
	//renderWindow->Render();
	//Resize(renderWindow->GetSize()[0], renderWindow->GetSize()[1]);
	interactor->Start();
	FunctionOut;
}

void CubeScreen::Desativar()
{
	FunctionIn;
	canRender = false;
	interactor->Disable();
	renderWindow->Finalize();
	//renderer->ReleaseGraphicsResources(renderWindow);
	FunctionOut;
}

void CubeScreen::ForceRender()
{
	FunctionIn;
	if (canRender)
		renderWindow->Render();
	FunctionOut;
}

void CubeScreen::Resize(int width, int height)
{
	renderWindow->SetSize(width, height);
	renderWindow->Modified();
}



vtkPolyData* CubeScreen::GetPolydataDoCirculoHorizontal()
{
	return cuboDeOrientacao->GetHorizontalPolydata();
}