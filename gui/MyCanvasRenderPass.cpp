#include "MyCanvasRenderPass.h"
#include <vtkObjectFactory.h>
#include <vtkOpenGLRenderer.h>
#include "assert.h"
#undef main
#include "SDL2/SDL.h" // SDL.h>

namespace tela
{
	vtkStandardNewMacro(MyCanvasRenderPass);

	MyCanvasRenderPass::MyCanvasRenderPass()
	{
		canvasBuffer = nullptr;
		storedWindowPointer = nullptr;
		width = -1;
		height = -1;
	}

	MyCanvasRenderPass::~MyCanvasRenderPass()
	{
		free(canvasBuffer);
	}

	void MyCanvasRenderPass::Clear()
	{
		SDL_RenderClear(canvasSdlRenderer);
	}

	void MyCanvasRenderPass::Clear(unsigned char* background, int __width, int __height )
	{

		//std::  << __FUNCTION__ << " w:" << width << " , " << height << std::endl;
		memcpy(canvasBuffer, background, __width * __height * NumeroDeCanais * sizeof(unsigned char));
	}

	void MyCanvasRenderPass::SetPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b)
	{
		assert(false);
	}
	
	void  MyCanvasRenderPass::BresenhamLine(int x0, int y0, int x1, int y1)
	{
		assert(false);
	}

	void MyCanvasRenderPass::NaiveLine(int x0, int y0, int x1, int y1)
	{
		assert(false);
	}

	void MyCanvasRenderPass::SetColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
	{
		if (SDL_SetRenderDrawColor(canvasSdlRenderer, r, g, b, a))
			assert(false);
	}


	void MyCanvasRenderPass::SdlLine(int x0, int y0, int x1, int y1)
	{
		assert(canvasSdlRenderer != nullptr);
		SDL_RenderDrawLine(canvasSdlRenderer, x0, y0, x1, y1);
	}

	void MyCanvasRenderPass::Line(int x0, int y0, int x1, int y1)
	{
		SdlLine(x0, y0, x1, y1);
	}

	void MyCanvasRenderPass::Rectangle(int x0, int y0, int x1, int y1)
	{
		SDL_Rect rect;
		rect.x = x0;
		rect.y = y0;
		rect.w = x1 - x0;
		rect.h = y1 - y0;
		SDL_RenderDrawRect(canvasSdlRenderer, &rect);
	}
	void MyCanvasRenderPass::PurgeCanvasBuffer()
	{
		if (canvasBuffer == nullptr)
			free(canvasBuffer);
		canvasBuffer = (unsigned char*)malloc(width * height * NumeroDeCanais * sizeof(unsigned char));
	}
	void MyCanvasRenderPass::InitCanvas(vtkRenderWindow* wnd)
	{
		//Guarda os tamanhos da tela.
		width = wnd->GetSize()[0];
		height = wnd->GetSize()[1];

		//aloca a array
		PurgeCanvasBuffer();
		//cria a infraestrutura da sdl;
		this->canvasSdlSurface = SDL_CreateRGBSurfaceFrom(canvasBuffer, width, height, 32, width*NumeroDeCanais, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
		if (canvasSdlSurface==nullptr)
		{
			assert(false);//throw CreateException(SDL_GetError(), __FUNCTION__);
		}
		this->canvasSdlRenderer = SDL_CreateSoftwareRenderer(canvasSdlSurface);
		if (canvasSdlRenderer==nullptr)
		{
			assert(false);// throw CreateException(SDL_GetError(), __FUNCTION__);
		}
		SetColor(0xff, 0xff, 0xff, 0xff);
		Clear();
	}

	void MyCanvasRenderPass::Render(const vtkRenderState* s)
	{
		vtkOpenGLRenderer* glRen = vtkOpenGLRenderer::SafeDownCast(s->GetRenderer());
		vtkRenderWindow* wnd = glRen->GetRenderWindow();
		if (canvasBuffer == nullptr)
		{
			storedWindowPointer = wnd;
			InitCanvas(wnd);
		}
		wnd->SetRGBACharPixelData(0, 0, width - 1, height - 1, canvasBuffer, 0);
	}
}