#include "LetrasDeOrientacao.h"
#include <vtkTextProperty.h>
#include <vtkCamera.h>
#include <vtkGeneralTransform.h>
#include <vtkPlane.h>
#include <vtkMath.h>
#include <array>
#include <vtkTransform.h>
using namespace  std;

void Letras::Desligar()
{
	letraEsquerda->VisibilityOff();
	letraDireita->VisibilityOff();
	letraSuperior->VisibilityOff();
	letraInferior->VisibilityOff();
}

void Letras::Ligar()
{
	letraEsquerda->VisibilityOn();
	letraDireita->VisibilityOn();
	letraSuperior->VisibilityOn();
	letraInferior->VisibilityOn();
}

void Letras::Calculate(std::array<double, 3> normalizedAxis)
{
//Gera os direction cosines pra fazer a matrix.
	int majorAxis;
	std::array<double, 3> c0;
	std::array<double, 3> c1;
	if (abs(normalizedAxis[0]) > abs(normalizedAxis[1]) && abs(normalizedAxis[0]) > abs(normalizedAxis[2]))
		majorAxis = 0;
	if (abs(normalizedAxis[1]) > abs(normalizedAxis[0]) && abs(normalizedAxis[1]) > abs(normalizedAxis[2]))
		majorAxis = 1;
	if (abs(normalizedAxis[2]) > abs(normalizedAxis[0]) && abs(normalizedAxis[2]) > abs(normalizedAxis[1]))
		majorAxis = 2;
	if (majorAxis == 0)
	{
		c0 = { 0, 1, 0 };
		c1 = { 0, 0, 1 };
	}
	if (majorAxis == 1)
	{
		c0 = { 1, 0, 0 };
		c1 = { 0, 0, 1 };
	}
	if (majorAxis == 2)
	{
		c0 = { 1, 0, 0 };
		c1 = { 0, 1, 0 };
	}

	//Aqui eu tenho os vetores apropriados para as contas que ser�o feitas. Agora vou calcular os
	//novos vetores. c0' = c1 x axis; c1' = c0 x axis
	array<double, 3> c0new, c1new;
	vtkMath::Cross(c1.data(), normalizedAxis.data(), c0new.data());
	vtkMath::Cross(c0.data(), normalizedAxis.data(), c1new.data());
	vtkMath::Normalize(c0new.data());
	vtkMath::Normalize(c1new.data());
	//Com os direction cosines transformados via produtos vetoriais talvez eu j� possa fazer o teste
	//O c0 � o da horizontal da tela e o c1 � o da vertical da tela, enquanto axis � o de dentro da tela.
	//Na vers�o original do m�todo eu rotacionava uma s�rie de vetores basicos usando o quaternion
	//da orienta��o da c�mera.
	const double origin[] = { 0, 0, 0 }; // A origem do sistema|
	const float tam = 0.5;
	{
		vtkSmartPointer<vtkPlane> planoEsquerdo = vtkSmartPointer<vtkPlane>::New();
		planoEsquerdo->SetOrigin(-tam, 0, 0);
		planoEsquerdo->SetNormal(1, 0, 0);
		double p;
		double x[3];
		std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
		int iL = planoEsquerdo->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
		int iR = planoEsquerdo->IntersectWithLine((double*)origin, c0new.data(), p, x);
		//int iT = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		//int iB = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		//int iA = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		//int iP = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letraLadoEsquerdo = "";
		if (iL) letraLadoEsquerdo = letraLadoEsquerdo + "L";
		if (iR) letraLadoEsquerdo = letraLadoEsquerdo + "R";
		//if (iT) letraLadoEsquerdo = letraLadoEsquerdo + "H";
		//if (iB) letraLadoEsquerdo = letraLadoEsquerdo + "F";
		//if (iA) letraLadoEsquerdo = letraLadoEsquerdo + "A";
		//if (iP) letraLadoEsquerdo = letraLadoEsquerdo + "P";
		cout << "Lado esquerdo = " << letraLadoEsquerdo << endl;
		letraEsquerda->SetInput(letraLadoEsquerdo.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoDireito = vtkSmartPointer<vtkPlane>::New();
		planoDireito->SetOrigin(tam, 0, 0);
		planoDireito->SetNormal(-1, 0, 0);
		double p;
		double x[3];
		std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
		int iL = planoDireito->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
		int iR = planoDireito->IntersectWithLine((double*)origin, c0new.data(), p, x);
		//int iT = planoDireito->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		//int iB = planoDireito->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		//int iA = planoDireito->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		//int iP = planoDireito->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		////v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		//if (iT) letra = letra + "H";
		//if (iB) letra = letra + "F";
		//if (iA) letra = letra + "A";
		//if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraDireita->SetInput(letra.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoSuperior = vtkSmartPointer<vtkPlane>::New();
		planoSuperior->SetOrigin(0, tam, 0);
		planoSuperior->SetNormal(0, -1, 0);
		double p;
		double x[3];
		std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
		int iL = planoSuperior->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
		int iR = planoSuperior->IntersectWithLine((double*)origin, c0new.data(), p, x);
		//int iT = planoSuperior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		//int iB = planoSuperior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		//int iA = planoSuperior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		//int iP = planoSuperior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		////v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		//if (iT) letra = letra + "H";
		//if (iB) letra = letra + "F";
		//if (iA) letra = letra + "A";
		//if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraSuperior->SetInput(letra.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoInferior = vtkSmartPointer<vtkPlane>::New();
		planoInferior->SetOrigin(0, -tam, 0);
		planoInferior->SetNormal(0, 1, 0);
		double p;
		double x[3];
		std::array<double, 3> inverseC0 = { -c0new[0], -c0new[1], -c0new[2], };
		int iL = planoInferior->IntersectWithLine((double*)origin, inverseC0.data(), p, x);
		int iR = planoInferior->IntersectWithLine((double*)origin, c0new.data(), p, x);
		//int iT = planoInferior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		//int iB = planoInferior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		//int iA = planoInferior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		//int iP = planoInferior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		////v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		//if (iT) letra = letra + "H";
		//if (iB) letra = letra + "F";
		//if (iA) letra = letra + "A";
		//if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraInferior->SetInput(letra.c_str());
	}




	////Com os direction cosines monta a matriz
	//vtkSmartPointer<vtkMatrix4x4> rotationMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
	////reslicer->SetResliceAxesDirectionCosines(c0new.data(), c1new.data(), normalizedAxis.data());
	//rotationMatrix->SetElement(0, 0, c0new[0]);
	//rotationMatrix->SetElement(1, 0, c0new[1]);
	//rotationMatrix->SetElement(2, 0, c0new[2]);
	//rotationMatrix->SetElement(3, 0, 0);

	//rotationMatrix->SetElement(0, 1, c1new[0]);
	//rotationMatrix->SetElement(1, 1, c1new[1]);
	//rotationMatrix->SetElement(2, 1, c1new[2]);
	//rotationMatrix->SetElement(3, 1, 0);

	//rotationMatrix->SetElement(0, 2, normalizedAxis[0]);
	//rotationMatrix->SetElement(1, 2, normalizedAxis[1]);
	//rotationMatrix->SetElement(2, 2, normalizedAxis[2]);
	//rotationMatrix->SetElement(3, 2, 0);

	//rotationMatrix->SetElement(0, 3, 0);
	//rotationMatrix->SetElement(1, 3, 0);
	//rotationMatrix->SetElement(2, 3, 0);
	//rotationMatrix->SetElement(3, 3, 1);

	//vtkSmartPointer<vtkTransform> _rotationTransform = vtkSmartPointer<vtkTransform>::New();
	//_rotationTransform->SetMatrix(rotationMatrix);
	//_rotationTransform->Update();
	//////Se der errado usar o vetor como o axis de um quaternion.
	////define algunas constantes:
	//const double origin[] = { 0, 0, 0 }; // A origem do sistema
	//const double nLeft[] = { 1, 0, 0 };
	//const double nRight[] = { -1, 0, 0 };
	//const double nTop[] = { 0, 1, 0 };
	//const double nBottom[] = { 0, -1, 0 };
	//const double nAnterior[] = { 0, 0, 1 };
	//const double nPosterior[] = { 0, 0, -1 };
	////Transforma as normais usando a transforma��o criada acima
	//double _newLeft[3], _newRight[3], _newTop[3], _newBottom[3], _newAnterior[3], _newPosterior[3];
	//_rotationTransform->TransformVectorAtPoint(origin, nLeft, _newLeft);
	//_rotationTransform->TransformVectorAtPoint(origin, nRight, _newRight);
	//_rotationTransform->TransformVectorAtPoint(origin, nTop, _newTop);
	//_rotationTransform->TransformVectorAtPoint(origin, nBottom, _newBottom);
	//_rotationTransform->TransformVectorAtPoint(origin, nAnterior, _newAnterior);
	//_rotationTransform->TransformVectorAtPoint(origin, nPosterior, _newPosterior);
	//const float tam = 0.5;
	//{
	//	vtkSmartPointer<vtkPlane> planoEsquerdo = vtkSmartPointer<vtkPlane>::New();
	//	planoEsquerdo->SetOrigin(-tam, 0, 0);
	//	planoEsquerdo->SetNormal(1, 0, 0);
	//	double p;
	//	double x[3];
	//	int iL = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
	//	int iR = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
	//	int iT = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
	//	int iB = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
	//	int iA = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
	//	int iP = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
	//	//v� o que est� acontecendo
	//	string letraLadoEsquerdo = "";
	//	if (iL) letraLadoEsquerdo = letraLadoEsquerdo + "L";
	//	if (iR) letraLadoEsquerdo = letraLadoEsquerdo + "R";
	//	if (iT) letraLadoEsquerdo = letraLadoEsquerdo + "H";
	//	if (iB) letraLadoEsquerdo = letraLadoEsquerdo + "F";
	//	if (iA) letraLadoEsquerdo = letraLadoEsquerdo + "A";
	//	if (iP) letraLadoEsquerdo = letraLadoEsquerdo + "P";
	//	cout << "Lado esquerdo = " << letraLadoEsquerdo << endl;
	//	letraEsquerda->SetInput(letraLadoEsquerdo.c_str());
	//}
	//{
	//	vtkSmartPointer<vtkPlane> planoDireito = vtkSmartPointer<vtkPlane>::New();
	//	planoDireito->SetOrigin(tam, 0, 0);
	//	planoDireito->SetNormal(-1, 0, 0);
	//	double p;
	//	double x[3];
	//	int iL = planoDireito->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
	//	int iR = planoDireito->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
	//	int iT = planoDireito->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
	//	int iB = planoDireito->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
	//	int iA = planoDireito->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
	//	int iP = planoDireito->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
	//	//v� o que est� acontecendo
	//	string letra = "";
	//	if (iL) letra = letra + "L";
	//	if (iR) letra = letra + "R";
	//	if (iT) letra = letra + "H";
	//	if (iB) letra = letra + "F";
	//	if (iA) letra = letra + "A";
	//	if (iP) letra = letra + "P";
	//	cout << "Lado esquerdo = " << letra << endl;
	//	letraDireita->SetInput(letra.c_str());
	//}
	//{
	//	vtkSmartPointer<vtkPlane> planoSuperior = vtkSmartPointer<vtkPlane>::New();
	//	planoSuperior->SetOrigin(0, tam, 0);
	//	planoSuperior->SetNormal(0, -1, 0);
	//	double p;
	//	double x[3];
	//	int iL = planoSuperior->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
	//	int iR = planoSuperior->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
	//	int iT = planoSuperior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
	//	int iB = planoSuperior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
	//	int iA = planoSuperior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
	//	int iP = planoSuperior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
	//	//v� o que est� acontecendo
	//	string letra = "";
	//	if (iL) letra = letra + "L";
	//	if (iR) letra = letra + "R";
	//	if (iT) letra = letra + "H";
	//	if (iB) letra = letra + "F";
	//	if (iA) letra = letra + "A";
	//	if (iP) letra = letra + "P";
	//	cout << "Lado esquerdo = " << letra << endl;
	//	letraSuperior->SetInput(letra.c_str());
	//}
	//{
	//	vtkSmartPointer<vtkPlane> planoInferior = vtkSmartPointer<vtkPlane>::New();
	//	planoInferior->SetOrigin(0, -tam, 0);
	//	planoInferior->SetNormal(0, 1, 0);
	//	double p;
	//	double x[3];
	//	int iL = planoInferior->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
	//	int iR = planoInferior->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
	//	int iT = planoInferior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
	//	int iB = planoInferior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
	//	int iA = planoInferior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
	//	int iP = planoInferior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
	//	//v� o que est� acontecendo
	//	string letra = "";
	//	if (iL) letra = letra + "L";
	//	if (iR) letra = letra + "R";
	//	if (iT) letra = letra + "H";
	//	if (iB) letra = letra + "F";
	//	if (iA) letra = letra + "A";
	//	if (iP) letra = letra + "P";
	//	cout << "Lado esquerdo = " << letra << endl;
	//	letraInferior->SetInput(letra.c_str());
	//}
}

void Letras::CalculateLetras(double* planeNormal)
{
	double *_cameraOrientation;
	//pega a orienta��o da c�mera e cria uma transform.
	_cameraOrientation = nullptr;// cameraDoVolume->GetOrientationWXYZ();
	vtkSmartPointer<vtkGeneralTransform> _rotationTransform = vtkSmartPointer<vtkGeneralTransform>::New();
	_rotationTransform->RotateWXYZ(_cameraOrientation[0], _cameraOrientation[1], _cameraOrientation[2], _cameraOrientation[3]);
	_rotationTransform->Update();
	//define algunas constantes:
	const double origin[] = { 0, 0, 0 }; // A origem do sistema
	const double nLeft[] = { 1, 0, 0 };
	const double nRight[] = { -1, 0, 0 };
	const double nTop[] = { 0, 1, 0 };
	const double nBottom[] = { 0, -1, 0 };
	const double nAnterior[] = { 0, 0, 1 };
	const double nPosterior[] = { 0, 0, -1 };
	//Transforma as normais usando a transforma��o criada acima
	double _newLeft[3], _newRight[3], _newTop[3], _newBottom[3], _newAnterior[3], _newPosterior[3];
	_rotationTransform->TransformVectorAtPoint(origin, nLeft, _newLeft);
	_rotationTransform->TransformVectorAtPoint(origin, nRight, _newRight);
	_rotationTransform->TransformVectorAtPoint(origin, nTop, _newTop);
	_rotationTransform->TransformVectorAtPoint(origin, nBottom, _newBottom);
	_rotationTransform->TransformVectorAtPoint(origin, nAnterior, _newAnterior);
	_rotationTransform->TransformVectorAtPoint(origin, nPosterior, _newPosterior);
	const float tam = 0.5;
	{
		vtkSmartPointer<vtkPlane> planoEsquerdo = vtkSmartPointer<vtkPlane>::New();
		planoEsquerdo->SetOrigin(-tam, 0, 0);
		planoEsquerdo->SetNormal(1, 0, 0);
		double p;
		double x[3];
		int iL = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letraLadoEsquerdo = "";
		if (iL) letraLadoEsquerdo = letraLadoEsquerdo + "L";
		if (iR) letraLadoEsquerdo = letraLadoEsquerdo + "R";
		if (iT) letraLadoEsquerdo = letraLadoEsquerdo + "H";
		if (iB) letraLadoEsquerdo = letraLadoEsquerdo + "F";
		if (iA) letraLadoEsquerdo = letraLadoEsquerdo + "A";
		if (iP) letraLadoEsquerdo = letraLadoEsquerdo + "P";
		cout << "Lado esquerdo = " << letraLadoEsquerdo << endl;
		letraEsquerda->SetInput(letraLadoEsquerdo.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoDireito = vtkSmartPointer<vtkPlane>::New();
		planoDireito->SetOrigin(tam, 0, 0);
		planoDireito->SetNormal(-1, 0, 0);
		double p;
		double x[3];
		int iL = planoDireito->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoDireito->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoDireito->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoDireito->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoDireito->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoDireito->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		if (iT) letra = letra + "H";
		if (iB) letra = letra + "F";
		if (iA) letra = letra + "A";
		if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraDireita->SetInput(letra.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoSuperior = vtkSmartPointer<vtkPlane>::New();
		planoSuperior->SetOrigin(0, tam, 0);
		planoSuperior->SetNormal(0, -1, 0);
		double p;
		double x[3];
		int iL = planoSuperior->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoSuperior->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoSuperior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoSuperior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoSuperior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoSuperior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		if (iT) letra = letra + "H";
		if (iB) letra = letra + "F";
		if (iA) letra = letra + "A";
		if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraSuperior->SetInput(letra.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoInferior = vtkSmartPointer<vtkPlane>::New();
		planoInferior->SetOrigin(0, -tam, 0);
		planoInferior->SetNormal(0, 1, 0);
		double p;
		double x[3];
		int iL = planoInferior->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoInferior->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoInferior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoInferior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoInferior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoInferior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		if (iT) letra = letra + "H";
		if (iB) letra = letra + "F";
		if (iA) letra = letra + "A";
		if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraInferior->SetInput(letra.c_str());
	}

}

void Letras::Execute(vtkSmartPointer<vtkCamera> cameraDoVolume, float offsetDoPlano)
{
	double *_cameraOrientation;
	//pega a orienta��o da c�mera e cria uma transform.
	_cameraOrientation = cameraDoVolume->GetOrientationWXYZ();
	vtkSmartPointer<vtkGeneralTransform> _rotationTransform = vtkSmartPointer<vtkGeneralTransform>::New();
	_rotationTransform->RotateWXYZ(_cameraOrientation[0], _cameraOrientation[1], _cameraOrientation[2], _cameraOrientation[3]);
	_rotationTransform->Update();
	//define algunas constantes:
	const double origin[] = { 0, 0, 0 }; // A origem do sistema
	const double nLeft[] = { 1, 0, 0 };
	const double nRight[] = { -1, 0, 0 };
	const double nTop[] = { 0, 1, 0 };
	const double nBottom[] = { 0, -1, 0 };
	const double nAnterior[] = { 0, 0, 1 };
	const double nPosterior[] = { 0, 0, -1 };
	//Transforma as normais usando a transforma��o criada acima
	double _newLeft[3], _newRight[3], _newTop[3], _newBottom[3], _newAnterior[3], _newPosterior[3];
	_rotationTransform->TransformVectorAtPoint(origin, nLeft, _newLeft);
	_rotationTransform->TransformVectorAtPoint(origin, nRight,_newRight );
	_rotationTransform->TransformVectorAtPoint(origin, nTop, _newTop);
	_rotationTransform->TransformVectorAtPoint(origin, nBottom, _newBottom);
	_rotationTransform->TransformVectorAtPoint(origin, nAnterior, _newAnterior);
	_rotationTransform->TransformVectorAtPoint(origin, nPosterior, _newPosterior);
	const float tam = 0.5;
	{
		vtkSmartPointer<vtkPlane> planoEsquerdo = vtkSmartPointer<vtkPlane>::New();
		planoEsquerdo->SetOrigin(-tam, 0, 0);
		planoEsquerdo->SetNormal(1, 0, 0);
		double p;
		double x[3];
		int iL = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoEsquerdo->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letraLadoEsquerdo = "";
		if (iL) letraLadoEsquerdo = letraLadoEsquerdo + "L";
		if (iR) letraLadoEsquerdo = letraLadoEsquerdo + "R";
		if (iT) letraLadoEsquerdo = letraLadoEsquerdo + "H";
		if (iB) letraLadoEsquerdo = letraLadoEsquerdo + "F";
		if (iA) letraLadoEsquerdo = letraLadoEsquerdo + "A";
		if (iP) letraLadoEsquerdo = letraLadoEsquerdo + "P";
		cout << "Lado esquerdo = " << letraLadoEsquerdo << endl;
		letraEsquerda->SetInput(letraLadoEsquerdo.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoDireito = vtkSmartPointer<vtkPlane>::New();
		planoDireito->SetOrigin(tam, 0, 0);
		planoDireito->SetNormal(-1, 0, 0);
		double p;
		double x[3];
		int iL = planoDireito->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoDireito->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoDireito->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoDireito->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoDireito->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoDireito->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		if (iT) letra = letra + "H";
		if (iB) letra = letra + "F";
		if (iA) letra = letra + "A";
		if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraDireita->SetInput(letra.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoSuperior = vtkSmartPointer<vtkPlane>::New();
		planoSuperior->SetOrigin(0, tam, 0);
		planoSuperior->SetNormal(0, -1, 0);
		double p;
		double x[3];
		int iL = planoSuperior->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoSuperior->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoSuperior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoSuperior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoSuperior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoSuperior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		if (iT) letra = letra + "H";
		if (iB) letra = letra + "F";
		if (iA) letra = letra + "A";
		if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraSuperior->SetInput(letra.c_str());
	}
	{
		vtkSmartPointer<vtkPlane> planoInferior = vtkSmartPointer<vtkPlane>::New();
		planoInferior->SetOrigin(0, -tam, 0);
		planoInferior->SetNormal(0, 1, 0);
		double p;
		double x[3];
		int iL = planoInferior->IntersectWithLine((double*)origin, (double*)_newLeft, p, x);
		int iR = planoInferior->IntersectWithLine((double*)origin, (double*)_newRight, p, x);
		int iT = planoInferior->IntersectWithLine((double*)origin, (double*)_newTop, p, x);
		int iB = planoInferior->IntersectWithLine((double*)origin, (double*)_newBottom, p, x);
		int iA = planoInferior->IntersectWithLine((double*)origin, (double*)_newAnterior, p, x);
		int iP = planoInferior->IntersectWithLine((double*)origin, (double*)_newPosterior, p, x);
		//v� o que est� acontecendo
		string letra = "";
		if (iL) letra = letra + "L";
		if (iR) letra = letra + "R";
		if (iT) letra = letra + "H";
		if (iB) letra = letra + "F";
		if (iA) letra = letra + "A";
		if (iP) letra = letra + "P";
		cout << "Lado esquerdo = " << letra << endl;
		letraInferior->SetInput(letra.c_str());
	}
}

void Letras::Posicionar(vtkRenderer* targetRenderer)
{
	int* screenSize = targetRenderer->GetSize();
	letraEsquerda->SetDisplayPosition(20, screenSize[1] / 2);
	letraDireita->SetDisplayPosition(screenSize[0] - 20, screenSize[1] / 2);
	letraSuperior->SetDisplayPosition(screenSize[0] / 2, screenSize[1] - 20);
	letraInferior->SetDisplayPosition(screenSize[0] / 2, 20);
}

void Letras::PosicionarNoMPR(vtkRenderer* targetRenderer)
{
	int* screenSize = targetRenderer->GetSize();
	letraEsquerda->SetDisplayPosition(25, screenSize[1] / 2);
	letraDireita->SetDisplayPosition(screenSize[0] - 25, screenSize[1] / 2);
	letraSuperior->SetDisplayPosition(screenSize[0] / 2, screenSize[1] - 25);
	letraInferior->SetDisplayPosition(screenSize[0] / 2, 25);
	letraEsquerda->GetTextProperty()->SetFontSize(10);
	letraDireita->GetTextProperty()->SetFontSize(10);
	letraSuperior->GetTextProperty()->SetFontSize(10);
	letraInferior->GetTextProperty()->SetFontSize(10);
}

Letras::Letras(vtkRenderer* targetRenderer)
{
	int* screenSize = targetRenderer->GetSize();
	letraEsquerda = vtkSmartPointer<vtkTextActor>::New();
	letraEsquerda->SetInput("R");
	vtkTextProperty* p = letraEsquerda->GetTextProperty();
	p->SetColor(1, 0, 0);
	p->SetBackgroundOpacity(0);
	p->SetFontSize(12);
	p->SetFontFamilyAsString("Tahoma");
	p->BoldOn();
	letraEsquerda->SetDisplayPosition(0, screenSize[1]/2);
	targetRenderer->AddActor(letraEsquerda);


	letraDireita = vtkSmartPointer<vtkTextActor>::New();
	letraDireita->SetInput("L");
	vtkTextProperty* p1 = letraDireita->GetTextProperty();
	p1->SetColor(1, 0, 0);
	p1->SetFontSize(12);
	p1->SetFontFamilyAsString("Tahoma");
	p1->SetBackgroundOpacity(0);
	p1->BoldOn();
	letraDireita->SetDisplayPosition(screenSize[0] - 20, screenSize[1] / 2);
	targetRenderer->AddActor2D(letraDireita);

	letraSuperior = vtkSmartPointer<vtkTextActor>::New();
	letraSuperior->SetInput("H");
	vtkTextProperty* p2 = letraSuperior->GetTextProperty();
	p2->SetColor(1, 0, 0);
	p2->SetFontSize(12);
	p2->SetFontFamilyAsString("Tahoma");
	p2->SetBackgroundOpacity(0);
	p2->BoldOn();
	letraSuperior->SetDisplayPosition(screenSize[0] / 2, screenSize[1] -10);
	targetRenderer->AddActor2D(letraSuperior);

	letraInferior = vtkSmartPointer<vtkTextActor>::New();
	letraInferior->SetInput("F");
	vtkTextProperty* p3 = letraInferior->GetTextProperty();
	p3->SetColor(1, 0, 0);
	p3->SetFontSize(12);
	p3->SetFontFamilyAsString("Tahoma");
	p3->BoldOn();
	p3->SetBackgroundOpacity(0);
	//p3->SetOpacity(0.5);
	letraInferior->SetDisplayPosition(screenSize[0] / 2, 0);
	targetRenderer->AddActor2D(letraInferior);
}

