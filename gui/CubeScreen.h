#pragma warning(disable:4996)
#ifndef ____cube_screen
#define ____cube_screen
#include <Windows.h>
#include <vtkRenderer.h>
#include <vtkWin32RenderWindowInteractor.h>
#include <vtkWin32OpenGLRenderWindow.h>
#include <atomic>
#include "CuboDeOrientacao.h"
#include <memory>
#include "TextureDataProvider.h"
#include "CameraRotationListener.h"
using namespace std;
using namespace tela;
///
class CubeScreen:public VolumeCameraRotationListener
{
public:
	void Execute(vtkSmartPointer<vtkCamera> cameraDoVolume, float offsetDoPlano = 0.5) override;
private:
	unique_ptr< tela::CuboDeOrientacao > cuboDeOrientacao;
	atomic_bool canRender;
	vtkRenderer* renderer;
	vtkWin32OpenGLRenderWindow* renderWindow;
	vtkWin32RenderWindowInteractor* interactor;

	//Opera��es n�o implementadas - construtor default, construtor de c�pia e atribui��o.
	CubeScreen();
	CubeScreen(CubeScreen&);
	CubeScreen &operator=(CubeScreen);
public:
	CubeScreen(HWND handle, HDC dc, HGLRC glrc, TextureDataProvider* texProvider);
	CubeScreen(HWND handle);
	~CubeScreen();
	void Ativar();
	void Desativar();
	void ForceRender();
	void Resize(int width, int height);

	vtkMatrix4x4* GetCurrentOrientacaoAsMatrix()
	{
		return cuboDeOrientacao->GetAtorEx()->GetMatrix();
	}


	vtkPolyData* GetPolydataDoCirculoHorizontal();
	vtkMatrix4x4* GetHorizontalCircleMatrix()
	{
		return cuboDeOrientacao->GetHorizontalCircleMatrix();
	}

	void ExecuteCameraRotation() override{
		assert("NAO IMPLEMENTADO - EXISTE PQ AS INTERFACES ESTAO ERRADAS" && false);
	};
};
#endif
