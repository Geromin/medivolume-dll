#pragma warning(disable:4996)
#ifndef ____volume_screen
#define ____volume_screen
#include <Windows.h>
#include <vtkRenderer.h>
#include <array>
#include <vtkWin32RenderWindowInteractor.h>
#include <vtkWin32OpenGLRenderWindow.h>
#include <atomic>
#include <vtkInteractorStyleImage.h>
#include <vtkSmartPointer.h>
#include <vtkImageAlgorithm.h>
#include "UserInteractionStrategy.h"
#include "CameraRotationListener.h"
#include "VolumeV2.h"
#include <vtkTextActor.h>
#include <vtkTextWidget.h>
#include <vtkTextRepresentation.h>
#include <vtkTextProperty.h>
#include "ImageWindowChangeListener.h"
#include <vtkCameraPass.h>
#include <vtkDefaultPass.h>
#include "ContourInterface.h"
#include <SDL2/SDL.h>
#include "CameraRotationListener.h"
#include <vtkBoxWidget.h>
#include "LetrasDeOrientacao.h"
#include "vtkBoxWidget.h"
#include <vtkImageResliceMapper.h>
//#include "TesteInteractionStyle.h"
#include "CuboDeCorteCallback.h"
#include <vtkImageProperty.h>
#include <vtkImageSlice.h>

#include <vtkPlaneSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkImageActor.h>
#include <vtkProperty.h>

#include <vtkPointWidget.h>

//#include "mprV3.h"
using namespace std;
using namespace tela;
using namespace extrusao;


class VolumeScreenInteractionStyle :public vtkInteractorStyleTrackballCamera
{
private:

	UserInteractionStrategy* currentStrategy;
	vr::VolumeV2* volumeObject;
public:

	void SetVolumeObject(vr::VolumeV2* vol)
	{
		this->volumeObject = vol;
	}
	static VolumeScreenInteractionStyle* New()
	{
		VolumeScreenInteractionStyle* v = new VolumeScreenInteractionStyle();
		v->currentStrategy = nullptr;
		v->volumeObject = nullptr;
		return v;
	}

	void SetCurrentStrategy(UserInteractionStrategy* s);
	void OnMouseMove()override;
	void OnLeftButtonDown()override;
	void OnLeftButtonUp()override;
	void OnMiddleButtonDown()override;
	void OnMiddleButtonUp()override;
	void OnRightButtonUp()override;
	void OnRightButtonDown()override;
};

class VolumeScreenRenderPasses:public vtkRenderPass,public ContourInterface
{
public:
	void DrawLasso(vector<int> is, vector<int> vector) override;
	void BeginDraw() override;
	void EndDraw() override;
private:
	SDL_Surface* canvasSDLSurface;
	SDL_Renderer* canvasSDLRenderer;
	vector<int> PontosX, PontosY;
	unsigned char* canvasBuffer;
	bool useDefault;
	vtkDefaultPass* defaultPass;
	virtual ~VolumeScreenRenderPasses()override;
	VolumeScreenRenderPasses();
	VolumeScreenRenderPasses(VolumeScreenRenderPasses&);
	VolumeScreenRenderPasses &operator=(VolumeScreenRenderPasses);
	int clampPonto(int pointVal, int limiteTela)
	{
		int p;
		p = pointVal < 0 ? 0 : pointVal;
		p = pointVal >= limiteTela ? limiteTela - 1 : pointVal;
		return p;
	}
public:
	static VolumeScreenRenderPasses* New()
	{
		VolumeScreenRenderPasses* v = new VolumeScreenRenderPasses();
		return v;
	}
	void Render(const vtkRenderState* s) override;
	void UseDefaultPasses();
	void UseContourPasses();
};

struct TesteMpr
{
	struct BarraDeReslice
	{
		vtkSmartPointer<vtkPlaneSource> planeSource;
		vtkSmartPointer<vtkPolyDataMapper> planeMapper;
		vtkSmartPointer<vtkActor> planeActor;
		vtkSmartPointer<vtkProperty> planeProperty;
		void Create()
		{
			planeSource = vtkSmartPointer<vtkPlaneSource>::New();
			planeSource->SetCenter(0, 0, 0);
			planeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
			planeMapper->SetInputConnection(planeSource->GetOutputPort());
			planeProperty = vtkSmartPointer<vtkProperty>::New();
			planeProperty->SetColor(1, 0, 0);
			planeActor = vtkSmartPointer<vtkActor>::New();
			planeActor->SetMapper(planeMapper);
			planeActor->SetProperty(planeProperty);
			planeActor->SetScale(1000, 1000, 1);
		}
	};
	BarraDeReslice barra;


	vtkSmartPointer<vtkRenderer> renderer;
	vtkSmartPointer<vtkRenderWindow> renderWindow;
	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor;
	vtkSmartPointer<vtkImageResliceMapper> resliceMapper;
	vtkSmartPointer<vtkImageProperty> resliceProperty;
	vtkSmartPointer<vtkImageSlice> imageActor;
	void Init()
	{
		renderer = vtkSmartPointer<vtkRenderer>::New();
		renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
		renderWindow->AddRenderer(renderer);
		renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
		renderWindow->SetInteractor(renderWindowInteractor);
		renderWindow->Start();
		renderWindow->Render();

	}

	void CreateMapper(vtkImageData* data, unsigned short valueOffset)
	{
		resliceMapper = vtkSmartPointer<vtkImageResliceMapper>::New();
		resliceMapper->SetInputData(data);
		resliceMapper->ResampleToScreenPixelsOn();
		resliceMapper->SliceFacesCameraOn();
		resliceMapper->SliceAtFocalPointOn();
		resliceMapper->SeparateWindowLevelOperationOn();

		resliceProperty = vtkSmartPointer<vtkImageProperty>::New();
		resliceProperty->SetColorWindow(250);
		resliceProperty->SetColorLevel(100 + valueOffset);
		resliceProperty->SetAmbient(0);
		resliceProperty->SetDiffuse(1);
		resliceProperty->SetOpacity(1);
		resliceProperty->SetInterpolationTypeToLinear();

		imageActor = vtkSmartPointer<vtkImageSlice>::New();
		imageActor->SetMapper(resliceMapper);
		imageActor->SetProperty(resliceProperty);

		renderer->AddViewProp(imageActor);
		renderer->GetActiveCamera()->ParallelProjectionOn();
		renderer->ResetCamera();
		//barra.Create();
		//renderer->AddActor(barra.planeActor);
	}

	void UpdatePipeline()
	{
		resliceMapper->Modified();
		imageActor->Update();
		renderWindow->Render();
	}

	void Destroy()
	{
		renderWindowInteractor = nullptr;
		renderWindow = nullptr;
		renderer = nullptr;
	}
};

class VolumeScreen : public ImageWindowChangeListener, public CuboDeCorteChangeListener
{
private:
	array<double, 3> segmentationSeed;
public:


	void ExecuteWindowChange(int windowCenter, int windowWidth) override;
	void ExecuteRelativeWindowChange(int dCenter, int dWidth) override;
	int GetWindowWidth() override;
	int GetWindowCenter() override;
	void SetCamera(double azimuth, double elevation);
	Letras* GetLetras()
	{
		return letras.get();
	}

	void RenderToImage();
	int* GetSize();
	vtkWindow* GetVtkWindow()
	{
		return renderWindow;
	}

	void AtivarCubo();
	void DesativarCubo();


private:

	vtkSmartPointer<vtkPointWidget> segmentationWidget;

	vtkSmartPointer<vtkRenderer> renderTeste;
	vtkSmartPointer<vtkRenderWindow> renderWindowTeste;
	unique_ptr<vr::VolumeV2>::pointer myVolumeObject;
	vtkSmartPointer<vtkImageResliceMapper> testeReslicerMapper;
	vtkSmartPointer<vtkRenderWindowInteractor> testeResliceInteractor;
	vtkSmartPointer<vtkImageSlice> testeImageActor;

	HWND handle;
	HDC dc;
	HGLRC glrc;
	vtkSmartPointer<CuboDeCorteCallback> callbackDeCuboDeCorte;
	vtkSmartPointer<vtkBoxWidget> boxWidget;
	unique_ptr<Letras> letras;
	vtkCameraPass* cameraPass;
	VolumeScreenRenderPasses* myPasses;
	vtkSmartPointer<vtkTextActor> windowText;

	//objetos interessados em saber quando ocorre rota��o da camera na tela do volume
	vector<tela::VolumeCameraRotationListener*> listenersDeRotacao;
	unique_ptr<UserInteractionStrategy> LeftMouseStrategy;
	unique_ptr<UserInteractionStrategy> MiddleMouseStrategy;
	unique_ptr<UserInteractionStrategy> RightMouseStrategy;

	atomic_bool canRender;

	vtkRenderer* renderer;
	vtkWin32OpenGLRenderWindow* renderWindow;
	vtkWin32RenderWindowInteractor* interactor;
// Old
	VolumeScreenInteractionStyle* interactionStyle;

	//Id do shape de remo��o
	int codShape;
	//Indica se � pra manter o que est� dentro ou o que est� fora do shape.
	bool keepInside;
	//Opera��es n�o implementadas - construtor default, construtor de c�pia e atribui��o.
	VolumeScreen();
	VolumeScreen(VolumeScreen&);
	VolumeScreen &operator=(VolumeScreen);

	vtkSmartPointer<vtkActor> formerPolygonalSegmentation;
public:
	void SetWindowTextFontSize(int sz)
	{
		windowText->GetTextProperty()->SetFontSize(sz);
		ForceRender();
	}

	vtkImageAlgorithm* finalDaPipeline;
	void SwitchWidgetDeSegmentacao()
	{
		if(segmentationWidget->GetEnabled())
		{
			segmentationWidget->EnabledOff();
		}
		else
		{
			segmentationWidget->EnabledOn();
		}
	}
	void AddPolygonalSegmentationResult(vtkSmartPointer<vtkPolyData> pd);
	void SetSegmentationSeed(double *posInImage)
	{
		segmentationSeed[0] = posInImage[0];
		segmentationSeed[1] = posInImage[1];
		segmentationSeed[2] = posInImage[2];
	}

	array<double, 3> GetSegmentationPosition()
	{
		return segmentationSeed;
	}

	void ExecuteAllRotationListners()
	{
		for (unsigned int i = 0; i < listenersDeRotacao.size(); i++)
			this->listenersDeRotacao[i]->Execute(renderer->GetActiveCamera());
	}

	void AddRotationListener(tela::VolumeCameraRotationListener* l)
	{
		this->listenersDeRotacao.push_back(l);
	}

	vtkRenderer* GetRenderer()
	{
		return renderer;
	}
	void AddVolume(vtkSmartPointer<vtkVolume> volume, std::string patientOrientation, std::array<double,3> patientPosition);
	VolumeScreen(HWND handle, HDC dc, HGLRC glrc);
	VolumeScreen(HWND handle);
	~VolumeScreen();
	void Ativar();
	void Desativar();
	void ForceRender();
	void Resize(int width, int height);
	void SetTipoDeRemocao(int cod_shape, bool keep_inside);
	void SetMouseDireito(int cod_operacao);
	void SetMouseMeio(int cod_operacao);
	void SetMouseEsquerdo(int cod_operacao);
	void OnMouseMove(HWND wnd, UINT n_flags, int i, int y);
	void OnLeftMouseDown(HWND wnd, UINT n_flags, int i, int y);
	void OnLeftMouseUp(HWND wnd, UINT n_flags, int i, int y);
	void OnMiddleMouseDown(HWND wnd, UINT n_flags, int i, int y);
	void OnMiddleMouseUp(HWND wnd, UINT n_flags, int i, int y);
	void OnRightMouseDown(HWND wnd, UINT n_flags, int i, int y);
	void OnRightMouseUp(HWND wnd, UINT n_flags, int i, int y);
	void AddVolumeObject(unique_ptr<vr::VolumeV2>::pointer get);

	void ExecutarBeginInteraction(double* bounds)
	{
		boxWidget->PlaceWidget(bounds);
	}
	void ExecutarEndInteraction(double* bounds)
	{
		boxWidget->PlaceWidget(bounds);
	}

	void ExecutarOnCuboDeCorteChange(vtkSmartPointer<vtkPolyData> pd)
	{
//		boxWidget->PlaceWidget(bounds);
	}

	void ExecutarOnCuboDeCorteChange(double* bounds)
	{
		boxWidget->PlaceWidget(bounds);
	}

	bool IsSegmentationWidgetActive(){
		return segmentationWidget->GetEnabled();
	}

};
#endif
