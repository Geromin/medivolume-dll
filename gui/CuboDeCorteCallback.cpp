#include "CuboDeCorteCallback.h"
#include <vtkBoxWidget.h>
#include <vtkPolyData.h>
namespace extrusao
{
	void CuboDeCorteCallback::AddCuboChangeListener(CuboDeCorteChangeListener* c)
	{
		//O listener normalmente ser� um VolumeV2, mas n�o posso programar pra implementa��o e 
		//sim pra interface
		CuboChangeListeners.push_back(c);
	}

	void CuboDeCorteCallback::Execute(vtkObject* caller, unsigned long ev, void* data)
	{
		std::cout << ev << std::endl;
		vtkBoxWidget *widget = reinterpret_cast<vtkBoxWidget*>(caller);
		vtkSmartPointer<vtkPolyData> pd = vtkSmartPointer<vtkPolyData>::New();
		widget->GetPolyData(pd);
		double* bounds = pd->GetBounds();
		if (ev == vtkCommand::InteractionEvent)
		{
			for (unsigned int i = 0; i < CuboChangeListeners.size(); i++){
				CuboChangeListeners[i]->ExecutarOnCuboDeCorteChange(bounds);
			}
		}
		if (ev == vtkCommand::StartInteractionEvent)
		{
			for (unsigned int i = 0; i < CuboChangeListeners.size(); i++){
				CuboChangeListeners[i]->ExecutarBeginInteraction(bounds); //OnCuboDeCorteChange(bounds);
			}
		}
		if (ev == vtkCommand::EndInteractionEvent)
		{
			for (unsigned int i = 0; i < CuboChangeListeners.size(); i++){
				CuboChangeListeners[i]->ExecutarEndInteraction(bounds); //OnCuboDeCorteChange(bounds);
			}
		}
		
	}
}