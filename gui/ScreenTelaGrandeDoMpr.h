#ifndef _screen_tela_grande_mpr_h
#define _screen_tela_grande_mpr_h
#include <Windows.h>
#include <vtkRenderer.h>
#include <vtkWin32OpenGLRenderWindow.h>
#include <vtkWin32RenderWindowInteractor.h>
#include <vtkImageViewer2.h>
#include <vtkResliceImageViewer.h>
#include <vtkImageAlgorithm.h>
#include <vtkImageImport.h>
#include <vtkWidgetRepresentation.h>
#include <vtkResliceCursorWidget.h>

class TelaGrandeDoMpr
{
private:
	int qualTela;
	bool canRender;
	//B�sicos
	vtkRenderer* renderer;
	vtkWin32OpenGLRenderWindow* renderWindow;
	vtkWin32RenderWindowInteractor* interactor;
	//O final da pipeline
	vtkImageImport* finalDaPipeline;
	vtkResliceImageViewer* resliceViewer;
	
public:
	TelaGrandeDoMpr(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela);
	~TelaGrandeDoMpr();
	void Ativar();
	void Desativar();
	void ForceRender();
	void Resize(int largura, int altura);
	void SetInput(vtkImageImport* imgSource);
	void Update();
	vtkResliceImageViewer* GetResliceImageViewer()
	{
		return resliceViewer;
	}

	vtkWidgetRepresentation* GetWidgetRepresentation()
	{
		return resliceViewer->GetResliceCursorWidget()->GetRepresentation();
	}

	void Reset();
};

#endif
