#include <vtkOpenGL.h>
#include "CuboDeOrientacao.h"
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkPNGReader.h>
#include <vtkTexture.h>
#include <vtkActor.h>
#include <vtkPlaneSource.h>
#include <vtkImageData.h>

#include <fstream>
using namespace std;
namespace tela{
	CuboDeOrientacao::Face::Face(std::string filepath, double nx, double ny, double nz)
	{
		std::string filename(filepath);
		std::string::size_type idx;
		idx = filename.rfind('.');
		std::string extension = filename.substr(idx + 1);
		if (extension != "png")
			throw std::exception("S� pode usar png como arquivo de textura da face do cubo.");

		vtkSmartPointer<vtkPNGReader> reader = vtkSmartPointer<vtkPNGReader>::New();
		reader->SetFileName(filepath.c_str());
		reader->Update();

		// Create an image
		imageSource = vtkSmartPointer<vtkImageData>::New();
		imageSource->DeepCopy(reader->GetOutput());// = reader->GetOutput()->DeepCopy();

		// Create a plane
		plane = vtkSmartPointer<vtkPlaneSource>::New();
		plane->SetCenter(0.0, 0.0, 0.0);
		plane->SetNormal(nx, ny, nz);

		// Apply the texture
		texture = vtkSmartPointer<vtkTexture>::New();
		texture->SetInputData(imageSource);

		planeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		planeMapper->SetInputConnection(plane->GetOutputPort());

		texturedPlane = vtkSmartPointer<vtkActor>::New();
		texturedPlane->SetMapper(planeMapper);
		texturedPlane->SetTexture(texture);
		texturedPlane->SetPosition(nx / 2, ny / 2, nz / 2);
	}
	CuboDeOrientacao::Face::Face(unsigned char* buffer, int sz, double nx, double ny, double nz)
	{
		const std::string tempfilename = "temppng.png";
		//salva o buffer temporariamente no disco 
		ofstream myfile;
		myfile.open(tempfilename.c_str(), ofstream::binary);
		myfile.write((char*)buffer, sz);
		myfile.close();
		// Create an image
		vtkSmartPointer<vtkPNGReader> reader = vtkSmartPointer<vtkPNGReader>::New();
		//reader->SetMemoryBuffer(buffer);
		//reader->SetMemoryBufferLength(sz);
		reader->SetFileName(tempfilename.c_str());
		reader->Update();
		imageSource = vtkSmartPointer<vtkImageData>::New();
		imageSource->DeepCopy(reader->GetOutput());// = reader->GetOutput()->DeepCopy();

		// Create a plane
		plane = vtkSmartPointer<vtkPlaneSource>::New();
		plane->SetCenter(0.0, 0.0, 0.0);
		plane->SetNormal(nx, ny, nz);

		// Apply the texture
		texture = vtkSmartPointer<vtkTexture>::New();
		texture->SetInputData(imageSource);

		planeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		planeMapper->SetInputConnection(plane->GetOutputPort());

		texturedPlane = vtkSmartPointer<vtkActor>::New();
		texturedPlane->SetMapper(planeMapper);
		texturedPlane->SetTexture(texture);
		texturedPlane->SetPosition(nx / 2, ny / 2, nz / 2);
		texturedPlane->SetPosition(nx / 2, ny / 2, nz / 2);

		remove(tempfilename.c_str());
		//free(buffer);
	}
	CuboDeOrientacao::Face::Face(const Face& f)
	{
		this->imageSource = f.imageSource;
		this->plane = f.plane;
		this->planeMapper = f.planeMapper;
		this->texture = f.texture;
		this->texturedPlane = f.texturedPlane;
	}
	
	vtkSmartPointer<vtkAssembly> CuboDeOrientacao::GetAtorEx(){
		return assembly;
	}

	void CuboDeOrientacao::SetTexturaLeft(unsigned char* buff, int buffer_size)
	{
		//n�o implementado
		assert(false);
	}

	vtkProp3D* CuboDeOrientacao::GetVerticalCircleActor()
	{
		return verticalCircleActor;
	}

	vtkProp3D* CuboDeOrientacao::GetHorizontalCircleActor()
	{
		return horizontalCircleActor;
	}

	vtkPolyData* CuboDeOrientacao::GetHorizontalPolydata()
	{
		horizontalCircleSource->Update();
		vtkPolyData* pd = horizontalCircleSource->GetOutput();
		return pd;
	}

	vtkMatrix4x4* CuboDeOrientacao::GetHorizontalCircleMatrix()
	{
		return horizontalCircleActor->GetMatrix();
	}

	CuboDeOrientacao::CuboDeOrientacao(TextureDataProvider* textureData){
		assert(textureData);//se null grita logo e morre.
		//Esquerda
		if (textureData->GetBufferLeft())//usa a textura vinda do delphi
		{
			Face f0(textureData->GetBufferLeft(), textureData->GetBufferLeftSize(), 1, 0, 0);
			faces.push_back(f0);
		}
		else//Usa o default
		{

			Face f0("left.png", 1, 0, 0);
			faces.push_back(f0);
		}
		//Direita
		if (textureData->GetBufferRight())//usa a textura vinda do delphi
		{
			Face f1(textureData->GetBufferRight(), textureData->GetBufferRightSize(), -1, 0, 0);
			faces.push_back(f1);
		}
		else//Usa o default
		{

			Face f1("right.png", -1, 0, 0);
			faces.push_back(f1);
		}

		if (textureData->GetBufferAnterior())//usa a textura vinda do delphi
		{
			Face f2(textureData->GetBufferAnterior(), textureData->GetBufferAnteriorSize(), 0, 0, 1);
			faces.push_back(f2);
		}
		else//Usa o default
		{

			Face f2("superior.png", 0, 0, 1);
			faces.push_back(f2);
		}

		if (textureData->GetBufferPosterior())//usa a textura vinda do delphi
		{
			Face f3(textureData->GetBufferPosterior(), textureData->GetBufferPosteriorSize(), 0, 0, -1);
			faces.push_back(f3);
		}
		else//Usa o default
		{

			Face f3("inferior.png", 0, 0, -1);
			faces.push_back(f3);
		}

		if (textureData->GetBufferInferior())//usa a textura vinda do delphi
		{
			Face f4(textureData->GetBufferInferior(), textureData->GetBufferInferiorSize(), 0, -1, 0);
			faces.push_back(f4);
		}
		else//Usa o default
		{

			Face f4("anterior.png", 0, -1, 0);
			faces.push_back(f4);
		}
		//Posterior
		if (textureData->GetBufferSuperior())//usa a textura vinda do delphi
		{
			Face f5(textureData->GetBufferSuperior(), textureData->GetBufferSuperiorSize(), 0, 1, 0);
			faces.push_back(f5);
		}
		else//Usa o default
		{

			Face f5("posterior.png", 0, 1, 0);
			faces.push_back(f5);
		}

		assembly = vtkSmartPointer<vtkAssembly>::New();
		assembly->AddPart(faces[0].getActor());
		assembly->AddPart(faces[1].getActor());
		assembly->AddPart(faces[2].getActor());
		assembly->AddPart(faces[3].getActor());
		assembly->AddPart(faces[4].getActor());
		assembly->AddPart(faces[5].getActor());
		//Cria os c�rculos ao redor do cubo de orienta��o
		verticalCircleSource = vtkSmartPointer<vtkRegularPolygonSource>::New();
		verticalCircleSource->SetNumberOfSides(360);
		verticalCircleSource->SetRadius(1);
		verticalCircleSource->GeneratePolygonOff();
		verticalCircleSource->GeneratePolylineOn();
		verticalCircleSource->SetCenter(assembly->GetCenter());
		verticalCircleMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		verticalCircleMapper->SetInputConnection(verticalCircleSource->GetOutputPort());
		verticalCircleActor = vtkSmartPointer<vtkActor>::New();
		verticalCircleActor->SetMapper(verticalCircleMapper);


		horizontalCircleSource = vtkSmartPointer<vtkRegularPolygonSource>::New();
		horizontalCircleSource->SetNumberOfSides(360);
		horizontalCircleSource->SetRadius(1);
		horizontalCircleSource->GeneratePolygonOff();
		horizontalCircleSource->GeneratePolylineOn();
		horizontalCircleSource->SetCenter(assembly->GetCenter());
		horizontalCircleSource->SetNormal(0, 1, 0);
		horizontalCircleMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		horizontalCircleMapper->SetInputConnection(horizontalCircleSource->GetOutputPort());
		horizontalCircleActor = vtkSmartPointer<vtkActor>::New();
		horizontalCircleActor->SetMapper(horizontalCircleMapper);
	}

}