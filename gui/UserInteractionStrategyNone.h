#ifndef  __uisnone
#define  __uisnone
#include "UserInteractionStrategy.h"

class UserInteractionStrategyNone :public UserInteractionStrategy
{


public:
	void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) override
	{

	}

	UserInteractionStrategyNone(const vtkSmartPointer<vtkInteractorStyle>& interactorStyle, callbackOnMouseMove cb)
		: UserInteractionStrategy(interactorStyle, cb)
	{
		nome = "none";
	}
	void Start() override
	{

	}

	void End() override
	{
	}
};

#endif
