#include "MprUtilsRenderPass.h"
#include <vtkOpenGLRenderer.h>
#include <vtkRenderWindow.h>
#include <array>
#include <SDL2/SDL.h>
#include <vtkCoordinate.h>
#include <vtkCoordinate.h>
using namespace std;

vtkSmartPointer<vtkCoordinate> MakeMyCoordinateObjectUsingMyPoint(double x, double y, double z)
{
	vtkSmartPointer<vtkCoordinate> coordObj = vtkSmartPointer<vtkCoordinate>::New();
	coordObj->SetCoordinateSystemToWorld();
	coordObj->SetValue(x, y, z);
	return coordObj;
}

MprUtilsRenderPass::MprUtilsRenderPass()
{
	defaultPass = vtkSmartPointer<vtkDefaultPass>::New();
}

void MprUtilsRenderPass::SetSegmentosDeReta(vector<array<double, 3>> pontosWS)
{
	this->pontos = pontosWS;
}

void MprUtilsRenderPass::Render(const vtkRenderState* s)
{
	//Algumas vari�veis uteis
	vtkOpenGLRenderer *glRen = vtkOpenGLRenderer::SafeDownCast(s->GetRenderer());
	vtkRenderWindow *wnd = glRen->GetRenderWindow();
	array<int, 2> wndSz;
	s->GetWindowSize(wndSz.data());
	//A renderiza��o sobre a qual eu desenharei coisas
	defaultPass->Render(s);
	//Pega o buffer gerado pela renderiza��o
	unsigned char *canvasBuffer = wnd->GetRGBACharPixelData(0, 0, wndSz[0] - 1, wndSz[1] - 1, 0);
	//Agora cria a superf�cie do sdl
	//SDL_Surface *canvasSDLSurface = SDL_CreateRGBSurfaceFrom(canvasBuffer, wndSz[0], wndSz[0], 32,
	//	wndSz[0] * 4, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
	SDL_Surface *canvasSDLSurface = SDL_CreateRGBSurfaceFrom(canvasBuffer, wndSz[0], wndSz[0], 32,
	wndSz[0] * 4, 0, 0, 0, 0);

	SDL_Renderer *canvasSDLRenderer = SDL_CreateSoftwareRenderer(canvasSDLSurface);
	//SDL_SetRenderDrawColor(canvasSDLRenderer, 0xff, 0x00, 0x00, 0xff);
	// faz um monte de coisas...
	//Lembrando que o sistema de coordenadas come�a no canto inferior esquerdo.
	//Desenha as linhas 
	int teste = (pontos.size() - 1);
	int i = 0;
	bool t = i < teste;
	for (; i < teste; i++)
	{
		//Os pontos est�o em world coordinates. Eles tem que ser projetados na tela
		vtkSmartPointer<vtkCoordinate> p1CoordTransform = vtkSmartPointer<vtkCoordinate>::New();
		p1CoordTransform->SetCoordinateSystemToWorld();
		p1CoordTransform->SetValue(pontos[i].data());
		int* displayCoord01 = p1CoordTransform->GetComputedDisplayValue(glRen);
		vtkSmartPointer<vtkCoordinate> p2CoordTransform = vtkSmartPointer<vtkCoordinate>::New();
		p2CoordTransform->SetCoordinateSystemToWorld();
		p2CoordTransform->SetValue(pontos[i+1].data());
		int* displayCoord02 = p2CoordTransform->GetComputedDisplayValue(glRen);
		//Com isso tenho as duas coords
		if (i % 2 == 0)
		{
			SDL_SetRenderDrawColor(canvasSDLRenderer, 255, 0, 0, 255);
		}
		else
		{
			SDL_SetRenderDrawColor(canvasSDLRenderer, 0, 255, 0, 255);
		}
		SDL_RenderDrawLine(canvasSDLRenderer, displayCoord01[0], displayCoord01[1],
			displayCoord02[0], displayCoord02[1]);
	}
	//desenha os pontos
	for (int j = 0; j < pontos.size(); j++)
	{
		vtkSmartPointer<vtkCoordinate> p1CoordTransform = vtkSmartPointer<vtkCoordinate>::New();
		p1CoordTransform->SetCoordinateSystemToWorld();
		p1CoordTransform->SetValue(pontos[j].data());
		int* displayCoord01 = p1CoordTransform->GetComputedDisplayValue(glRen);
		if (j % 2 == 0)
		{
			SDL_SetRenderDrawColor(canvasSDLRenderer, 255, 0, 0, 255);
		}
		else
		{
			SDL_SetRenderDrawColor(canvasSDLRenderer, 0, 255, 0, 255);
		}
		SDL_RenderDrawLine(canvasSDLRenderer, displayCoord01[0] - 4, displayCoord01[1],
			displayCoord01[0] + 4, displayCoord01[1]);
		SDL_RenderDrawLine(canvasSDLRenderer, displayCoord01[0], displayCoord01[1] - 4,
			displayCoord01[0], displayCoord01[1] + 4);
	}
	//desenha o plano de corte
	//Primeiro tem que sair do worldspace pro displayspace
	//Come�ando com o centro do plano.
	vtkSmartPointer<vtkCoordinate> planeCenterCoord = MakeMyCoordinateObjectUsingMyPoint(planeCenter[0], planeCenter[1], planeCenter[2]);
	int *planeCenterInDisplay = planeCenterCoord->GetComputedDisplayValue(glRen);
	//Agora as retas, baseadas nos direction cosines
	array<double, 3> reta01 = {
		planeCenter[0] + 50 * c0[0],
		planeCenter[1] + 50 * c0[1],
		planeCenter[2] + 50 * c0[2],
	};
	array<double, 3> reta02 = {
		planeCenter[0] + 50 * c1[0],
		planeCenter[1] + 50 * c1[1],
		planeCenter[2] + 50 * c1[2],
	};
	array<double, 3> reta03 = {
		planeCenter[0] - 50 * c0[0],
		planeCenter[1] - 50 * c0[1],
		planeCenter[2] - 50 * c0[2],
	};
	array<double, 3> reta04 = {
		planeCenter[0] - 50 * c1[0],
		planeCenter[1] - 50 * c1[1],
		planeCenter[2] - 50 * c1[2],
	};

	vtkSmartPointer<vtkCoordinate> r01Coord = MakeMyCoordinateObjectUsingMyPoint(reta01[0], reta01[1], reta01[2]);
	int *r01InDisplay = r01Coord->GetComputedDisplayValue(glRen);
	vtkSmartPointer<vtkCoordinate> r02Coord = MakeMyCoordinateObjectUsingMyPoint(reta02[0], reta02[1], reta02[2]);
	int *r02InDisplay = r02Coord->GetComputedDisplayValue(glRen);
	vtkSmartPointer<vtkCoordinate> r03Coord = MakeMyCoordinateObjectUsingMyPoint(reta03[0], reta03[1], reta03[2]);
	int *r03InDisplay = r03Coord->GetComputedDisplayValue(glRen);
	vtkSmartPointer<vtkCoordinate> r04Coord = MakeMyCoordinateObjectUsingMyPoint(reta04[0], reta04[1], reta04[2]);
	int *r04InDisplay = r04Coord->GetComputedDisplayValue(glRen);
	//Agora as paradas est�o todas no sistema de coordenada do display, � s� desenhar os bagulhos
	SDL_SetRenderDrawColor(canvasSDLRenderer, 0, 0, 255,255);
	SDL_RenderDrawLine(canvasSDLRenderer, planeCenterInDisplay[0], planeCenterInDisplay[1], r01InDisplay[0], r01InDisplay[1]);
	SDL_RenderDrawLine(canvasSDLRenderer, planeCenterInDisplay[0], planeCenterInDisplay[1], r03InDisplay[0], r03InDisplay[1]);

	SDL_RenderDrawLine(canvasSDLRenderer, planeCenterInDisplay[0], planeCenterInDisplay[1], r02InDisplay[0], r02InDisplay[1]);
	SDL_RenderDrawLine(canvasSDLRenderer, planeCenterInDisplay[0], planeCenterInDisplay[1], r04InDisplay[0], r04InDisplay[1]);

	//Seta os dados dos pixels, depois de todas as modifica��es que eu fiz.
	wnd->SetRGBACharPixelData(0, 0, wndSz[0] - 1, wndSz[1] - 1, canvasBuffer, 0);
	//Limpa os recursos.
	SDL_DestroyRenderer(canvasSDLRenderer);
	SDL_FreeSurface(canvasSDLSurface);
	canvasSDLRenderer = nullptr;
	canvasSDLSurface = nullptr;
}

void MprUtilsRenderPass::SetPosicaoEOrientacaoDoPlano(const array<double, 3> center, array<double, 3> c0new, array<double, 3> c1new, const array<double, 3> normalizedAxis)
{
	/*os vetores j� foram transformados, j� est�o com as rota��es apropriadas de acordo com a orienta��o e sentido de normalized axis.*/
	this->planeCenter = center;
	this->axis = normalizedAxis;
	this->c0 = c0new;
	this->c1 = c1new;
}
