#include "ScreenTelaGrandeDoMpr.h"
#include <vtkResliceCursorWidget.h>
#include <vtkResliceCursorLineRepresentation.h>
#include <vtkResliceCursorActor.h>
#include <vtkResliceCursorPolyDataAlgorithm.h>

TelaGrandeDoMpr::TelaGrandeDoMpr(HDC dcDaJanela, HWND handleDaJanela, HGLRC glrcDaJanela)
{
	canRender = false;
	renderer = vtkRenderer::New();
	renderer->SetBackground(0, 1, 0);
	renderWindow = vtkWin32OpenGLRenderWindow::New();
	renderWindow->InitializeFromCurrentContext();
	renderWindow->AddRenderer(renderer);
	interactor = vtkWin32RenderWindowInteractor::New();
	interactor->SetRenderWindow(renderWindow);
	interactor->SetInstallMessageProc(false);//Est� dando problema - quando as telas est�o livres e independentes se essa ln est� ativa os eventos n�o funcionam
	interactor->Initialize();
	finalDaPipeline = nullptr;
	resliceViewer = nullptr;
	qualTela = 1;
}

TelaGrandeDoMpr::~TelaGrandeDoMpr()
{
	renderer->Delete();
	renderWindow->Delete();
	interactor->Delete();
	if (resliceViewer)
		resliceViewer->Delete();
}

void TelaGrandeDoMpr::Ativar()
{
	canRender = true;
	renderWindow->Render();
}

void TelaGrandeDoMpr::Desativar()
{
	canRender = false;
	interactor->Disable();
	renderWindow->Finalize();
	renderer->ReleaseGraphicsResources(renderWindow);
}

void TelaGrandeDoMpr::Resize(int largura, int altura)
{
	renderWindow->SetSize(largura, altura);
	renderWindow->Modified();
}

void TelaGrandeDoMpr::SetInput(vtkImageImport* imgSource)
{
	this->finalDaPipeline = imgSource;
	resliceViewer = vtkResliceImageViewer::New();
	resliceViewer->SetRenderWindow(renderWindow);
	resliceViewer->SetupInteractor(renderWindow->GetInteractor());
	resliceViewer->SetResliceModeToOblique();
	resliceViewer->SetInputData(finalDaPipeline->GetOutput());
	resliceViewer->GetResliceCursorWidget()->GetResliceCursorRepresentation()->UseImageActorOff();
	//resliceViewer->GetResliceCursorWidget()->GetResliceCursorRepresentation()->ShowReslicedImageOn();
	ForceRender();
	vtkResliceCursorLineRepresentation *cursorRepresentation01 = vtkResliceCursorLineRepresentation::SafeDownCast(resliceViewer->GetResliceCursorWidget()->GetRepresentation());
	cursorRepresentation01->GetResliceCursorActor()->GetCursorAlgorithm()->SetReslicePlaneNormal(2);
	resliceViewer->SetSliceOrientation(2);
	ForceRender();

}

void TelaGrandeDoMpr::Reset()
{
	resliceViewer->Reset();
}

void TelaGrandeDoMpr::Update()
{

}

void TelaGrandeDoMpr::ForceRender()
{
	//if (canRender)
		renderWindow->Render();
}