#ifndef __user_interaction_strategy
#define __user_interaction_strategy
#include <vtkSmartPointer.h>
#include <vtkInteractorStyle.h>
#include <vtkRenderWindowInteractor.h>
#include "VolumeV2.h"
#include <vtkInteractorStyleTrackballCamera.h>
#include <string>
/*
A interface das estrat�gias de intera��o com o usu�rio
*/
class UserInteractionStrategy
{
protected:
	typedef void(vtkInteractorStyleTrackballCamera::*callbackOnMouseMove)();
	vtkSmartPointer<vtkInteractorStyle> interactorStyle;
	callbackOnMouseMove onMouseMoveCallback;
	
public:
	std::string nome;
	virtual ~UserInteractionStrategy()
	{
	}
	UserInteractionStrategy(vtkSmartPointer<vtkInteractorStyle> interactorStyle, callbackOnMouseMove cb)
	{
		this->interactorStyle = interactorStyle;
		onMouseMoveCallback = cb;
	}
	//Invocado quando � pra come�ar o uso da estrat�gia. Normalmente mouseDown.
	virtual void Start() = 0;
	//Execu��o da estrat�gia propriamente dita.
	virtual void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) = 0;
	//Invocado quando � para terminar o uso da estrat�gia. Normalmente mouseUp.
	virtual void End() = 0;
};
#endif