#ifndef __thumbScreen_h
#define __thumbScreen_h
#include <Windows.h>
#include <vtkRenderer.h>
#include <vtkWin32OpenGLRenderWindow.h>
#include <vtkVolume.h>
class ThumbScreen
{
private:
	bool canRender;
	vtkRenderer* renderer;
	vtkWin32OpenGLRenderWindow* renderWindow;
public:
	vtkRenderWindow* getRenderWindow()
	{
		return renderWindow;
	}

	~ThumbScreen()
	{
		renderer->Delete();
		renderWindow->Delete();
	}
	void Resize(int width, int height)
	{
		renderWindow->SetSize(width, height);
		renderWindow->Modified();
		renderer->ResetCamera();
		renderWindow->Modified();
		renderWindow->Render();
	}

	ThumbScreen(HWND handle, HDC dc, HGLRC glrc)
	{
		canRender = false;
		renderer = vtkRenderer::New();
		renderer->SetBackground(0, 0, 0);
		renderWindow = vtkWin32OpenGLRenderWindow::New();
		renderWindow->InitializeFromCurrentContext();
		renderWindow->AddRenderer(renderer);
	}
	void Ativar()
	{
		canRender = true;
		//renderWindow->Render();

	}
	void AddVolume(vtkVolume* v)
	{
		renderer->AddVolume(v);
		renderer->ResetCamera();
		renderer->GetActiveCamera()->ParallelProjectionOn();

	}

	void ForceRender()
	{
		if (canRender)	{
			renderWindow->Render();
		}

	}

	vtkRenderer* GetRenderer()
	{
		return renderer;
	}


};
#endif
