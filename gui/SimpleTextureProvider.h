#ifndef __simple_texture_provider
#define __simple_texture_provider
#include "TextureDataProvider.h"

class CubeScreen;

class SimpleTextureProvider:public TextureDataProvider
{
private:
	unsigned char* BufferDataCuboLeft;
	int BufferSzCuboLeft;
	unsigned char* BufferDataCuboRight;
	int BufferSzCuboRight;
	unsigned char* BufferDataCuboAnterior;
	int BufferSzCuboAnterior;
	unsigned char* BufferDataCuboPosterior;
	int BufferSzCuboPosterior;
	unsigned char* BufferDataCuboInferior;
	int BufferSzCuboInferior;
	unsigned char* BufferDataCuboSuperior;
	int BufferSzCuboSuperior;

	SimpleTextureProvider(const SimpleTextureProvider&);
	SimpleTextureProvider &operator=(SimpleTextureProvider);
public:
	SimpleTextureProvider()
	{
		BufferDataCuboAnterior = 0;
		BufferDataCuboRight = 0;
		BufferDataCuboInferior = 0;
		BufferDataCuboLeft = 0;
		BufferDataCuboPosterior = 0;
		BufferDataCuboSuperior = 0;
		BufferSzCuboAnterior = 0;
		BufferSzCuboInferior = 0;
		BufferSzCuboLeft = 0;
		BufferSzCuboPosterior = 0;
		BufferSzCuboRight = 0;

	}
	~SimpleTextureProvider()
	{

	}
	void SetTexturaLeftDoCubo(unsigned char* buff, int bufferSize)
	{

		BufferDataCuboLeft = buff;
		BufferSzCuboLeft = bufferSize;
	}
	void SetTexturaRightDoCubo(unsigned char* buff, int bufferSize)
	{
		BufferDataCuboRight = buff;
		BufferSzCuboRight = bufferSize;
	}
	void SetTexturaSuperiorDoCubo(unsigned char* buff, int bufferSize)
	{
		BufferDataCuboSuperior = buff;
		BufferSzCuboSuperior = bufferSize;
	}
	void SetTexturaInferiorDoCubo(unsigned char* buff, int bufferSize)
	{
		BufferDataCuboInferior = buff;
		BufferSzCuboInferior = bufferSize;
	}
	void SetTexturaAnteriorDoCubo(unsigned char* buff, int bufferSize)
	{
		BufferDataCuboAnterior = buff;
		BufferSzCuboAnterior = bufferSize;
	}
	void SetTexturaPosteriorDoCubo(unsigned char* buff, int bufferSize)
	{
		BufferDataCuboPosterior = buff;
		BufferSzCuboPosterior = bufferSize;
	}

	unsigned char* GetBufferLeft() override{
		return BufferDataCuboLeft;
	}
	unsigned GetBufferLeftSize() override{
		return BufferSzCuboLeft;
	}
	unsigned char* GetBufferRight() override{
		return BufferDataCuboRight;
	}
	unsigned GetBufferRightSize() override{
		return BufferSzCuboRight;
	}
	unsigned char* GetBufferAnterior() override{
		return BufferDataCuboAnterior;
	}
	unsigned GetBufferAnteriorSize() override{
		return BufferSzCuboAnterior;
	}
	unsigned char* GetBufferPosterior() override{
		return BufferDataCuboPosterior;
	}
	unsigned GetBufferPosteriorSize() override{
		return BufferSzCuboPosterior;
	}
	unsigned char* GetBufferSuperior() override{
		return BufferDataCuboSuperior;
	}
	unsigned GetBufferSuperiorSize() override{
		return BufferSzCuboSuperior;
	}
	unsigned char* GetBufferInferior() override{
		return BufferDataCuboInferior;
	}
	unsigned GetBufferInferiorSize() override{
		return BufferSzCuboInferior;
	}
};
#endif
