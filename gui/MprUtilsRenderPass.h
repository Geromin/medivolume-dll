#ifndef __mpr_utils_render_pass
#define __mpr_utils_render_pass
#include <vtkRenderPass.h>
#include <vtkRenderState.h>
#include <vtkDefaultPass.h>
#include <vtkSmartPointer.h>
#include <vector>
#include <array>
using namespace std;
//Serve pra desenhar as paradas do MPR, como os pontos
//dos segmentos de reta e onde est� no reslice
class MprUtilsRenderPass : public vtkRenderPass
{
private:	
	array<double, 3> planeCenter;
	array<double, 3> axis;
	array<double, 3> c0;
	array<double, 3> c1;
	vtkSmartPointer<vtkDefaultPass> defaultPass;
	MprUtilsRenderPass();
public:
	void SetSegmentosDeReta(vector<array<double, 3>> pontosWS);
	static MprUtilsRenderPass* New()
	{
		MprUtilsRenderPass* r = new MprUtilsRenderPass();
		return r;
	}
	void Render(const vtkRenderState* s) override;
	vector<array<double, 3>> pontos;
	void SetPosicaoEOrientacaoDoPlano(const array<double, 3> center, array<double, 3> c0new, array<double, 3> c1new, const array<double, 3> normalizedAxis);

};
#endif