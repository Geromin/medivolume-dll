#include "VolumeScreen.h"
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <sstream>
#include <vtkActor2DCollection.h>
#include "UtilitiesV2.h"
#include <vtkCamera.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include "CodigosDeOperacaoProvider.h"
#include "UserInteractionStrategyZoom.h"
#include "UserInteractionStrategyPan.h"
#include "UserInteractionStrategyRotate.h"
#include "UserInteractionStrategyNone.h"
#include "UserInteractionStrategyWindow.h"
#include "UserInteractionStrategyStencil.h"
#include <sstream>
#include <vtkOpenGLRenderer.h>
#include <vtkRenderState.h>
#include <vtkProperty2D.h>
#include <vtkWindowToImageFilter.h>
#include <vtkRendererCollection.h>
#include <vtkCommand.h>
#include <vtkTextActor.h>
#include <vtkInformation.h>
#include <vtkInformationKey.h>
#include <vtkInformationIntegerVectorKey.h>
#include <vtkInformationStringKey.h>
#include <vtkDICOMImageReader.h>
#include <vtkAlgorithmOutput.h>

class MyVTKKeyClass
{
public:
	static vtkInformationIntegerVectorKey* IS_MY_TEXT_OUTPUT();
};

vtkInformationKeyMacro(MyVTKKeyClass, IS_MY_TEXT_OUTPUT, IntegerVector);

class BeginSegmentationWidgetInteraction:public vtkCommand
{
private:
	vtkSmartPointer<vtkImageData> img;
	vtkSmartPointer<vtkTextActor> textInfo;
public:
	static BeginSegmentationWidgetInteraction *New()
	{
		BeginSegmentationWidgetInteraction *obj = new BeginSegmentationWidgetInteraction();
		obj->img = nullptr;
		obj->textInfo = nullptr;
		return obj;
	}

	void SetImage(vtkSmartPointer<vtkImageData> i){ img = i; }

	void Execute(vtkObject* caller, unsigned long eventId, void* callData)
	{
		vtkPointWidget *widget = vtkPointWidget::SafeDownCast(caller);
		if (!textInfo)
		{
			textInfo = vtkSmartPointer<vtkTextActor>::New();
			textInfo->SetTextScaleModeToProp();
			textInfo->SetDisplayPosition(5, widget->GetInteractor()->GetRenderWindow()->GetSize()[1] - 50);
			//textInfo->SetInput("point:");
			textInfo->GetTextProperty()->SetFontSize(8);
			textInfo->GetTextProperty()->SetFontFamilyToArial();
			textInfo->GetTextProperty()->SetColor(0, 1, 0);
			textInfo->GetTextProperty()->SetFontSize(8);

			//vtkAlgorithm::CAN_PRODUCE_SUB_EXTENT()
			vtkInformation *info = textInfo->GetPropertyKeys();
			if (!info)
			{
				vtkSmartPointer<vtkInformation> informacoes = vtkSmartPointer<vtkInformation>::New();
				textInfo->SetPropertyKeys(informacoes);
			}
			textInfo->GetPropertyKeys()->Append(MyVTKKeyClass::IS_MY_TEXT_OUTPUT(), 1);
			widget->GetCurrentRenderer()->AddActor2D(textInfo);
			widget->GetInteractor()->GetRenderWindow()->Render();
			cout << "--------------------TEXTO NASCE AQUI-------------------------------------" << endl;
		}
		else
		{
			widget->GetCurrentRenderer()->RemoveActor2D(textInfo);
			textInfo = nullptr;
			cout << "--------------------TEXTO ACABA AQUI-------------------------------------" << endl;
		}
		//Widget Pos est� em world space
		cout << __FUNCTION__ << endl;
	}
};

class SegmentationWidgetProbeData : public vtkCommand
{
private:
	vtkSmartPointer<vtkImageData> img;

public:
	VolumeScreen *vs;

	static SegmentationWidgetProbeData *New()
	{
		SegmentationWidgetProbeData *obj = new SegmentationWidgetProbeData();
		obj->vs = nullptr;
		return obj;
	}
	void SetImage(vtkSmartPointer<vtkImageData> i){ img = i; }
	void Execute(vtkObject* caller, unsigned long eventId, void* callData)
	{
		cout << __FUNCTION__ << endl;
		assert("VS n�o pode ser null"&&vs);
		assert("Img n�o pode ser null"&&img);
		vtkPointWidget *widget = vtkPointWidget::SafeDownCast(caller);

		vtkTextActor *saidaDeTexto = nullptr;
		//Pega o meu objeto. Como podem haver v�rios text actors, vou por uma property
		//key posta no meu objeto desejado.
		//vtkActor2DCollection *actors = widget->GetCurrentRenderer()->GetActors2D();
		//actors->InitTraversal();
		//vtkActor2D *currentActor = actors->GetNextActor2D();
		//while (currentActor!=nullptr)
		//{
		//	if (currentActor->GetPropertyKeys()->Has(MyVTKKeyClass::IS_MY_TEXT_OUTPUT()))
		//	{
		//		saidaDeTexto = vtkTextActor::SafeDownCast(currentActor);
		//		break;
		//	}
		//	currentActor = actors->GetNextActor2D();
		//}
		//Calcula a posi��o
		double widgetPos[3];
		widget->GetPosition(widgetPos);
		//Widget Pos est� em world space
		double spacing[3];
		img->GetSpacing(spacing);
		double origin[3];
		img->GetOrigin(origin);
		double posInImage[] = {
			(widgetPos[0] - origin[0]) / spacing[0],
			(widgetPos[1] - origin[1]) / spacing[1],
			(widgetPos[2] - origin[2]) / spacing[2],
		};
		//Sa�das.
		float v = img->GetScalarComponentAsDouble(posInImage[0], posInImage[1], posInImage[2], 0);
		stringstream ss;
		ss << "[" << posInImage[0] << ", " << posInImage[1] << ", " << posInImage[2] << "] = "<<v;
		cout << ss.str() << endl;
		if (saidaDeTexto)
		{
			saidaDeTexto->SetInput(ss.str().c_str());
		}
		vs->SetSegmentationSeed(posInImage);
	}
};

void VolumeScreenInteractionStyle::SetCurrentStrategy(UserInteractionStrategy* s)
{
	this->currentStrategy = s;
}

void VolumeScreenInteractionStyle::OnMouseMove()
{
	if (currentStrategy)
	{
		vtkInteractorStyleTrackballCamera::OnMouseMove();
		currentStrategy->Execute(GetInteractor(), volumeObject);
	}

}

void VolumeScreenInteractionStyle::OnLeftButtonDown()
{
	if (currentStrategy)
		currentStrategy->Start();

}

void VolumeScreenInteractionStyle::OnLeftButtonUp()
{
	if (currentStrategy)
		currentStrategy->End();

}

void VolumeScreenInteractionStyle::OnMiddleButtonDown()
{
	if (currentStrategy)
		currentStrategy->Start();
}

void VolumeScreenInteractionStyle::OnMiddleButtonUp()
{
	if (currentStrategy)
		currentStrategy->End();
}

void VolumeScreenInteractionStyle::OnRightButtonUp()
{
	if (currentStrategy)
		currentStrategy->End();
}

void VolumeScreenInteractionStyle::OnRightButtonDown()
{
	if (currentStrategy)
		currentStrategy->Start();
}

void VolumeScreenRenderPasses::DrawLasso(vector<int> x, vector<int> y)
{
	this->PontosX = x;
	this->PontosY = y;
}

void VolumeScreenRenderPasses::BeginDraw()
{
	PontosX.clear();
	PontosY.clear();
	useDefault = false;
}

void VolumeScreenRenderPasses::EndDraw()
{
	useDefault = true;
	PontosX.clear();
	PontosY.clear();
}

VolumeScreenRenderPasses::~VolumeScreenRenderPasses()
{
	defaultPass->Delete();
	if (canvasSDLRenderer)
		SDL_DestroyRenderer(canvasSDLRenderer);
	if (canvasSDLSurface)
		SDL_FreeSurface(canvasSDLSurface);
	if (canvasBuffer)
		delete canvasBuffer;
}

VolumeScreenRenderPasses::VolumeScreenRenderPasses()
{
	useDefault = true;
	defaultPass = vtkDefaultPass::New();
	canvasBuffer = nullptr;
	canvasSDLRenderer = nullptr;
	canvasSDLSurface = nullptr;
}

void VolumeScreenRenderPasses::Render(const vtkRenderState* s)
{
	vtkOpenGLRenderer* glRen = vtkOpenGLRenderer::SafeDownCast(s->GetRenderer());
	vtkRenderWindow* wnd = glRen->GetRenderWindow();
	int wndSz[2];
	s->GetWindowSize(wndSz);
	if (useDefault)
	{
		defaultPass->Render(s);
		if (canvasBuffer != nullptr)
			delete canvasBuffer;
		canvasBuffer = wnd->GetRGBACharPixelData(0, 0, wndSz[0] - 1, wndSz[1] - 1, 0);
	}
	else
	{
		unsigned char * tempbuff = new unsigned char[wndSz[0] * wndSz[1] * 4];

		this->canvasSDLSurface = SDL_CreateRGBSurfaceFrom(tempbuff, wndSz[0], wndSz[0], 32,
								  wndSz[0] * 4, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
		this->canvasSDLRenderer = SDL_CreateSoftwareRenderer(this->canvasSDLSurface);
		SDL_SetRenderDrawColor(canvasSDLRenderer, 0xff, 0xff, 0xff, 0xff);
		memcpy(tempbuff, canvasBuffer, wndSz[0] * wndSz[1] * 4 * sizeof(unsigned char));
		SDL_SetRenderDrawColor(canvasSDLRenderer, 0xff, 0, 0, 0xff);
		if (PontosX.size()>1)
		{
			for (unsigned int i = 0; i < PontosX.size()-1; i++)
			{
				int _x0 = PontosX[i];
				int _y0 = PontosY[i];
				try{
					int px0 = clampPonto(PontosX[i], wndSz[0] - 10);
					int py0 = clampPonto(PontosY[i], wndSz[1]-10);
					int px1 = clampPonto(PontosX[i + 1], wndSz[0] - 10);
					int py1 = clampPonto(PontosY[i + 1], wndSz[1] - 10);
					SDL_RenderDrawLine(canvasSDLRenderer, px0, py0, px1, py1);
					SDL_RenderDrawLine(canvasSDLRenderer, px0 + 1, py0, px1 + 1, py1);
					SDL_RenderDrawLine(canvasSDLRenderer, px0, py0 + 1, px1, py1 + 1);
				}
				catch (...)
				{

				}
			}
			int lastX = clampPonto(PontosX[PontosX.size() - 1], wndSz[0]);
			int lastY = clampPonto(PontosY[PontosX.size() - 1], wndSz[1]);
			int firstX = clampPonto(PontosX[0], wndSz[0]);
			int firstY = clampPonto(PontosY[0], wndSz[1]);
			SDL_RenderDrawLine(canvasSDLRenderer, lastX, lastY, firstX, firstY);
		}
		wnd->SetRGBACharPixelData(0, 0, wndSz[0] - 1, wndSz[1] - 1, tempbuff, 0);
		SDL_DestroyRenderer(canvasSDLRenderer);
		SDL_FreeSurface(canvasSDLSurface);
		canvasSDLRenderer = nullptr;
		canvasSDLSurface = nullptr;
		delete tempbuff;
	}
}

void VolumeScreenRenderPasses::UseDefaultPasses()
{
	useDefault = true;
	Modified();
}

void VolumeScreenRenderPasses::UseContourPasses()
{
	useDefault = false;
	Modified();
}

void VolumeScreen::ExecuteWindowChange(int windowCenter, int windowWidth)
{
	stringstream ss;
	ss << "WW = ";
	ss << windowWidth;
	ss << " WC = ";
	ss << windowCenter;
	windowText->SetInput(ss.str().c_str());
}

void VolumeScreen::ExecuteRelativeWindowChange(int dCenter, int dWidth)
{
	assert(false);//n�o implementado
}

int VolumeScreen::GetWindowWidth()
{
	assert(false);//n�o implementado
	return 0;
}



void VolumeScreen::SetCamera(double azimuth, double elevation)
{
	double _paralel_scale = renderer->GetActiveCamera()->GetParallelScale();
	renderer->SetActiveCamera(nullptr);
	renderer->ResetCamera();
	renderer->GetActiveCamera()->SetViewUp(0, 1, 0);
	renderer->GetActiveCamera()->ParallelProjectionOn();
	renderer->GetActiveCamera()->Azimuth(azimuth);
	renderer->GetActiveCamera()->Elevation(elevation);
	renderer->GetActiveCamera()->SetParallelScale(_paralel_scale);
	for (unsigned int i = 0; i < listenersDeRotacao.size(); i++)
	{
		listenersDeRotacao[i]->Execute(renderer->GetActiveCamera());
	}
}

void VolumeScreen::RenderToImage()
{

}

void VolumeScreen::DesativarCubo()
{
	boxWidget->Off();
}

void VolumeScreen::AtivarCubo()
{
	boxWidget->On();
}

int* VolumeScreen::GetSize()
{
	return renderWindow->GetSize();
}

int VolumeScreen::GetWindowCenter()
{
	assert(false);//n�o implementado
	return 0;
}

void VolumeScreen::AddPolygonalSegmentationResult(vtkSmartPointer<vtkPolyData> pd)
{
	if (formerPolygonalSegmentation)
	{
		renderer->RemoveActor(formerPolygonalSegmentation);
		//renderWindow->Render();
	}

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(pd);
	//mapper->ScalarVisibilityOn();

	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->GetProperty()->SetColor(1, 0.5, 0.25);
	actor->GetProperty()->SetInterpolationToPhong();
	actor->GetProperty()->ShadingOn();
	actor->SetMapper(mapper);

	cout << "vai dar merda?" << endl;
	double x = ((double)(finalDaPipeline->GetOutput()->GetBounds()[1] - finalDaPipeline->GetOutput()->GetBounds()[0])) / 2 * finalDaPipeline->GetOutput()->GetSpacing()[0] + finalDaPipeline->GetOutput()->GetOrigin()[0];
	double y = ((double)(finalDaPipeline->GetOutput()->GetBounds()[3] - finalDaPipeline->GetOutput()->GetBounds()[2])) / 2 * finalDaPipeline->GetOutput()->GetSpacing()[1] + finalDaPipeline->GetOutput()->GetOrigin()[1];
	double z = ((double)(finalDaPipeline->GetOutput()->GetBounds()[5] - finalDaPipeline->GetOutput()->GetBounds()[4])) / 2 * finalDaPipeline->GetOutput()->GetSpacing()[2] + finalDaPipeline->GetOutput()->GetOrigin()[2];
	actor->SetPosition(x, y,z);
	actor->SetScale(1, 1, 1);
	formerPolygonalSegmentation = actor;
	renderer->AddActor(formerPolygonalSegmentation);
	renderer->Render();
}

void VolumeScreen::AddVolume(vtkSmartPointer<vtkVolume> volume, std::string patientOrientation, std::array<double, 3> patientPosition)
{
	renderWindow->Render();
#ifdef NDEBUG
	//renderer->SetBackground(0, 1, 0);
#endif
	vtkLightCollection* lc1 = renderer->GetLights();
	renderer->AddVolume(volume);
	renderer->AutomaticLightCreationOn();
	renderer->ResetCamera();
	renderer->TwoSidedLightingOn();
	renderer->LightFollowCameraOn();
	renderer->UpdateLightsGeometryToFollowCamera();
	vtkLightCollection* lc2 = renderer->GetLights();
	renderer->GetActiveCamera()->ParallelProjectionOn();
	renderer->CreateLight();
	vtkLightCollection* lc3 = renderer->GetLights();
	vtkLight* l = vtkLight::SafeDownCast( lc3->GetItemAsObject(0));
	l->Print(cout);
	renderer->LightFollowCameraOn();
	renderer->UpdateLightsGeometryToFollowCamera();
	l->Print(cout);
	renderWindow->Render();	//� aqui, read of 0000
	////Rota��es para posicionar a imagem corretamente
	//std::array<double, 3> center = { { volume->GetCenter()[0], volume->GetCenter()[1], volume->GetCenter()[2] } };
	//volume->SetOrigin(center.data());
	//vtkCamera *cam = renderer->GetActiveCamera();
	//if (patientOrientation == "FFS"){
	//	cam->Elevation(-90);
	//	renderWindow->Render();
	//}
	//if (patientOrientation == "HFS"){
	//	volume->RotateX(-90);
	//	std::cout << "n�o implementado" << std::endl;
	//}

	//Cria��o do widget de texto de segmenta��o
	windowText = vtkSmartPointer<vtkTextActor>::New();
	windowText->SetInput("");
	vtkTextProperty* p = windowText->GetTextProperty();
	p->SetColor(1, 1, 1);
	p->SetFontSize(20);
	p->SetFontFamilyAsString("Tahoma");
	windowText->SetDisplayPosition(10, 10);
	renderer->AddActor2D(windowText);
	//volume->PickableOff();


	boxWidget = vtkSmartPointer<vtkBoxWidget>::New();
	boxWidget->SetPlaceFactor(1.1);
	boxWidget->PlaceWidget(volume->GetBounds());


	boxWidget->RotationEnabledOff();
	boxWidget->ScalingEnabledOn();
	boxWidget->SetHandleSize(0.0125);
	boxWidget->SetInteractor(interactor);


	callbackDeCuboDeCorte = vtkSmartPointer<CuboDeCorteCallback>::New();
	boxWidget->AddObserver(vtkCommand::InteractionEvent, callbackDeCuboDeCorte);
	boxWidget->AddObserver(vtkCommand::EndInteractionEvent, callbackDeCuboDeCorte);
	boxWidget->AddObserver(vtkCommand::StartInteractionEvent, callbackDeCuboDeCorte);


	Ativar();
	segmentationWidget = vtkSmartPointer<vtkPointWidget>::New();//Area do erro come�a por aqui
	segmentationWidget->PlaceWidget(volume->GetBounds());
	segmentationWidget->SetInteractor(interactor);
	segmentationWidget->GetProperty()->SetColor(0, 1, 0);
	segmentationWidget->Off();


	vtkSmartPointer<BeginSegmentationWidgetInteraction> bswi = vtkSmartPointer<BeginSegmentationWidgetInteraction>::New();

	bswi->SetImage(this->myVolumeObject->GetPipeline()->GetFinalDaPipeline()->GetOutput());
	vtkSmartPointer<SegmentationWidgetProbeData> swpd = vtkSmartPointer<SegmentationWidgetProbeData>::New();
	swpd->vs = this;
	swpd->SetImage(this->myVolumeObject->GetPipeline()->GetFinalDaPipeline()->GetOutput());
	segmentationWidget->AddObserver(vtkCommand::EnableEvent, bswi);
	segmentationWidget->AddObserver(vtkCommand::StartInteractionEvent, bswi);
	segmentationWidget->AddObserver(vtkCommand::EndInteractionEvent, bswi);
	segmentationWidget->AddObserver(vtkCommand::InteractionEvent, swpd);

	callbackDeCuboDeCorte->AddCuboChangeListener(myVolumeObject);


	letras->Ligar();
	segmentationWidget->Off();

}

VolumeScreen::VolumeScreen(HWND handle, HDC dc, HGLRC glrc)
{
	formerPolygonalSegmentation = nullptr;
	canRender = false;
	renderer = vtkRenderer::New();
	renderer->SetBackground(0, 0, 0);
	renderWindow = vtkWin32OpenGLRenderWindow::New();
	renderWindow->InitializeFromCurrentContext();
	std::cout << "SR = " << renderWindow->GetStereoRender();
	renderWindow->StereoCapableWindowOff();
	renderWindow->StereoRenderOff();
	this->handle = handle;
	this->dc = dc;
	this->glrc = glrc;
	renderWindow->AddRenderer(renderer);
	interactor = vtkWin32RenderWindowInteractor::New();
	interactor->SetRenderWindow(renderWindow);
	interactor->SetInstallMessageProc(false);//sem isso trava no Ativar();
	interactor->Initialize();
	interactionStyle = VolumeScreenInteractionStyle::New();
	interactor->SetInteractorStyle(interactionStyle);
	letras = make_unique<Letras>(renderer);
	letras->Desligar();
	AddRotationListener(letras.get());
	SetMouseEsquerdo(CodigosDeOperacaoProvider::GetCodigoOperacao("none"));
	SetMouseMeio(CodigosDeOperacaoProvider::GetCodigoOperacao("none"));
	SetMouseDireito(CodigosDeOperacaoProvider::GetCodigoOperacao("none"));
}

VolumeScreen::VolumeScreen(HWND handle)
{
	assert(false);
}

VolumeScreen::~VolumeScreen()
{
	FunctionIn;
	cameraPass->Delete();
	myPasses->Delete();
	interactor->ExitCallback();
	interactionStyle->Delete();
	interactor->Delete();

	listenersDeRotacao.clear();

	renderWindow->MakeCurrent();
	renderWindow->Clean();
	renderWindow->Finalize();
	renderWindow->Delete();
	renderer->Delete();
	FunctionOut;
}

//vtkSmartPointer<vtkVolumeProperty> volumeProp;
//vtkSmartPointer<vtkColorTransferFunction> colorFn;
//vtkSmartPointer<vtkPiecewiseFunction> opacityFn;
//float wt = 500;
void VolumeScreen::Ativar()
{
	FunctionIn;
	canRender = true;
	//renderWindow->Render();//FNORD
	cameraPass = vtkCameraPass::New();
	myPasses = VolumeScreenRenderPasses::New();
	((vtkOpenGLRenderer*)renderer)->SetPass(cameraPass);
	cameraPass->SetDelegatePass(myPasses);
	//renderWindow->Render();	//FNORD
	interactor->Start();
	FunctionOut;
}

void VolumeScreen::Desativar()
{
	FunctionIn;
	canRender = false;
	renderWindow->BreakOnError();
	//renderWindow->DebugOn();
	interactor->Disable();
	renderWindow->Finalize();
	//renderer->ReleaseGraphicsResources(renderWindow); - Crasha

	FunctionOut;
}

void VolumeScreen::Resize(int width, int height)
{
	renderWindow->SetSize(width, height);
	letras->Posicionar(renderer);
	renderWindow->Modified();
}

void VolumeScreen::SetTipoDeRemocao(int cod_shape, bool keep_inside)
{
	this->codShape = cod_shape;
	this->keepInside = keep_inside;
}

void VolumeScreen::SetMouseDireito(int cod_operacao)
{
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("zoom"))
	{
		RightMouseStrategy = make_unique<UserInteractionStrategyZoom>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("pan"))
	{
		RightMouseStrategy = make_unique<UserInteractionStrategyPan>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("rotate"))
	{
		RightMouseStrategy = make_unique<UserInteractionStrategyRotate>(interactionStyle, nullptr, this->listenersDeRotacao);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("none"))
	{
		RightMouseStrategy = make_unique<UserInteractionStrategyNone>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("window"))
	{
		RightMouseStrategy = make_unique<UserInteractionStrategyWindow>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("stencil"))
	{
		RightMouseStrategy = make_unique<UserInteractionStrategyStencil>(this->interactionStyle, nullptr, codShape, keepInside, myPasses);
	}
}

void VolumeScreen::SetMouseMeio(int cod_operacao)
{
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("zoom"))
	{
		MiddleMouseStrategy = make_unique<UserInteractionStrategyZoom>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("pan"))
	{
		MiddleMouseStrategy = make_unique<UserInteractionStrategyPan>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("rotate"))
	{
		MiddleMouseStrategy = make_unique<UserInteractionStrategyRotate>(interactionStyle, nullptr, this->listenersDeRotacao);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("none"))
	{
		MiddleMouseStrategy = make_unique<UserInteractionStrategyNone>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("window"))
	{
		MiddleMouseStrategy = make_unique<UserInteractionStrategyWindow>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("stencil"))
	{
		MiddleMouseStrategy = make_unique<UserInteractionStrategyStencil>(this->interactionStyle, nullptr, codShape, keepInside, myPasses);
	}
}

void VolumeScreen::SetMouseEsquerdo(int cod_operacao)
{

	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("zoom"))
	{
		LeftMouseStrategy = make_unique<UserInteractionStrategyZoom>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("pan"))
	{
		LeftMouseStrategy = make_unique<UserInteractionStrategyPan>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("rotate"))
	{
		LeftMouseStrategy = make_unique<UserInteractionStrategyRotate>(interactionStyle, nullptr, this->listenersDeRotacao);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("none"))
	{
		LeftMouseStrategy = make_unique<UserInteractionStrategyNone>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("window"))
	{
		LeftMouseStrategy = make_unique<UserInteractionStrategyWindow>(this->interactionStyle, &vtkInteractorStyleTrackballCamera::OnMouseMove);
	}
	if (cod_operacao == CodigosDeOperacaoProvider::GetCodigoOperacao("stencil"))
	{
		LeftMouseStrategy = make_unique<UserInteractionStrategyStencil>(this->interactionStyle, nullptr, codShape, keepInside, myPasses);
	}
}

void VolumeScreen::OnMouseMove(HWND wnd, UINT n_flags, int x, int y)
{
	interactor->OnMouseMove(wnd, n_flags, x, y);
}

void VolumeScreen::OnLeftMouseDown(HWND wnd, UINT n_flags, int x, int y)
{
	//interactor->OnLButtonDown(wnd, n_flags, x, y);

	if (LeftMouseStrategy)
	{
		interactionStyle->SetCurrentStrategy(this->LeftMouseStrategy.get());
		interactor->OnLButtonDown(wnd, n_flags, x, y);
	}
}

void VolumeScreen::OnLeftMouseUp(HWND wnd, UINT n_flags, int x, int y)
{
//	interactor->OnLButtonUp(wnd, n_flags, x, y);

	if (LeftMouseStrategy)
	{
		interactor->OnLButtonUp(wnd, n_flags, x, y);
		interactionStyle->SetCurrentStrategy(nullptr);
	}
}

void VolumeScreen::OnMiddleMouseDown(HWND wnd, UINT n_flags, int x, int y)
{
//	interactor->OnMButtonDown(wnd, n_flags, x, y);
	if (MiddleMouseStrategy)
	{
		interactionStyle->SetCurrentStrategy(this->MiddleMouseStrategy.get());
		interactor->OnMButtonDown(wnd, n_flags, x, y);
	}
}

void VolumeScreen::OnMiddleMouseUp(HWND wnd, UINT n_flags, int x, int y)
{
//	interactor->OnMButtonUp(wnd, n_flags, x, y);
	if (MiddleMouseStrategy)
	{
		interactor->OnMButtonUp(wnd, n_flags, x, y);
		interactionStyle->SetCurrentStrategy(nullptr);
	}
}

void VolumeScreen::OnRightMouseDown(HWND wnd, UINT n_flags, int x, int y)
{
//	interactor->OnRButtonDown(wnd, n_flags, x, y);
	if (RightMouseStrategy)
	{
		interactionStyle->SetCurrentStrategy(this->RightMouseStrategy.get());
		interactor->OnRButtonDown(wnd, n_flags, x, y);
	}
}

class GambiStyle :public vtkInteractorStyleTrackballCamera
{
public:
	double az;
	vtkSmartPointer<vtkImageResliceMapper> mapper;
	vtkSmartPointer<vtkImageSlice> Slice;
	static GambiStyle* New(){ GambiStyle *g = new GambiStyle(); g->az = 0; return g; }
	void OnLeftButtonDown()override
	{
		vtkCamera* cam = this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->GetActiveCamera();
		cam->Azimuth(az);
		az = az + 1;

		mapper->UpdateWholeExtent();
		Slice->Update();
		this->Interactor->GetRenderWindow()->Render();

		std::cout << "gambi" << std::endl;
	}
	void OnLeftButtonUp()override
	{
		std::cout << "style" << std::endl;
	}
	void OnMouseMove()override
	{
		std::cout << "lol" << std::endl;
	}
};

void VolumeScreen::AddVolumeObject(unique_ptr<vr::VolumeV2>::pointer get)
{
	get->volumeScreenToResetCuboDeCorte = this;
	this->interactionStyle->SetVolumeObject(get);

	myVolumeObject = get;
}

void VolumeScreen::OnRightMouseUp(HWND wnd, UINT n_flags, int x, int y)
{
	if (RightMouseStrategy)
	{
		interactor->OnRButtonUp(wnd, n_flags, x, y);
		interactionStyle->SetCurrentStrategy(nullptr);
	}
}

void VolumeScreen::ForceRender()
{
	FunctionIn;
	if (canRender)
	{
		renderWindow->Render();
	}
	FunctionOut;
}