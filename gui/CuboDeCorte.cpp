//#include "CuboDeCorte.h"
//#include <vtkRenderWindow.h>
//#include <vtkRendererCollection.h>
//#include <vtkBoxWidget.h>
//#include <vtkImageStencilData.h>
//#include <vtkSmartPointer.h>
//#include <vtkPolyDataToImageStencil.h>
//namespace extrusao{
//	void CuboDeCorte::ExecutarOnCuboDeCorteChange(vtkSmartPointer<vtkPolyData> pd)
//	{
//		assert(false);//n�o implementado pq passar polydata est� deprecado
//	}
//
//	void CuboDeCorte::ExecutarOnCuboDeCorteChange(double* bounds)
//	{
//		memcpy(CubeBoundsStore, bounds, 6 * sizeof(double));
//		BoundsAreSet = true;
//	}
//
//	void CuboDeCorte::CreateWidget()
//	{	
//		Widget = vtkSmartPointer<vtkBoxWidget>::New();
//		Widget->SetInteractor(interactor);
//		Widget->SetPlaceFactor(1.1);
//		Widget->SetProp3D(volume);
//		Widget->SetDefaultRenderer(interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
//		Widget->InsideOutOn();
//		if (!BoundsAreSet)
//			Widget->PlaceWidget();
//		else
//			Widget->PlaceWidget(CubeBoundsStore);
//		Widget->AddObserver(vtkCommand::StartInteractionEvent, Callback);
//		//Widget->AddObserver(vtkCommand::InteractionEvent, Callback);
//		Widget->AddObserver(vtkCommand::EndInteractionEvent, Callback);
//		Widget->RotationEnabledOff();
//		
//		Widget->TranslationEnabledOn();
//		Widget->ScalingEnabledOn();
//
//		Widget->EnabledOn();
//	}
//
//	void CuboDeCorte::DestroyWidget()
//	{
//		Widget->RemoveAllObservers();
//		interactor->Render();
//		Widget->EnabledOff();
//		interactor->Render();
//		Widget = nullptr;
//		interactor->Render();
//	}
//
//	void CuboDeCorte::AddOnCuboDeCorteChangeListener(CuboDeCorteChangeListener* c){
//		Callback->AddCuboChangeListener(c);
//	}
//
//CuboDeCorte :: CuboDeCorte(vtkSmartPointer<vtkRenderWindowInteractor> iren, vtkSmartPointer<vtkProp3D> vol ){
//	CubeBoundsStore = new double[6];
//	memset(CubeBoundsStore, 0.0, sizeof(double) * 6);
//	BoundsAreSet = false;
//	Callback = vtkSmartPointer<CuboDeCorteCallback>::New();
//	Callback->AddCuboChangeListener(this);
//	this->volume = vol;
//	this->interactor = iren;
//}
//
//bool CuboDeCorte :: IsAtivo(){
//	if (Widget != nullptr && Widget->GetEnabled())
//		return true;
//	else
//		return false;
//}
//
//void CuboDeCorte :: Ativar(){
//	CreateWidget();
//}
//
//void CuboDeCorte :: Desativar(){
//	DestroyWidget();
//}
//
//	CuboDeCorte::~CuboDeCorte()
//	{
//		delete[] CubeBoundsStore;
//	}
//
//	vtkAlgorithmOutput* CuboDeCorte::GetAsStencil(vtkImageData* imageDataToUseAsBasis)
//	{
//		vtkSmartPointer<vtkPolyData> pd = vtkSmartPointer<vtkPolyData>::New();
//		Widget->GetPolyData(pd);
//		vtkSmartPointer<vtkPolyDataToImageStencil> polydataToStencil = vtkSmartPointer<vtkPolyDataToImageStencil>::New();
//		polydataToStencil->SetInputData(pd);
//		polydataToStencil->SetInformationInput(imageDataToUseAsBasis);
//		polydataToStencil->Update();
//		return polydataToStencil->GetOutputPort();
//
//	}
//
//	vtkSmartPointer<vtkImageStencilData> CuboDeCorte::GetAsStencilData(vtkImageData* get_output)
//	{
//		vtkSmartPointer<vtkPolyData> pd = vtkSmartPointer<vtkPolyData>::New();
//		Widget->GetPolyData(pd);
//		vtkSmartPointer<vtkPolyDataToImageStencil> polydataToStencil = vtkSmartPointer<vtkPolyDataToImageStencil>::New();
//		polydataToStencil->SetInputData(pd);
//		polydataToStencil->SetInformationInput(get_output);
//		polydataToStencil->Update();
//		return polydataToStencil->GetOutput();
//	}
//}