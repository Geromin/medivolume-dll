//#include <vtkSmartPointer.h>
//#include <vtkBoxWidget.h>
//#include <vtkRenderWindowInteractor.h>
//#include <vtkProp3D.h>
//#include "CuboDeCorteCallback.h"
//#include "CuboDeCorteChangeListener.h"
//#include <vtkImageData.h>
//#include <memory>
//
//#pragma once
//#include <vtkImageStencilAlgorithm.h>
//using namespace std;
///**
//	Essa classe controle a funcionalidade do cubo de corte. Ela � controlada pelo Contexto, respons�vel
//	por cri�-la e ativar/desativar a funcionalidade.
//	Para usar essa classe deve ser informado o ator do volume e o interactor da tela onde o interactor
//	vai ser usado. � necess�rio informar tamb�m um listener de mudan�as de cubo de corte, uma classe
//	que implemente CuboDeCorteChangeListener. A funcionalidade � ativada com os m�todos Ativar e Desativar.
//	� o listener que � respons�ve por lidar com a informa��o do cubo de corte.
//*/
//namespace extrusao{
//class CuboDeCorte:public CuboDeCorteChangeListener{
//public:
//	void ExecutarOnCuboDeCorteChange(vtkSmartPointer<vtkPolyData> pd) override;
//	void ExecutarOnCuboDeCorteChange(double* bounds) override;
//	//Gera um stencil do cubo de corte.
//	vtkAlgorithmOutput* GetAsStencil(vtkImageData* imageDataToUseAsBasis);
//	vtkSmartPointer<vtkImageStencilData> GetAsStencilData(vtkImageData* get_output);
//private:
//	//vtkSmartPointer<vtkBoxWidget> OldWidget;
//	double* CubeBoundsStore;
//	bool BoundsAreSet;
//	vtkSmartPointer<CuboDeCorteCallback> Callback;
//	vtkSmartPointer<vtkBoxWidget> Widget;
//
//	vtkSmartPointer<vtkProp3D> volume;
//	vtkSmartPointer<vtkRenderWindowInteractor> interactor;
//
//	void CreateWidget();
//	void DestroyWidget();
//public:
//	void AddOnCuboDeCorteChangeListener(CuboDeCorteChangeListener* c);
//	CuboDeCorte(vtkSmartPointer<vtkRenderWindowInteractor> iren, vtkSmartPointer<vtkProp3D> vol );
//	void Ativar();
//	void Desativar();
//	bool IsAtivo();
//	~CuboDeCorte();
//};
//}