#ifndef _cdoph
#define _cdoph
#include <string>
using namespace std;
//tem uso em interfacev2
class CodigosDeOperacaoProvider
{
public:
	static int GetCodigoTipoDeRemocao(char* nome)
	{
		string str(nome);
		if (str == "circulo")
			return 100;
		if (str == "laco")
			return 101;
		if (str == "retangulo")
			return 102;
		return 0;
	}

	static int GetCodigoBotao(char* botao)
	{
		string str(botao);
		if (str == "direito")
			return 1;
		if (str == "meio")
			return 2;
		if (str == "esquerdo")
			return 3;
		return 0;
	}
	static int GetCodigoOperacao(char* nome)
	{
		string str(nome);
		if (str == "zoom")
			return 10;
		if (str == "pan")
			return 11;
		if (str == "rotate")
			return 12;
		if (str == "none")
			return 13;
		if (str == "window")
			return 14;
		if (str == "stencil")
			return 15;
		if (str == "seed_move")
			return 16;
		return 0;
	}
};

#endif
