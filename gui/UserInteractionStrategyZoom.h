#ifndef  __uisz
#define  __uisz
#include "UserInteractionStrategy.h"

class UserInteractionStrategyZoom:public UserInteractionStrategy
{
private:
	bool isPressionado;

public:
	void Execute(vtkRenderWindowInteractor* interactor, vr::VolumeV2* vol) override
	{
		if (isPressionado)
		{
			callbackOnMouseMove();
		}

	}
	UserInteractionStrategyZoom(const vtkSmartPointer<vtkInteractorStyle>& interactorStyle, callbackOnMouseMove cb)
		: UserInteractionStrategy(interactorStyle, cb)
	{
		nome = "zoom";
		isPressionado = false;
	}

	void Start() override
	{
		isPressionado = true;
		interactorStyle->StartDolly();
	}

	void End() override
	{
		isPressionado = false;
		interactorStyle->EndDolly();
	}
};

#endif
