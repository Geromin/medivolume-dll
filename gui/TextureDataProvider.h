#pragma once
class TextureDataProvider
{
public:
	virtual ~TextureDataProvider()
	{
	}

	virtual unsigned char* GetBufferLeft() = 0;
	virtual unsigned int GetBufferLeftSize() = 0;

	virtual unsigned char* GetBufferRight() = 0;
	virtual unsigned int GetBufferRightSize() = 0;

	virtual unsigned char* GetBufferAnterior() = 0;
	virtual unsigned int GetBufferAnteriorSize() = 0;

	virtual unsigned char* GetBufferPosterior() = 0;
	virtual unsigned int GetBufferPosteriorSize() = 0;

	virtual unsigned char* GetBufferSuperior() = 0;
	virtual unsigned int GetBufferSuperiorSize() = 0;

	virtual unsigned char* GetBufferInferior() = 0;
	virtual unsigned int GetBufferInferiorSize() = 0;
};
