#include "dicomGenerator.h"
#include <stdafx.h>
#include <vtkImagePermute.h>

DicomGenerator::DicomGenerator(string idExame, string idSerie)
{
	this->idExame = idExame;
	this->idSerie = idSerie;
	imagem = nullptr;
	imagemImportadaPraVTK = nullptr;
}

array<double, 3> DicomGenerator::CalculateSliceCenter(int qualFatia) {
	std::array<double, 3> physicalOrigin, spacing;
	std::array<int, 3> dimensions;
	auto img = imagemImportadaPraVTK->GetOutput();
	img->GetSpacing(spacing.data());
	img->GetOrigin(physicalOrigin.data());
	img->GetDimensions(dimensions.data());
	std::array<double, 3> resultado;
	imagemImportadaPraVTK->GetOutput()->GetCenter(resultado.data());
	resultado[2] = 0;
	std::array<double, 3> zVec = { { 0,0,1 } };
	zVec = physicalOrigin + double(qualFatia - 1) * spacing[2] * zVec;
	zVec[0] = 0;
	zVec[1] = 0;
	return resultado + zVec;
}

void DicomGenerator::SetProperties(int qualFatia, TipoOrientacao o)
{	
	this->sliceCenter = CalculateSliceCenter(qualFatia);
	this->orientacao = o;
}

void DicomGenerator::SetImage(itk::Image<short, 3>::Pointer imagem)
{
	this->imagem = imagem;
	imagemImportadaPraVTK = CreateVTKImage(imagem);
}

void DicomGenerator::Execute()
{
	//1)Cria o filtro de reslice
	//1.1)a janela offscreen
	auto renderer = NewVTK(vtkRenderer);
	auto renderWindow = NewVTK(vtkWin32OpenGLRenderWindow);
	renderWindow->OffScreenRenderingOn();
	renderWindow->AddRenderer(renderer);
	renderer->ResetCamera();
	//1.2)o mapper/actor
	auto im = NewVTK(vtkImageResliceMapper);
	im->SetInputConnection(imagemImportadaPraVTK->GetOutputPort());
	im->SliceFacesCameraOn();
	im->SliceAtFocalPointOn();
	im->BorderOff();
	im->AutoAdjustImageQualityOn();
	im->ResampleToScreenPixelsOff();
	im->GetImageReslice()->BorderOff();
	auto ip = NewVTK(vtkImageProperty);
	ip->SetColorWindow(350);
	ip->SetColorLevel(50);
	ip->SetInterpolationTypeToLinear();
	auto ia = NewVTK(vtkImageSlice);
	ia->SetMapper(im);
	ia->SetProperty(ip);
	renderer->AddViewProp(ia);
	//2)Seta as propriedades	
	//2.1)Posicao
	vtkCamera* camera = renderer->GetActiveCamera();
	std::array<double, 3> initialPosition, initialFocus;
	camera->GetPosition(initialPosition.data());
	camera->GetFocalPoint(initialFocus.data());
	std::array<double, 3> dInformedInitial = sliceCenter - initialFocus;
	std::array<double, 3> newFocus = initialFocus + dInformedInitial;
	std::array<double, 3> newPosition = initialPosition + dInformedInitial;
	camera->SetFocalPoint(newFocus.data());
	camera->SetPosition(newPosition.data());
	auto flipper = NewVTK(vtkImageFlip);
	auto permutter = NewVTK(vtkImagePermute);
	//2.2)Orientašao
	switch (orientacao) {
	case AXIAL:
		flipper->SetFilteredAxis(1);
		break;
	case CORONAL:
		camera->Pitch(90);
		break;
	case SAGITAL:
		camera->Azimuth(90);
		permutter->SetFilteredAxes(1, 0, 2);
		//camera->Elevation(90);
		// camera->OrthogonalizeViewUp();
		
		break;
	}
	//3)Execucao
	renderWindow->Render();
	auto reslicer = im->GetImageReslice();
	flipper->SetInputConnection(reslicer->GetOutputPort());
	permutter->SetInputConnection(flipper->GetOutputPort());
	permutter->Update();

	////Debug
	boost::posix_time::ptime current_date_microseconds = boost::posix_time::microsec_clock::local_time();
	long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
	std::string filename = "C:\\medivolume_src\\resliceCubico\\dump\\" + boost::lexical_cast<std::string>(milliseconds) + ".vti";
	vtkSmartPointer<vtkXMLImageDataWriter> debugsave = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	debugsave->SetFileName(filename.c_str());
	debugsave->SetInputData(permutter->GetOutput());
	debugsave->BreakOnError();
	debugsave->Write();
}
