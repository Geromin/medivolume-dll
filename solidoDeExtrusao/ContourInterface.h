#ifndef  __contour_interface
#define  __contour_interface
#include <vector>
using namespace std;
//Tem uso em volumeScreen
class ContourInterface
{
public:
	virtual ~ContourInterface()
	{
	}

	virtual void BeginDraw() = 0;
	virtual void EndDraw() = 0;
	virtual void DrawLasso(vector<int> is, vector<int> vector) = 0;
};
#endif
