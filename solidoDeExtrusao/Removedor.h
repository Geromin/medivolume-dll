#ifndef __removedor_h
#define __removedor_h
#include "PontoNaTela.h"
#include <vector>
#include <vtkSmartPointer.h>
#include <vtkProp3D.h>
#include <vtkImageAlgorithm.h>
#include <vtkRenderer.h>
#include <vector>
#include <vtkImageStencilData.h>

using namespace std;
namespace extrusao{
	class Removedor{
	private:
		vector<vtkSmartPointer<vtkImageStencilData>> stencilStack;
		//itk::MyItkStencil::Pointer Stencil;
		vtkSmartPointer<vtkImageAlgorithm> ImageSource;
		vtkSmartPointer<vtkProp3D> Actor;
		vtkSmartPointer<vtkRenderer> Renderer;
		vtkSmartPointer<vtkImageStencilData> stencilUndone;
		
	public:
		Removedor();
		~Removedor()
		{

		}

		//remove a regi�o
		vtkSmartPointer<vtkImageStencilData> CreateStencil(std::vector<extrusao::PontoNaTela> pontos_na_tela, bool keep_inside);

		
		//seta o ator - necess�rio pro processo de remo��o
		void SetActor(vtkSmartPointer<vtkProp3D> a)
		{
			this->Actor = a;
		}

		void SetImageSource(vtkSmartPointer<vtkImageAlgorithm> a)
		{
			ImageSource = a;
		}

		void SetRenderer(vtkSmartPointer<vtkRenderer> e)
		{
			Renderer = e;
		}
	};
}
#endif