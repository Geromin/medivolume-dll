#include "Removedor.h"
#include <vtkImageStencilData.h>
#include <vtkSmartPointer.h>
#include <vtkBox.h>
#include <vtkGeneralTransform.h>
#include <vtkImplicitFunctionToImageStencil.h>
#include <vtkImageData.h>
#include <memory>
#include "SolidoDeEstrusao.h"
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkPolyDataToImageStencil.h>
#include <vtkImageStencil.h>
#include <vtkPNGWriter.h>
#include <vtkImageToImageStencil.h>
using namespace std;
namespace extrusao
{
	Removedor::Removedor()
	{
		stencilUndone = nullptr;
		ImageSource = nullptr;
		Actor = nullptr;
		Renderer = nullptr;
	}



	//A regi�o que ser� removida � a que est� dentro do stencil: 110011, se a imagem fosse unidimensional.
	//Se for um stencil para manter o que est� dentro, ou seja 001100 o stencil deve ser gerado e invertido.
	//S� que o stencil n�o � uma matriz, � uma codifica��o estranha ent�o gambiarras s�o necess�rias.
	vtkSmartPointer<vtkImageStencilData> Removedor::CreateStencil(std::vector<extrusao::PontoNaTela> pontos_na_tela, bool keep_inside)
	{
		//Sanity check
		assert(ImageSource != nullptr);
		assert(Actor != nullptr);
		assert(Renderer != nullptr);
		//Gera o stencil do contorno dado
		shared_ptr<extrusao::SolidoDeEstrusao> solido = shared_ptr<extrusao::SolidoDeEstrusao>(new extrusao::SolidoDeEstrusao(pontos_na_tela, Renderer, keep_inside, this->Actor));
		vtkSmartPointer<vtkPolyDataToImageStencil> polydataToStencil = vtkSmartPointer<vtkPolyDataToImageStencil>::New();
		polydataToStencil->SetInputData(solido->GetPolyData());
		polydataToStencil->SetInformationInput(vtkImageData::SafeDownCast(ImageSource->GetOutputDataObject(0)));
		polydataToStencil->Update();
		vtkSmartPointer<vtkImageStencilData> sd = polydataToStencil->GetOutput();
		if (keep_inside) //invers�o do sd caso necess�rio
		{
			vtkImageData* propSrc = ImageSource->GetOutput();
			vtkSmartPointer<vtkImageData> tempImage = vtkSmartPointer<vtkImageData>::New();
			tempImage->SetDimensions(propSrc->GetDimensions());
			tempImage->SetSpacing(propSrc->GetSpacing());
			tempImage->SetOrigin(propSrc->GetOrigin());
			tempImage->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
			memset(tempImage->GetScalarPointer(), 0XFF, tempImage->GetDimensions()[0] * tempImage->GetDimensions()[1] * tempImage->GetDimensions()[2]);
			vtkSmartPointer<vtkImageStencil> sten = vtkSmartPointer<vtkImageStencil>::New();
			sten->SetInputData(tempImage);
			sten->SetStencilData(sd);
			sten->ReverseStencilOn();
			sten->SetBackgroundValue(0x00);
			sten->Update();
			//Aqui tenho o stencil para conservar somente o interior do contorno com um vtkImageData 
			//preciso transformar o imageData em um vtkImageStencilData
			vtkSmartPointer<vtkImageToImageStencil> imgToSten = vtkSmartPointer<vtkImageToImageStencil>::New();
			imgToSten->SetInputData(sten->GetOutput());
			imgToSten->ThresholdByUpper(254);
			imgToSten->Update();
			sd = imgToSten->GetOutput();
		}
		return sd;

	}
	
}