#include "SolidoDeEstrusao.h"
#include <vector>
#include <vtkProperty.h>
#include <vtkCubeSource.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
using namespace std;

namespace extrusao{
	/*void SolidoDeEstrusao::SetGeneratedStencil(vtkSmartPointer<vtkImageStencilData> sd){
		this->StencilCache = sd;
	}
*/
	//vtkSmartPointer<vtkImageStencilData> SolidoDeEstrusao::GetGeneratedStencil(){
	//	return StencilCache;
	//}

	//bool SolidoDeEstrusao::HasGeneratedStencil(){
	//	if (StencilCache.GetPointer() == 0)
	//		return false;
	//	else
	//		return true;
	//}

	void SolidoDeEstrusao::FabricarTriangulador()
	{
		Triangulador = vtkSmartPointer<vtkTriangleFilter>::New();
		Triangulador->SetInputData(Resultado);
		Triangulador->Update();
	}

	vtkSmartPointer<vtkPolyData> SolidoDeEstrusao::GetPolyData()
	{
		return Triangulador->GetOutput();
	}

	vtkSmartPointer<vtkActor> SolidoDeEstrusao::GetPropDoContorno()
	{
		return PropDoContorno;
	}

	vtkSmartPointer<vtkPolyData> SolidoDeEstrusao::FabricarContorno(vtkSmartPointer<vtkPoints> pts)
	{
		vtkSmartPointer<vtkCellArray> poly = vtkSmartPointer<vtkCellArray>::New();
		int cellSize = Pontos->GetNumberOfPoints();
		poly->InsertNextCell(cellSize);
		for (int i = 0; i < cellSize; i++){
			poly->InsertCellPoint(i);
		}
		vtkSmartPointer<vtkPolyData> pdata = vtkSmartPointer<vtkPolyData>::New();
		pdata->SetPoints(Pontos);
		pdata->SetPolys(poly);
		return pdata;
	}

	SolidoDeEstrusao::SolidoDeEstrusao(vtkSmartPointer<vtkPoints> lstPontos, double vetor[3], double size)
	{
		StencilCache = 0;
		//a lista j� est� criada
		Pontos = vtkSmartPointer<vtkPoints>::New();
		Pontos->DeepCopy(lstPontos);
		Contorno = FabricarContorno(Pontos);

		FabricarExtrusao(vetor);

		FabricarRepresentacao();

		FabricarTriangulador();
	}

	void SolidoDeEstrusao::FabricarExtrusao(double vetor[3], bool keepInside)
	{
		Extrusor = vtkSmartPointer<vtkLinearExtrusionFilter>::New();
		Extrusor->CappingOn();
		Extrusor->SetInputData(Contorno);
		Extrusor->SetScaleFactor(5000);
		Extrusor->SetExtrusionTypeToVectorExtrusion();
		Extrusor->SetVector(vetor);
		Extrusor->Update();

		if (false)
		{
			//long t0 = GetCurrentTime();
		

			static float x[8][3] = { 
				{ -1000, -1000, -1000 }, { 1000, -1000, -1000 }, { 1000, 1000, -1000 }, { -1000, 1000, -1000 },
				{ -1000, -1000, 1000 }, { 1000, -1000, 1000 }, { 1000, 1000, 1000 }, { -1000, 1000, 1000 }
			};
			static vtkIdType pts[6][4] = { 
				{ 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 0, 1, 5, 4 },
				{ 1, 2, 6, 5 }, { 2, 3, 7, 6 }, { 3, 0, 4, 7 } 
			};

			// We'll create the building blocks of polydata including data attributes.
			vtkPolyData *cube = vtkPolyData::New();
			vtkPoints *points = vtkPoints::New();
			vtkCellArray *polys = vtkCellArray::New();
			vtkFloatArray *scalars = vtkFloatArray::New();

			// Load the point, cell, and data attributes.
			for (int i = 0; i<8; i++) points->InsertPoint(i, x[i]);
			for (int i = 0; i<6; i++) polys->InsertNextCell(4, pts[i]);
			for (int i = 0; i<8; i++) scalars->InsertTuple1(i, i);

			// We now assign the pieces to the vtkPolyData.
			cube->SetPoints(points);
			points->Delete();
			cube->SetPolys(polys);
			polys->Delete();
			cube->GetPointData()->SetScalars(scalars);
			scalars->Delete();

			vtkSmartPointer<vtkPolyData> pd = vtkSmartPointer<vtkPolyData>::New();
			pd->SetPoints(points);
			pd->SetPolys(polys);
			polys->Delete();
			cube->GetPointData()->SetScalars(scalars);
			scalars->Delete();

			vtkSmartPointer<vtkTriangleFilter> t1 = vtkSmartPointer<vtkTriangleFilter>::New();
			t1->SetInputData(Extrusor->GetOutput());
			t1->Update();

			vtkSmartPointer<vtkTriangleFilter> t2 = vtkSmartPointer<vtkTriangleFilter>::New();
			t2->SetInputData(pd);
			t2->Update();
			//vtkSmartPointer<vtkCubeSource> cubeSrc = vtkSmartPointer<vtkCubeSource>::New();
			//cubeSrc->SetXLength(1000);
			//cubeSrc->SetYLength(1000);
			//cubeSrc->SetZLength(1000);//SetBounds(VolumeActor->GetBounds());
			//cubeSrc->SetCenter(VolumeActor->GetCenter());
			//cubeSrc->Update();
			//vtkPolyData* r = cubeSrc->GetOutput();
			//vtkPolyData* e = Extrusor->GetOutput();

			vtkSmartPointer<vtkBooleanOperationPolyDataFilter> intersector = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
			intersector->SetOperationToIntersection();
			intersector->SetInputData(0, Extrusor->GetOutput());
			intersector->SetInputData(1, pd);
			intersector->Update();
			Resultado = intersector->GetOutput();
			//long t = GetCurrentTime();
		
		}
		else
		{
			Resultado = Extrusor->GetOutput();
		}
		//se � para manter o que est� dentro eu tenho que criar um s�lido S1 com as dimens�es do volume e
		//fazer com que o resultado seja S1 - s�lido
		/*	if(this->KeepInside){
				vtkSmartPointer<vtkCubeSource> cubo = vtkSmartPointer<vtkCubeSource>::New();
				cubo->SetCenter(VolumeActor->GetCenter());
				cubo->SetBounds(VolumeActor->GetBounds());
				cubo->Update();
				vtkPolyData* cd = cubo->GetOutput();
				vtkPolyData* ce = Extrusor->GetOutput();
				//a partir daqui est� criado o cubo. Posso us�-lo pra fazer a subtra��o.
				vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOp = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
				booleanOp->SetOperationToIntersection();
				booleanOp->SetInputData(0, ce);
				//booleanOp->SetInputData(1, ce);
				booleanOp->Update();
				Resultado = booleanOp->GetOutput();
				}
				else{
				Resultado = Extrusor->GetOutput();
				}*/
	}

	void SolidoDeEstrusao::FabricarRepresentacao()
	{
		MapperDoContorno = vtkSmartPointer<vtkPolyDataMapper>::New();
		MapperDoContorno->SetInputData(Resultado);
		PropDoContorno = vtkSmartPointer<vtkActor>::New();
		PropDoContorno->GetProperty()->SetRepresentationToSurface(); //Wireframe();
		PropDoContorno->GetProperty()->SetOpacity(0.9);
		PropDoContorno->SetMapper(MapperDoContorno);
	}

	SolidoDeEstrusao::SolidoDeEstrusao(vector<PontoNaTela> lstPontos, vtkSmartPointer<vtkRenderer> renderer, bool keepInside, vtkProp3D* _volActor){
		this->KeepInside = keepInside;
		this->VolumeActor = _volActor;
		StencilCache = 0;
		this->Pontos = vtkSmartPointer<vtkPoints>::New();
		vector<vtkSmartPointer<vtkCoordinate>> lstCoordinates;
		for (unsigned int i = 0; i < lstPontos.size(); i++){
			vtkSmartPointer<vtkCoordinate> c = vtkSmartPointer<vtkCoordinate>::New();
			c->SetCoordinateSystemToDisplay();
			lstCoordinates.push_back(c);
		}
		for (unsigned int i = 0; i < lstPontos.size(); i++){
			PontoNaTela p = lstPontos[i];
			vtkCoordinate* coord = lstCoordinates[i];
			coord->SetValue(p.x, p.y);
			double* wc = coord->GetComputedWorldValue(renderer);//� aqui que uso o renderer pedido no construtor

			Pontos->InsertNextPoint(wc[0], wc[1], wc[2]);
		}
		Contorno = FabricarContorno(Pontos);

		double* v = renderer->GetActiveCamera()->GetDirectionOfProjection();
		double vetor[] = { v[0], v[1], v[2] };
		FabricarExtrusao(vetor, keepInside);
		FabricarRepresentacao();
		FabricarTriangulador();
	}

	SolidoDeEstrusao::SolidoDeEstrusao(vector<PontoNaTela> lstPontos, vtkSmartPointer<vtkRenderer> renderer){
			StencilCache = 0;
		//cria a lista de pontos.
		this->Pontos = vtkSmartPointer<vtkPoints>::New();
		//bacalhau pq n�o sei trabalhar direito com vtkCoordinate.
		vector<vtkSmartPointer<vtkCoordinate>> lstCoordinates;
		for (unsigned int i = 0; i < lstPontos.size(); i++){
			vtkSmartPointer<vtkCoordinate> c = vtkSmartPointer<vtkCoordinate>::New();
			c->SetCoordinateSystemToDisplay();
			lstCoordinates.push_back(c);
		}
		for (unsigned int i = 0; i < lstPontos.size(); i++){
			PontoNaTela p = lstPontos[i];
			vtkCoordinate* coord = lstCoordinates[i];
			coord->SetValue(p.x, p.y);
			double* wc = coord->GetComputedWorldValue(renderer);//� aqui que uso o renderer pedido no construtor

			Pontos->InsertNextPoint(wc[0], wc[1], wc[2]);
		}
		//cria o contorno
		Contorno = FabricarContorno(Pontos);
		//cria a extrus�o
		double* v = renderer->GetActiveCamera()->GetDirectionOfProjection();

		double vetor[] = { v[0], v[1], v[2] };

		FabricarExtrusao(vetor);

		FabricarRepresentacao();

		FabricarTriangulador();
	}


	SolidoDeEstrusao::~SolidoDeEstrusao(void)
	{
	}
}