#ifndef _____remo_h
#define _____remo_h
#include <vector>

namespace extrusao{
	class PontoNaTela
	{
	public:
		int x;
		int y;

		PontoNaTela(void)
		{
			x = 0;
			y = 0;
		}

		PontoNaTela(int x, int y)
		{
			this->x = x;
			this->y = y;
		}

		PontoNaTela(const PontoNaTela* src){
			x = src->x;
			y = src->y;
		}

		bool operator==(const PontoNaTela& other){
			if (this->x == other.x && this->y == other.y)
				return true;
			else
				return false;
		}

		~PontoNaTela(void)
		{
		}
	};

	struct  Contorno{
		int Tamanho;
		bool KeepInside;
		int* X;
		int* Y;
	};
	//Func��o de convers�o de contorno para pontos.
	inline std::vector<PontoNaTela> ContornoToPontos(Contorno* contour)
	{
		std::vector<PontoNaTela> pontos;
		for (int i = 0; i < contour->Tamanho; i++){
			PontoNaTela p(contour->X[i], contour->Y[i]);
			pontos.push_back(p);
		}
		return pontos;
	}
}
#endif
