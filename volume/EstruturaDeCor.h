#pragma once
/*
	Carrega as estruturas de cor entre as regi�es do sistema.
*/
namespace vr{
	struct EstruturaDeCor{
		int Tamanho;
		int* x;
		float* r;
		float* g;
		float *b;
		float *a;
		float *midpoint;
		float *sharpness;
		int tipoDeCor;
		bool clamping;
		float scalarUnitDistance;
		int blending;
		bool shading;
		float ambient;
		float diffuse;
		float specularCoef;
		float specularPower;
		bool usarGradiente;
		int tamanhoFuncaoGradiente;
		bool clampingGradient;
		float* grad_x;
		float* grad_a;
		float* grad_midpoint;
		float* grad_sharpness;
		///**
		//	Quantidade de pontos
		//	*/
		//int Tamanho;
		///**
		//	Dom�nio da fun��o
		//	*/
		//int* x;
		///**
		//	Canal vermelho
		//	*/
		//float* r;
		///**
		//	Canal verde
		//	*/
		//float* g;
		///**
		//	Canal azul
		//	*/
		//float* b;
		///**
		//	Canal alpha
		//	*/
		//float* a;

		//float* midpoint;
		//
		//float* sharpness;

		//int tipoDeCor;

		//bool clamping;
		//
		//bool shading;

		//float scalarUnitDistance;

		//float specularPower;

		//int TamanhoFuncaoGradiente;
		//bool clampingGradiente;
		//float* grad_x;
		//float* grad_a;
		//float* grad_midpoint;
		//float* grad_sharpness;

	};
}