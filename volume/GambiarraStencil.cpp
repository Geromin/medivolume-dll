//#include "GambiarraStencil.h"
//#include <iostream>
//#include <vtkZLibDataCompressor.h>
//#include <thread>
//#include <algorithm>
//#include <fstream>
//
//using namespace std;
//namespace vr{
//GambiarraStencil::~GambiarraStencil()
//{
//#ifdef usando_compressao_na_memoria
//	delete compressedImage;
//#endif
//#ifdef usando_buffer_de_disco
//	bufferFile.close();
//	DeleteFile("_undobuf.dat");
//#endif
//	imageData = nullptr;
//}
//
//void GambiarraStencil::SetOriginalImage(itk::Image<short, 3>::Pointer image)
//{
//	imageData = image;
//#ifdef usando_buffer_de_disco
//	const long t0 = GetTickCount();
//	bufferFile.open("_undobuf.dat",fstream::in | fstream::out | fstream::binary | fstream::trunc); 
//	bufferFile.seekg(0);
//	short* dataBuffer = imageData->GetBufferPointer();
//	bufferFile.seekg(0, ios::beg);
//	bufferFile.write(reinterpret_cast<const char*>(dataBuffer), image->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
//	//bufferFile.seekg(0, ios::beg);
//	bufferFile.close();
//#endif
//
//#ifdef usando_compressao_na_memoria
//	const long t0 = GetTickCount();
//	if (compressedImage)
//		delete compressedImage;
//	vtkSmartPointer<vtkZLibDataCompressor> compressor = vtkSmartPointer<vtkZLibDataCompressor>::New();
//	const unsigned char* originalPtr = (const unsigned char*)image->GetBufferPointer();
//	unsigned long originalSize = image->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
//	unsigned long maxCompressionSpace = compressor->GetMaximumCompressionSpace(originalSize);
//	compressedImage = new unsigned char[maxCompressionSpace];
//	unsigned long  actualSz = compressor->Compress(originalPtr, originalSize, compressedImage, maxCompressionSpace);
//	compressedImageSize = actualSz;
//	unsigned char* temp = new unsigned char[actualSz];
//	memcpy(temp, compressedImage, actualSz);
//	delete []compressedImage;
//	compressedImage = temp;
//	std::cout << "tempo de compressao = " << GetTickCount() - t0 << endl;
//#endif
//}
//
//GambiarraStencil::GambiarraStencil()
//{
//#ifdef usando_compressao_na_memoria
//	compressedImage = nullptr;
//	compressedImageSize = -1;
//#endif
//	imageData = nullptr;
//}
//
//void GambiarraStencil::AddStencil(vtkSmartPointer<vtkImageStencilData> data)
//{
//	Stencils.push_back(data);
//}
//
//void GambiarraStencil::Undo()
//{
//	const long t0 = GetTickCount();
//	if (Stencils.size()>0)
//	{
//		StencilsUndone.push_back(Stencils[Stencils.size() - 1]);
//		Stencils.pop_back();
//	}
//#ifdef usando_buffer_de_disco
//	bufferFile.open("_undobuf.dat", fstream::in | fstream::binary | fstream::out);
//	bufferFile.seekg(0);
//	bufferFile.read((char*)imageData->GetBufferPointer(), imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
//	//char* temp = new char[imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short)];
//	//bufferFile.read(temp, imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
//	//memcpy(imageData->GetBufferPointer(), temp, imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
//	//delete[] temp;
//	//bufferFile.seekg(0, ios::beg);
//	bufferFile.close();
//#endif
//#ifdef usando_compressao_na_memoria
//	//reinfla a imagem comprimida
//	vtkSmartPointer<vtkZLibDataCompressor> compressor = vtkSmartPointer<vtkZLibDataCompressor>::New();
//	unsigned long uncompressedBufferSize = imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
//	unsigned char* originalPtr = (unsigned char*)imageData->GetBufferPointer();
//	compressor->Uncompress(compressedImage, compressedImageSize, originalPtr, uncompressedBufferSize);
//	cout << "tempo de decompress�o no undo = " << GetTickCount() - t0 << endl;
//	//copia o buffer pra dentro do imageData: retornei ao estado original
//	//Aplica a lista de stencils sem o stencil que acabei de remover
//	//ApplyStencil(imageData);
//#endif
//}
//
//void GambiarraStencil::Redo()
//{
//	if (StencilsUndone.size()>0)
//	{
//		Stencils.push_back(StencilsUndone[StencilsUndone.size() - 1]);
//		StencilsUndone.pop_back();
//	}
//}
//
//void GambiarraStencil::Clear()
//{
//	const long t0 = GetTickCount();
//	for (int i = Stencils.size() - 1; i >= 0; i--)
//	{
//		StencilsUndone.push_back(Stencils[i]);
//		Stencils.pop_back();
//	}
//#ifdef usando_buffer_de_disco
//	bufferFile.open("_undobuf.dat", fstream::in | fstream::binary | fstream::out);
//	bufferFile.seekg(0);
//	bufferFile.read((char*)imageData->GetBufferPointer(), imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short));
//	bufferFile.seekg(0, ios::beg);
//	bufferFile.close();
//#endif
//#ifdef usando_compressao_na_memoria
//	vtkSmartPointer<vtkZLibDataCompressor> compressor = vtkSmartPointer<vtkZLibDataCompressor>::New();
//	unsigned long uncompressedBufferSize = imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
//	unsigned char* originalPtr = (unsigned char*)imageData->GetBufferPointer();
//	compressor->Uncompress(compressedImage, compressedImageSize, originalPtr, uncompressedBufferSize);
//	cout << "tempo de decompress�o no undo = " << GetTickCount() - t0 << endl;
//#endif
//}
////� aqui que � a parada
//
//void GambiarraStencil::ApplyStencil(itk::Image<short, 3>::Pointer image)
//{
//#ifdef usando_compressao_na_memoria
//	assert(compressedImage && "Esqueceu de gerar a vers�o comprimida da imagem original?"); //Esqueceu de setar qual � a imagem original?
//#endif
//	long t0 = GetTickCount();
//	//as dimens�es da imagem
//	int szX = image->GetLargestPossibleRegion().GetSize()[0];
//	int szY = image->GetLargestPossibleRegion().GetSize()[1];
//	int szZ = image->GetLargestPossibleRegion().GetSize()[2];
//	int numeroDeElementos = image->GetLargestPossibleRegion().GetNumberOfPixels();
//	short* ptr = image->GetBufferPointer();
//	vector<thread> workers;
//	for (int i = 0; i < 8; i++)
//	{
//		int inicioZ = szZ / 8 * i;
//		int fimZ = inicioZ + szZ / 8;
//		workers.push_back(thread([inicioZ, fimZ,szY, this, szX, ptr]()
//		{
//			for (int z = inicioZ; z < fimZ; z++)
//			{
//				for (int y = 0; y < szY; y++)
//				{
//					for (unsigned int i = 0; i < Stencils.size(); i++)
//					{
//						int hasNextExtent = 0;
//						int iter = 0;
//						do
//						{
//							int lowerAddr = (int)ptr;
//							int r1, r2;
//							//Eu preciso garantir que essa fn seja inline. Um dia recompilar a vtk pra isso
//							hasNextExtent =	 Stencils[i]->GetNextExtent(r1, r2, 0, szX - 1, y, z, iter);
//							if (r1<r2)
//							{
//								int i0 = r1 + y*szX + z * szX*szY;
//								int i1 = r2 + y*szX + z * szX*szY;
//								int ini = lowerAddr + i0*sizeof(short);
//								int fim = lowerAddr + i1*sizeof(short);
//								//novo
//								memset((void*)ini, 0x00, fim - ini);
//							}
//						} while (hasNextExtent != 0);
//					}
//				}
//			}
//		}));
//	}
//	for_each(workers.begin(), workers.end(), [](std::thread &t)
//	{
//		t.join();
//	});
//	long t = GetTickCount();
//	cout << "custo do stencil = " << (t - t0) << endl;
//
//}
//}