#include <vtkSmartPointer.h>
#include <vtkColorTransferFunction.h>
#pragma once
namespace corEOpacidade{
	class ScalarColorChangeListener{
	public:
		virtual void ExecuteColorChange(vtkSmartPointer<vtkColorTransferFunction> ct) = 0;
	};
}