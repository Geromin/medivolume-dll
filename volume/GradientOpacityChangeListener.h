#include <vtkSmartPointer.h>
#include <vtkPiecewiseFunction.h>
#pragma once

namespace vr{
	class GradientOpacityChangeListener{
	public:
		virtual ~GradientOpacityChangeListener()
		{
		}

		virtual void ExecuteGradientOpacityChange(vtkSmartPointer<vtkPiecewiseFunction> ct) = 0;
	};
}