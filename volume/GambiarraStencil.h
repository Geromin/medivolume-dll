//#define usando_compressao_na_memoria
#define usando_buffer_de_disco
#pragma warning(disable:4996)
#pragma warning(disable:4049)
#include <thread>

#ifndef __Gambi_stencil_h
#define __Gambi_stencil_h
#include <itkImage.h>
#include <vtkSmartPointer.h>
#include <vtkImageStencilData.h>
#include <vector>
#ifdef usando_buffer_de_disco
#include <fstream>
#endif
using namespace std;
namespace vr{
	template<class TImageType>
	class GambiarraStencil
	{
	private:
		typename TImageType::Pointer imageData;
		vector<vtkSmartPointer<vtkImageStencilData>> Stencils;
		vector<vtkSmartPointer<vtkImageStencilData>> StencilsUndone;
		std::fstream bufferFile;
	public:
		~GambiarraStencil()
		{
			bufferFile.close();
			DeleteFile("_undobuf.dat");
			imageData = nullptr;
		}
		GambiarraStencil()
		{
			imageData = nullptr;
		}
		void SetOriginalImage(typename TImageType::Pointer image)
		{
			imageData = image;
			bufferFile.open("_undobuf.dat", fstream::in | fstream::out | fstream::binary | fstream::trunc);
			bufferFile.seekg(0);
			typename TImageType::PixelType *dataBuffer = imageData->GetBufferPointer();
			size_t buffSize = image->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(TImageType::PixelType);
			bufferFile.seekg(0, ios::beg);
			bufferFile.write(reinterpret_cast<const char*>(dataBuffer), buffSize);
			bufferFile.close();
		}
		void AddStencil(vtkSmartPointer<vtkImageStencilData> data)
		{
			Stencils.push_back(data);
		}
		void ApplyStencil(typename TImageType::Pointer image)
		{
			int szX = image->GetLargestPossibleRegion().GetSize()[0];
			int szY = image->GetLargestPossibleRegion().GetSize()[1];
			int szZ = image->GetLargestPossibleRegion().GetSize()[2];
			int numeroDeElementos = image->GetLargestPossibleRegion().GetNumberOfPixels();
			typename TImageType::PixelType *ptr = image->GetBufferPointer();
			vector<thread> workers;
			for (int i = 0; i < 8; i++)
			{
				int inicioZ = szZ / 8 * i;
				int fimZ = inicioZ + szZ / 8;
				workers.push_back(thread([inicioZ, fimZ, szY, this, szX, ptr]()
				{
					for (int z = inicioZ; z < fimZ; z++)
					{
						for (int y = 0; y < szY; y++)
						{
							for (unsigned int i = 0; i < Stencils.size(); i++)
							{
								int hasNextExtent = 0;
								int iter = 0;
								do
								{
									int lowerAddr = (int)ptr;
									int r1, r2;
									//Eu preciso garantir que essa fn seja inline. Um dia recompilar a vtk pra isso
									hasNextExtent = Stencils[i]->GetNextExtent(r1, r2, 0, szX - 1, y, z, iter);
									if (r1<r2)
									{
										int i0 = r1 + y*szX + z * szX*szY;
										int i1 = r2 + y*szX + z * szX*szY;
										int ini = lowerAddr + i0*sizeof(short);
										int fim = lowerAddr + i1*sizeof(short);
										//novo
										memset((void*)ini, 0x00, fim - ini);
									}
								} while (hasNextExtent != 0);
							}
						}
					}
				}));
			}
			for_each(workers.begin(), workers.end(), [](std::thread &t)
			{
				t.join();
			});
		}
		void Undo()
		{
			if (Stencils.size()>0)
			{
				StencilsUndone.push_back(Stencils[Stencils.size() - 1]);
				Stencils.pop_back();
			}
			bufferFile.open("_undobuf.dat", fstream::in | fstream::binary | fstream::out);
			bufferFile.seekg(0);
			bufferFile.read((char*)imageData->GetBufferPointer(), imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(typename TImageType::PixelType));
			bufferFile.close();
		}
		void Redo()
		{
			if (StencilsUndone.size()>0)
			{
				Stencils.push_back(StencilsUndone[StencilsUndone.size() - 1]);
				StencilsUndone.pop_back();
			}
		}
		void Clear()
		{
			for (int i = Stencils.size() - 1; i >= 0; i--)
			{
				StencilsUndone.push_back(Stencils[i]);
				Stencils.pop_back();
			}

			bufferFile.open("_undobuf.dat", fstream::in | fstream::binary | fstream::out);
			bufferFile.seekg(0);
			bufferFile.read((char*)imageData->GetBufferPointer(), imageData->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(typename TImageType::PixelType));
			bufferFile.seekg(0, ios::beg);
			bufferFile.close();
		}

	};
//	class GambiarraStencil
//	{
//	private:
//		itk::Image<short, 3>::Pointer imageData;
//		vector<vtkSmartPointer<vtkImageStencilData>> Stencils;
//		vector<vtkSmartPointer<vtkImageStencilData>> StencilsUndone;
//#ifdef usando_compressao_na_memoria
//		unsigned char* compressedImage;
//		unsigned long compressedImageSize;
//#endif
//#ifdef usando_buffer_de_disco
//		std::fstream bufferFile;
//#endif
//	public:
//		~GambiarraStencil();
//		GambiarraStencil();
//		void SetOriginalImage(itk::Image<short, 3>::Pointer image);
//		void AddStencil(vtkSmartPointer<vtkImageStencilData> data);
//		void ApplyStencil(itk::Image<short, 3>::Pointer image);
//		void Undo();
//		void Redo();
//		void Clear();
//	};
}
#endif
