﻿#include "VisualizacaoDoVolume.h"
#include "GPUVolumeMapper.h"


#include "CPUVolumeMapper.h"
#include <assert.h>
#include <sstream>
#include <vtkSmartPointer.h>
using namespace std;
static vtkSmartPointer<vtkPiecewiseFunction> _ScalarOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
static vtkSmartPointer<vtkColorTransferFunction> _ScalarColor = vtkSmartPointer<vtkColorTransferFunction>::New();
namespace vr
{

//		WindowCenter = center;
//	WindowWidth = width;
//	this->CalcularCorDaJanelaProSlicer(WindowCenter, WindowWidth);
//	if (JanelamentoAtivo){
	//	CalcularOpacidadeDaJanela(WindowCenter, WindowWidth);
	//	CalcularCorDaJanela(WindowCenter, WindowWidth);
	//	//propaga para quem está interessado em mudanças nas funções de cor/opacidade
	//	SmartMapper->Update();
	//	UpdateColorChangeListeners();
	//}
	static float _a[] = { 0, 1 };
	static float _mid[] = { 0.5, 0.5 };
	static float _sharp[] = { 0, 0 };
	static int center = -100;
	static int width = 100;

	void VisualizacaoDoVolume::SetEstruturaDeCorRaw(vr::EstruturaDeCor* e)
	{
		funcaoDeTransferencia.ClearFunction();
		for (int i = 0; i < e->Tamanho; i++)
		{
			funcaoDeTransferencia.AddPonto(e->x[i], e->r[i], e->g[i], e->b[i], e->a[i], e->midpoint[i], e->sharpness[i]);
		}
		//Gera a funções
		funcaoDeTransferencia.CriarFuncaoDaVtk(_ScalarColor, _ScalarOpacity);
		funcaoDeTransferencia.ComprimirFuncao(funcaoDeTransferencia.GetInitialWL()[0]);
		funcaoDeTransferencia.MoverFuncao(funcaoDeTransferencia.GetInitialWL()[1]);
		funcaoDeTransferencia.CriarFuncaoDaVtk(_ScalarColor, _ScalarOpacity);
		volumeMapper->SetFuncaoDeCor(_ScalarColor);
		volumeMapper->SetFuncaoDeOpacidadeEscalar(_ScalarOpacity);
		_SetWLAndCallListeners(funcaoDeTransferencia.GetInitialWL()[0], funcaoDeTransferencia.GetInitialWL()[1]);
		//passa pro thumb
		thumbProperty->GetRGBTransferFunction()->RemoveAllPoints();
		thumbProperty->GetRGBTransferFunction()->DeepCopy(_ScalarColor);
		thumbProperty->GetScalarOpacity()->RemoveAllPoints();
		thumbProperty->GetScalarOpacity()->DeepCopy(_ScalarOpacity);

	}

	void VisualizacaoDoVolume::SetClamping(bool clamping)
	{
		if (clamping)
		{
			_ScalarOpacity->ClampingOn();
			_ScalarColor->ClampingOn();
		}
		else
		{
			_ScalarOpacity->ClampingOff();
			_ScalarColor->ClampingOff();
		}
	}

	void VisualizacaoDoVolume::GetPontoDeCor(int i, double xes[6])
	{
		//vector<_pontoDeCor> pontosDeCor;
		xes[0] = pontosDeCor[i].x - scalarOffset;
		xes[1] = pontosDeCor[i].r;
		xes[2] = pontosDeCor[i].g;
		xes[3] = pontosDeCor[i].b;
		xes[4] = 0.5; //hardcoded
		xes[5] = 0.0; //hardcoded

	}

	void VisualizacaoDoVolume::GetPontoDeAlpha(int i, double xes[4])
	{
		xes[0] = pontosDeCor[i].x - scalarOffset;
		xes[1] = pontosDeCor[i].a;
		xes[2] = 0.5;//hardcoded
		xes[3] = 0.0;//hardcoded
	}

	vtkSmartPointer<vtkPiecewiseFunction> VisualizacaoDoVolume::GetOpacidadeGradiente()
	{
		vtkSmartPointer<vtkPiecewiseFunction> fn = vtkSmartPointer<vtkPiecewiseFunction>::New();
		fn->DeepCopy(volumeMapper->GetFuncaoDeOpacidadeGradiente());
		return fn;
	}

	void VisualizacaoDoVolume::SwitchGradient(bool is_to_use)
	{
		if (is_to_use)
			volumeMapper->GetProperties()->DisableGradientOpacityOff();
		else
			volumeMapper->GetProperties()->DisableGradientOpacityOn();
	}

	void VisualizacaoDoVolume::SwitchGradientDoThumb(bool is_to_use)
	{
		if (is_to_use)
			thumbProperty->DisableGradientOpacityOff();
		else
			thumbProperty->DisableGradientOpacityOn();
	}

	void VisualizacaoDoVolume::SetShadingDoThumb(bool shade)
	{
		if (shade)
			thumbProperty->ShadeOn();
		else
			thumbProperty->ShadeOff();
	}

	void VisualizacaoDoVolume::SetEstruturaDeCorDoThumb(EstruturaDeCor* e)
	{
		vtkSmartPointer<vtkColorTransferFunction> colorFn = vtkSmartPointer<vtkColorTransferFunction>::New();
		vtkSmartPointer<vtkPiecewiseFunction> opFn = vtkSmartPointer<vtkPiecewiseFunction>::New();
		FuncaoDeTranferencia foo;
		foo.ClearFunction();
		for (int i = 0; i < e->Tamanho; i++)
		{
			foo.AddPonto(e->x[i], e->r[i], e->g[i], e->b[i], e->a[i], e->midpoint[i], e->sharpness[i]);
		}
		foo.CriarFuncaoDaVtk(colorFn, opFn);
		//passa pro thumb
		thumbProperty->GetRGBTransferFunction()->RemoveAllPoints();
		thumbProperty->GetRGBTransferFunction()->DeepCopy(colorFn);
		thumbProperty->GetScalarOpacity()->RemoveAllPoints();
		thumbProperty->GetScalarOpacity()->DeepCopy(opFn);
	}

	void VisualizacaoDoVolume::SetClampingDoThumb(bool clamping)
	{
		if (clamping)
		{
			thumbProperty->GetRGBTransferFunction()->ClampingOn();
			thumbProperty->GetGrayTransferFunction()->ClampingOn();
		}
		else
		{
			thumbProperty->GetRGBTransferFunction()->ClampingOff();
			thumbProperty->GetGrayTransferFunction()->ClampingOff();

		}
	}

	void VisualizacaoDoVolume::SetDiffuseDoThumb(float x)
	{
		thumbProperty->SetDiffuse(x);
	}

	void VisualizacaoDoVolume::SetAmbientDoThumb(float x)
	{
		thumbProperty->SetAmbient(x);
	}

	void VisualizacaoDoVolume::PushColorTransferFunction()
	{
		funcaoDeTransferencia.Push();
	}

	void VisualizacaoDoVolume::PopColorTransferFunction()
	{
		funcaoDeTransferencia.Pop();
		funcaoDeTransferencia.CriarFuncaoDaVtk(_ScalarColor, _ScalarOpacity);
		volumeMapper->SetFuncaoDeCor(_ScalarColor);
		volumeMapper->SetFuncaoDeOpacidadeEscalar(_ScalarOpacity);
	}

	void VisualizacaoDoVolume::BackdoorCor()
	{
		vtkSmartPointer<vtkPiecewiseFunction> opacity_function = vtkSmartPointer<vtkPiecewiseFunction>::New();
		opacity_function->AddPoint(21 + scalarOffset, 0);
		opacity_function->AddPoint(202 + scalarOffset, 0.027);
		opacity_function->AddPoint(404 + scalarOffset, 0.133);
		opacity_function->AddPoint(549 + scalarOffset, 0.682);
		//opacity_function->ClampingOn();
		vtkSmartPointer<vtkColorTransferFunction> color_function = vtkSmartPointer<vtkColorTransferFunction>::New();
		color_function->AddRGBPoint(21 + scalarOffset, 0, 0, 0);
		color_function->AddRGBPoint(202 + scalarOffset, 1, 0, 0);
		color_function->AddRGBPoint(404 + scalarOffset, 1, 1, 0);
		color_function->AddRGBPoint(549 + scalarOffset, 1, 1, 1);
		volumeMapper->SetFuncaoDeCor(color_function);
		volumeMapper->SetFuncaoDeOpacidadeEscalar(opacity_function);
		volumeMapper->GetProperties()->SetAmbient(1);
		volumeMapper->GetProperties()->SetDiffuse(1);
		volumeMapper->GetProperties()->SetSpecular(4);
		volumeMapper->GetProperties()->SetSpecularPower(50);
		volumeMapper->GetProperties()->SetInterpolationTypeToLinear();
		volumeMapper->GetProperties()->GetScalarOpacityUnitDistance(0.6770);
		volumeMapper->GetProperties()->ShadeOn();
	}
	vtkSmartPointer<vtkPiecewiseFunction> VisualizacaoDoVolume::createOpacity()
	{
		return createOpacity(pontosDeCor);
	}

	vtkSmartPointer<vtkPiecewiseFunction> VisualizacaoDoVolume::createGradient()
	{
		return createGrandient(pontosDeGradiente);
	}

	vtkSmartPointer<vtkColorTransferFunction> VisualizacaoDoVolume::createColor()
	{
		return createColor(pontosDeCor);
	}

	vtkSmartPointer<vtkPiecewiseFunction> VisualizacaoDoVolume::createGrandient(vector<_pontoDeOpacidadeGradiente> data)
	{
		vtkSmartPointer<vtkPiecewiseFunction> result = vtkSmartPointer<vtkPiecewiseFunction>::New();
		for (_pontoDeOpacidadeGradiente &p : data)
		{
			result->AddPoint(p.x, p.a, p.mid, p.shp);
		}
		return result;
	}

	vtkSmartPointer<vtkPiecewiseFunction> VisualizacaoDoVolume::createOpacity(vector<_pontoDeCor> data)
	{
		vtkSmartPointer<vtkPiecewiseFunction> result = vtkSmartPointer<vtkPiecewiseFunction>::New();
		for (_pontoDeCor &p : data)
		{
			result->AddPoint(p.x, p.a);
		}
		return result;
	}
	vtkSmartPointer<vtkColorTransferFunction> VisualizacaoDoVolume::createColor(vector<_pontoDeCor> data)
	{
		vtkSmartPointer<vtkColorTransferFunction> result = vtkSmartPointer<vtkColorTransferFunction>::New();
		for (_pontoDeCor &p : data)
		{
			result->AddRGBPoint(p.x, p.r, p.g, p.b);
		}
		return result;
	}


	void VisualizacaoDoVolume::SetFuncaoDeCor(EstruturaDeCor* estrutura_de_cor)
	{
		//struct _pontoDeCor{ float x, r, g, b, a; };
		//vector<_pontoDeCor> pontosDeCor;
		pontosDeCor.clear();
		for (int i = 0; i < estrutura_de_cor->Tamanho; i++)
		{
			_pontoDeCor p = { estrutura_de_cor->x[i], estrutura_de_cor->r[i], estrutura_de_cor->g[i], estrutura_de_cor->b[i], estrutura_de_cor->a[i] };
			pontosDeCor.push_back(p);
		}
		pontosDeGradiente.clear();
		if (estrutura_de_cor->usarGradiente)
		{
			for (int i = 0; i < estrutura_de_cor->tamanhoFuncaoGradiente; i++)
			{
				_pontoDeOpacidadeGradiente p = { estrutura_de_cor->grad_x[i],
					estrutura_de_cor->grad_a[i],
					estrutura_de_cor->grad_midpoint[i],
					estrutura_de_cor->grad_sharpness[i] };
					pontosDeGradiente.push_back(p);
			}
			volumeMapper->SetFuncaoDeOpacidadeGradiente(createGradient());
			volumeMapper->GetProperties()->DisableGradientOpacityOff();
		}
		else
		{
			volumeMapper->GetProperties()->DisableGradientOpacityOn();
		}

		volumeMapper->SetFuncaoDeCor(createColor());
		volumeMapper->SetFuncaoDeOpacidadeEscalar(createOpacity());

		volumeMapper->GetProperties()->SetAmbient(estrutura_de_cor->ambient);
		volumeMapper->GetProperties()->SetDiffuse(estrutura_de_cor->diffuse);
		volumeMapper->GetProperties()->SetSpecular(estrutura_de_cor->specularCoef);
		volumeMapper->GetProperties()->SetSpecularPower(estrutura_de_cor->specularPower);
		volumeMapper->GetProperties()->SetInterpolationTypeToLinear();//fixo
		volumeMapper->GetProperties()->GetScalarOpacityUnitDistance(estrutura_de_cor->scalarUnitDistance);

		if (estrutura_de_cor->blending == vtkVolumeMapper::MAXIMUM_INTENSITY_BLEND)
		{
			volumeMapper->ShadeOff();// GetProperties()->ShadeOff();//fixo, por hora
			volumeMapper->GetMapper()->SetBlendMode(estrutura_de_cor->blending);
		}
		if (estrutura_de_cor->blending == vtkVolumeMapper::COMPOSITE_BLEND)	{
			volumeMapper->ShadeOn();
			volumeMapper->GetMapper()->SetBlendModeToComposite();
		}
		if (estrutura_de_cor->blending == vtkVolumeMapper::MINIMUM_INTENSITY_BLEND){
			volumeMapper->ShadeOff();
			volumeMapper->GetMapper()->SetBlendModeToMinimumIntensity();
		}
		else
		{
//			volumeMapper->GetProperties()->ShadeOn();
			/*if (estrutura_de_cor->shading == true)
				volumeMapper->GetProperties()->ShadeOn();
			if (estrutura_de_cor->shading == false)
				volumeMapper->GetProperties()->ShadeOff();
			volumeMapper->GetMapper()->SetBlendMode(estrutura_de_cor->blending);*/
		}

		WindowWidth = pontosDeCor[pontosDeCor.size() - 1].x - pontosDeCor[0].x;
		WindowCenter = (pontosDeCor[pontosDeCor.size() - 1].x + pontosDeCor[0].x) / 2 - scalarOffset;
		InitialWW = WindowWidth;
		InitialWC = WindowCenter;
		janelaSetada = true;
	}

	void VisualizacaoDoVolume::SetGradientDoThumb(vtkSmartPointer<vtkPiecewiseFunction> fn)
	{
		thumbProperty->GetStoredGradientOpacity()->DeepCopy(fn);

	}

	void VisualizacaoDoVolume::SetSpecularDoThumb(float x)
	{
		thumbProperty->SetSpecular(x);
	}

	void VisualizacaoDoVolume::GetPontoDeOpacidadeGradiente(int i, double xes[4])
	{
		vtkSmartPointer<vtkPiecewiseFunction> fn = volumeMapper->GetFuncaoDeOpacidadeGradiente();
		fn->GetNodeValue(i, xes);
		//int sz = fn->GetSize();
		//for (int i = 0; i < sz; i++)
		//{
		//	double data[4];
		//	fn->GetNodeValue(i, data);
		//}
		//xes[0] = funcaoDeTransferencia.getX(i) - scalarOffset;
		//xes[1] = funcaoDeTransferencia.getA(i);
		//xes[2] = funcaoDeTransferencia.getMidpoint(i);
		//xes[3] = funcaoDeTransferencia.getSharpness(i);
	}

	void VisualizacaoDoVolume::SetGradientOpacity(vtkSmartPointer<vtkPiecewiseFunction> fn)
	{
		volumeMapper->SetFuncaoDeOpacidadeGradiente(fn);
		//passa pro thumb
		thumbProperty->GetGradientOpacity()->RemoveAllPoints();
		thumbProperty->GetGradientOpacity()->DeepCopy(fn);
	}
	void VisualizacaoDoVolume::_SetWLAndCallListeners(int w, int l)
	{
		this->WindowCenter = l;
		this->WindowWidth = w;
		for (unsigned int i = 0; i < OnWindowChangeListeners.size(); i++)
		{
			OnWindowChangeListeners[i]->ExecuteWindowChange(WindowCenter, WindowWidth); //(CalcularCorDaJanela(WindowCenter, WindowWidth));
		}
	}
	void VisualizacaoDoVolume::SetWindow(int width, int center)
	{
		double dLargura;
		double dCentro;
		if (janelaSetada)
		{
			dLargura = width - WindowWidth;
			dCentro = center - WindowCenter;
		}
		else
		{
			janelaSetada = true;
			_SetWLAndCallListeners(width, center);
			dLargura = width - WindowWidth;
			dCentro = center - WindowCenter;
		}
		dLargura = WindowWidth - InitialWW;
		dCentro = WindowCenter - InitialWC;
		vector<_pontoDeCor> localData = pontosDeCor;
		if (localData.size() == 0)
		{
			//struct _pontoDeCor{ float x, r, g, b, a; };
			_pontoDeCor p0 = { center - width, 1, 1, 1, 0 };
			_pontoDeCor p1 = { center + width, 1, 1, 1, 1 };
			localData.push_back(p0);
			localData.push_back(p1);
		}

		for (size_t i = 0; i < localData.size() / 2; i++)
		{
			localData[i].x = localData[i].x - (dLargura / 2) * ((localData.size()/(i+1)));
		}
		for (size_t i = localData.size() - 1; i >= localData.size() / 2; i--)
		{
			localData[i].x = localData[i].x + (dLargura / 2) * ((localData.size() / (i+ 1)));
		}
		for (_pontoDeCor &pt : localData)
		{
			pt.x = pt.x+dCentro;
		}
		std::cout << " -------- " << std::endl;
		size_t currentIndex = 0;
		while (currentIndex < localData.size())
		{
			for (size_t i = currentIndex + 1; i < localData.size(); i++)
			{
				if (localData[i].x <= localData[currentIndex].x)
					localData[i].x = localData[currentIndex].x + 1;
			}
			currentIndex++;
		}
		for (size_t i = 0; i < localData.size(); i++){
			std::cout << localData[i].x - scalarOffset << std::endl;
		}
		volumeMapper->SetFuncaoDeCor(createColor(localData));
		volumeMapper->SetFuncaoDeOpacidadeEscalar(createOpacity(localData));
		_SetWLAndCallListeners(width, center);

	}
	void VisualizacaoDoVolume::Update()
	{
		this->volumeMapper->Update();

	}



	vtkSmartPointer<vtkPiecewiseFunction>  VisualizacaoDoVolume::CalcularOpacidadeDaJanela(int wc, int ww)
	{
		_ScalarOpacity->RemoveAllPoints();
		_ScalarOpacity->AddPoint(0, 0);
		_ScalarOpacity->AddPoint((wc - ww / 2) + scalarOffset, 0);
		_ScalarOpacity->AddPoint((wc + ww / 2) +scalarOffset, 1);
		_ScalarOpacity->AddPoint(VTK_SHORT_MAX, 1);
		_ScalarOpacity->ClampingOn();
		return _ScalarOpacity;
	}

	vtkSmartPointer<vtkColorTransferFunction> VisualizacaoDoVolume::CalcularCorDaJanela(int wc, int ww)
	{
		_ScalarColor->RemoveAllPoints();
		_ScalarColor->AddRGBPoint(0, 0, 0, 0);
		_ScalarColor->AddRGBPoint((wc - ww / 2)+scalarOffset, 0, 0, 0);
		_ScalarColor->AddRGBPoint((wc - ww / 2)+scalarOffset, 1, 1, 1);
		_ScalarOpacity->AddPoint(VTK_SHORT_MAX, 1, 1, 1);
		_ScalarColor->ClampingOn();
		return _ScalarColor;
	}

	void VisualizacaoDoVolume::UpdateWindowChangeListeners()
	{
		for (unsigned int i = 0; i < OnWindowChangeListeners.size(); i++)
		{
			OnWindowChangeListeners[i]->ExecuteWindowChange(this->WindowCenter, this->WindowWidth);
		}
	}

	void VisualizacaoDoVolume::AtivarCorNoJanelamento()
	{
		this->UsandoCorNoJanelamento = true;
		this->volumeMapper->SetFuncaoDeCor(OldColor);
	}

	void VisualizacaoDoVolume::DesativarCorNoJanelamento()
	{
		this->UsandoCorNoJanelamento = false;
		this->volumeMapper->SetFuncaoDeCor(CalcularCorDaJanela(WindowCenter, WindowWidth));

	}

	void VisualizacaoDoVolume::AddOnScalarOpacityChangeListener(corEOpacidade::ScalarOpacityChangeListener* l){
		OnScalarOpacityChangeListeners.push_back(l);
	}

	void VisualizacaoDoVolume::AddOnGradientOpacityChangeListener(vr::GradientOpacityChangeListener* l){
		OnGradientOpacityChangeListeners.push_back(l);
	}

	void VisualizacaoDoVolume::UpdateGradientOpacityChangeListeners(){
		for (unsigned int i = 0; i < OnGradientOpacityChangeListeners.size(); i++){
			vr::GradientOpacityChangeListener* current = OnGradientOpacityChangeListeners[i];
			current->ExecuteGradientOpacityChange(this->GetMapper()->GetFuncaoDeOpacidadeGradiente());
		}
	}

	void VisualizacaoDoVolume::AddOnColorChangeListener(corEOpacidade::ScalarColorChangeListener* l){
		OnColorChangeListeners.push_back(l);
	}

	void VisualizacaoDoVolume::UpdateColorChangeListeners(){
		for (unsigned int i = 0; i < OnColorChangeListeners.size(); i++){
			corEOpacidade::ScalarColorChangeListener* current = OnColorChangeListeners[i];
			current->ExecuteColorChange(GetMapper()->GetFuncaoDeCor());
		}
	}

	void VisualizacaoDoVolume::UpdateScalarOpacityChangeListeners(){
		for (unsigned int i = 0; i < OnScalarOpacityChangeListeners.size(); i++){
			corEOpacidade::ScalarOpacityChangeListener* current = OnScalarOpacityChangeListeners[i];
			current->ExecuteScalarOpacityChange(this->GetMapper()->GetFuncaoDeOpacidadeEscalar());
		}
	}

	void VisualizacaoDoVolume::AtivarJanelamento()
	{
		this->JanelamentoAtivo = true;
		//guarda a tabela de cor e opacidade antiga
		this->OldOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
		this->OldOpacity->DeepCopy( volumeMapper->GetFuncaoDeOpacidadeEscalar() );
		this->OldColor = vtkSmartPointer<vtkColorTransferFunction>::New();
		this->OldColor->DeepCopy(volumeMapper->GetFuncaoDeCor());
		//calcula a nova
		volumeMapper->GetProperties()->SetInterpolationTypeToNearest();
		volumeMapper->SetFuncaoDeOpacidadeEscalar( CalcularOpacidadeDaJanela(WindowCenter, WindowWidth) );
		volumeMapper->GetProperties()->ShadeOn();
	}

	void VisualizacaoDoVolume::DesativarJanelamento()
	{
		this->JanelamentoAtivo = false;
		//restaura a tabela guardada em AtivarJanelamento
		if (OldColor != nullptr){
			volumeMapper->SetFuncaoDeCor(OldColor);
			volumeMapper->SetFuncaoDeOpacidadeEscalar(OldOpacity);
		}
		volumeMapper->GetProperties()->ShadeOn();
//		this->volumeMapper->RegenerateMask();
	}

	vtkSmartPointer<vtkColorTransferFunction> VisualizacaoDoVolume::GetTabelaDeCorJanelada()
	{
		return volumeMapper->GetFuncaoDeCor();
	}

	void VisualizacaoDoVolume::SetSamplingDistance(double min, double max)
	{

		volumeMapper->SetSamplingDistance(min, max);
//		this->volumeMapper->RegenerateMask();
	}



	VisualizacaoDoVolume::VisualizacaoDoVolume(int _tipo, vtkSmartPointer<vtkImageAlgorithm> imgSrc, vtkSmartPointer<vtkImageAlgorithm> resampledForThumb, map<string, string> metadata)
	{
		janelaSetada = false;
		//funcaoDeTransferencia = FuncaoDeTranferencia(0);
		//Window Center (0028,1050) and Window Width (0028,1051)
		{
			string sCenter = metadata["0028|1050"];
			int iCenter = atoi(sCenter.c_str());
			DefaultWindowCenter = iCenter;
		}
		{
			string sCenter = metadata["0028|1051"];
			int iWidth = atoi(sCenter.c_str());
			DefaultWindowWidth = iWidth;
		}
		//cria o mapper da thumb
		thumbMapper = vtkGPUVolumeRayCastMapper::New();
		thumbMapper->SetInputDataObject(resampledForThumb->GetOutput());
		thumbProperty = vtkVolumeProperty::New();
		vtkSmartPointer<vtkPiecewiseFunction> scalarOpacityFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
		scalarOpacityFunction->AddPoint(0, 0);
		scalarOpacityFunction->AddPoint(256, 1);
		vtkSmartPointer<vtkPiecewiseFunction> gradientOpacityFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
		gradientOpacityFunction->AddPoint(0, 1);
		gradientOpacityFunction->AddPoint(100, 1);
		vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction = vtkSmartPointer<vtkColorTransferFunction>::New();
		colorTransferFunction->AddRGBPoint(0, 1, 1, 1);
		thumbProperty->SetColor(colorTransferFunction); thumbProperty->SetScalarOpacity(scalarOpacityFunction); thumbProperty->SetScalarOpacity(gradientOpacityFunction);
		thumbActor = vtkVolume::New();
		thumbActor->SetMapper(thumbMapper);
		thumbActor->SetProperty(thumbProperty);
		//-------------

		//this->WindowWidth = DefaultWindowWidth;
		//this->WindowCenter = DefaultWindowCenter;
		Mapper* _map = nullptr;
		switch (_tipo)
		{
		case 2:
			_map = new CPUVolumeMapper(imgSrc);
			break;
		case 1:{
			imgSrc->Update();
			_map = new GPUVolumeMapper(imgSrc);
		}
			break;

		}
		this->volumeMapper = _map;
		volumeMapper->SetScalarOffset(scalarOffset);
	}


	vtkSmartPointer<vtkPiecewiseFunction> VisualizacaoDoVolume::GetOpacidadeEscalar()
	{
		vtkSmartPointer<vtkPiecewiseFunction> fn = vtkSmartPointer<vtkPiecewiseFunction>::New();
		fn->DeepCopy(volumeMapper->GetFuncaoDeOpacidadeEscalar());
		return fn;
	}


	void VisualizacaoDoVolume::SetScalarOpacity(vtkSmartPointer<vtkPiecewiseFunction> sotf)
	{
		volumeMapper->SetFuncaoDeOpacidadeEscalar(sotf);
	}



	void VisualizacaoDoVolume::SetColor(vtkSmartPointer<vtkColorTransferFunction> ctf)
	{
		volumeMapper->SetFuncaoDeCor(ctf);
	}

	void VisualizacaoDoVolume::SetWindowDefault()
	{
		//corEOpacidade::EstruturaDeCor e;
		//e.x = new int[2];
		//e.x[0] = DefaultWindowCenter - DefaultWindowWidth / 2 + scalarOffset;
		//e.x[1] = DefaultWindowCenter + DefaultWindowWidth / 2 + scalarOffset;
		//
		//e.r = new float[2];
		//e.r[0] = 0;
		//e.r[1] = 1;

		//e.g = new float[2];
		//e.g[0] = 0;
		//e.g[1] = 1;

		//e.b = new float[2];
		//e.b[0] = 0;
		//e.b[1] = 1;

		//e.a = new float[2];
		//e.a[0] = 0;
		//e.a[1] = 1;

		//e.midpoint = new float[2];
		//e.midpoint[0] = 0.5;
		//e.midpoint[1] = 0.5;

		//e.sharpness = new float[2];
		//e.sharpness[0] = 0;
		//e.sharpness[1] = 0;

		//e.Tamanho = 2;
		//this->SetEstruturaDeCorRaw(&e);
		//SetWindow(DefaultWindowWidth, DefaultWindowCenter);
		//this->funcaoDeTransferencia.SetValoresIniciais(DefaultWindowWidth, DefaultWindowCenter);
	}
}