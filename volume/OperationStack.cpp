#include "OperationStack.h"
namespace vr
{
	OperationStack::~OperationStack()
	{
		for (Operation* op : undoStack)
			delete op;
		for (Operation* op : redoStack)
			delete op;
	}

	vector<Operation*> OperationStack::getOperationsByType(Operation::Types typeId)
	{
		vector<Operation*> result;
		for (Operation* op : undoStack)
		{
			if (op->getType() == typeId)
				result.push_back(op);
		}
		return result;
	}

	void OperationStack::pushOperation(Operation* op)
	{
		undoStack.push_back(op);
	}

	void OperationStack::redoOperation()
	{
		if (redoStack.size() == 0) return;
		Operation* op = redoStack[redoStack.size() - 1];
		lastRedone = op->getType();
		redoStack.pop_back();
		undoStack.push_back(op);
	}

	void OperationStack::undoOperation()
	{
		if (undoStack.size() == 0) return;
		Operation* op = undoStack[undoStack.size() - 1];
		lastUndone = op->getType();
		undoStack.pop_back();
		redoStack.push_back(op);
	}
}