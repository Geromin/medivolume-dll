#ifndef __operation_h
#define __operation_h
#include <vtkSmartPointer.h>
#include <vtkImageStencilData.h>
#include <array>
namespace vr
{
	class Operation
	{
	public:
		enum Types { Stencil, CuboDeCorte };
		virtual Types getType() = 0;
	};

	class StencilOperation : public Operation
	{
	private:

	public:
		vtkSmartPointer<vtkImageStencilData> data;
		
		virtual Types getType()
		{
			return Stencil;
		}
	};

	class CuboDeCorteOperation : public Operation
	{
	public:
		std::array<double, 6> data;
		virtual Types getType()
		{
			return CuboDeCorte;
		}
	};
}
#endif