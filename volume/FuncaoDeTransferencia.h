#ifndef __funcao_de_transferencia_h
#define __funcao_de_transferencia_h
#include <vector>
#include <vtkColorTransferFunction.h>
#include <vtkPiecewiseFunction.h>
#include <assert.h>

using namespace std;
namespace vr
{
	class FuncaoDeTranferencia
	{
	private:
		short offset;
		struct _Fn
		{
			vector<float> x, r, g, b, a, midpoint, sharpness;
		};
		vector<_Fn> myStack;
		vector<float> x, r, g, b, a, midpoint, sharpness;
		float currentDeslocamento;
		float currentCompressao;
		float default_window_width;
		float default_window_center;
		int wl[2];
	public:
		float getX(int i){ return x[i]; }
		float getR(int i){ return r[i]; }
		float getG(int i){ return g[i]; }
		float getB(int i){ return b[i]; }
		float getA(int i){ return a[i]; }
		float getMidpoint(int i){ return midpoint[i]; }
		float getSharpness(int i){ return sharpness[i]; }
		void ClearFunction()
		{
			x.clear();
			r.clear();
			g.clear();
			b.clear();
			a.clear();
			midpoint.clear();
			sharpness.clear();
		}
		FuncaoDeTranferencia()
		{
			this->offset = 0;
			ClearFunction();
			currentDeslocamento = 0;
			currentCompressao = 0;
		}
		void SetOffset(short offset)
		{
			this->offset = offset;
		}

		void CriarFuncaoDaVtk(vtkColorTransferFunction* color, vtkPiecewiseFunction* scalarOpacity);

		void AddPonto(float x, float r, float g, float b, float a, float midpoint, float sharpness)
		{
			this->x.push_back(x);
			this->r.push_back(r);
			this->g.push_back(g);
			this->b.push_back(b);
			this->a.push_back(a);
			this->midpoint.push_back(midpoint);
			this->sharpness.push_back(sharpness);
		}
		void MoverFuncao(float dX);
		void ComprimirFuncao(float dX);
		void SetValoresIniciais(int default_window_width, int default_window_center);
		void Push();
		void Pop();
		int* GetInitialWL()
		{
			return wl;
		}
	};
}
#endif