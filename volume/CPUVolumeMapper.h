#ifndef __CPU_VOLUME_MAPPER_h
#define __CPU_VOLUME_MAPPER_h
#include "Mapper.h"

namespace vr
{
	class CPUVolumeMapper : public Mapper
	{
	public:
		void ShadeOff() override;
		void ShadeOn() override;
		void SetSamplingDistance(double min, double max) override;
		void SetBlendMode(int id_blend_mode) override;
	protected:
		virtual void CreateMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource) override;
	public:
		CPUVolumeMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource);
	};
}

#endif
