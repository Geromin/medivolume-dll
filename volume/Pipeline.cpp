#include "Pipeline.h"
#include <vtkMath.h>
#include <vector>
#include "UtilitiesV2.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include <itkChangeInformationImageFilter.h>

namespace vr{
	void __parseDirectionCosines(string str, double* x, double* y)
	{
		char* localStr = (char*)str.c_str();
		char* pch;
		vector<string> templst;
		pch = strtok(localStr, "\\");
		while (pch != nullptr)
		{
			templst.push_back(pch);
			pch = strtok(nullptr, "\\");
		}
		x[0] = atof(templst[0].c_str());
		x[1] = atof(templst[1].c_str());
		x[2] = atof(templst[2].c_str());

		y[0] = atof(templst[3].c_str());
		y[1] = atof(templst[4].c_str());
		y[2] = atof(templst[5].c_str());
	}

	template<typename T1, typename T2> void DeepCopy2(typename T1::Pointer input, typename T2::Pointer output)
	{
		int inSzX = input->GetLargestPossibleRegion().GetSize()[0];
		int inSzY = input->GetLargestPossibleRegion().GetSize()[1];
		int inSzZ = input->GetLargestPossibleRegion().GetSize()[2];
		double inSpacingX = input->GetSpacing()[0];
		double inSpacingY = input->GetSpacing()[1];
		double inSpacingZ = input->GetSpacing()[2];

		output->SetRegions(input->GetLargestPossibleRegion());
		output->Allocate();

		itk::ImageRegionConstIterator<T1> inputIterator(input, input->GetLargestPossibleRegion());
		itk::ImageRegionIterator<T2> outputIterator(output, output->GetLargestPossibleRegion());
		const double inSpacing[] = { inSpacingX, inSpacingY, inSpacingZ };
		output->SetSpacing(inSpacing);
		while (!inputIterator.IsAtEnd())
		{
			outputIterator.Set(inputIterator.Get());
			++inputIterator;
			++outputIterator;
		}
	}



	//template <class TImageType>
	//Pipeline<TImageType>::Pipeline(typename TImageType::Pointer src, DelphiProgressCallback dProgressCallback, int mapperType, map<string, string> metadata, short offset)
	//{
	//	this->imagemItk = src;
	//	this->metadata = metadata;
	//	//O observer de progresso originalmente era obrigat�rio. Isso � inaceit�vel na vers�o atual do sistema
	//	//pois o Vincenzo tirou a barra de progresso de filtro.
	//	if (dProgressCallback){
	//		ItkProgressObserver = vr::ItkProgressCommand::New();
	//		ItkProgressObserver->ProgressCallback = dProgressCallback;
	//	}
	//	else
	//	{
	//		ItkProgressObserver = nullptr;
	//	}
	//	valueOffset = offset;
	//	//bota no stencil
	//	gambiarraStencil.SetOriginalImage(imagemItk);
	//	vtkImporter = vtkSmartPointer<vtkImageImport>::New();
	//	this->tipoDeMapperEscolhido = mapperType;
	//	SetupVTKImporter(vtkImporter);
	//	//A vers�o mini pras thumbs
	//	resampledForThumbs = vtkSmartPointer<vtkImageResample>::New();
	//	resampledForThumbs->SetAxisMagnificationFactor(0, 0.33);
	//	resampledForThumbs->SetAxisMagnificationFactor(1, 0.33);
	//	resampledForThumbs->SetAxisMagnificationFactor(2, 0.33);
	//	resampledForThumbs->SetInputData(vtkImporter->GetOutput());
	//	resampledForThumbs->Update();//uma duplica�ao aqu
	//}

	template <class TImageType>
	void Pipeline<TImageType>::DesfazerRemocao()
	{
		gambiarraStencil.Undo();
		this->Update();
	}
	template <class TImageType>
	void Pipeline<TImageType>::RefazerRemocao()
	{
		gambiarraStencil.Redo();
		this->Update();
	}
	//template <class TImageType>
	//void Pipeline<TImageType>::AddStencil(vtkSmartPointer<vtkImageStencilData> sd)
	//{
	//	gambiarraStencil.AddStencil(sd);

	//}
	//template <class TImageType>
	//void Pipeline<TImageType>::DesfazerTudo()
	//{
	//	gambiarraStencil.Clear();
	//}


	//template <class TImageType>
	//void Pipeline<TImageType>::SetupVTKImporter(vtkSmartPointer<vtkImageImport> i)
	//{
	//	int szX = imagemItk->GetLargestPossibleRegion().GetSize()[0];
	//	int szY = imagemItk->GetLargestPossibleRegion().GetSize()[1];
	//	int szZ = imagemItk->GetLargestPossibleRegion().GetSize()[2];
	//	double sX = imagemItk->GetSpacing()[0];
	//	double sY = imagemItk->GetSpacing()[1];
	//	double sZ = imagemItk->GetSpacing()[2];
	//	double oX = imagemItk->GetOrigin()[0];
	//	double oY = imagemItk->GetOrigin()[1];
	//	double oZ = imagemItk->GetOrigin()[2];
	//	vtkImporter->SetDataSpacing(sX, sY, sZ);
	//	vtkImporter->SetDataOrigin(oX, oY, oZ);
	//	vtkImporter->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
	//	vtkImporter->SetDataExtentToWholeExtent();
	//	
	//	/*//Por enquanto eu vou assumir que � s� floats
	//	if (tipoDeMapperEscolhido == 2)
	//		vtkImporter->SetDataScalarTypeToUnsignedShort();
	//	if (tipoDeMapperEscolhido == 1)
	//		vtkImporter->SetDataScalarTypeToShort();
	//	*/
	//	vtkImporter->SetDataScalarTypeToFloat();
	//	void* imgPtr = imagemItk->GetBufferPointer();
	//	vtkImporter->SetImportVoidPointer(imgPtr, 1);
	//	vtkImporter->Update();
	//}
	//template <class TImageType>
	//void Pipeline<TImageType>::CreateVTKExporter(TipoDeMapper mapperType){
	//	vtkImporter = vtkSmartPointer<vtkImageImport>::New();
	//	this->tipoDeMapperEscolhido = mapperType;
	//	SetupVTKImporter(vtkImporter);
	//}

}
