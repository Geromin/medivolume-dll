#ifndef __mapper_h
#define __mapper_h
#include <vtkSmartPointer.h>
#include <vtkPiecewiseFunction.h>
#include <vtkColorTransferFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkVolume.h>
#include <vtkImageAlgorithm.h>
#include <vtkVolumeMapper.h>
#include <vtkImageData.h>
#include <vtkImageStencilData.h>
namespace vr
{
	//Encapsula os mappers. Existem v�rios mappers com detalhes particulares
	//Essa classe disponibiliza a interface minima dos mappers.
	class Mapper
	{
	protected:
		//O objeto abstrato de vtkMapper
		vtkSmartPointer<vtkVolumeMapper> mapper;
		//A fonte da imagem
		vtkSmartPointer<vtkImageAlgorithm> imageSource;
		//A fun��o de opacidade escalar
		vtkSmartPointer<vtkPiecewiseFunction> scalarOpacityFunction;
		//A fun��o de opacidade gradiente
		vtkSmartPointer<vtkPiecewiseFunction> gradientOpacityFunction;
		//A fun��o de transferencia de cor
		vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction;
		//As propriedades do volume
		vtkSmartPointer<vtkVolumeProperty> volumeProperties;
		//O actor.
		vtkSmartPointer<vtkVolume> volumeActor;

		virtual void CreateMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource) = 0;
		//cria as fun��es de cor
		virtual void CreateFunctions();

		virtual void CreateProperty();
		short scalarOffset;
		virtual void CreateVolume();
	public:

		
		vtkSmartPointer<vtkImageData> GetInput()
		{
			vtkSmartPointer<vtkImageData> r = imageSource->GetOutput();
			return r;
		}

		vtkSmartPointer<vtkVolumeMapper> GetMapper(){ return mapper; }

		virtual ~Mapper()
		{
		}

		void Crop(double* bounds);

		vtkSmartPointer<vtkVolumeProperty> GetProperties(){ return volumeProperties; }
		
		//Construtor da classe.
		Mapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource);

		void Update();

		vtkSmartPointer<vtkColorTransferFunction> GetFuncaoDeCor()
		{
			vtkSmartPointer<vtkColorTransferFunction> fn = vtkSmartPointer<vtkColorTransferFunction>::New();
			fn->DeepCopy(this->colorTransferFunction);
			return fn;
		}
		void SetFuncaoDeCor(vtkSmartPointer<vtkColorTransferFunction> cor);
		vtkSmartPointer<vtkPiecewiseFunction> GetFuncaoDeOpacidadeEscalar(){ return scalarOpacityFunction; }
		void SetFuncaoDeOpacidadeEscalar(vtkSmartPointer<vtkPiecewiseFunction> fn);
		vtkSmartPointer<vtkPiecewiseFunction> GetFuncaoDeOpacidadeGradiente(){ return gradientOpacityFunction; }
		void SetFuncaoDeOpacidadeGradiente(vtkSmartPointer<vtkPiecewiseFunction> fn);
		//Retorna o ator do volume, que � o que deve ser posto em um vtkRenderer.
		vtkSmartPointer<vtkVolume> GetActor()
		{
			return volumeActor;
		}

		virtual void SetSamplingDistance(double min, double max) = 0;
		void SetDiffuse(float x);
		void SetAmbient(float x);
		void SetSpecular(float x);
		void SetSpecularPower(float power);


		float GetSpecularPower()
		{
			return volumeProperties->GetSpecularPower();
		}

		void SetShading(bool shade);


		void SetScalarOffset(short scalar_offset)
		{
			this->scalarOffset = scalar_offset;
		}

		bool GetUsandoGradient();
		virtual void SetBlendMode(int id_blend_mode);
		virtual void ShadeOff() = 0;
		virtual void ShadeOn() = 0;
	};
}
#endif
