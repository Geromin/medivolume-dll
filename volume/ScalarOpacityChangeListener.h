#include <vtkSmartPointer.h>
#include <vtkPiecewiseFunction.h>
#pragma once

namespace corEOpacidade{
	class ScalarOpacityChangeListener{
	public:
		virtual void ExecuteScalarOpacityChange(vtkSmartPointer<vtkPiecewiseFunction> ct) = 0;
	};
}