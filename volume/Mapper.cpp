#include "Mapper.h"

namespace vr
{
	void Mapper::CreateFunctions()
	{
		scalarOpacityFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
		scalarOpacityFunction->AddPoint(0, 0);
		scalarOpacityFunction->AddPoint(256, 1);
		gradientOpacityFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
		gradientOpacityFunction->AddPoint(0, 1);
		gradientOpacityFunction->AddPoint(100, 1);
		colorTransferFunction = vtkSmartPointer<vtkColorTransferFunction>::New();
		colorTransferFunction->AddRGBPoint(0, 1, 1, 1);
	}

	void Mapper::CreateProperty()
	{
		volumeProperties = vtkSmartPointer<vtkVolumeProperty>::New();
		volumeProperties->ShadeOn();
		volumeProperties->SetScalarOpacity(scalarOpacityFunction);
		volumeProperties->SetGradientOpacity(gradientOpacityFunction);
		volumeProperties->SetColor(colorTransferFunction);
		volumeProperties->SetInterpolationTypeToLinear();
		volumeProperties->SetScalarOpacityUnitDistance(0.6);
	}

	void Mapper::CreateVolume()
	{
		volumeActor = vtkSmartPointer<vtkVolume>::New();
		volumeActor->SetProperty(volumeProperties);
		volumeActor->SetMapper(mapper);//ta crashando ocasionalmente por aqui.
	}

	void Mapper::Crop(double* bounds)
	{
		mapper->CroppingOn();
		mapper->SetCroppingRegionPlanes(bounds);
		mapper->SetCroppingRegionFlagsToSubVolume();
		mapper->Update();
	}

	Mapper::Mapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource)
	{
		this->imageSource = _imageSource;
	}

	void Mapper::Update()
	{
		imageSource->Modified();
		imageSource->Update();
		mapper->Modified();
		mapper->Update();
		volumeActor->Modified();
		volumeActor->Update();
	}

	void Mapper::SetFuncaoDeCor(vtkSmartPointer<vtkColorTransferFunction> cor)
	{
		colorTransferFunction->RemoveAllPoints();
		colorTransferFunction->DeepCopy(cor);
	}

	void Mapper::SetFuncaoDeOpacidadeEscalar(vtkSmartPointer<vtkPiecewiseFunction> fn)
	{
		scalarOpacityFunction->RemoveAllPoints();
		scalarOpacityFunction->DeepCopy(fn);
	}



	void Mapper::SetFuncaoDeOpacidadeGradiente(vtkSmartPointer<vtkPiecewiseFunction> fn)
	{
		gradientOpacityFunction->RemoveAllPoints();
		gradientOpacityFunction->DeepCopy(fn);
	}

	void Mapper::SetDiffuse(float x)
	{
		volumeProperties->SetDiffuse(x);
	}

	void Mapper::SetAmbient(float x)
	{
		volumeProperties->SetAmbient(x);
	}

	void Mapper::SetSpecular(float x)
	{
		volumeProperties->SetSpecular(x);
	}

	void Mapper::SetSpecularPower(float power)
	{
		volumeProperties->SetSpecularPower(power);
	}

	void Mapper::SetShading(bool shade)
	{
		if (shade)
			volumeProperties->ShadeOn();
		else
			volumeProperties->ShadeOff();
	}

	bool Mapper::GetUsandoGradient()
	{
		return !(volumeProperties->GetDisableGradientOpacity());
	}

	void Mapper::SetBlendMode(int id_blend_mode)
	{
		mapper->SetBlendMode(id_blend_mode);
	}
}