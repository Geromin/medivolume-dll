#ifndef _gpu_volume_mapper_h
#define _gpu_volume_mapper_h
#include "Mapper.h"
#include <vtkSmartPointer.h>
#include <vtkGPUVolumeRayCastMapper.h>
#include <vector>
#include <vtkImageStencilData.h>
using namespace std;
namespace vr
{
	class GPUVolumeMapper : public Mapper
	{
	public:
		void ShadeOff() override;
		void ShadeOn() override;
		void SetBlendMode(int id_blend_mode) override;
	protected:

	public:
	
	protected:
		virtual void CreateMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource) override;

	public:
		GPUVolumeMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource);
		void SetSamplingDistance(double min, double max) override;
		
		//void PopStencil() override;
		//void UnpopStencil() override;
		//void PushStencil(vtkSmartPointer<vtkImageStencilData> sten) override;
	private:
		~GPUVolumeMapper()
		{
			this->LastStencilUndone = nullptr;
			this->mapper = nullptr;
			this->volumeActor = nullptr;
			this->pilhaDeStencils.clear();
		}

		vector< vtkSmartPointer<vtkImageStencilData>> pilhaDeStencils;
		vtkSmartPointer<vtkImageStencilData> LastStencilUndone;

		//vtkSmartPointer<vtkImageData> stencilBase;
		//vtkSmartPointer<vtkImageData> testMask;
	};
}

#endif
