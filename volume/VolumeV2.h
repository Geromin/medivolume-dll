#pragma once
#pragma warning(disable:4996)
#include "Pipeline.h"
#include "VisualizacaoDoVolume.h"
#include "Removedor.h"
#include <vtkSmartPointer.h>
#include "CuboDeCorteChangeListener.h"
#include <memory>
#include <vtkRenderer.h>
#include <itkImageSource.h>
#include <itkImage.h>
#include <itkImportImageFilter.h>
#include "OperationStack.h"
#include <itkConnectedThresholdImageFilter.h>
#include <map>
#include <array>
#include <itkConfidenceConnectedImageFilter.h>
#pragma once
using namespace std;
//defini��es de tipo para uso da itk.
typedef itk::Image<short, 3> ImageType;
typedef itk::ImportImageFilter<short,3> ImageImportFilterType;
typedef itk::ImageSource<ImageType> ImageSourceType;
//typedef itk::MyItkStencil ItkStencilType;
typedef itk::Image<unsigned char, 3> TSegmentationResult;
typedef itk::ConnectedThresholdImageFilter<ImageType, TSegmentationResult> TConnectedThreshold;
typedef itk::ConfidenceConnectedImageFilter<ImageType, TSegmentationResult> TConfidenceConnection;

using namespace extrusao;
namespace vr{


	class VolumeV2 :public CuboDeCorteChangeListener{
	public:
		void Segment(array<double, 3> pto);


		CuboDeCorteChangeListener* volumeScreenToResetCuboDeCorte;
		//Serve para invocar os pares modified()/update() de toda a pipeline, na ordem que tem que ser.
		void RefreshAllPipeline();
		void ExecutarOnCuboDeCorteChange(vtkSmartPointer<vtkPolyData> pd) override;
		void ExecutarOnCuboDeCorteChange(double* bounds) override;
		extrusao::Removedor* GetRemovedor();
		short Pick(int x, int y, int z);
		/*
		Limpa a pilha de stencils
		*/
		void UndoAll();
		void RemoverOssos();
		
		void RenderCanonicalView(vtkRenderer* ren, vtkImageData* vtk_image_data);
		void SetMip(bool b);
		void BackdoorCor();
	private:
		
		std::unique_ptr<OperationStack> operacoes;

		bool canPopDueToMip, canPushDueToMip;
		short scalarOffset;
		std::shared_ptr< Pipeline<ShortImage> > pipe;
		std::shared_ptr< VisualizacaoDoVolume > visualizacao;
		std::shared_ptr< Removedor > removedor;

	public:
		void MinIpOn()
		{
			visualizacao->GetMapper()->SetBlendMode(2);
			visualizacao->GetMapper()->SetShading(false);
		}
		
		//Retorna o objeto que controla a visualiza��o do volume
		VisualizacaoDoVolume* GetVisualizacao()
		{
			return visualizacao.get();
		}
		//Retorna o objeto da pipeline
		Pipeline<ShortImage>* GetPipeline(){ return pipe.get(); }
		//guarda o valor de soma para garantir que n�o tenham n�meros negativos - se usando a cpu isso � importante
		//pois o mapper de cpu n�o suporta valores negativos.
		static short UnsignedCastOffset;
		//Ponteiro de fun��o pro callback
		static DelphiProgressCallback ProgressCallback;

		void RedoRemove();
		void UndoRemove();
		void RemoverRegiao(vtkSmartPointer<vtkImageStencilData> sd);
		void RemoverRegiao(Contorno* contour);

		VolumeV2(ShortImage::Pointer src, vtkSmartPointer<vtkRenderer> ren, bool usarGPU, float minimimSample, float maximumSample, map<string, string> metadata,
			short offset);
		virtual ~VolumeV2()override;
		virtual void ExecutarBeginInteraction(double* bounds)override;
		virtual void ExecutarEndInteraction(double* bounds) override;
	};
}