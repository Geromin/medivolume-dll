#include "GPUVolumeMapper.h"
#include <assert.h>
#include <vtkImageData.h>

#include <vtkGPUVolumeRayCastMapper.h>
#include <thread>
#include <Windows.h>
#include <vector>
#include <vtkImageStencil.h>
using namespace std;
namespace vr
{
	void GPUVolumeMapper::ShadeOff()
	{
		volumeProperties->ShadeOff();
	}

	void GPUVolumeMapper::ShadeOn()
	{
		volumeProperties->ShadeOn();
	}

	void GPUVolumeMapper::SetBlendMode(int id_blend_mode)
	{
		vtkSmartPointer<vtkGPUVolumeRayCastMapper> m = vtkGPUVolumeRayCastMapper::SafeDownCast(mapper);
		if (id_blend_mode == 0)
		{
			volumeProperties->ShadeOn();
			m->SetBlendModeToComposite();
		}
		if (id_blend_mode == 1)
		{
			volumeProperties->ShadeOff();
			m->SetBlendModeToMaximumIntensity();
		}
		if (id_blend_mode == 2)
		{
			volumeProperties->ShadeOn();
			m->SetBlendModeToMinimumIntensity();
		}
		if (id_blend_mode == 3)
		{
			volumeProperties->ShadeOff();
			m->SetBlendModeToAdditive();
		}
	}

	void GPUVolumeMapper::CreateMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource)
	{
		this->imageSource = _imageSource;
		vtkSmartPointer<vtkGPUVolumeRayCastMapper> m = vtkSmartPointer<vtkGPUVolumeRayCastMapper>::New();
		_imageSource->Update();
		vtkImageData* i = _imageSource->GetOutput();
		assert(i->GetExtent()[1]!=-1);
		m->SetInputConnection(_imageSource->GetOutputPort());
		m->SetMaxMemoryFraction(.99);
		m->AutoAdjustSampleDistancesOn();
		m->SetImageSampleDistance(1);
		m->SetSampleDistance(1);
		m->SetMaximumImageSampleDistance(2);
		m->SetMinimumImageSampleDistance(1);
		this->mapper = m;
	}

	GPUVolumeMapper::GPUVolumeMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource) :Mapper(_imageSource)
	{
		CreateMapper(_imageSource);
		CreateFunctions();
		CreateProperty();
		CreateVolume();

	}



	void GPUVolumeMapper::SetSamplingDistance(double min, double max)
	{
		vtkGPUVolumeRayCastMapper* m = vtkGPUVolumeRayCastMapper::SafeDownCast(mapper);
		m->AutoAdjustSampleDistancesOff();
		m->SetImageSampleDistance(min);
		m->SetSampleDistance(min);
		m->SetMinimumImageSampleDistance(min);
		m->SetMaximumImageSampleDistance(min + .1);
	}

}