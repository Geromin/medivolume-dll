#ifndef __itk_progress_command_h
#define __itk_progress_command_h

#include <itkCommand.h>
#include <itkSmartPointer.h>
#include <itkProcessObject.h>
#include "misc.h"

namespace vr{
	class ItkProgressCommand : public itk::Command{
	public:
		typedef ItkProgressCommand Self;
		typedef itk::Command Superclass;
		typedef itk::SmartPointer<Self> Pointer;

		itkNewMacro(ItkProgressCommand);
		itkTypeMacro(ItkProgressCommand, itk::Command);

		DelphiProgressCallback ProgressCallback;
		void Execute(itk::Object *caller, const itk::EventObject & event){
			Execute((const itk::Object *)caller, event);
		}

		void Execute(const itk::Object * object, const itk::EventObject & event){
			itk::ProcessObject* pObject = (itk::ProcessObject*)(object);
			ProgressCallback(pObject->GetProgress());
			//std:: <<pObject->GetProgress()<<std::endl;
		}
	};
}
#endif