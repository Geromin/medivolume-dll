#include "VolumeV2.h"
#include <vtkImageData.h>
#include "PontoNaTela.h"
#include <array>
#include <vtkSmartPointer.h>
#include <itkImage.h>

#include "itkImageSeriesWriter.h"
#include "UtilitiesV2.h"
namespace vr{
	void VolumeV2::Segment(array<double, 3> pto)
	{

	}



	short VolumeV2::Pick(int x, int y, int z)
	{
		vtkImageData* imageData = this->pipe->GetFinalDaPipeline()->GetOutput();
		int* extent = imageData->GetExtent();
		if (x < extent[0] || x >= extent[1])
			return VTK_SHORT_MIN;
		if (y < extent[2] || y >= extent[3])
			return VTK_SHORT_MIN;
		if (z < extent[4] || z >= extent[5])
			return VTK_SHORT_MIN;
		return imageData->GetScalarComponentAsDouble(x, y, z, 0);
	}

	void VolumeV2::UndoAll()
	{
		while (operacoes->canUndo())
		{
			this->UndoRemove();
		}
		pipe->Update();
		visualizacao->Update();
	}

	void VolumeV2::RemoverOssos()
	{

	}



	DelphiProgressCallback VolumeV2::ProgressCallback = NULL;
	short VolumeV2::UnsignedCastOffset = 0;


	VolumeV2::VolumeV2(ShortImage::Pointer src, vtkSmartPointer<vtkRenderer> ren, bool usarGPU, float minimimSample, float maximumSample,
		map<string, string> metadata, short offset): volumeScreenToResetCuboDeCorte(nullptr)
	{
		operacoes = std::make_unique<OperationStack>();

		canPopDueToMip = false;
		canPushDueToMip = true;;
		//� no construtor da pipeline que o ponteiro est� se perdendo.
		pipe = make_shared<Pipeline<ShortImage>>(src, VolumeV2::ProgressCallback, usarGPU ? 1 : 2, metadata, offset, Pipeline<ShortImage>::UseCase::VR);// new Pipeline();//instancia a nova pipeline
		this->scalarOffset = pipe->GetValueOffset();
		visualizacao = make_shared<VisualizacaoDoVolume>(usarGPU ? 1 : 2, pipe->GetFinalDaPipeline(), pipe->getResampledForThumbs(), metadata);
		visualizacao->SetScalarOffset(scalarOffset);
		visualizacao->SetWindow(2000, 1000 + scalarOffset); //(scalarOffset / 2, scalarOffset / 2);
		removedor = nullptr;
		removedor = make_shared<Removedor>();
		removedor->SetImageSource(pipe->GetFinalDaPipeline());
		removedor->SetActor(visualizacao->GetActor());
		removedor->SetRenderer(ren);
	}

	void VolumeV2::RedoRemove()
	{
		//Se a opera��o restaurada � um stencil tem que atualizar o removedor. Mas se a opera��o restaurada � um cubo de corte
		//tem que atualizar o crop.
		operacoes->redoOperation();
		if (operacoes->getRedoneOperationType() == Operation::Types::Stencil)
		{
			vector<Operation*> lstStencils = operacoes->getOperationsByType(Operation::Types::Stencil);
			pipe->DesfazerTudo();
			for (Operation* o : lstStencils)
				pipe->AddStencil(((StencilOperation*)o)->data);
			pipe->Update();
		}
		if (operacoes->getRedoneOperationType() == Operation::Types::CuboDeCorte)
		{
			vector<Operation*> lstCortes = operacoes->getOperationsByType(Operation::Types::CuboDeCorte);
			if (lstCortes.size() != 0)
			{
				CuboDeCorteOperation* ultimoCorte = dynamic_cast<CuboDeCorteOperation*>(lstCortes[lstCortes.size() - 1]);
				visualizacao->GetMapper()->Crop(ultimoCorte->data.data());
			}
		}
	}
	void VolumeV2::ExecutarBeginInteraction(double* bounds)
	{
		operacoes->TrySetInitialBounds(bounds);
	}
	void VolumeV2::UndoRemove()
	{
		///Se a opera��o desfeita foi um cubo de corte, pega o cubo de corte mais recente e atualiza o crop pois o crop mudou
		///Se a opera��o desfeita for um stencil, pega a lista atualizada de stencils e atualiza o cortador.
		operacoes->undoOperation();
		if (operacoes->getUndoneOperationType() == Operation::Types::Stencil)
		{
			vector<Operation*> lstStencils = operacoes->getOperationsByType(Operation::Types::Stencil);
			pipe->DesfazerTudo();
			for (Operation* o : lstStencils)
				pipe->AddStencil(((StencilOperation*)o)->data);
			pipe->Update();
		}
		if (operacoes->getUndoneOperationType() == Operation::Types::CuboDeCorte)
		{
			vector<Operation*> lstCortes = operacoes->getOperationsByType(Operation::Types::CuboDeCorte);
			if (lstCortes.size() != 0)
			{
				CuboDeCorteOperation* ultimoCorte = dynamic_cast<CuboDeCorteOperation*>(lstCortes[lstCortes.size() - 1]);
				visualizacao->GetMapper()->Crop(ultimoCorte->data.data());
				volumeScreenToResetCuboDeCorte->ExecutarOnCuboDeCorteChange(ultimoCorte->data.data());
			}
			else
			{
				std::array<double, 6> condicoesIniciais = operacoes->getInitialBounds();
				visualizacao->GetMapper()->Crop(condicoesIniciais.data());
				volumeScreenToResetCuboDeCorte->ExecutarOnCuboDeCorteChange(condicoesIniciais.data());
			}

		}

		////Antigo
		//pipe->DesfazerRemocao();
		//pipe->Update();
		//visualizacao->Update();
	}
	void VolumeV2::ExecutarOnCuboDeCorteChange(double* bounds)
	{
		visualizacao->GetMapper()->Crop(bounds);
	}
	void VolumeV2::ExecutarEndInteraction(double* bounds)
	{
		CuboDeCorteOperation* op = new CuboDeCorteOperation();
		for (int i = 0; i < 6; i++)
			op->data[i] = bounds[i];
		operacoes->pushOperation(op);
	}
	void VolumeV2::RemoverRegiao(vtkSmartPointer<vtkImageStencilData> sd)
	{
		StencilOperation* op = new StencilOperation();
		op->data = sd;
		operacoes->pushOperation(op);
		vector<Operation*> lstStencils = operacoes->getOperationsByType(Operation::Types::Stencil);
		pipe->DesfazerTudo();
		for (Operation* o : lstStencils)
			pipe->AddStencil(((StencilOperation*)o)->data);
		pipe->Update();

	}
	void VolumeV2::RemoverRegiao(Contorno* contour)
	{
		vector<PontoNaTela> pontos = ContornoToPontos(contour);
		assert(removedor && "Obviamente ele n�o pode ser null aqui");
		vtkSmartPointer<vtkImageStencilData> sd = removedor->CreateStencil(pontos, contour->KeepInside);
		StencilOperation* op = new StencilOperation();
		op->data = sd;
		operacoes->pushOperation(op);
		vector<Operation*> lstStencils = operacoes->getOperationsByType(Operation::Types::Stencil);
		pipe->DesfazerTudo();
		for (Operation* o : lstStencils)
			pipe->AddStencil(((StencilOperation*)o)->data);
		pipe->Update();
	}
	void VolumeV2::RefreshAllPipeline()
	{
		pipe->Update();
		visualizacao->Update();
	}
	void VolumeV2::ExecutarOnCuboDeCorteChange(vtkSmartPointer<vtkPolyData> pd)
	{
		throw "nao implementado";
	}



	extrusao::Removedor* VolumeV2::GetRemovedor()
	{
		return removedor.get();
	}

	VolumeV2::~VolumeV2()
	{
		pipe = nullptr;
		visualizacao = nullptr;
		removedor = nullptr;
		operacoes = nullptr;
	}

	void VolumeV2::RenderCanonicalView(vtkRenderer* ren, vtkImageData* vtk_image_data)
	{
		vtkVolume* vol = visualizacao->GetActor();
		int blendMode = visualizacao->GetMapper()->GetMapper()->GetBlendMode();
		double viewDirection[3] = { 0, 0, -1 };
		double viewUp[3] = { 0, 1, 0 };
		vtkGPUVolumeRayCastMapper* m = vtkGPUVolumeRayCastMapper::SafeDownCast(visualizacao->GetMapper()->GetMapper());
		m->CreateCanonicalView(ren, vol, vtk_image_data, blendMode, viewDirection, viewUp);
	}

	void VolumeV2::SetMip(bool b)
	{
		if (b)
		{
			if (!canPushDueToMip)
				return;
			visualizacao->GetMapper()->SetBlendMode(1);
			visualizacao->GetMapper()->SetShading(false);
			visualizacao->PushColorTransferFunction();
			vr::EstruturaDeCor e;
			e.Tamanho = 4;
			e.x = new int[e.Tamanho];
			e.r = new float[e.Tamanho];
			e.g = new float[e.Tamanho];
			e.b = new float[e.Tamanho];
			e.a = new float[e.Tamanho];
			e.midpoint = new float[e.Tamanho];
			e.sharpness = new float[e.Tamanho];
			e.x[0] = -3024; e.r[0] = 0; e.g[0] = 0; e.b[0] = 0;  e.a[0] = 0; e.midpoint[0] = 0.5; e.sharpness[0] = 0;
			e.x[1] = -637; e.r[1] = 1; e.g[1] = 1; e.b[1] = 1;  e.a[1] = 0; e.midpoint[1] = 0.5; e.sharpness[1] = 0;
			e.x[2] = 700; e.r[2] = 1; e.g[2] = 1; e.b[2] = 1;  e.a[2] = 1; e.midpoint[2] = 0.5; e.sharpness[2] = 0;
			e.x[3] = 3071; e.r[3] = 1; e.g[3] = 1; e.b[3] = 1;  e.a[3] = 1; e.midpoint[3] = 0.5; e.sharpness[3] = 0;
			for (int i = 0; i < e.Tamanho; i++)
			{
				e.x[i] = e.x[i] + GetPipeline()->GetValueOffset();
			}
			GetVisualizacao()->SetEstruturaDeCorRaw(&e);
			GetVisualizacao()->SetClamping(e.clamping);
			delete[]e.x; delete[]e.r; delete[]e.g; delete[]e.b; delete[]e.a; delete[]e.midpoint; delete[]e.sharpness;
			canPushDueToMip = false;
			canPopDueToMip = true;
		}
		else
		{
			if (!canPopDueToMip)
				return;
			visualizacao->GetMapper()->SetBlendMode(0);
			visualizacao->GetMapper()->SetShading(true);
			visualizacao->PopColorTransferFunction();
			canPushDueToMip = true;
			canPopDueToMip = false;
		}
	}

	void VolumeV2::BackdoorCor()
	{
		//Seta a curva de cor e opacidade para ser igual a do programa de teste e v� o que acontece
		visualizacao->BackdoorCor();
		//Seta o mapper pra ser igual ao do programa de teste pra ver como fica
		visualizacao->GetMapper()->SetBlendMode(0);
		visualizacao->GetMapper()->SetSamplingDistance(0.9, 1);
		vtkGPUVolumeRayCastMapper::SafeDownCast( visualizacao->GetMapper()->GetMapper())->AutoAdjustSampleDistancesOff();
	}
}