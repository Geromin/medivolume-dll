#pragma once
#pragma warning(disable:4996)
#ifndef __pipe_h
#define __pipe_h
#include <itkImage.h>
#include <vtkImageImport.h>
#include "ItkProgressCommand.h"
#include <vtkSmartPointer.h>
#include "VisualizacaoDoVolume.h"
#include <vtkImageData.h>
#include <assert.h>
#include "itktypes.h"
#include <map>
#include <vtkImageResample.h>
#include <itkOrientImageFilter.h>
#include "GambiarraStencil.h"

using namespace std;
namespace vr{
	//Encapsula a pipeline do volume - importa de uma estrutura de mem�ria, aplica os filtros
	//e termina disponibilizando o resultado como uma imagem da vtk.
	template<class TImageType>
	class Pipeline{
	public:
		enum UseCase { VR, MPR };
	private:
		GambiarraStencil<TImageType> gambiarraStencil;
		typename TImageType::Pointer imagemItk;
		vtkSmartPointer<vtkImageResample> resampledForThumbs;
		int tipoDeMapperEscolhido;
		vr::ItkProgressCommand::Pointer ItkProgressObserver;
		short valueOffset;
		vtkSmartPointer<vtkImageImport> vtkImporter;
		map<string, string> metadata;
		void SetupVTKImporter(vtkSmartPointer<vtkImageImport> i)
		{
			int szX = imagemItk->GetLargestPossibleRegion().GetSize()[0];
			int szY = imagemItk->GetLargestPossibleRegion().GetSize()[1];
			int szZ = imagemItk->GetLargestPossibleRegion().GetSize()[2];
			double sX = imagemItk->GetSpacing()[0];
			double sY = imagemItk->GetSpacing()[1];
			double sZ = imagemItk->GetSpacing()[2];
			double oX = imagemItk->GetOrigin()[0];
			double oY = imagemItk->GetOrigin()[1];
			double oZ = imagemItk->GetOrigin()[2];
			vtkImporter->SetDataSpacing(sX, sY, sZ);
			vtkImporter->SetDataOrigin(oX, oY, oZ);
			vtkImporter->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
			vtkImporter->SetDataExtentToWholeExtent();
			if (use == VR)
			{
				vtkImporter->SetDataScalarTypeToShort();
				typename TImageType::PixelType *imgPtr = imagemItk->GetBufferPointer();
				vtkImporter->SetImportVoidPointer(imgPtr, 1);
				vtkImporter->Update();
				cout << "foo" << endl;
			}
			if (use == MPR)
			{
				vtkImporter->SetDataScalarTypeToShort();
				typename TImageType::PixelType *imgPtr = imagemItk->GetBufferPointer();
				vtkImporter->SetImportVoidPointer(imgPtr, 1);
				vtkImporter->Update();
				cout << "foo" << endl;
			}
		}
	public:
		//template <class TImageType>
		//void CreateVTKExporter(TipoDeMapper mapperType){
		//	vtkImporter = vtkSmartPointer<vtkImageImport>::New();
		//	this->tipoDeMapperEscolhido = mapperType;
		//	SetupVTKImporter(vtkImporter);
		//}

		void ChangeItkImage(typename TImageType::Pointer ptr)
		{
			this->imagemItk = ptr;
			//bota no stencil
			gambiarraStencil.SetOriginalImage(imagemItk);
			vtkImporter->SetImportVoidPointer(imagemItk->GetBufferPointer());
			//vtkImporter = vtkSmartPointer<vtkImageImport>::New();
			//this->tipoDeMapperEscolhido = mapperType;
			//SetupVTKImporter(vtkImporter);
			vtkImporter->Modified();
			vtkImporter->Update();
		}

		~Pipeline()
		{
			vtkImporter = nullptr;
			ItkProgressObserver = nullptr;
			imagemItk = nullptr;
		}
		vtkSmartPointer<vtkImageAlgorithm> GetFinalDaPipeline()
		{
			assert(vtkImporter->GetOutput()->GetExtent()[1] != -1);
			return vtkImporter;
		}
		typename TImageType::Pointer GetItkImage()
		{
			return imagemItk;
		}
		vtkSmartPointer<vtkImageResample> getResampledForThumbs()
		{
			return resampledForThumbs;
		}

		UseCase use;

		Pipeline(typename TImageType::Pointer src, DelphiProgressCallback dProgressCallback,
			int mapperType, map<string, string> metadata, short offset, UseCase uc)
		{
			this->use = uc;
			this->imagemItk = src;
			this->metadata = metadata;
			//O reorientador vem aqui
			////Reorienta a imagem
			itk::OrientImageFilter<itk::Image<short, 3>, itk::Image<short, 3>>::Pointer orienter = itk::OrientImageFilter<itk::Image<short, 3>, itk::Image<short, 3>>::New();
			orienter->UseImageDirectionOn();
			////////Consultar https://itk.org/Doxygen/html/namespaceitk_1_1SpatialOrientation.html#a8240a59ae2e7cae9e3bad5a52ea3496eaa5d3197482c4335a63a1ad99d6a9edee
			////////para a lista de orienta��es
			orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RIP);
			orienter->SetInput(src);
			orienter->Update();
			imagemItk = orienter->GetOutput();
			valueOffset = offset;
			//bota no stencil
			gambiarraStencil.SetOriginalImage(imagemItk);
			vtkImporter = vtkSmartPointer<vtkImageImport>::New();
			this->tipoDeMapperEscolhido = mapperType;
			SetupVTKImporter(vtkImporter);
			//A vers�o mini pras thumbs.Isso aqui � necess�rio pro caso do vr e proibido pro caso do mpr
			if (uc == VR)
			{
				resampledForThumbs = vtkSmartPointer<vtkImageResample>::New();
				resampledForThumbs->SetAxisMagnificationFactor(0, 0.33);
				resampledForThumbs->SetAxisMagnificationFactor(1, 0.33);
				resampledForThumbs->SetAxisMagnificationFactor(2, 0.33);
				resampledForThumbs->SetInputData(vtkImporter->GetOutput());
				resampledForThumbs->Update();//uma duplica�ao aqu
			}
			if (uc == MPR)
			{
				resampledForThumbs = nullptr;
			}
		}

		void Update()
		{
			gambiarraStencil.ApplyStencil(imagemItk);
			vtkImporter->Modified();
		}

		void DesfazerRemocao();
		void RefazerRemocao();
		void DesfazerTudo()
		{
			gambiarraStencil.Clear();
		}

		void AddStencil(vtkSmartPointer<vtkImageStencilData> sd)
		{
			gambiarraStencil.AddStencil(sd);
		}
		short GetValueOffset()
		{
			return valueOffset;
		}
	};
}
#endif