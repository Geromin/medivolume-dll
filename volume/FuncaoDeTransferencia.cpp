#include "FuncaoDeTransferencia.h"
namespace vr
{
void FuncaoDeTranferencia::ComprimirFuncao(float dX)
{
	//currentCompressao = currentCompressao + dX;
	currentCompressao = dX;
}

void FuncaoDeTranferencia::Pop()
{
	_Fn f = myStack[myStack.size() - 1];
	myStack.pop_back();
	midpoint = f.midpoint;
	sharpness = f.sharpness;
	x = f.x;
	r = f.r;
	g = f.g;
	b = f.b;
	a = f.a;

}

void FuncaoDeTranferencia::Push()
{
	_Fn f;
	f.midpoint = midpoint;
	f.sharpness = sharpness;
	f.x = x;
	f.r = r;
	f.g = g;
	f.b = b;
	f.a = a;
	myStack.push_back(f);
}

void FuncaoDeTranferencia::SetValoresIniciais(int default_window_width, int default_window_center)
{
	this->default_window_width = default_window_width;
	this->default_window_center = default_window_center;
	this->currentCompressao = default_window_width;
	this->currentDeslocamento = default_window_center;
}

void FuncaoDeTranferencia::MoverFuncao(float dX)
{
	//currentDeslocamento = currentDeslocamento + dX;
	currentDeslocamento = dX;
}
void FuncaoDeTranferencia::CriarFuncaoDaVtk(vtkColorTransferFunction* color, vtkPiecewiseFunction* scalarOpacity)
{
	if (!color || !scalarOpacity)
		assert(false);//T� tudo zuado se isso aconteceu.
	//Qual � a janela da curva?
	int base = x[0];
	int topo = x[x.size() - 1];
	int fnWW = topo - base;
	int fnWC = (base - offset + topo - offset) / 2; //- (x[1]-x[0]);

	this->wl[0] = fnWW;
	this->wl[1] = fnWC;
	//1)Garante que est� limpa
	color->RemoveAllPoints();
	scalarOpacity->RemoveAllPoints();
	vector<float> localX = x;
	////compressao
	//Calcula para a 1a metade dos pontos.
	for (unsigned int i = 0; i < localX.size()/2; i++)
	{
		localX[i] = localX[i] - (currentCompressao - fnWW) / 2 ;
	}
	//Calcula para a 2a metade dos pontos
	for (unsigned int i = localX.size() - 1; i >= localX.size() / 2; i--)
	{
		localX[i] = localX[i] + (currentCompressao - fnWW) / 2 ;
	}
	//Garante que os escalares sejam estritamente crescentes mesmo que foda a curva.
	for (unsigned int i = 1; i < localX.size(); i++)
	{
		if (localX[i] < localX[i - 1])
			localX[i] = localX[i - 1] + 0.5;
	}

	/*3)Desloca a fun��o*/
	for (unsigned int i = 0; i < localX.size(); i++)
	{
		localX[i] = localX[i] + currentDeslocamento - fnWC;
	}

	//2)Insere meus pontos
	for (unsigned int i = 0; i < x.size(); i++)
	{

		color->AddRGBPoint(localX[i], r[i], g[i], b[i], midpoint[i], sharpness[i]);
		scalarOpacity->AddPoint(localX[i], a[i], midpoint[i], sharpness[i]);
	}
	color->SetColorSpaceToDiverging();
	

}
}