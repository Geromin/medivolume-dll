#ifndef __operation_stack_h
#define __operation_stack_h
#include <vector>
#include <array>
#include "Operation.h"
using namespace std;
namespace vr
{
	
	class OperationStack
	{
	private:
		Operation::Types lastUndone;
		Operation::Types lastRedone;
		/**Dois stacks, um pra desfazer outro pra refazer.*/
		vector<Operation*> undoStack, redoStack;
		array<double, 6> initialBounds;
		bool areInitialBoundsSet;
	public:
		bool canUndo()
		{
			if (undoStack.size() == 0)
				return false;
			else
				return true;
		}

		array<double, 6> getInitialBounds()
		{
			return initialBounds;
		}
		OperationStack()
		{
			areInitialBoundsSet = false;
		}
		void TrySetInitialBounds(double* bounds)
		{
			if (!areInitialBoundsSet)
			{
				for (int i = 0; i < 6; i++)
					initialBounds[i] = bounds[i];
				areInitialBoundsSet = true;
			}
		}
		Operation::Types getRedoneOperationType() { return lastRedone; }
		Operation::Types getUndoneOperationType() { return lastUndone; }
		/**Adiciona a opera��o ao undoStack.*/
		void pushOperation(Operation* op);
		/**Desfaz a opera��o mais recente. Tira do undoStack e p�e no redoStack.*/
		void undoOperation();
		/**Refaz a opera��o desfeita mais recente. Tira do redoStack e p�e no undoStack.*/
		void redoOperation();
		/**Pega a lista de opera��es do tipo dado.*/
		vector<Operation*> getOperationsByType(Operation::Types typeId);
		~OperationStack();
	};
}

#endif