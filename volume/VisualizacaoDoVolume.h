﻿#ifndef __visu_vol_h
#define __visu_vol_h
#include "Mapper.h"
#include <vtkImageAlgorithm.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkColorTransferFunction.h>
#include <vtkPiecewiseFunction.h>
#include <vtkVolumeProperty.h>
#include <vector>
#include "ScalarColorChangeListener.h"
#include "ScalarOpacityChangeListener.h"
#include "GradientOpacityChangeListener.h"
#include "ImageWindowChangeListener.h"
#include <vtkGPUVolumeRayCastMapper.h>

#include <vtkImageData.h>
#include <memory>
#include "EstruturaDeCor.h"
#include <map>
#include "FuncaoDeTransferencia.h"
#include "vtkSmartVolumeMapper.h"
using namespace std;
using namespace corEOpacidade;

namespace vr{
	enum TipoDeMapper{GPU, CPU, FIXED_POINT};
	class VisualizacaoDoVolume{
	private:
		struct _pontoDeCor{ float x, r, g, b, a; };
		vector<_pontoDeCor> pontosDeCor;
		struct _pontoDeOpacidadeGradiente { float x, a, mid, shp; };
		vector<_pontoDeOpacidadeGradiente> pontosDeGradiente;
		int InitialWW;
		int InitialWC;
		vtkSmartPointer<vtkPiecewiseFunction> createOpacity(vector<_pontoDeCor> data);
		vtkSmartPointer<vtkColorTransferFunction> createColor(vector<_pontoDeCor> data);
		vtkSmartPointer<vtkPiecewiseFunction> createGrandient(vector<_pontoDeOpacidadeGradiente> data);
		vtkSmartPointer<vtkPiecewiseFunction> createOpacity();
		vtkSmartPointer<vtkPiecewiseFunction> createGradient();
		vtkSmartPointer<vtkColorTransferFunction> createColor();

		void _SetWLAndCallListeners(int w, int l);
		FuncaoDeTranferencia funcaoDeTransferencia;

		vector<corEOpacidade::ScalarColorChangeListener*> OnColorChangeListeners;
		vector<corEOpacidade::ScalarOpacityChangeListener*> OnScalarOpacityChangeListeners;
		vector<vr::GradientOpacityChangeListener*> OnGradientOpacityChangeListeners;
		vector<ImageWindowChangeListener*> OnWindowChangeListeners;
		vtkSmartPointer<vtkPiecewiseFunction> OldOpacity;
		vtkSmartPointer<vtkColorTransferFunction> OldColor;
		Mapper* volumeMapper;
		int WindowCenter;
		int WindowWidth;
		bool JanelamentoAtivo;
		bool UsandoCorNoJanelamento;
		int DefaultWindowCenter;
		int DefaultWindowWidth;
		bool janelaSetada;
		vtkSmartPointer<vtkPiecewiseFunction> CalcularOpacidadeDaJanela(int wc, int ww);
		vtkSmartPointer<vtkColorTransferFunction> CalcularCorDaJanela(int wc, int ww);
		void  UpdateWindowChangeListeners();
		short scalarOffset;

		vtkGPUVolumeRayCastMapper* thumbMapper;
		vtkVolumeProperty* thumbProperty;
		vtkVolume* thumbActor;
	public:
		vtkVolume* GetThumbActor()
		{
			return thumbActor;
		}

		void SetEstruturaDeCorRaw(vr::EstruturaDeCor* e);

		~VisualizacaoDoVolume()
		{
			OnWindowChangeListeners.clear();
			delete volumeMapper;
		}

		void AddWindowChangeListener(ImageWindowChangeListener* l)
		{
			OnWindowChangeListeners.push_back(l);
		}
		void AtivarCorNoJanelamento();
		void DesativarCorNoJanelamento();
		void AddOnColorChangeListener(corEOpacidade::ScalarColorChangeListener* l);
		void UpdateColorChangeListeners();
		void AddOnScalarOpacityChangeListener(corEOpacidade::ScalarOpacityChangeListener* l);
		void AddOnGradientOpacityChangeListener(vr::GradientOpacityChangeListener* l);
		void UpdateGradientOpacityChangeListeners();
		void UpdateScalarOpacityChangeListeners();
		void AtivarJanelamento();
		void DesativarJanelamento();
		int GetWindowLevel()
		{
			return WindowCenter;
		}
		int GetWindowWidth()
		{
			return WindowWidth;
		}
		
		VisualizacaoDoVolume(int _tipo, vtkSmartPointer<vtkImageAlgorithm> imgSrc, vtkSmartPointer<vtkImageAlgorithm> resampledForThumb, map<string, string> metadata);
		
		vtkSmartPointer<vtkColorTransferFunction> GetTabelaDeCorJanelada();

		void SetSamplingDistance( double min, double max );

		void SetWindow(int width, int center);

		Mapper* GetMapper()
		{
			return volumeMapper;
		}

		vtkSmartPointer<vtkVolume> GetActor()
		{
			return this->volumeMapper->GetActor();
		}

		double GetDiffuse()
		{
			return volumeMapper->GetProperties()->GetDiffuse();
		}

		double GetSpecular()
		{
			return volumeMapper->GetProperties()->GetSpecular();
		}

		double GetAmbient()
		{
			vtkVolumeProperty* prop = volumeMapper->GetProperties();
			double v = prop->GetAmbient(0);
			return v;
		}


		//void Update(vtkSmartPointer<vtkImageAlgorithm> imgSrc);
		void Update();
		void Bar(float wc, float ww)
		{

		}
		/*Retorna a função de transferência sem a opacidade, somente com o RGB.*/
		vtkSmartPointer<vtkColorTransferFunction> GetFuncaoDeTransferencia()
		{
			return this->volumeMapper->GetFuncaoDeCor();
		}

		int GetShading()
		{
			return volumeMapper->GetProperties()->GetShade();
		}

		vtkSmartPointer<vtkPiecewiseFunction> GetOpacidadeEscalar();
		void SetWindowDefault();
		void SetColor(vtkSmartPointer<vtkColorTransferFunction> ctf);
		void SetScalarOpacity(vtkSmartPointer<vtkPiecewiseFunction> sotf);


		void SetScalarOffset(short scalar_offset)
		{
			this->scalarOffset = scalar_offset;
			this->funcaoDeTransferencia.SetOffset(scalarOffset);
		}

		void SetClamping(bool clamping);
		void GetPontoDeCor(int i, double xes[6]);
		void GetPontoDeAlpha(int i, double xes[4]);
		vtkSmartPointer<vtkPiecewiseFunction> GetOpacidadeGradiente();
		void SetGradientOpacity(vtkSmartPointer<vtkPiecewiseFunction> fn);
		void SwitchGradient(bool is_to_use);
		void GetPontoDeOpacidadeGradiente(int i, double xes[4]);
		void SwitchGradientDoThumb(bool is_to_use);
		void SetShadingDoThumb(bool shade);
		void SetEstruturaDeCorDoThumb(EstruturaDeCor* estrutura_de_cor);
		void SetClampingDoThumb(bool clamping);
		void SetDiffuseDoThumb(float x);
		void SetAmbientDoThumb(float x);
		void SetSpecularDoThumb(float x);
		vtkVolumeMapper* GetThumbMapper()
		{
			return thumbMapper;
		}

		void SetGradientDoThumb(vtkSmartPointer<vtkPiecewiseFunction> fn);
		vtkVolumeProperty* GetThumbProperty()
		{
			return thumbProperty;
		}

		void PushColorTransferFunction();
		void PopColorTransferFunction();
		void BackdoorCor();

		void SetFuncaoDeCor(EstruturaDeCor* estrutura_de_cor);
		int GetBlendMode()
		{
			return volumeMapper->GetMapper()->GetBlendMode();
		}

		float GetScalarOpacityUnitRange()
		{
			return volumeMapper->GetProperties()->GetScalarOpacityUnitDistance();
		}
	};
}
#endif