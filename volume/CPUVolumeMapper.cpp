#include "CPUVolumeMapper.h"
#include <vtkFixedPointVolumeRayCastMapper.h>

void vr::CPUVolumeMapper::ShadeOff()
{
	volumeProperties->ShadeOff();
}

void vr::CPUVolumeMapper::ShadeOn()
{
	volumeProperties->ShadeOff();
}

void vr::CPUVolumeMapper::SetSamplingDistance(double min, double max)
{
	vtkSmartPointer<vtkFixedPointVolumeRayCastMapper> _mapper = vtkFixedPointVolumeRayCastMapper::SafeDownCast(mapper);
	_mapper->AutoAdjustSampleDistancesOn();
	if (min < 1)
		min = 1;
	_mapper->SetMaximumImageSampleDistance(min+1);
	_mapper->SetMinimumImageSampleDistance(min);
	_mapper->SetImageSampleDistance(min);
	
}

void vr::CPUVolumeMapper::SetBlendMode(int id_blend_mode)
{
}

void vr::CPUVolumeMapper::CreateMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource)
{
	vtkSmartPointer<vtkFixedPointVolumeRayCastMapper> _mapper = vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New();
	_mapper->SetInputConnection(_imageSource->GetOutputPort());
	_mapper->AutoAdjustSampleDistancesOn();
	_mapper->SetMaximumImageSampleDistance(1 + 1);
	_mapper->SetMinimumImageSampleDistance(1);
	_mapper->SetImageSampleDistance(1);
	mapper = _mapper;
}

vr::CPUVolumeMapper::CPUVolumeMapper(vtkSmartPointer<vtkImageAlgorithm> _imageSource) :Mapper(_imageSource)
{
	CreateMapper(_imageSource);
	CreateFunctions();
	CreateProperty();
	CreateVolume();
}