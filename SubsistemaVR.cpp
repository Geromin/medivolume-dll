#include <itkOutputWindow.h>
#include <itkTextOutput.h>
#include "SubsistemaVR.h"
#include <itktypes.h>
#include <itkThresholdImageFilter.h>
#include <assert.h>
#include <vtkWindowToImageFilter.h>
#include <vtkSmartPointer.h>
#include <vtkBMPWriter.h>
#include <itkSigmoidImageFilter.h>
#include <itkTimeProbe.h>
#include <vtkImageDilateErode3D.h>
#include <itkGPUGradientAnisotropicDiffusionImageFilter.h>
#include <vtkXMLImageDataWriter.h>
#include <itkFastMarchingImageFilter.h>
#include <vtkMarchingCubes.h>
#include <vtkImageToImageStencil.h>
#include <itkCurvatureFlowImageFilter.h>
#include "SistemaDeSegmentacao.h"
#include <SegmentacaoRPC.h>
#include <Windows.h>
#include <itkOrientImageFilter.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/regex.hpp>
#include <fstream>
#define ASSERT_THUMB_SCREEN_INICIALIZADA assert("ThumbScreen foi inicializada?" && thumbScreen);
#define ASSERT_VOLUME_SCREEN_INICIALIZADA assert("VolumeScreen foi inicializada?" && volumeScreen);
#define ASSERT_CUBE_SCREEN_INICIALIZADA assert("CubeScreen foi inicializada?" && cubeScreen);

void DeepCopyFloatCPU(itk::GPUImage< float, 3 >::Pointer input, itk::Image<float, 3>::Pointer output)
{
	int inSzX = input->GetLargestPossibleRegion().GetSize()[0];
	int inSzY = input->GetLargestPossibleRegion().GetSize()[1];
	int inSzZ = input->GetLargestPossibleRegion().GetSize()[2];
	double inSpacingX = input->GetSpacing()[0];
	double inSpacingY = input->GetSpacing()[1];
	double inSpacingZ = input->GetSpacing()[2];

	output->SetRegions(input->GetLargestPossibleRegion());
	output->Allocate();

	itk::ImageRegionConstIterator<itk::GPUImage< float, 3 >> inputIterator(input, input->GetLargestPossibleRegion());
	itk::ImageRegionIterator<itk::Image<float, 3>> outputIterator(output, output->GetLargestPossibleRegion());
	const double inSpacing[] = { inSpacingX, inSpacingY, inSpacingZ };
	output->SetSpacing(inSpacing);
	while (!inputIterator.IsAtEnd())
	{
		float v = inputIterator.Get();
		outputIterator.Set(v);
		++inputIterator;
		++outputIterator;
	}
}

void DeepCopyFloatGPU(itk::GPUImage< float, 3 >::Pointer input, itk::Image<short, 3>::Pointer output)
{
	int inSzX = input->GetLargestPossibleRegion().GetSize()[0];
	int inSzY = input->GetLargestPossibleRegion().GetSize()[1];
	int inSzZ = input->GetLargestPossibleRegion().GetSize()[2];
	double inSpacingX = input->GetSpacing()[0];
	double inSpacingY = input->GetSpacing()[1];
	double inSpacingZ = input->GetSpacing()[2];

	output->SetRegions(input->GetLargestPossibleRegion());
	output->Allocate();

	itk::ImageRegionConstIterator<itk::GPUImage< float, 3 >> inputIterator(input, input->GetLargestPossibleRegion());
	itk::ImageRegionIterator<itk::Image<short, 3>> outputIterator(output, output->GetLargestPossibleRegion());
	const double inSpacing[] = { inSpacingX, inSpacingY, inSpacingZ };
	output->SetSpacing(inSpacing);
	while (!inputIterator.IsAtEnd())
	{
		float v = inputIterator.Get();
		outputIterator.Set(v);
		++inputIterator;
		++outputIterator;
	}
}


void DeepCopyFloatGPU(itk::GPUImage< float, 3 >::Pointer input, itk::Image<float, 3>::Pointer output)
{
	int inSzX = input->GetLargestPossibleRegion().GetSize()[0];
	int inSzY = input->GetLargestPossibleRegion().GetSize()[1];
	int inSzZ = input->GetLargestPossibleRegion().GetSize()[2];
	double inSpacingX = input->GetSpacing()[0];
	double inSpacingY = input->GetSpacing()[1];
	double inSpacingZ = input->GetSpacing()[2];

	output->SetRegions(input->GetLargestPossibleRegion());
	output->Allocate();

	itk::ImageRegionConstIterator<itk::GPUImage< float, 3 >> inputIterator(input, input->GetLargestPossibleRegion());
	itk::ImageRegionIterator<itk::Image<float, 3>> outputIterator(output, output->GetLargestPossibleRegion());
	const double inSpacing[] = { inSpacingX, inSpacingY, inSpacingZ };
	output->SetSpacing(inSpacing);
	while (!inputIterator.IsAtEnd())
	{
		float v = inputIterator.Get();
		outputIterator.Set(v);
		++inputIterator;
		++outputIterator;
	}
}

//Segmentador::Segmentador(FloatImage::Pointer input, short scalarOffset)
//{
//	this->input = input;
//	this->prop = nullptr;
//	tipoSmooth = 0;
//	this->offset = scalarOffset;
//}
//
//void Segmentador::SetProperty(int tipoSuavizacao, SEG_ConfidenceConnectedProperties* props)
//{
//	this->prop = props;
//	this->tipoSmooth = tipoSuavizacao;
//}
//
//vtkSmartPointer<vtkPolyData> Segmentador::getResultAsMarchingCube()
//{
//	itk::TimeProbe mc_time_probe;
//	mc_time_probe.Start();
//	vtkSmartPointer<vtkMarchingCubes> surface = vtkSmartPointer<vtkMarchingCubes>::New();
//	vtkSmartPointer<vtkImageImport> segmentationResultAsVTKImage = CreateVTKImage(resultadoDaSegmenta��o);
//	surface->SetInputConnection(segmentationResultAsVTKImage->GetOutputPort());
//	surface->ComputeNormalsOn();
//	surface->ComputeGradientsOn();
//	surface->SetValue(0, fastMarchingThreshold);
//	mc_time_probe.Stop();
//	cout << "@-@-@-@ Gastou " << mc_time_probe.GetMeanTime() << " no marching cube" << endl;
//	surface->Update();
//	return surface->GetOutput();
//}
//
//vtkSmartPointer<vtkImageStencilData> Segmentador::getResultAsStencil()
//{
//	//Transforma em uma imagem bin�ria pq o resultado da segmenta��o � um mapa de dist�ncia
//	ThreshType::Pointer binarizador = ThreshType::New();
//	binarizador->SetInput(resultadoDaSegmenta��o);
//	binarizador->SetUpper(1);
//	binarizador->SetOutsideValue(2);
//	binarizador->InPlaceOff();//Quero preservar o resultado.
//	binarizador->Update();
//
//	vtkSmartPointer<vtkImageImport> versaoVTK = CreateVTKImage(binarizador->GetOutput());
//	vtkSmartPointer<vtkImageToImageStencil> imageToStencilFilter = vtkSmartPointer<vtkImageToImageStencil>::New();
//	imageToStencilFilter->SetInputConnection(versaoVTK->GetOutputPort());
//	imageToStencilFilter->ThresholdByUpper(1.9);
//	imageToStencilFilter->Update();
//	return imageToStencilFilter->GetOutput();
//}
//
//void Segmentador::Segment(std::array<double, 3> _seed)
//{
//	assert(this->fastMarchingThreshold > 0);
//	assert(this->input);
//	//a imagem j� foi previamente suavizada, ent�o vou direto pro sigmoide
//	FloatImage::PixelType myScalar = GetScalarNoInput(_seed[0], _seed[1], _seed[2]);
//	cout << myScalar - offset;
//	float alpha = 5;//prop->sigmoidAlpha;
//	float beta = myScalar;
//	itk::TimeProbe sigmoidProbe;
//	sigmoidProbe.Start();
//	typedef itk::SigmoidImageFilter<FloatImage, FloatImage> SigmoidFilter;
//	SigmoidFilter::Pointer mySigmoid;
//	mySigmoid = SigmoidFilter::New();
//	mySigmoid->SetInput(input);
//	mySigmoid->SetAlpha(alpha);
//	mySigmoid->SetBeta(beta);
//	mySigmoid->SetOutputMinimum(0);
//	mySigmoid->SetOutputMaximum(1);
//	cout << "Fazendo o sigmoide" << endl;
//	mySigmoid->Update();
//	sigmoidResult = mySigmoid->GetOutput();
//	sigmoidProbe.Stop();
//	cout << "-------- dt sigmoide = " << sigmoidProbe.GetMeanTime() << endl;
//
//	////Segunda etapa: eros�o
//	//itk::TimeProbe sigmoid_erosao_t_time_probe;
//	//sigmoid_erosao_t_time_probe.Start();
//	//vtkSmartPointer<vtkImageImport> sigmoideVTK = CreateVTKImage(mySigmoid->GetOutput());
//	//vtkSmartPointer<vtkImageDilateErode3D> eroderVTK = vtkSmartPointer<vtkImageDilateErode3D>::New();
//	//eroderVTK->SetInputConnection(sigmoideVTK->GetOutputPort());
//	//eroderVTK->SetErodeValue(3);
//	//eroderVTK->Update();
//	//vtkSmartPointer<vtkImageData> eroderVTKResult = eroderVTK->GetOutput();
//	//float* sourceBuffer = (float*)eroderVTKResult->GetScalarPointer();
//	//FloatImage::Pointer erodedItk = mySigmoid->GetOutput();
//	//float* destBuffer = erodedItk->GetBufferPointer();
//	//memcpy(destBuffer, sourceBuffer, eroderVTKResult->GetDimensions()[0] * eroderVTKResult->GetDimensions()[1] * eroderVTKResult->GetDimensions()[2] * sizeof(float));
//	////Se tudo deu certo, o bagulho est� copiado pra erodedItk.
//	//eroderVTKResult = nullptr;
//	//eroderVTK = nullptr;
//	//sigmoid_erosao_t_time_probe.Stop();
//	//cout << "------- dt eros�o = " << sigmoid_erosao_t_time_probe.GetMeanTime() << endl;
//
//	cout << "-------- Come�ando o fast march - ele � lento no momento..." << endl;
//	//Teceira etapa: fast march a partir da seed
//	itk::TimeProbe fastMarchTimeProbe;
//	fastMarchTimeProbe.Start();
//	typedef  itk::FastMarchingImageFilter< FloatImage, FloatImage >    FastMarchingFilterType;
//	typedef FastMarchingFilterType::NodeContainer           NodeContainer;
//	typedef FastMarchingFilterType::NodeType                NodeType;
//	FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();
//	fastMarching->SetInput(sigmoidResult);
//	NodeContainer::Pointer seeds = NodeContainer::New();
//	FloatImage::IndexType  seedPosition;
//	seedPosition[0] = _seed[0];
//	seedPosition[1] = _seed[1];
//	seedPosition[2] = _seed[2];
//	NodeType node;
//	const double seedValue = 0.0;
//	node.SetValue(seedValue);
//	node.SetIndex(seedPosition);
//	seeds->Initialize();
//	seeds->InsertElement(0, node);
//	fastMarching->SetOutputSize(sigmoidResult->GetBufferedRegion().GetSize());
//	fastMarching->SetTrialPoints(seeds);
//	fastMarching->SetStoppingValue(fastMarchingThreshold+5);
//	fastMarching->SetNormalizationFactor(1);
//	fastMarching->Update();
//	fastMarchTimeProbe.Stop();
//	cout << "------- dt fastMarch = " << fastMarchTimeProbe.GetMeanTime() << endl;
//
//
//	itk::TimeProbe theshTime;
//	theshTime.Start();
//	ThreshType::Pointer threshold = ThreshType::New();
//	threshold->ThresholdAbove((float)fastMarchingThreshold+1.0);
//	threshold->SetOutsideValue(0);
//	threshold->SetInput(fastMarching->GetOutput());
//	threshold->Update();
//	theshTime.Stop();
//	cout << "-------- dt threshold = " << theshTime.GetMeanTime() << endl;
//
//	itk::TimeProbe theshPersist;
//	theshPersist.Start();
//	vtkSmartPointer<vtkImageImport> threshVTK = CreateVTKImage(threshold->GetOutput());
//	vtkSmartPointer<vtkXMLImageDataWriter> treshPersistenceTimeProbe = vtkSmartPointer<vtkXMLImageDataWriter>::New();
//	treshPersistenceTimeProbe->SetInputConnection(threshVTK->GetOutputPort());
//	treshPersistenceTimeProbe->SetFileName("C:\\Users\\geronimo\\dicom\\threshVTK.vti");
//	treshPersistenceTimeProbe->Write();
//	theshPersist.Stop();
//	cout << "-------- dt thresh salvando no hd pra debug = " << theshPersist.GetMeanTime() << endl;
//
//	////Chegando aqui, acabei a segmenta��o propriamente dita, e o resultado est� no Threshold. O Threshold tem o mapa da
//	////dist�ncia do ponto inicial at� o limite de dist�ncia estabelecida pelo usu�rio. Agora � gerar as sa�das pro usu�rio
//	resultadoDaSegmenta��o = threshold->GetOutput();
//
//	//itk::TimeProbe fmPersistenceTimeProbe;
//	//fmPersistenceTimeProbe.Start();
//	//vtkSmartPointer<vtkImageImport> fmVTK = CreateVTKImage(fastMarching->GetOutput());
//	//vtkSmartPointer<vtkXMLImageDataWriter> fmPersist = vtkSmartPointer<vtkXMLImageDataWriter>::New();
//	//fmPersist->SetInputConnection(fmVTK->GetOutputPort());
//	//fmPersist->SetFileName("C:\\Users\\geronimo\\dicom\\fmVTK.vti");
//	//fmPersist->Write();
//	//fmPersistenceTimeProbe.Stop();
//	//cout << "-------- dt fm salvando no hd pra debug = " << fmPersistenceTimeProbe.GetMeanTime() << endl;
//}
//
//vtkSmartPointer<vtkImageStencilData> Segmentador::getOutput()
//{
//	return nullptr;
//}

SubsistemaVR::SubsistemaVR(char* idExame, char* idSerie): usarGPU(false), offset(0)
{
	this->rpcExecutable = "";
	this->rpcPort = "";
	this->rpcFileBuffer = "";
	this->idExame = idExame;
	this->idSerie = idSerie;
	this->isTelaEstavel = false;
}

void SubsistemaVR::CreateThumbScreen(HWND handle, HDC dc, HGLRC glrc)
{
	thumbScreen = make_unique<ThumbScreen>(handle, dc, glrc);;
}

void SubsistemaVR::ActivateThumbScreen()
{
	ASSERT_THUMB_SCREEN_INICIALIZADA;
	thumbScreen->Ativar();
}

void SubsistemaVR::CreateVolumeScreen(HWND handle, HDC dc, HGLRC glrc)
{
	volumeScreen = make_unique<VolumeScreen>(handle, dc, glrc);
}

void SubsistemaVR::ActivateVolumeScreen()
{
	ASSERT_VOLUME_SCREEN_INICIALIZADA;
	volumeScreen->Ativar();

}

void SubsistemaVR::CreateCubeScreen(HWND handle, HDC dc, HGLRC glrc, SimpleTextureProvider* textureProvider)
{
	cubeScreen = make_unique<CubeScreen>(handle, dc, glrc, textureProvider);
}

void SubsistemaVR::ActivateCubeScreen()
{
	ASSERT_CUBE_SCREEN_INICIALIZADA;
	cubeScreen->Ativar();
}

void SubsistemaVR::ResizeVolumeScreen(int w, int h)
{
	ASSERT_VOLUME_SCREEN_INICIALIZADA;
	volumeScreen->Resize(w, h);
}

void SubsistemaVR::ResizeCubeScreen(int w, int h)
{
	ASSERT_CUBE_SCREEN_INICIALIZADA;
	cubeScreen->Resize(w, h);
}

void SubsistemaVR::CreatePipeline(ShortImage::Pointer src, bool usarGPU, map<string, string> metadata, short offset, int curvatureFlowSteps)
{
	cout << __FUNCTION__ << endl;
	itk::OutputWindow::SetInstance(itk::TextOutput::New().GetPointer());
	//No passado do sistema todas as imagens eram sempre shorts. N�o mais, agora que tenho que usar algoritmos
	//que exigem floats. Esses algoritmos s�o usados no VR no momento.
	//ShortToFloatCasterType::Pointer caster = ShortToFloatCasterType::New();
	//caster->SetInput(src);
	//caster->Update();
	//a partir daqui a imagem � float.
	this->image = src;
	///////////////////A REORIENTA��O OCORRE AQUI////////////////////////
	////	//Ajeita o que � parte de baixo e o que � parte de cima
	//cout << "WARINING : Reorientador ativo" << endl;
	//typedef itk::OrientImageFilter<ImageType, ImageType> TOrientImageFilter;
	//TOrientImageFilter::Pointer reorientador = nullptr;
	//reorientador = TOrientImageFilter::New();
	//reorientador->SetInput(image);
	//reorientador->UseImageDirectionOn();
	//reorientador->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RIP);
	//reorientador->Update();
	//DeepCopy<itk::Image<short, 3>>(reorientador->GetOutput(), image);
	//////////////////FIM DA REORIENTA��O/////////////////
	this->usarGPU = usarGPU;
	this->metadata = metadata;
	this->offset = offset;
	////ANTES DE SAIR USANDO O VR, SUAVIZA��O ANISOTR�PICA NA PARADA
	//try
	//{
	//	typedef itk::GPUImage< float, 3 >   GPUFloatImage;
	//	//typedef itk::GPUImage<short, 3> GPUShortImage;
	//	typedef itk::GPUGradientAnisotropicDiffusionImageFilter<ShortImage, GPUFloatImage> GPUAnisoDiffFilterType;
	//	GPUAnisoDiffFilterType::Pointer GPUFilter = GPUAnisoDiffFilterType::New();
	//	itk::TimeProbe gputimer;
	//	gputimer.Start();
	//	long size_in_bytes = image->GetLargestPossibleRegion().GetSize()[0] * image->GetLargestPossibleRegion().GetSize()[1] * image->GetLargestPossibleRegion().GetSize()[2] * sizeof(short);
	//	if ((size_in_bytes * 3) >= (1024 * 1024 * 1024))
	//		throw std::exception("TOO BIG!");
	//	GPUFilter->SetInput(image);
	//	GPUFilter->SetNumberOfIterations(5);
	//	GPUFilter->SetTimeStep(std::min(std::min(image->GetSpacing()[0], image->GetSpacing()[1]), image->GetSpacing()[2]) / pow(2.0, 3 + 1));//125 );
	//	GPUFilter->SetConductanceParameter(3);
	//	GPUFilter->UseImageSpacingOn();
	//	//throw itk::ExceptionObject();
	//	GPUFilter->Update();
	//	GPUFilter->GetOutput()->UpdateBuffers(); // synchronization point
	//	gputimer.Stop();
	//	std::cout << "GPU Anisotropic diffusion took " << gputimer.GetMean() << " seconds.\n" << std::endl;
	//	//2) Volta da gpu e libera o recurso pra poder eerizar mais � frente
	//	itk::TimeProbe returnTime;
	//	returnTime.Start();
	//	itk::ImageRegionConstIterator<GPUFloatImage > inputIterator(GPUFilter->GetOutput(), GPUFilter->GetOutput()->GetLargestPossibleRegion());
	//	itk::ImageRegionIterator<itk::Image<short, 3>> outputIterator(image, image->GetLargestPossibleRegion());
	//	while (!inputIterator.IsAtEnd())
	//	{
	//		float v = inputIterator.Get();
	//		outputIterator.Set(v);
	//		++inputIterator;
	//		++outputIterator;
	//	}
	//	//		DeepCopyFloatGPU(GPUFilter->GetOutput(), image);
	//	GPUFilter = nullptr;
	//	returnTime.Stop();
	//}
	//catch (itk::ExceptionObject& excp){
	//	////1)Salva a imagem num buffer
	//	//const long gravacaoBufferT0 = GetCurrentTime();
	//	//std::fstream bufferFile;
	//	//bufferFile.open("buffer.dat", fstream::in | fstream::out | fstream::binary | fstream::trunc);
	//	//bufferFile.seekg(0);
	//	//short *dataBuffer = image->GetBufferPointer();
	//	//size_t buffSize = image->GetLargestPossibleRegion().GetNumberOfPixels() * sizeof(short);
	//	//bufferFile.seekg(0, ios::beg);
	//	//bufferFile.write(reinterpret_cast<const char*>(dataBuffer), buffSize);
	//	//bufferFile.close();
	//	//const long gravacaoBufferT1 = GetCurrentTime();
	//	//cout << "Tempo gasto gravando o buffer : " << (gravacaoBufferT1 - gravacaoBufferT0) << endl;
	//	////2)Chama o processo auxiliar que deve rodar tudo muito r�pido
	//	////3)Pega o resultado
	//	itk::TimeProbe curvatureFlowTimeProbe;
	//	curvatureFlowTimeProbe.Start();
	//	std::cout << "Caught exception during GPUFilter->Update(), fallback pra itk curvature flow" << excp << std::endl;
	//	typedef itk::CurvatureFlowImageFilter< ShortImage, ShortImage >  CurvatureFlowFilterType;
	//	CurvatureFlowFilterType::Pointer filter = CurvatureFlowFilterType::New();
	//	filter->SetInput(image);
	//	filter->SetNumberOfIterations(curvatureFlowSteps);
	//	filter->SetTimeStep(0.3);
	//	filter->Update();
	//	image = filter->GetOutput();
	//	curvatureFlowTimeProbe.Stop();
	//	cout << " Custo do curvature flow = " << curvatureFlowTimeProbe.GetMeanTime() << endl;
	//}
	//catch (std::exception& excp)
	//{
	//	std::cout << excp.what() << std::endl;
	//	std::cout << "Grande demais - n�o suaviza de forma alguma" << std::endl;
	//}

	//Fim da suaviza��o na gpu
	usarGPU = true;
	cout << "WARNING - SEMPRE USANDO GPU" << std::endl;
	volumeObject = make_unique<VolumeV2>(image, volumeScreen->GetRenderer(), usarGPU, 1, 1, metadata, offset);

	//Pega a orienta��o do paciente na metadata
	std::string orientation = metadata.at("0018|5100");
	boost::trim(orientation);
	std::string strPosition = metadata.at("0020|0032");
	boost::trim(strPosition);
	std::array<double, 3> patientPos;
	boost::regex re("\\\\");
	boost::sregex_token_iterator i(strPosition.begin(), strPosition.end(), re, -1);
	boost::sregex_token_iterator j;
	unsigned count = 0;
	while (i != j){
		std::string v = *i++;
		boost::trim(v);
		patientPos[count] = boost::lexical_cast<double>(v);
		count++;
	}


	volumeScreen->AddVolumeObject(volumeObject.get());
	volumeScreen->AddVolume(volumeObject->GetVisualizacao()->GetActor(),orientation,patientPos);

	//Pega a posi��o do paciente na metadata
	volumeScreen->finalDaPipeline = this->volumeObject->GetPipeline()->GetFinalDaPipeline();
	volumeScreen->AddRotationListener(cubeScreen.get());
	volumeScreen->AddRotationListener(volumeScreen->GetLetras());
	volumeObject->GetVisualizacao()->AddWindowChangeListener(volumeScreen.get());
	volumeObject->GetVisualizacao()->AtivarJanelamento();
	volumeObject->GetVisualizacao()->SetWindowDefault();

	thumbScreen->AddVolume(volumeObject->GetVisualizacao()->GetThumbActor());
	thumbScreen->Resize(50, 50);
	thumbScreen->ForceRender();

}

void SubsistemaVR::GenerateCanonicalView2(float* x, float* r, float* g, float* b, float* a, float* colorMid, float* colorSharpness, int qtdDePontos, float* gx, float* ga, float* gMid, float* gSharpness, int qtdDeGradiente, float ambient, float diffuse, float specular, bool shading, bool clamping, bool gradientOn)
{
	std::cout << __FUNCTION__ << std::endl;
	const int valueOffset = volumeObject->GetPipeline()->GetValueOffset();
	FuncaoDeTranferencia calculadorDasFns;
	for (int i = 0; i < qtdDePontos; i++)
	{
		calculadorDasFns.AddPonto(x[i] + valueOffset, r[i], g[i], b[i], a[i], colorMid[i], colorSharpness[i]);
	}
	vtkVolumeProperty* prop = volumeObject->GetVisualizacao()->GetThumbProperty();
	vtkSmartPointer<vtkPiecewiseFunction> scalarOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
	vtkSmartPointer<vtkColorTransferFunction> color = vtkSmartPointer<vtkColorTransferFunction>::New();
	calculadorDasFns.CriarFuncaoDaVtk(color, scalarOpacity);
	/////////////////////////////////////////////////
	calculadorDasFns.ComprimirFuncao(calculadorDasFns.GetInitialWL()[0]);
	calculadorDasFns.MoverFuncao(calculadorDasFns.GetInitialWL()[1]);
	calculadorDasFns.CriarFuncaoDaVtk(color, scalarOpacity);
	prop->SetAmbient(ambient);
	prop->SetDiffuse(diffuse);
	prop->SetSpecular(specular);
	if (shading)
		prop->ShadeOn();
	else
		prop->ShadeOff();
	if (gradientOn)
		prop->DisableGradientOpacityOff();
	else
		prop->DisableGradientOpacityOn();
	if (clamping)
		scalarOpacity->ClampingOn();
	else
		scalarOpacity->ClampingOff();
	prop->SetScalarOpacity(scalarOpacity);
	if (clamping)
		color->ClampingOn();
	else
		color->ClampingOff();
	for (int i = 0; i < qtdDePontos; i++)
		prop->SetColor(color);
	if (gradientOn)
	{
		vtkSmartPointer<vtkPiecewiseFunction> gradient = vtkSmartPointer<vtkPiecewiseFunction>::New();
		for (int i = 0; i < qtdDeGradiente; i++)
		{
			gradient->AddPoint(gx[i], ga[i], gMid[i], gSharpness[i]);
		}
		prop->SetGradientOpacity(gradient);
	}
#ifdef OPENGL_LEGADO
	thumbScreen->Resize(150, 150);

	vtkSmartPointer<vtkWindowToImageFilter> windowToImage = vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImage->SetInput(thumbScreen->getRenderWindow());
	windowToImage->SetMagnification(1);
	windowToImage->SetInputBufferTypeToRGB();
	windowToImage->ReadFrontBufferOn();
	windowToImage->Update();
	vtkImageData *i2 = windowToImage->GetOutput();
	vtkSmartPointer<vtkBMPWriter> bmpWriter = vtkSmartPointer<vtkBMPWriter>::New();
	bmpWriter->SetFileName("_tempthumb.bmp");
	bmpWriter->SetInputData(i2);
	bmpWriter->Write();
#else //Vers�o com opengl2 funciona na moral
	vtkVolumeMapper* _m = volumeObject->GetVisualizacao()->GetThumbMapper();
	vtkGPUVolumeRayCastMapper* mapper = vtkGPUVolumeRayCastMapper::SafeDownCast(_m);
	mapper->RenderToImageOn();
	vtkImageData* i2 = vtkImageData::New();
	thumbScreen->ForceRender();
	mapper->GetColorImage(i2);
	mapper->RenderToImageOff();

	vtkSmartPointer<vtkBMPWriter> bmpWriter = vtkSmartPointer<vtkBMPWriter>::New();
	bmpWriter->SetFileName("_tempthumb.bmp");
	bmpWriter->SetInputData(i2);
	bmpWriter->Write();
	i2->Delete();
#endif

	std::cout << "Fim de " << __FUNCTION__ << std::endl;
}

void SubsistemaVR::ResizeThumbScreen(int w, int h)
{
	thumbScreen->Resize(w, h);
}

void SubsistemaVR::SetFuncaoDeCor(vr::EstruturaDeCor* e)
{
	volumeObject->GetVisualizacao()->SetFuncaoDeCor(e);
	volumeScreen->ForceRender();
}

void SubsistemaVR::SetEstruturaDeCorDoThumb(vr::EstruturaDeCor* e)
{
	volumeObject->GetVisualizacao()->SetEstruturaDeCorDoThumb(e);
}

void SubsistemaVR::SetClampingDoThumb(bool clamp)
{
	volumeObject->GetVisualizacao()->SetClampingDoThumb(clamp);
}

void SubsistemaVR::SetWindow(int w, int l)
{
	volumeObject->GetVisualizacao()->SetWindow(w, l);
}

void SubsistemaVR::RenderAll()
{
	const long t0 = GetCurrentTime();
	volumeScreen->ForceRender();
	cubeScreen->ForceRender();
	thumbScreen->ForceRender();
	const long t1 = GetCurrentTime();
	cout << "FPS = " << 1000 / (t1 - t0) << endl;
}

void SubsistemaVR::SetMip(bool flag)
{
	volumeObject->SetMip(flag);
	volumeScreen->ForceRender();
}

void SubsistemaVR::SetCamera(float azi, float elev)
{
	volumeScreen->SetCamera(azi, elev);
	volumeScreen->ForceRender();
}

void SubsistemaVR::RenderVolume()
{
	volumeScreen->ForceRender();
}

void SubsistemaVR::RenderCuboDeOrientacao()
{
	cubeScreen->ForceRender();
}

void SubsistemaVR::OnMouseMove(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnMouseMove(wnd, nFlags, X, Y);
}

SubsistemaVR::~SubsistemaVR()
{
	volumeScreen->Desativar();//quando eu chego aqui image.image.m_importPointer j� � null e isso n pode acontecer. Pq?
	cubeScreen->Desativar();
	thumbScreen = nullptr;
	cubeScreen = nullptr;
	volumeScreen = nullptr;
	volumeObject = nullptr; //a pipeline tem que morrer. Isso deve ativar a cascata de destrutores.

	cout << "aaaa";
}

void SubsistemaVR::UndoRemocao()
{
	volumeObject->UndoRemove();
	volumeScreen->ForceRender();
}

void SubsistemaVR::RedoRemocao()
{
	volumeObject->RedoRemove();
	volumeScreen->ForceRender();
}

void SubsistemaVR::SetTipoDeRemocao(int codShape, bool keepInside)
{
	volumeScreen->SetTipoDeRemocao(codShape, keepInside);
}

void SubsistemaVR::ExecutarRotationListenersDaTelaDoVolume()
{
	volumeScreen->ExecuteAllRotationListners();
}

void SubsistemaVR::SetMouseDireito(int codOperacao)
{
	volumeScreen->SetMouseDireito(codOperacao);
}

void SubsistemaVR::SetMouseMeio(int codOperacao)
{
	volumeScreen->SetMouseMeio(codOperacao);
}

void SubsistemaVR::SetMouseEsquerdo(int codOperacao)
{
	volumeScreen->SetMouseEsquerdo(codOperacao);
}

void SubsistemaVR::OnVolumeRMouseUp(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnRightMouseUp(wnd, nFlags, X, Y);
}

void SubsistemaVR::OnVolumeRMouseDown(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnRightMouseDown(wnd, nFlags, X, Y);
}

void SubsistemaVR::OnVolumeMMouseUp(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnMiddleMouseUp(wnd, nFlags, X, Y);
}

void SubsistemaVR::OnVolumeMMouseDown(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnMiddleMouseDown(wnd, nFlags, X, Y);
}

void SubsistemaVR::OnVolumeLMouseUp(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnLeftMouseUp(wnd, nFlags, X, Y);
}

void SubsistemaVR::OnVolumeLMouseDown(HWND wnd, UINT nFlags, int X, int Y)
{
	volumeScreen->OnLeftMouseDown(wnd, nFlags, X, Y);
}

void SubsistemaVR::UndoAll()
{
	volumeObject->UndoAll();
	volumeScreen->ForceRender();
}

void SubsistemaVR::AtivarCuboDeCorte()
{
	volumeScreen->AtivarCubo();
}

void SubsistemaVR::DesativarCuboDeCorte()
{
	volumeScreen->DesativarCubo();
}

void SubsistemaVR::SetSamplingDistance(float min, float max)
{

	volumeObject->GetVisualizacao()->GetMapper()->SetSamplingDistance(min, max);
	volumeScreen->ForceRender();
}

array<int, 2> SubsistemaVR::GetWL()
{
	array<int, 2> retorno = { volumeObject->GetVisualizacao()->GetWindowWidth(), volumeObject->GetVisualizacao()->GetWindowLevel() };
	return retorno;
}

void SubsistemaVR::SetSegmentacao(int suavizacao, int metodo, void* props)
{
//	mySeg = make_unique<SistemaDeSegmentacao>();
	mySeg = make_unique<SegmentacaoRPC>();
}

void SubsistemaVR::Segmentar()
{
	cout << __FUNCTION__ << endl;
	if (volumeScreen->IsSegmentationWidgetActive())
	{
		//this->rpcExecutable = executable;
		//this->rpcPort = port;
		//this->rpcFileBuffer = bufferName;
		//rpcExecutable = "C:\\medivolume_bin\\rpc_server\\Debug\\rpc_server.exe";
		//rpcPort = "4747";
		//rpcFileBuffer = "C:\\tutorial_vr\\rpc_buffer.bin";
		array<double, 3> pto = this->volumeScreen->GetSegmentationPosition();
		mySeg->SetInput(this->image);
		mySeg->SetSeed(pto);
		SegmentacaoRPC* rpc = dynamic_cast<SegmentacaoRPC*>(mySeg.get());
		if (rpc)
		{
			rpc->SetPropriedadesDeConexao(this->rpcExecutable, this->rpcPort, this->rpcFileBuffer);
		}
		mySeg->Segmentar();
		vtkSmartPointer<vtkImageStencilData> stencilDaSegmentacao = mySeg->GetOutput();
		//vtkSmartPointer<vtkPolyData> pd = mySeg->GetMesh();
		//volumeScreen->AddPolygonalSegmentationResult(pd);
		volumeObject->RemoverRegiao(stencilDaSegmentacao);
		this->RenderAll();
	}
	else{
		cout << "nao vai segmentar pq o segmentador t� inativo." << endl;
	}
}
