#include  "serializacao.h"
#include <sstream>
namespace serializao_vvt{
	TiXmlElement* create_root_element(std::string nome)
	{
		TiXmlElement* root_element = new TiXmlElement("TransferFunctions");
		root_element->SetAttribute("Modality", "CT");
		root_element->SetAttribute("Comment", nome.c_str());
		root_element->SetAttribute("BlendMode", "2");
		root_element->SetAttribute("RelativeVisibleValueRange", "0 1");
		return root_element;
	}

	TiXmlElement* create_volume_property()
	{
		TiXmlElement* volume_property = new TiXmlElement("VolumeProperty");
		volume_property->SetAttribute("Version", "1.13");
		volume_property->SetAttribute("InterpolationType", "1");
		volume_property->SetAttribute("IndependentComponents", "1");
		return volume_property;
	}
	TiXmlElement* create_component(int numero_componente, int shade, float ambient, float diffuse, float specular, float specular_power)
	{
		TiXmlElement* component = new TiXmlElement("Component");
		component->SetAttribute("Index", numero_componente);
		std::stringstream ss_shading;
		ss_shading << shade;
		component->SetAttribute("Shade", ss_shading.str().c_str());
		std::stringstream ss_ambient;
		ss_ambient << ambient;
		component->SetAttribute("Ambient", ss_ambient.str().c_str());
		std::stringstream ss_diff;
		ss_diff << diffuse;
		component->SetAttribute("Diffuse", ss_diff.str().c_str());
		std::stringstream ss_spec;
		ss_spec << specular;
		component->SetAttribute("Specular", ss_spec.str().c_str());
		component->SetAttribute("SpecularPower", static_cast<int>(specular_power));
		component->SetAttribute("ColorChannels", "3");
		component->SetAttribute("DisableGradientOpacity", "1");
		component->SetAttribute("ComponentWeight", "1");
		component->SetAttribute("ScalarOpacityUnitDistance", "0.891927");
		return component;
	}

	TiXmlElement* create_rgb_transfer_function(vr::EstruturaDeCor* e)
	{
		TiXmlElement* rgb_transfer_function_01 = new TiXmlElement("RGBTransferFunction");
		TiXmlElement* color_transfer_function = new TiXmlElement("ColorTransferFunction");
		rgb_transfer_function_01->LinkEndChild(color_transfer_function);
		color_transfer_function->SetAttribute("Version", "1.8");
		color_transfer_function->SetAttribute("Size", e->Tamanho);
		color_transfer_function->SetAttribute("Clamping", (int)e->clamping);
		color_transfer_function->SetAttribute("ColorSpace", "1");
		for (int i = 0; i < e->Tamanho; i++)
		{
			TiXmlElement* ponto = new TiXmlElement("Point");
			color_transfer_function->LinkEndChild(ponto);
			std::stringstream s_x;
			s_x << e->x[i];
			ponto->SetAttribute("X", s_x.str().c_str());

			std::stringstream s_v;
			s_v << e->r[i];
			s_v << " ";
			s_v << e->g[i];
			s_v << " ";
			s_v << e->b[i];
			std::string _val_data = s_v.str();
			ponto->SetAttribute("Value", _val_data.c_str());

			std::stringstream s_m;
			s_m << e->midpoint[i];
			ponto->SetAttribute("MidPoint", s_m.str().c_str());

			std::stringstream s_s;
			s_s << e->sharpness[i];
			ponto->SetAttribute("Sharpness", s_s.str().c_str());
		}
		return rgb_transfer_function_01;
	}
	TiXmlElement* create_scalar_opacity_transfer_function(vr::EstruturaDeCor* e)
	{
		TiXmlElement* scalar_opacity = new TiXmlElement("ScalarOpacity");
		TiXmlElement* piecewise_function = new TiXmlElement("PiecewiseFunction");
		scalar_opacity->LinkEndChild(piecewise_function);
		scalar_opacity->SetAttribute("Version", "1.7");
		int scalar_opacity_size = e->Tamanho;
		std::stringstream ss_op_sz;
		ss_op_sz << scalar_opacity_size;
		scalar_opacity->SetAttribute("Size", ss_op_sz.str().c_str());
		scalar_opacity->SetAttribute("Clamping", (int)e->clamping);
		for (int i = 0; i < scalar_opacity_size; i++)
		{
			TiXmlElement* ponto = new TiXmlElement("Point");
			scalar_opacity->LinkEndChild(ponto);
			std::stringstream ss_x, ss_mp, ss_sh;
			ss_x << e->x[i];
			ponto->SetAttribute("X", ss_x.str().c_str());
			std::stringstream a_x;
			a_x << e->a[i];
			ponto->SetAttribute("Value", a_x.str().c_str());
			ss_mp << e->midpoint[i];
			ponto->SetAttribute("MidPoint", ss_mp.str().c_str());
			ss_sh << e->sharpness[i];
			ponto->SetAttribute("Sharpness", ss_sh.str().c_str());
		}
		return scalar_opacity;
	}
	TiXmlElement* create_gradient_opacity_transfer_function(vr::EstruturaDeCor* e)
	{
		TiXmlElement* GradientOpacity = new TiXmlElement("GradientOpacity");
		TiXmlElement* piecewise_function = new TiXmlElement("PiecewiseFunction");
		GradientOpacity->LinkEndChild(piecewise_function);
		GradientOpacity->SetAttribute("Version", "1.7");
		int gradient_fn_sz = e->tamanhoFuncaoGradiente; //TamanhoFuncaoGradiente;
		std::stringstream ss_op_sz;
		ss_op_sz << gradient_fn_sz;
		GradientOpacity->SetAttribute("Clamping", (int)e->clampingGradient);
		//gradiente � opcional - o tamanho pode ser zero. Nesse caso, tem que gerar texto default
		if (gradient_fn_sz != 0)
		{
			GradientOpacity->SetAttribute("Size", ss_op_sz.str().c_str());
			for (int i = 0; i < gradient_fn_sz; i++)
			{
				TiXmlElement* ponto = new TiXmlElement("Point");
				GradientOpacity->LinkEndChild(ponto);
				std::stringstream ss_x, ss_mp, ss_sh;
				ss_x << e->grad_x[i];
				ponto->SetAttribute("X", ss_x.str().c_str());
				std::stringstream a_x;
				a_x << e->grad_a[i];
				ponto->SetAttribute("Value", a_x.str().c_str());
				ss_mp << e->grad_midpoint[i];
				ponto->SetAttribute("MidPoint", ss_mp.str().c_str());
				ss_sh << e->grad_sharpness[i];
				ponto->SetAttribute("Sharpness", ss_sh.str().c_str());
			}
		}
		else
		{
			GradientOpacity->SetAttribute("Size", 2);
			TiXmlElement* ponto0 = new TiXmlElement("Point");
			GradientOpacity->LinkEndChild(ponto0);
			ponto0->SetAttribute("X", "0");
			ponto0->SetAttribute("Value", "1");
			ponto0->SetAttribute("MidPoint", "0.5");
			ponto0->SetAttribute("Sharpness", "0");

			TiXmlElement* ponto1 = new TiXmlElement("Point");
			GradientOpacity->LinkEndChild(ponto1);
			ponto1->SetAttribute("X", "255");
			ponto1->SetAttribute("Value", "1");
			ponto1->SetAttribute("MidPoint", "0.5");
			ponto1->SetAttribute("Sharpness", "0");
		}
		return GradientOpacity;
	}
}