#ifndef _subsistema_vr
#define _subsistema_vr
#include "itktypes.h"
#include <loadedImage.h>
#include <VolumeV2.h>
#include <VolumeScreen.h>
#include <ThumbScreen.h>
#include <CubeScreen.h>
#include <memory>
#include <SimpleTextureProvider.h>
#include <vector>
#include <IMprVRInteroperation.h>
#include <array>
#include <itkThresholdImageFilter.h>
#include <SistemaDeSegmentacaoInterface.h>
#include "SistemaDeSegmentacao.h"
#include "SegmentacaoRPC.h"
using namespace std;
using namespace vr;
//
//class Segmentador
//{
//private:
//	typedef itk::ThresholdImageFilter<FloatImage> ThreshType;
//	SEG_ConfidenceConnectedProperties* prop;
//	FloatImage::Pointer input;
//	int tipoSmooth;
//	short offset;
//	FloatImage::Pointer sigmoidResult;
//	int fastMarchingThreshold;
//	FloatImage::Pointer resultadoDaSegmentação;
//
//	FloatImage::PixelType GetScalarNoInput(int x, int y, int z)
//	{
//		FloatImage::IndexType posNaInput;
//		posNaInput[0] = x;      // x position of the pixel
//		posNaInput[1] = y;      // y position of the pixel
//		posNaInput[2] = z;      // z position of the pixel
//		FloatImage::PixelType      myScalar = input->GetPixel(posNaInput);
//		return myScalar;
//	}
//public:
//	FloatImage::Pointer GetSigmoide()
//	{
//		return sigmoidResult;
//	}
//	Segmentador(FloatImage::Pointer input, short scalarOffset);
//	void SetProperty(int tipoSuavizacao, SEG_ConfidenceConnectedProperties *props);
//	void Segment(std::array<double, 3> _seed);
//	vtkSmartPointer<vtkImageStencilData> getOutput();
//	void SetFastMarchingThreshold(int threshold)
//	{
//		this->fastMarchingThreshold = threshold;
//
//	}
//	vtkSmartPointer<vtkPolyData> getResultAsMarchingCube();
//	vtkSmartPointer<vtkImageStencilData> getResultAsStencil();
//};

//O subsistema vr tem que conter as telas e a pipeline que mexe na imagem pra por na tela.
class SubsistemaVR : public IMprVRInteroperation
{
private:
	unique_ptr<SistemaDeSegmentacaoInterface> mySeg;
	array<int, 3> storedPickedPoint;
	string idExame, idSerie;
	unique_ptr<VolumeScreen> volumeScreen;
	unique_ptr<CubeScreen> cubeScreen;
	unique_ptr<ThumbScreen> thumbScreen;
	unique_ptr<VolumeV2> volumeObject;
	ShortImage::Pointer image;

	bool usarGPU;
	map<string, string> metadata;
	short offset;
	vector<IMprVRInteroperation*> linkedSubsystems;
	string rpcExecutable, rpcPort, rpcFileBuffer;

	bool isTelaEstavel;
public:
	void SetRPCProperties(string executable, string port, string bufferName)
	{
		this->rpcExecutable = executable;
		this->rpcPort = port;
		this->rpcFileBuffer = bufferName;
	}

	void SetSegmentacaoManterDentro(bool manterDentro)
	{
		if (!mySeg)
		{
			//mySeg = make_unique<SistemaDeSegmentacao>();
			mySeg = make_unique<SegmentacaoRPC>();
		}
		mySeg->SetDentroOuFora(manterDentro ? ManterDentroOuFora::DENTRO : ManterDentroOuFora::FORA);
	}

	void GetPosicao(int x, int y, int z, short scalar, std::string msg)
	{
		if (msg == "mouse_click")
		{
			storedPickedPoint[0] = x;
			storedPickedPoint[1] = y;
			storedPickedPoint[2] = z;
			std::cout << "Recebeu uma posição de mouse" << std::endl;
		}

	}

	void LinkSubsystem(IMprVRInteroperation* sysToLink)
	{
		linkedSubsystems.push_back(sysToLink);
	}

	bool IsThisExam(char* idExame, char* idSerie)
	{
		string e = idExame; string s = idSerie;
		if (this->idExame == e && this->idSerie == s)
			return true;
		else
			return false;
	}
	//Cria o novo subsistema pro exame-serie dado.
	SubsistemaVR(char* idExame, char* idSerie);

	void CreateThumbScreen(HWND handle, HDC dc, HGLRC glrc);
	void ActivateThumbScreen();

	void CreateVolumeScreen(HWND handle, HDC dc, HGLRC glrc);
	void ActivateVolumeScreen();

	void CreateCubeScreen(HWND handle, HDC dc, HGLRC glrc, SimpleTextureProvider* textureProvider);
	void ActivateCubeScreen();

	void ResizeVolumeScreen(int w, int h);
	void ResizeCubeScreen(int w, int h);

	void CreatePipeline(ShortImage::Pointer src, bool usarGPU, map<string, string> metadata, short offset, int curvatureFlowSteps=1);

	void  GenerateCanonicalView2(float* x, float* r, float* g, float* b,
		float* a, float* colorMid, float* colorSharpness, int qtdDePontos,
		float* gx, float* ga, float* gMid, float* gSharpness, int qtdDeGradiente,
		float ambient, float diffuse, float specular, bool shading, bool clamping, bool gradientOn);

	void ResizeThumbScreen(int w, int h);

	void SetFuncaoDeCor(vr::EstruturaDeCor* e);

	void SetEstruturaDeCorDoThumb(vr::EstruturaDeCor* e);
	void SetClampingDoThumb(bool clamp);

	short GetValueOffset(){ return offset; }

	void SetWindow(int w, int h);

	void RenderAll();
	void SetMip(bool flag);

	void SetCamera(float azi,float elev);
	void RenderVolume();
	void RenderCuboDeOrientacao();

	void OnMouseMove(HWND wnd, UINT nFlags, int X, int Y);
	~SubsistemaVR();
	void UndoRemocao();
	void RedoRemocao();
	void SetTipoDeRemocao(int codShape, bool keepInside);

	vtkSmartPointer<vtkRenderer> GetRendererDaTelaDoVolume()
	{
		return volumeScreen->GetRenderer();
	}

	void ExecutarRotationListenersDaTelaDoVolume();
	void SetMouseDireito(int codOperacao);
	void SetMouseMeio(int codOperacao);
	void SetMouseEsquerdo(int codOperacao);
	void OnVolumeRMouseUp(HWND wnd, UINT nFlags, int X, int Y);
	void OnVolumeRMouseDown(HWND wnd, UINT nFlags, int X, int Y);
	void OnVolumeMMouseUp(HWND wnd, UINT nFlags, int X, int Y);
	void OnVolumeMMouseDown(HWND wnd, UINT nFlags, int X, int Y);
	void OnVolumeLMouseUp(HWND wnd, UINT nFlags, int X, int Y);
	void OnVolumeLMouseDown(HWND wnd, UINT nFlags, int X, int Y);
	vtkSmartPointer<vtkWindow> GetVolumeVtkWindow()
	{
		return volumeScreen->GetVtkWindow();
	}
	double* GetVolumeCenter()
	{
		return volumeObject->GetVisualizacao()->GetActor()->GetCenter();
	}
	void UndoAll();
	void AtivarCuboDeCorte();
	void DesativarCuboDeCorte();
	void SetSamplingDistance(float min, float max);
	array<int, 2> GetWL();

	VisualizacaoDoVolume* GetVisualizacaoDoVolume()
	{
		return volumeObject->GetVisualizacao();
	}

	void SetSegmentacao(int suavizacao, int metodo, void* props);
	void Segmentar();
	void SetFastMarchingProperties(int threshold)
	{
		if (!mySeg)
		{
			//mySeg = make_unique<SistemaDeSegmentacao>();
			mySeg = make_unique<SegmentacaoRPC>();
		}
		mySeg->SetDistanciaDoFastMarch(threshold);
	}

	void SwitchWidgetDeSegmentacao()
	{
		volumeScreen->SwitchWidgetDeSegmentacao();
	}

	void SetFontSize(int sz)
	{
		volumeScreen->SetWindowTextFontSize(sz);
	}
};
#endif