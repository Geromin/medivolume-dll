#include "precompiled_header.h"
#include "LogSystem.h"

static const WORD MAX_CONSOLE_LINES = 500;
using namespace std;


void LogSystem::RedirectIOToFile(){
	//cria a stream pro arquivo
	string filename = PathDeSaida + "log_vr.txt";
	Out = new ofstream(filename);
	//guarda o   original
	old  = std:: cout.rdbuf();
	//muda o pra ser a sa�da de arquivo
	std::cout.rdbuf(Out->rdbuf());
	vtkSmartPointer<vtkFileOutputWindow> fw = vtkSmartPointer<vtkFileOutputWindow>::New();
	fw->SetFileName("vtklog.txt");
	vtkOutputWindow::SetInstance(fw);
}

void LogSystem::RedirectIOToConsole(){
	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;
	// allocate a console for this app
	AllocConsole();
	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),coninfo.dwSize);
	// redirect unbuffered STDOUT to the console
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stdout = *fp;
	setvbuf( stdout, NULL, _IONBF, 0 );
	// redirect unbuffered STDIN to the console
	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "r" );
	*stdin = *fp;
	setvbuf( stdin, NULL, _IONBF, 0 );
	// redirect unbuffered STDERR to the console
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stderr = *fp;
	setvbuf( stderr, NULL, _IONBF, 0 );
	ios::sync_with_stdio();

	vtkSmartPointer<vtkFileOutputWindow> fw = vtkSmartPointer<vtkFileOutputWindow>::New();
	fw->SetFileName("vtklog.txt");
	vtkOutputWindow::SetInstance(fw);
}

LogSystem::~LogSystem(){
	if (SaidaEscolhida == LogSystem::Arquivo)
	{
		std::cout.rdbuf(old); //reset to standard output again
		delete Out;
	}
	if (SaidaEscolhida == LogSystem::Console)
	{
		FreeConsole();
	}

}

void LogSystem::DefinirLocalDoArquivoDeSaida(){
	//pega o nome do execut�vel, o arquivo de sa�da ficar� no mesmo dir.
	char path[512];
	GetModuleFileName(NULL, path, 512);
	string Path(path);
	//picota a string pra pegar o diretorio da aplica��o.
	vector<string> pedacos;
	regex fileSeparator("\\\\+");
	//cria o iterator de tokens pra string do caminho de arquivos
	sregex_token_iterator tokens(Path.begin(), Path.end(), fileSeparator, -1);
	//for_each - o iterator de in�cio s�o os tokens, o de fim � token_iterator default, e o lambda captura 'peda�os'
	//do escopo superior e guarda a string recebida como par�metro nela.
	for_each(tokens, sregex_token_iterator(),[&pedacos](const string& s){
		pedacos.push_back(s);
	});
	//agora guarda a string do path - s�o todos os peda�os menos o ultimo, que � o nome do execut�vel.
	stringstream ss;
	for(unsigned int i=0; i<pedacos.size()-1; i++){
		ss<<pedacos[i]<<"\\";
	}
	string result;
	result = ss.str();
	PathDeSaida = result;
}
LogSystem::LogSystem(TipoDeSaida tipo){
	DefinirLocalDoArquivoDeSaida();
	this->SaidaEscolhida = tipo;
	switch (SaidaEscolhida)
	{
	case LogSystem::Arquivo:
		this->RedirectIOToFile();
		break;
	case LogSystem::Console:
		this->RedirectIOToConsole();
		break;
	case LogSystem::Nada:
	{
		vtkSmartPointer<vtkFileOutputWindow> fw = vtkSmartPointer<vtkFileOutputWindow>::New();
		fw->SetFileName("vtklog.txt");
		vtkOutputWindow::SetInstance(fw);
	}
		break;
	default:
		throw "Parametro maluco em SistemaDeLog::SistemaDeLog";
		break;
	}
}