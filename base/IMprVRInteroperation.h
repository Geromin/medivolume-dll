#pragma once
#include "precompiled_header.h"
#include "CallbackDePosicaoDoMouseMPR.h"
class IMprVRInteroperation:public CallbackDePosicaoDoMouseMPR
{
public:
	//� pra que o subsistema saiba a que imagem est� se referindo e cada imagem � identificada por um par exame/serie
	virtual bool IsThisExam(char* idExame, char* idSerie) = 0;
	//Linka this a um subsistema dado;
	virtual void LinkSubsystem(IMprVRInteroperation *sysToLink) = 0;

	virtual ~IMprVRInteroperation()
	{

	}
};

