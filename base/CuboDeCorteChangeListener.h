#pragma once
#include "precompiled_header.h"

namespace extrusao{
	class CuboDeCorteChangeListener{
	public:
		virtual ~CuboDeCorteChangeListener()
		{
		}
		virtual void ExecutarBeginInteraction(double* bounds) = 0;
		virtual void ExecutarEndInteraction(double* bounds) = 0;
		virtual void ExecutarOnCuboDeCorteChange(vtkSmartPointer<vtkPolyData> pd) = 0;
		virtual void ExecutarOnCuboDeCorteChange(double* bounds) = 0;
	};
}