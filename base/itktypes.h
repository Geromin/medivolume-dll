#pragma once
#include "precompiled_header.h"
#include <vtkImageData.h>


//A imagem recebida � short ou � float?
enum ReceivedITKImageType { IS_SHORT, IS_FLOAT };
//defini��o do tipo da imagem
typedef itk::Image<short, 3> ImageType;
//A imagem � carregada como short
typedef itk::Image<short, 3> ShortImage;
//A pipeline do VR usa imagens float
typedef itk::Image<float, 3> FloatImage;
//Certos algoritmos exigem imagens na GPU.
typedef itk::GPUImage<float, 3> GpuFloatImage;
//defini��o do filtro de importa��o
typedef itk::ImportImageFilter<short, 3> ImageImportFilterType;
typedef itk::FlipImageFilter<ImageType> ImageFlipType;


typedef itk::CastImageFilter<ShortImage, FloatImage> ShortToFloatCasterType;

typedef itk::ResampleImageFilter<ImageType, ImageType> ResamplerType;

//Cria o filtro que permite acesso por parte da vtk � imagem itk. Vers�o Short.
static inline vtkImageImport* CreateVTKImage(ShortImage::Pointer img)
{
	int szX = img->GetLargestPossibleRegion().GetSize()[0];
	int szY = img->GetLargestPossibleRegion().GetSize()[1];
	int szZ = img->GetLargestPossibleRegion().GetSize()[2];
	double sX = img->GetSpacing()[0];
	double sY = img->GetSpacing()[1];
	double sZ = img->GetSpacing()[2];
	double oX = img->GetOrigin()[0];
	double oY = img->GetOrigin()[1];
	double oZ = img->GetOrigin()[2];
	vtkImageImport* vtkImage = vtkImageImport::New();
	vtkImage = vtkImageImport::New();
	vtkImage->SetDataSpacing(sX, sY, sZ);
	vtkImage->SetDataOrigin(oX, oY, oZ);
	vtkImage->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
	vtkImage->SetDataExtentToWholeExtent();
	vtkImage->SetDataScalarTypeToShort();
	void* imgPtr = img->GetBufferPointer();
	vtkImage->SetImportVoidPointer(imgPtr, 1);
	vtkImage->Update();
	vtkImageData *r = vtkImage->GetOutput();
	r->Print(cout);
	return vtkImage;
}

static inline vtkImageImport* CreateVTKImage(itk::Image<unsigned char ,3>::Pointer img)
{
	int szX = img->GetLargestPossibleRegion().GetSize()[0];
	int szY = img->GetLargestPossibleRegion().GetSize()[1];
	int szZ = img->GetLargestPossibleRegion().GetSize()[2];
	double sX = img->GetSpacing()[0];
	double sY = img->GetSpacing()[1];
	double sZ = img->GetSpacing()[2];
	double oX = img->GetOrigin()[0];
	double oY = img->GetOrigin()[1];
	double oZ = img->GetOrigin()[2];
	vtkImageImport* vtkImage = vtkImageImport::New();
	vtkImage = vtkImageImport::New();
	vtkImage->SetDataSpacing(sX, sY, sZ);
	vtkImage->SetDataOrigin(oX, oY, oZ);
	vtkImage->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
	vtkImage->SetDataExtentToWholeExtent();
	vtkImage->SetDataScalarTypeToUnsignedChar();
	void* imgPtr = img->GetBufferPointer();
	vtkImage->SetImportVoidPointer(imgPtr, 1);
	vtkImage->Update();
	return vtkImage;
}

//Cria o filtro que permite acesso por parte da vtk � imagem itk. Vers�o Short.
static inline vtkImageImport* CreateVTKImage(itk::Image<unsigned short, 3>::Pointer img)
{
	int szX = img->GetLargestPossibleRegion().GetSize()[0];
	int szY = img->GetLargestPossibleRegion().GetSize()[1];
	int szZ = img->GetLargestPossibleRegion().GetSize()[2];
	double sX = img->GetSpacing()[0];
	double sY = img->GetSpacing()[1];
	double sZ = img->GetSpacing()[2];
	double oX = img->GetOrigin()[0];
	double oY = img->GetOrigin()[1];
	double oZ = img->GetOrigin()[2];
	vtkImageImport* vtkImage = vtkImageImport::New();
	vtkImage = vtkImageImport::New();
	vtkImage->SetDataSpacing(sX, sY, sZ);
	vtkImage->SetDataOrigin(oX, oY, oZ);
	vtkImage->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
	vtkImage->SetDataExtentToWholeExtent();
	vtkImage->SetDataScalarTypeToUnsignedShort();
	void* imgPtr = img->GetBufferPointer();
	vtkImage->SetImportVoidPointer(imgPtr, 1);
	vtkImage->Update();
	return vtkImage;
}

//Cria o filtro que permite acesso por parte da vtk � imagem itk. Vers�o float.
static inline vtkImageImport* CreateVTKImage(FloatImage::Pointer img)
{
	int szX = img->GetLargestPossibleRegion().GetSize()[0];
	int szY = img->GetLargestPossibleRegion().GetSize()[1];
	int szZ = img->GetLargestPossibleRegion().GetSize()[2];
	double sX = img->GetSpacing()[0];
	double sY = img->GetSpacing()[1];
	double sZ = img->GetSpacing()[2];
	double oX = img->GetOrigin()[0];
	double oY = img->GetOrigin()[1];
	double oZ = img->GetOrigin()[2];
	vtkImageImport* vtkImage = vtkImageImport::New();
	vtkImage = vtkImageImport::New();
	vtkImage->SetDataSpacing(sX, sY, sZ);
	vtkImage->SetDataOrigin(oX, oY, oZ);
	vtkImage->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
	vtkImage->SetDataExtentToWholeExtent();
	vtkImage->SetDataScalarTypeToFloat();
	void* imgPtr = img->GetBufferPointer();
	vtkImage->SetImportVoidPointer(imgPtr, 1);
	vtkImage->Update();
	return vtkImage;
}

template<typename TImage> void DeepCopy(typename TImage::Pointer input, typename TImage::Pointer output)
{
	int inSzX = input->GetLargestPossibleRegion().GetSize()[0];
	int inSzY = input->GetLargestPossibleRegion().GetSize()[1];
	int inSzZ = input->GetLargestPossibleRegion().GetSize()[2];
	double inSpacingX = input->GetSpacing()[0];
	double inSpacingY = input->GetSpacing()[1];
	double inSpacingZ = input->GetSpacing()[2];

	output->SetRegions(input->GetLargestPossibleRegion());
	output->Allocate();

	itk::ImageRegionConstIterator<TImage> inputIterator(input, input->GetLargestPossibleRegion());
	itk::ImageRegionIterator<TImage> outputIterator(output, output->GetLargestPossibleRegion());
	const double inSpacing[] = { inSpacingX, inSpacingY, inSpacingZ };
	output->SetSpacing(inSpacing);
	while (!inputIterator.IsAtEnd())
	{
		outputIterator.Set(inputIterator.Get());
		++inputIterator;
		++outputIterator;
	}
}

struct SEG_ConfidenceConnectedProperties
{
	float sigmoidAlpha;
	float sigmoidBeta;
	float numberOfIterations;
	float varianceMultiplier;
	float initialNeighborhoodRadius;
};

struct MPRV2_DicomData
{
	double directionCosines[6];
	double voxelSpacing[3];
	double sliceOrigin[3];
	int voxelDimensions[3];
	double slabThickness;
	int bufferSize;
	short *bufferData;
	char* letraDoLadoEsquerdo;
	char* letraDoLadoDireito;
	char* letraDoLadoSuperior;
	char* letraDoLadoInferior;
};

