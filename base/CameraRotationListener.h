#pragma once
#include "precompiled_header.h" 

/**
    Interface daqueles que querem ouvir eventos de mudan�a de orienta��o da camera da tela do volume, informada pelo
    TelaVolumeInteractionStyle.
*/
namespace tela{
	//tem uso em cubeScreen
	class VolumeCameraRotationListener{
	public:
		virtual void ExecuteCameraRotation() = 0;
		virtual void Execute(vtkSmartPointer<vtkCamera> cameraDoVolume, float offsetDoPlano=0.5) = 0;
		virtual ~VolumeCameraRotationListener()
		{

		}
	};

}