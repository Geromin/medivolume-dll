#pragma once
#include "precompiled_header.h"
using namespace std;
/**
	Meu sisteminha de log. Ele faz urucas com o   para modificar para onde � a
	sa�da.
*/
class LogSystem{
public:
	enum TipoDeSaida{Arquivo=1, Console=2, Nada=3};
	LogSystem(TipoDeSaida tipo);
	~LogSystem();
private:
	std::streambuf* old ;
	std::ofstream* Out;
	string PathDeSaida;
	TipoDeSaida SaidaEscolhida;
	void DefinirLocalDoArquivoDeSaida();
	void RedirectIOToFile();
	void RedirectIOToConsole();

};

