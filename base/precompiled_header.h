#pragma once
//Windows
#include <Windows.h>
//Standard Template Library
#include <algorithm>
#include <array>
#include <cctype>
#include <chrono>
#include <ctime>
#include <fcntl.h>
#include <fstream>
#include <functional>
#include <io.h>
#include <iostream>
#include <iterator>
#include <locale>
#include <map>
#include <regex>
#include <stdio.h>
#include <sstream>
#include <string>
#include <vector>
//VTK
#include <vtkCamera.h>
#include <vtkFileOutputWindow.h>
#include <vtkImageData.h>
#include <vtkImageImport.h>
#include <vtkImageStencilData.h>
#include <vtkOutputWindow.h>
#include <vtkPNGWriter.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
//ITK
#include <itkCastImageFilter.h>
#include<itkCommand.h>
#include <itkFlipImageFilter.h>
#include <itkGPUImage.h>
#include <itkImage.h>
#include <itkImageSeriesReader.h>
#include <itkImportImageFilter.h>
#include <itkResampleImageFilter.h>
#include <itkSmartPointer.h>
