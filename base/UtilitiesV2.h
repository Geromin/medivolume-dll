#pragma once
#include "precompiled_header.h"

//Macros pra rastreas qual fn estou - dependem de windows.h e iostream.
#define FunctionIn  ;//std::cout <<"in " << __FUNCTION__ << std::endl; long __t0=GetTickCount();
#define FunctionOut ;//std::cout <<"out " << __FUNCTION__ << " dt = "<<GetTickCount()-__t0<<std::endl;



template<typename T, unsigned int dim>
static inline std::array<T, dim> Div2Arrays(std::array<T, dim> a, std::array<T, dim> b)
{
	std::array<T, dim> arr;
	for (auto i = 0; i < dim; i++)
		arr[i] = a[i] / b[i];
	return arr;
}

template<typename T, unsigned int dim>
static inline std::array<T, dim> CArrayToStlArray(T* src)
{
	std::array<T, dim> arr;
	for (auto i = 0; i < dim; i++)
		arr[i] = src[i];
	return arr;
}
// trim from start
static inline std::string &ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
	return ltrim(rtrim(s));
}

// trim from start
static inline std::wstring &wltrim(std::wstring &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
static inline std::wstring &wrtrim(std::wstring &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
static inline std::wstring &wtrim(std::wstring &s) {
	return wltrim(wrtrim(s));
}



static inline  std::vector<std::string> SplitString(const std::string& input, const std::string& regex) {
	// passing -1 as the submatch index parameter performs splitting
	std::regex re(regex);
	std::sregex_token_iterator
		first{ input.begin(), input.end(), re, -1 },
		last;
	return{ first, last };
}

static inline  std::vector<std::wstring> wSplitString(const std::wstring& input, const std::wstring& regex) {
	// passing -1 as the submatch index parameter performs splitting
	std::wregex re(regex);
	std::wsregex_token_iterator
		first{ input.begin(), input.end(), re, -1 },
		last;
	return{ first, last };
}

//Current time as string
static inline std::string GetDateAsString()
{
	std::chrono::time_point<std::chrono::system_clock> hora = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(hora);
	std::string t(std::ctime(&end_time));
	t = trim(t);
	return t;
}

//A conta feita aqui coincide com a conta que � feita no volview e d� o mesmo resultado.
template <typename ImageType> array<double, 3> GetImageCenterLikeInVolView(typename ImageType::Pointer ptrImg)
{
	typename ImageType::PointType loadedImagePhysicalOrigin = ptrImg->GetOrigin();
	typename ImageType::SpacingType loadedImageSpacing = ptrImg->GetSpacing();
	typename ImageType::RegionType loadedImageRegion = ptrImg->GetLargestPossibleRegion();
	typename ImageType::RegionType::SizeType loadedImageSize = loadedImageRegion.GetSize();
	std::array<double, 3> loadedImageCenter = { { loadedImageSize[0] * loadedImageSpacing[0] / 2,
		loadedImageSize[1] * loadedImageSpacing[1] / 2,
		loadedImageSize[2] * loadedImageSpacing[2] / 2 } };
	std::array<double, 3> loadedImageCenterSpatialLocation = { { loadedImageCenter[0] + loadedImagePhysicalOrigin[0] - loadedImageSpacing[0],
		loadedImageCenter[1] + loadedImagePhysicalOrigin[1] - loadedImageSpacing[1],
		loadedImageCenter[2] + loadedImagePhysicalOrigin[2] - loadedImageSpacing[2] } };
	std::cout << "..Loaded Image center's spatial location = " << loadedImageCenterSpatialLocation[0] << ", " << loadedImageCenterSpatialLocation[1] << ", " << loadedImageCenterSpatialLocation[2] << std::endl;
	return loadedImageCenterSpatialLocation;
}

