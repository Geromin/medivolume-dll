#pragma once
#include "precompiled_header.h"
/**
	� nesse arquivo que eu guardo as coisas que n�o tem lugar definido.
*/




typedef void (*DelphiProgressCallback)(double progress);

typedef void (*DelphiPickedCellCallback)(int i, int j, int k, short v);

using namespace std;
/**
	O numero de threads � o numero de n�cleos que a plataforma tem.
*/
int GetNumberOfThreads();
/**
	Faz o dump de um stencil pra disco na forma de fatias em fun��o de x.
*/
void _MyStencilDebugOutput(vtkImageStencilData* stencil,  const char *filename);

// maximum mumber of lines the output console should have
static const WORD MAX_CONSOLE_LINES = 500;

string GetRaiz();
