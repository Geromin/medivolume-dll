#pragma once
#include "precompiled_header.h"
typedef void(__stdcall *CallbackDeMousePositionMPR)(int x, int y, int z, short scalar);

class CallbackDePosicaoDoMouseMPR
{
public:
	virtual void GetPosicao(int x, int y, int z, short scalar, std::string texto) = 0;
	virtual ~CallbackDePosicaoDoMouseMPR()
	{

	}
};

