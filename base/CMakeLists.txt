cmake_minimum_required(VERSION 2.8)

project(BASE)
file(GLOB my_source_files "*.h" "*.cpp" "*.txx" "*.c")
add_library (base ${my_source_files} ) 
# Expose B's public includes (including Boost transitively) to other
# subprojects through cache variable.
set(${PROJECT_NAME}_INCLUDE_DIRS ${PROJECT_SOURCE_DIR}
    CACHE INTERNAL "${PROJECT_NAME}: Include Directories" FORCE)
	
