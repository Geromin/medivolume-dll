#pragma once
#include "precompiled_header.h"



using namespace std;

class ISubsistema
{
private:
	std::string idExame, idSerie;
public:
	std::string GetIdExame()const;
	std::string GetIdSerie()const;

	ISubsistema(char *idExame, char *idSerie);

	bool IsThisExam(char* idExame, char* idSerie)const;
	virtual void CreatePipeline(itk::Image<short, 3>::Pointer image, map<string, string> metadata, short scalarOffset) = 0;
	virtual void CreateScreen(HWND handle, int qual=0) = 0;
	virtual void Resize(int w, int h, int qual = 0) = 0;
	virtual void Render(int qual = 0) = 0;
	virtual void OnMouseMove(HWND wnd, UINT nFlags, int X, int Y, int qual) = 0;
};