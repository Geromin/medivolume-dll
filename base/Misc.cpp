#pragma once
#include "precompiled_header.h"
#include "Misc.h"

string GetRaiz(){
	//pega o nome do execut�vel, o arquivo de sa�da ficar� no mesmo dir.
	char path[512];
	GetModuleFileName(NULL, path, 512);
	string Path(path);
	//picota a string pra pegar o diretorio da aplica��o.
	vector<string> pedacos;
	regex fileSeparator("\\\\+");
	//cria o iterator de tokens pra string do caminho de arquivos
	sregex_token_iterator tokens(Path.begin(), Path.end(), fileSeparator, -1);
	//for_each - o iterator de in�cio s�o os tokens, o de fim � token_iterator default, e o lambda captura 'peda�os'
	//do escopo superior e guarda a string recebida como par�metro nela.
	for_each(tokens, sregex_token_iterator(),[&pedacos](const string& s){
		pedacos.push_back(s);
	});
	//agora guarda a string do path - s�o todos os peda�os menos o ultimo, que � o nome do execut�vel.
	stringstream ss;
	for(unsigned int i=0; i<pedacos.size()-1; i++){
		ss<<pedacos[i]<<"\\";
	}
	string result;
	result = ss.str();
	return result;
}

int GetNumberOfThreads(){
	SYSTEM_INFO sysinfo;
	GetSystemInfo( &sysinfo );
	int numCPU = sysinfo.dwNumberOfProcessors;
	return numCPU;
}

void _MyStencilDebugOutput(vtkImageStencilData* stencil,  const char *filename)
{
	int extent[6];
	stencil->GetExtent(extent);
	vtkImageData* tempImage = vtkImageData::New();
	tempImage->SetExtent(extent);
	vtkInformation* info = tempImage->GetInformation();
	vtkDataObject::SetPointDataActiveScalarInfo(info,VTK_UNSIGNED_CHAR, 1);
	tempImage->AllocateScalars(info);
	//preeche
	for(int x = extent[0] ; x <= extent[1]; x++){
		for(int y = extent[2] ; y <= extent[3]; y++){
			for(int z = extent[4] ; z <= extent[5]; z++){
				if(stencil->IsInside(x,y,z))
					tempImage->SetScalarComponentFromDouble(x,y,z, 0, 255);
				else
					tempImage->SetScalarComponentFromDouble(x,y,z, 0, 0);
			}
		}
	}
  vtkPNGWriter *writer = vtkPNGWriter::New();
  writer->SetFileDimensionality(2);
  writer->SetInputData( tempImage );
  writer->SetFilePattern ("c://experiencia 01//debugImages//stencil%d.png");
  writer->Write();
}