#pragma once
#include "precompiled_header.h"
namespace corEOpacidade
{
	class ImageWindowChangeListener
	{
	public:
		virtual void ExecuteWindowChange(int level, int window) = 0;
		virtual void ExecuteRelativeWindowChange(int dCenter, int dWidth) = 0;
		virtual int GetWindowWidth() = 0;
		virtual int GetWindowCenter() = 0;

		virtual ~ImageWindowChangeListener(){}
	};
}
