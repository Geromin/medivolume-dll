#pragma once
#include "stdafx.h"

class myLetraRenderPass : public vtkRenderPass {
private:
	TTF_Font* font;
	SDL_Surface* canvasSdlSurface;
	SDL_Renderer* canvasSdlRenderer;
	//vtkSmartPointer<vtkCameraPass> cameraPass;
	//vtkSmartPointer<vtkLightsPass> lightsPass;
	//vtkSmartPointer<vtkDefaultPass> defaultPass;
	//vtkSmartPointer<vtkRenderPassCollection> passes;
	//vtkSmartPointer<vtkSequencePass> sequencePass;
	std::string letraEsquerda, letraDireita, letraCima, letraBaixo;
	myLetraRenderPass();
	~myLetraRenderPass();
	float thickness, window, level;
	std::array<BYTE, 3> corOrientacao, corInformacao;
public:
	static myLetraRenderPass *New();
	void Render(const vtkRenderState* s);
	void Calculate(std::array<double, 4> orientation);
	void SetEspessura(float e) { thickness = e; }
	void SetWL(float w, float l) { window = w; level = l; }
	void SetCorDaOrientacao(BYTE r, BYTE g, BYTE b) {
		corOrientacao = { {r,g,b} };
	}
	void SetCorDaInformacao(BYTE r, BYTE g, BYTE b) {
		corInformacao = { {r,g,b} };
	}
};