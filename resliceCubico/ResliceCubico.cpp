#include "stdafx.h"
#include "ResliceCubico.h"
#include <itktypes.h>
#include <boost/algorithm/string.hpp>
void CalculateCameraOffscreenMovement(vtkCamera* camera, double spacing, int fatia, std::array<double, 3>& outNewPos, std::array<double, 3>& outNewFocus);



vtkSmartPointer<vtkImageFlip> FlipResultingImageToCorrectOrientation(vtkSmartPointer<vtkImageData> resultadoDoReslice, short scalarOffset, std::string imageOrientation) {
	assert(resultadoDoReslice->GetExtent()[1] != -1);
	auto shifter = NewVTK(vtkImageShiftScale);
	shifter->SetInputData(resultadoDoReslice);
	shifter->SetShift(-scalarOffset);
	shifter->SetOutputScalarTypeToShort();

	if (imageOrientation == "HFS"){
		auto flipperX = NewVTK(vtkImageFlip);
		flipperX->SetInputConnection(shifter->GetOutputPort());
		flipperX->SetFilteredAxis(0);
		flipperX->Update();
		return flipperX;
	}
	else if (imageOrientation == "FFS"){
		auto flipperX = NewVTK(vtkImageFlip);
		flipperX->SetInputConnection(shifter->GetOutputPort());
		flipperX->SetFilteredAxis(0);
		flipperX->Update();
		return flipperX;
	}
	else{
		return nullptr;
	}
}

ResliceCubico::RepositionCubeObserver::RepositionCubeObserver(){
	camera = nullptr;
	cubeActor = nullptr;
}

void ResliceCubico::RepositionCubeObserver::Execute(vtkObject *caller, unsigned long ev, void *calldata) {
	//o cubo deve ficar onde o focus da c�mera est�
	cubeActor->SetPosition(camera->GetFocalPoint());
	vtkInteractorObserver *s = vtkRenderer::SafeDownCast(caller)->GetRenderWindow()->GetInteractor()->GetInteractorStyle();//um vtkInteractorSytleImage
}

ResliceCubico::CaptureCurrentSliceObserver::CaptureCurrentSliceObserver() {
	resliceMapper = nullptr;
	outer = nullptr;
}

void ResliceCubico::CaptureCurrentSliceObserver::Execute(vtkObject *caller, unsigned long ev, void *calldata) {
	assert(resliceMapper);
	vtkImageResliceToColors* reslice = resliceMapper->GetImageReslice();
	vtkSmartPointer<vtkImageData> imagem = reslice->GetOutput();//Aqui est� o resultado do reslice, antes do window/level.
	outer->resultadoDoReslice = imagem;//guarda.
	vtkSmartPointer<vtkMatrix4x4> currentAxes = NewVTK(vtkMatrix4x4);
	if (outer->resultadoDoReslice->GetExtent()[1] == -1)
		return;
	currentAxes->DeepCopy(reslice->GetResliceAxes());
	outer->resliceAxes = currentAxes;
	outer->renderPass->SetWL(outer->ip->GetColorWindow(), outer->ip->GetColorLevel() - outer->scalarOffset);
	//Deveria ser aqui que eu envio a imagem pro delphi
	//flipa
	std::string imageOrientation = outer->metadata["0018|5100"];
	boost::algorithm::trim(imageOrientation);
	//auto imagemCorrigida = FlipResultingImageToCorrectOrientation(reslice->GetOutput(), outer->scalarOffset, imageOrientation);
	//imagemCorrigida->Update();
	//envio pro delphi
	//vtkImageData* img = imagemCorrigida->GetOutput();
	ImageDataToDelphi result;
	//o buffer de mem�ria
	const int imageSize = imagem->GetDimensions()[0] * imagem->GetDimensions()[1];
	short *bufferData = new short[imageSize];
	memcpy(bufferData, imagem->GetScalarPointer(), imageSize * sizeof(short));
	result.bufferData = bufferData;
	result.bufferSize = imageSize * sizeof(short);
	//As dimens�es da imagem
	result.imageSize[0] = imagem->GetDimensions()[0];
	result.imageSize[1] = imagem->GetDimensions()[1];
	//O spacing
	result.spacing[0] = imagem->GetSpacing()[0];
	result.spacing[1] = imagem->GetSpacing()[1];
	result.spacing[2] = outer->im->GetSlabThickness();
	//os direction cosines
	result.uVector[0] = outer->resliceAxes->Element[0][0];
	result.uVector[1] = outer->resliceAxes->Element[0][1];
	result.uVector[2] = outer->resliceAxes->Element[0][2];
	result.vVector[0] = outer->resliceAxes->Element[1][0];
	result.vVector[1] = outer->resliceAxes->Element[1][1];
	result.vVector[2] = outer->resliceAxes->Element[1][2];
	//A physical origin. A physical origin 3d, espacial, � perdida quando se faz o reslice, ela tem que ser recalculada - Essa conta est� proxima do que o mediworks espera.
	std::array<double, 3> center, u, v, _ss;
	outer->imagem->GetOutput()->GetSpacing(_ss.data());
	u = { { result.uVector[0], result.uVector[1], result.uVector[2], } };
	v = { { result.vVector[0], result.vVector[1], result.vVector[2], } };
	outer->renderer->GetActiveCamera()->GetFocalPoint(center.data());
	const double du = ((1.0)*result.imageSize[0]) / 2.0 * result.spacing[0];
	const double dv = ((1.0)*result.imageSize[1]) / 2.0 * result.spacing[1];
	std::array<double, 3> f;
	f = (center + du*(u)) + dv*(-1.0 * v) + _ss;//F � physical origin calculada, desenhe os vetores pra entender o que t� acontecendo.
	std::cout << "Meu caluculo maluco = " << f[0] << ", " << f[1] << ", " << f[2] << std::endl;
	result.physicalOrigin[0] = f[0];//imagem->GetOrigin()[0];
	result.physicalOrigin[1] = f[1];//imagem->GetOrigin()[1];
	result.physicalOrigin[2] = f[2];//imagem->GetOrigin()[2];
	//std::cout << "    " << __FUNCTION__ << " position = " << imagem->GetOrigin()[0] << ", " << imagem->GetOrigin()[1] << ", " << imagem->GetOrigin()[0] << std::endl;
	//o center agora
	result.center[0] = outer->renderer->GetActiveCamera()->GetFocalPoint()[0];
	result.center[1] = outer->renderer->GetActiveCamera()->GetFocalPoint()[1];
	result.center[2] = outer->renderer->GetActiveCamera()->GetFocalPoint()[2];
	outer->onResliceCallback(result, (long)outer);
	//Teste do calculo do voxel a partir do focus
	//std::array<double, 3> focalPoint, spacing, origin, calculatedVoxel;
	//outer->renderer->GetActiveCamera()->GetFocalPoint(focalPoint.data());
	//outer->imagem->GetOutput()->GetSpacing(spacing.data());
	//outer->imagem->GetOutput()->GetOrigin(origin.data());
	//calculatedVoxel[0] = (focalPoint[0] - origin[0]) / spacing[0] + spacing[0];
	//calculatedVoxel[1] = (focalPoint[1] - origin[1]) / spacing[1] + spacing[1];
	//calculatedVoxel[2] = (focalPoint[2] - origin[2]) / spacing[2] + spacing[2];
	//cout << "   voxel em [" << calculatedVoxel[0] << ", " << calculatedVoxel[1] << ", " << calculatedVoxel[2] << "]" << endl;

	//std::array<double, 3> __c, __o;
	//outer->imagem->GetOutput()->GetCenter(__c.data());
	//outer->imagem->GetOutput()->GetOrigin(__o.data());
	//std::array<double, 6> __b;
	//outer->imagem->GetOutput()->GetBounds(__b.data());
	//vtkImageData* img = outer->imagem->GetOutput();

}

ResliceCubico::RenderPassDoResliceCubico::RenderPassDoResliceCubico() {
	cameraPass = NewVTK(vtkCameraPass);
	clearZPass = NewVTK(vtkClearZPass);
	lightsPass = NewVTK(vtkLightsPass);
	opaquePass = NewVTK(vtkOpaquePass);
	translucentPass = NewVTK(vtkTranslucentPass);
	volumetricPass = NewVTK(vtkVolumetricPass);
	overlayPass = NewVTK(vtkOverlayPass);
	letrasPass = NewVTK(myLetraRenderPass);
}

vtkSmartPointer<vtkImageImport> ResliceCubico::RenderPassDoResliceCubico::importScreenBufferToVTK(int screenWidth, int screenHeight, BYTE* bufferComImagem) {
	auto renderizacaoDaImagem = NewVTK(vtkImageImport);
	int szX = screenWidth;
	int szY = screenHeight;
	int szZ = 1;
	double sX = 1;
	double sY = 1;
	double sZ = 1;
	double oX = 0;
	double oY = 0;
	double oZ = 0;
	renderizacaoDaImagem->SetDataSpacing(sX, sY, sZ);
	renderizacaoDaImagem->SetDataOrigin(oX, oY, oZ);
	renderizacaoDaImagem->SetWholeExtent(0, szX - 1, 0, szY - 1, 0, szZ - 1);
	renderizacaoDaImagem->SetDataExtentToWholeExtent();
	renderizacaoDaImagem->SetNumberOfScalarComponents(4);
	renderizacaoDaImagem->SetDataScalarTypeToUnsignedChar();
	renderizacaoDaImagem->SetImportVoidPointer(bufferComImagem, 1);
	renderizacaoDaImagem->Update();//Aqui eu tenho a pixeldata da tela com a imagem renderizada empacotada de uma maneira conveniente.
	return renderizacaoDaImagem;
}

void ResliceCubico::RenderPassDoResliceCubico::Render(const vtkRenderState *s){
	vtkOpenGLRenderer *ren = vtkOpenGLRenderer::SafeDownCast(s->GetRenderer());
	vtkOpenGLRenderWindow *wnd = vtkOpenGLRenderWindow::SafeDownCast(ren->GetRenderWindow());
	//Renderiza a imagem
	cameraPass->Render(s);
	clearZPass->Render(s);
	lightsPass->Render(s);
	image->Render(s->GetRenderer());
	const int screenWidth = wnd->GetSize()[0];
	const int screenHeight = wnd->GetSize()[1];
	BYTE *bufferComImagem = wnd->GetRGBACharPixelData(0, 0, screenWidth - 1, screenHeight - 1, 0);
	//guarda o resultado da renderiza��o em uma imagem na RAM.
	auto renderizacaoDaImagem = importScreenBufferToVTK(screenWidth, screenHeight, bufferComImagem);
	//Agora renderiza o cubo, com a imagem como fundo
	cameraPass->Render(s);
	clearZPass->Render(s);
	lightsPass->Render(s);
	cube->RenderOpaqueGeometry(s->GetRenderer());

	BYTE *bufferComCubo = wnd->GetRGBACharPixelData(0, 0, screenWidth - 1, screenHeight - 1, 0);
	//guarda o resultado da renderiza��o em uma imagem na RAM.
	auto renderizacaoDoCubo = importScreenBufferToVTK(screenWidth, screenHeight, bufferComCubo);
	//combina as imagens, com a 2a imagem sendo posta por cima da primeira. Isso aqui t� lent�o.
	const int numberOfPixels = screenWidth * screenHeight;
	for (auto i = 0; i < numberOfPixels; i++) {
		if (bufferComCubo[i * 4 + 3] == 255)
			bufferComCubo[i * 4 + 3] = bufferComCubo[i * 4 + 3];
		else
		{
			bufferComCubo[i * 4 + 0] = bufferComImagem[i * 4 + 0];
			bufferComCubo[i * 4 + 1] = bufferComImagem[i * 4 + 1];
			bufferComCubo[i * 4 + 2] = bufferComImagem[i * 4 + 2];
			bufferComCubo[i * 4 + 3] = bufferComImagem[i * 4 + 3];
		}
	}
	//passa de volta pra tela
	wnd->SetRGBACharPixelData(0, 0, screenWidth - 1, screenHeight - 1, bufferComCubo, 0);
	delete bufferComCubo;
	delete bufferComImagem;
	std::array<double, 4> camOrientatation;
	camOrientatation[0] = s->GetRenderer()->GetActiveCamera()->GetOrientationWXYZ()[0];
	camOrientatation[1] = s->GetRenderer()->GetActiveCamera()->GetOrientationWXYZ()[1];
	camOrientatation[2] = s->GetRenderer()->GetActiveCamera()->GetOrientationWXYZ()[2];
	camOrientatation[3] = s->GetRenderer()->GetActiveCamera()->GetOrientationWXYZ()[3];
	letrasPass->Calculate(camOrientatation);
	letrasPass->Render(s);
}

void ResliceCubico::ApplyFuncaoInterpolacaoAndThickness() {
	if (thingsAreSet) {
		im->SetSlabThickness(slabThicknessInMM);
		switch (tipoFuncao)
		{
		case MIP:
			im->SetSlabTypeToMax();
			break;
		case MinP:
			im->SetSlabTypeToMin();
			break;
		case Composite:
			im->SetSlabTypeToMean();
			break;
		}
		switch (tipoInterpolacao) {
		case Linear:
			im->GetImageReslice()->SetInterpolationModeToLinear();
			break;
		case Nearest:
			im->GetImageReslice()->SetInterpolationModeToNearestNeighbor();
			break;
		case Cubica:
			im->GetImageReslice()->SetInterpolationModeToCubic();
			break;
		}
	}
	renderPass->SetEspessura(slabThicknessInMM);
	renderPass->SetWL(this->ip->GetColorWindow(), this->ip->GetColorLevel());
}

void ResliceCubico::SetUp() {
	if (!imagem || !renderer)
		return;
	else {
		//cria��o do renderpass
		vtkImageData* xxx = imagem->GetOutput();
		std::array<double, 3> xxxCenter;
		xxx->GetCenter(xxxCenter.data());
		renderPass = NewVTK(RenderPassDoResliceCubico);
		vtkOpenGLRenderer::SafeDownCast(renderer)->SetPass(renderPass);
		//Cria��o do mapper, property e actor do reslice
		im = NewVTK(vtkImageResliceMapper);
		//A slab thickness deve ser a da metadata
		double imageSlabThickness = 1.0;
		try{
			imageSlabThickness = boost::lexical_cast<double>(boost::trim_left_copy(boost::trim_right_copy(metadata.at("0018|0050"))));
		}
		catch (std::exception &ex){
			cout <<__FUNCTION__<<" "<<__LINE__<<" Vai usar o default, (1.0)." << endl;
			cout << ex.what() << endl;
		}
		this->slabThicknessInMM = imageSlabThickness;
		im->SetSlabThickness(imageSlabThickness);
		im->SetInputConnection(imagem->GetOutputPort());
		im->SliceFacesCameraOn();
		im->SliceAtFocalPointOn();
		im->BorderOff();
		im->AutoAdjustImageQualityOn();
		im->ResampleToScreenPixelsOff();
		im->GetImageReslice()->BorderOff();

		ip = NewVTK(vtkImageProperty);
		ip->SetColorWindow(initialW);
		ip->SetColorLevel(initialL + scalarOffset);
		ip->SetInterpolationTypeToLinear();
		ia = NewVTK(vtkImageSlice);
		renderPass->image = ia;//O renderpass precisa conhecer a image actor
		ia->SetMapper(im);
		ia->SetProperty(ip);
		renderer->AddViewProp(ia);
		style = NewVTK(myStyle);
		style->SetInteractionModeToImage3D();
		renderer->GetRenderWindow()->GetInteractor()->SetInteractorStyle(style);
		renderer->GetActiveCamera()->ParallelProjectionOn();
		renderer->ResetCameraClippingRange();
		renderer->ResetCamera();
		//Agora � o cubo
		auto cubeSource = NewVTK(vtkCubeSource);
		cubeSource->SetXLength(100);
		cubeSource->SetYLength(100);
		cubeSource->SetZLength(100);
		auto cubeMapper = NewVTK(vtkPolyDataMapper);
		cubeMapper->SetInputConnection(cubeSource->GetOutputPort());
		auto _subCubeActor = NewVTK(vtkActor);
		_subCubeActor->SetMapper(cubeMapper);
		_subCubeActor->GetProperty()->BackfaceCullingOff();
		_subCubeActor->GetProperty()->SetRepresentationToWireframe();
		_subCubeActor->GetProperty()->SetColor(0, 0.75, 0);
		_subCubeActor->GetProperty()->LightingOff();
		_subCubeActor->GetProperty()->BackfaceCullingOff();
		_subCubeActor->GetProperty()->SetLineWidth(2);
		//O ponto central do cubo
		auto s = NewVTK(vtkSphereSource);
		s->SetRadius(2.5);
		auto sm = NewVTK(vtkPolyDataMapper);
		sm->SetInputConnection(s->GetOutputPort());
		auto _sa = NewVTK(vtkActor);
		_sa->SetMapper(sm);
		_sa->GetProperty()->BackfaceCullingOff();
		_sa->GetProperty()->SetColor(0, 0.75, 0);
		_sa->GetProperty()->LightingOff();
		_sa->GetProperty()->BackfaceCullingOff();
		//A assembly
		cubeActor = NewVTK(vtkAssembly);
		cubeActor->SetPosition(renderer->GetActiveCamera()->GetFocalPoint());
		cubeActor->AddPart(_sa);
		cubeActor->AddPart(_subCubeActor);

		renderPass->cube = cubeActor;

		renderer->AddActor(cubeActor);
		renderer->ResetCamera();
		renderer->GetActiveCamera()->Zoom(2.0);
		vtkCamera* camAqui = renderer->GetActiveCamera();
		//Agora os observers.
		auto eventObserver = NewVTK(RepositionCubeObserver);
		eventObserver->camera = renderer->GetActiveCamera();
		eventObserver->cubeActor = cubeActor;
		renderer->AddObserver(vtkCommand::EndEvent, eventObserver);
		renderer->AddObserver(vtkCommand::StartEvent, eventObserver);
		//renderer->AddObserver(vtkCommand::RenderEvent, eventObserver);
		auto captureSlice = NewVTK(CaptureCurrentSliceObserver);
		captureSlice->outer = this;
		captureSlice->resliceMapper = im;
		renderer->AddObserver(vtkCommand::EndEvent, captureSlice);
		renderer->AddObserver(vtkCommand::StartEvent, captureSlice);
		// renderer->AddObserver(vtkCommand::RenderEvent, captureSlice);
		//Est� tudo pronto...
		thingsAreSet = true;
		ApplyFuncaoInterpolacaoAndThickness();
		//Rotaciona de acordo com a orienta��o
		std::string imageOrientation = metadata["0018|5100"];
		boost::algorithm::trim(imageOrientation);
		if (imageOrientation == "FFS"){
			vtkCamera *cam = renderer->GetActiveCamera();
			cam->Roll(180);
			cam->Yaw(180);
			cam->OrthogonalizeViewUp();
		}
		else if (imageOrientation == "HFS"){
			vtkCamera *cam = renderer->GetActiveCamera();
			//cam->Roll(180);
			cam->Yaw(180);
			cam->Roll(180);
			//vtkCamera *cam = renderer->GetActiveCamera();
			//cam->Roll(180);
			//cam->Yaw(180);
		}
		else{
			std::string erro = "ORIENTACAO DESCONHECIDA = " + imageOrientation;
			cout << erro << endl;
			std::exception ex = std::exception(erro.c_str());
			BOOST_THROW_EXCEPTION(ex);
		}


	}
}

ResliceCubico::ResliceCubico(const char* idExame, const char* idSerie) {
	hasSetInitialWL = false;
	initialW = -1;
	initialL = -1;
	onResliceCallback = nullptr;
	slabThicknessInMM = 1.0;
	tipoFuncao = Composite;
	tipoInterpolacao = Linear;
	thingsAreSet = false;
	ia = nullptr;
	ip = nullptr;
	im = nullptr;
	imagem = nullptr;
	renderer = nullptr;
	renderPass = nullptr;
	resultadoDoReslice = nullptr;
	exame = idExame;
	serie = idSerie;
	deslocamentoDaFatiaDoResliceOffline = 0;
	canRender = false;
}

void ResliceCubico::SetSlabThicknessInMM(double mm) {
	slabThicknessInMM = mm;
	ApplyFuncaoInterpolacaoAndThickness();
}

void ResliceCubico::SetFuncao(ResliceCubico::Funcao f) {
	tipoFuncao = f;
	ApplyFuncaoInterpolacaoAndThickness();
}

void ResliceCubico::SetInterpolacao(ResliceCubico::Interpolacao i) {
	tipoInterpolacao = i;
	ApplyFuncaoInterpolacaoAndThickness();
}

void ResliceCubico::SetImage(vtkImageImport* img, std::map<std::string, std::string> metadata, short scalarOffset) {
	canRender = false;
	imagem = img;
	this->metadata = metadata;
	this->scalarOffset = scalarOffset;
	SetUp();
}

void ResliceCubico::SetRenderer(vtkRenderer* renderer) {
	this->renderer = renderer;
	SetUp();
}

void ResliceCubico::SetPosicao(){
	if (!thingsAreSet)
		return;
	std::array<double, 3> imgCenter;
	imagem->GetOutput()->GetCenter(imgCenter.data());
	SetPosicao(imgCenter);
}

void ResliceCubico::SetPosicao(std::array<double, 3> newPos) {
	if (!thingsAreSet)
		return;
	//Quando se move uma c�mera na vtk tem que mover a position (que � o Eye no lookat do
	//opengl) e o focus ao mesmo tempo, com o mesmo deslocamento.
	vtkCamera* camera = renderer->GetActiveCamera();
	std::array<double, 3> initialPosition, initialFocus;
	camera->GetPosition(initialPosition.data());
	camera->GetFocalPoint(initialFocus.data());
	std::array<double, 3> dInformedInitial = newPos - initialFocus;//Lembrando que essa opera��o array<double,3>-array<double,3> e outras est�o definidas no stdafx.h
	//soma � diferen�a ao focus e posicao
	std::array<double, 3> newFocus = initialFocus + dInformedInitial;
	std::array<double, 3> newPosition = initialPosition + dInformedInitial;
	//aplica
	camera->SetFocalPoint(newFocus.data());
	camera->SetPosition(newPosition.data());
	//agora renderiza
	//renderer->GetRenderWindow()->Re nder();
}

void ResliceCubico::Destroy() {
	Desligar();
	im = nullptr;
	ip = nullptr;
	ia = nullptr;
	style = nullptr;
	cubeActor = nullptr;
	imagem = nullptr;
	letraRenderPass = nullptr;

}

ImageDataToDelphi ResliceCubico::GetFatia(double spacing, double espessura, int fatia) {
	assert(false && "OBSOLETO");


	return ImageDataToDelphi();
}

void ResliceCubico::CreateRenderer(HWND handle) {
	canRender = false;
	renderer = NewVTK(vtkOpenGLRenderer);
	renderer->GetActiveCamera()->ParallelProjectionOn();
	renderer->ResetCamera();
	renderer->SetBackground(0, 0, 0);
	 renderWindow = NewVTK(vtkWin32OpenGLRenderWindow);
	renderWindow->SetWindowId(handle);
	renderWindow->AddRenderer(renderer);
	renderWindowInteractor = NewVTK(vtkWin32RenderWindowInteractor);
	renderWindowInteractor->SetRenderWindow(renderWindow);
	vtkWin32RenderWindowInteractor::SafeDownCast(renderWindowInteractor)->InstallMessageProcOn();
	if (canRender)
		renderWindow->Render();
}


void ResliceCubico::Resize(int w, int h) {
	renderWindow->SetSize(w, h);
}

void ResliceCubico::Render() {
	if (canRender)
		renderWindow->Render();
}

void ResliceCubico::SetWL(double ww, double wl) {
	if (ia)//O actor j� foi criado, posso mexer nele
	{
		ia->GetProperty()->SetColorWindow(ww);
		ia->GetProperty()->SetColorLevel(wl+scalarOffset);
		if (canRender)
			renderWindow->Render();
	}
	else//Guardo o window/level pra usar mais tarde
	{
		hasSetInitialWL = true;
		initialW = ww;
		initialL = wl;
	}
}

int ResliceCubico::LMouseDown(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnLButtonDown(wnd, nFlags, X, Y);
	else
		return 0;
}
int ResliceCubico::LMouseUp(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnLButtonUp(wnd, nFlags, X, Y);
	else
		return 0;
}
int ResliceCubico::MMouseDown(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnMButtonDown(wnd, nFlags, X, Y);
	else
		return 0;
}
int ResliceCubico::MMouseUp(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnMButtonUp(wnd, nFlags, X, Y);
	else
		return 0;
}
int ResliceCubico::RMouseDown(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnRButtonDown(wnd, nFlags, X, Y);
	else
		return 0;
}
int ResliceCubico::RMouseUp(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnRButtonUp(wnd, nFlags, X, Y);
	else
		return 0;
}

void ResliceCubico::Ligar(HWND handle) {
	if (renderWindow != nullptr && vtkWin32OpenGLRenderWindow::SafeDownCast(renderWindow)->GetWindowId() == handle){
		//A tela j� est� criada, n�o precisa fazer o ritual
		cout << "j� est� criada" << endl;
	}
	else{
		//A tela foi destruida e precisa ser recriada.
		deslocamentoDaFatiaDoResliceOffline = 0;
		//Guarda a c�mera do renderer atual (o offscreen)
		auto tempCamera = NewVTK(vtkCamera);
		tempCamera->DeepCopy(renderer->GetActiveCamera());
		DestroyOffScreenRenderer();
		//Agora � recriar o onscreen renderer.
		CreateRenderer(handle);
		renderer->GetActiveCamera()->DeepCopy(tempCamera);
		//Linka as coisas necess�rias pro cubo funcionar.
		renderer->AddViewProp(ia);
		style->SetInteractionModeToImage3D();
		renderer->GetRenderWindow()->GetInteractor()->SetInteractorStyle(style);
		renderer->AddActor(cubeActor);
		auto eventObserver = NewVTK(RepositionCubeObserver);
		eventObserver->camera = renderer->GetActiveCamera();
		eventObserver->cubeActor = cubeActor;
		renderer->AddObserver(vtkCommand::EndEvent, eventObserver);
		renderer->AddObserver(vtkCommand::StartEvent, eventObserver);
		auto captureSlice = NewVTK(CaptureCurrentSliceObserver);
		captureSlice->outer = this;
		captureSlice->resliceMapper = im;
		renderer->AddObserver(vtkCommand::EndEvent, captureSlice);
		renderer->AddObserver(vtkCommand::StartEvent, captureSlice);
		vtkOpenGLRenderer::SafeDownCast(renderer)->SetPass(renderPass);
	}
	if (!hasSetInitialWL) {
		initialW = ip->GetColorWindow();
		initialL = ip->GetColorLevel();
		hasSetInitialWL = true;
	}
	canRender = true;
}

void  ResliceCubico::DestroyRenderer() {
	if(renderWindowInteractor)
		renderWindowInteractor->DebugOn();
	renderWindow->DebugOn();
	renderer->DebugOn();
	renderWindowInteractor = nullptr;
	renderer->ReleaseGraphicsResources(renderWindow);
	renderer = nullptr;
	renderWindow = nullptr;
	canRender = false;
	//No final desse m�todo todos os 3 objetos estar�o mortos, como comporvado observando os logs.
}

void  ResliceCubico::DestroyOffScreenRenderer() {
	this->DestroyRenderer();//N�o vejo pq n�o usar o mesmo m�todo.
}

void ResliceCubico::CreateOffScreenRenderer(vtkSmartPointer<vtkCamera> oldCamere) {
	//Cria pra offscreen rendering
	renderer = NewVTK(vtkRenderer);
	renderWindow = NewVTK(vtkWin32OpenGLRenderWindow);
	renderWindow->OffScreenRenderingOn();
	renderWindow->AddRenderer(renderer);
	renderer->ResetCamera();
	renderer->GetActiveCamera()->DeepCopy(oldCamere);
	//Poe o image actor
	renderer->AddActor(ia);
	//Liga ao observer que captura os resultados e os p�e em this.resultadoDoReslice
	auto captureSlice = NewVTK(CaptureCurrentSliceObserver);
	captureSlice->outer = this;
	captureSlice->resliceMapper = im;
	renderer->AddObserver(vtkCommand::EndEvent, captureSlice);
	renderer->AddObserver(vtkCommand::StartEvent, captureSlice);
}

void ResliceCubico::Desligar() {
	//O sistema de renderiza��o que existe ter� que ser destruido e outro criado. O novo sistema
	//ser� offscreen e deve receber a c�mera do est� sendo destruido.
	auto tempCamera = NewVTK(vtkCamera);
	tempCamera->DeepCopy(renderer->GetActiveCamera());
	tempCamera->Print(cout);
	DestroyRenderer();
	CreateOffScreenRenderer(tempCamera);
	initialCamera = tempCamera;
}

void CalculateCameraOffscreenMovement(vtkCamera* camera, double spacing, int fatia, std::array<double, 3>& outNewPos, std::array<double, 3>& outNewFocus) {
	std::array<double, 3> viewDirectionVector, focalPoint, position;
	camera->GetDirectionOfProjection(viewDirectionVector.data());
	vtkMath::Normalize(viewDirectionVector.data());
	camera->GetFocalPoint(focalPoint.data());
	camera->GetPosition(position.data());
	outNewPos = position + (spacing*fatia) * viewDirectionVector;
	outNewFocus = focalPoint + (spacing*fatia) * viewDirectionVector;
	//outNewFocus = focalPoint + 1.0 * viewDirectionVector;
}

ImageDataToDelphi ResliceCubico::GetReslice( int fatia) {
	long t0 = GetCurrentTime();
	std::cout << "   desl:" << deslocamentoDaFatiaDoResliceOffline << endl;
	if (fatia > 0)
		deslocamentoDaFatiaDoResliceOffline++;
	else if (fatia < 0)
		deslocamentoDaFatiaDoResliceOffline--;
	else
		deslocamentoDaFatiaDoResliceOffline = deslocamentoDaFatiaDoResliceOffline + 0;
	//move o reslice
	std::array<double, 3> newPos, newFocus;
	auto camera = renderer->GetActiveCamera();
	double spacing = resultadoDoReslice->GetSpacing()[2];
//	CalculateCameraOffscreenMovement(camera, spacing, deslocamentoDaFatiaDoResliceOffline, newPos, newFocus);
	CalculateCameraOffscreenMovement(initialCamera, spacing, deslocamentoDaFatiaDoResliceOffline, newPos, newFocus);
	//O z est� fora da imagem? Se estiver eu devo ou impedir, desfazendo o movimento anterior, ou ir para o outro lado da imagem
	std::array<double, 6> imageBounds;
	imagem->GetOutput()->GetBounds(imageBounds.data());
	if (newFocus[2]<imageBounds[4] || newFocus[2]>imageBounds[5]) {
		//estourou o limite, reposiciona o focus e a c�mera pra corrigir isso e ficar travado no lugar correto

	}

	camera->SetFocalPoint(newFocus.data());
	camera->SetPosition(newPos.data());
	//gera a nova fatia
	renderWindow->Render();
	//flipa
	std::string imageOrientation = metadata["0018|5100"];
	boost::algorithm::trim(imageOrientation);
	auto imagemCorrigida = FlipResultingImageToCorrectOrientation(resultadoDoReslice, scalarOffset, imageOrientation);
	imagemCorrigida->Update();
	//A imagem tem 	Rescale intercept, (0028|1052)?
	std::string rescaleIntercept = metadata.at("0028|1052");
	boost::trim(rescaleIntercept);
	vtkImageData* img = imagemCorrigida->GetOutput();

		double vIntercept = boost::lexical_cast<double>(rescaleIntercept);
		auto shifter = NewVTK(vtkImageShiftScale);
		shifter->SetInputConnection(imagemCorrigida->GetOutputPort());
		shifter->SetShift(-(vIntercept));
		shifter->SetOutputScalarTypeToShort();
		shifter->Update();
		img = shifter->GetOutput();
	//envio pro delphi

	ImageDataToDelphi result;
	//o buffer de mem�ria
	const int imageSize = img->GetDimensions()[0] * img->GetDimensions()[1];
	short *bufferData = new short[imageSize];
 	memcpy(bufferData, img->GetScalarPointer(), imageSize * sizeof(short));
	result.bufferData = bufferData;
	result.bufferSize = imageSize * sizeof(short);
	//As dimens�es da imagem
	result.imageSize[0] = img->GetDimensions()[0];
	result.imageSize[1] = img->GetDimensions()[1];
	//O spacing
	result.spacing[0] = img->GetSpacing()[0];
	result.spacing[1] = img->GetSpacing()[1];
	result.spacing[2] = im->GetSlabThickness();
	//os direction cosines
	result.uVector[0] = resliceAxes->Element[0][0];
	result.uVector[1] = resliceAxes->Element[0][1];
	result.uVector[2] = resliceAxes->Element[0][2];
	result.vVector[0] = resliceAxes->Element[1][0];
	result.vVector[1] = resliceAxes->Element[1][1];
	result.vVector[2] = resliceAxes->Element[1][2];
	////A physical origin. A physical origin 3d, espacial, � perdida quando se faz o reslice, ela tem que ser recalculada
	//std::array<double, 3> center, u, v;
	//u = { { result.uVector[0], result.uVector[1], result.uVector[2], } };
	//v = { { result.vVector[0], result.vVector[1], result.vVector[2], } };
	//renderer->GetActiveCamera()->GetFocalPoint(center.data());
	//const double du = ((1.0)*result.imageSize[0]) / 2.0 * result.spacing[0];
	//const double dv = ((1.0)*result.imageSize[1]) / 2.0 * result.spacing[1];
	//std::array<double, 3> f;
	//f = center + du*(-1.0 * u) + dv *(-1.0 * v);//F � physical origin calculada, desenhe os vetores pra entender o que t� acontecendo.
	result.physicalOrigin[0] = resultadoDoReslice->GetOrigin()[0];
	result.physicalOrigin[1] = resultadoDoReslice->GetOrigin()[1];
	result.physicalOrigin[2] = resultadoDoReslice->GetOrigin()[2];
	std::cout << "    " << __FUNCTION__ << " position = " << result.physicalOrigin[0] << ", " << result.physicalOrigin[1] << ", " << result.physicalOrigin[0] << std::endl;
	//o center agora
	result.center[0] = newFocus[0];
	result.center[1] = newFocus[1];
	result.center[2] = newFocus[2];
	////Debug
	//boost::posix_time::ptime current_date_microseconds = boost::posix_time::microsec_clock::local_time();
	//long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
	//std::string filename = "C:\\medivolume_src\\resliceCubico\\dump\\" + boost::lexical_cast<std::string>(milliseconds) + ".vti";
	//vtkSmartPointer<vtkXMLImageDataWriter> debugsave = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	//debugsave->SetFileName(filename.c_str());
	//debugsave->SetInputData(img);
	//debugsave->BreakOnError();
	//debugsave->Write();

	//tudo pronto...
	long t1 = GetCurrentTime();
	std::cout <<__FUNCTION__<< "  Tempo gasto = " << (t1 - t0) << std::endl;
	return result;
}

void ResliceCubico::TesteOffscreenReslice() {
	assert(false && "Teste, codigo obsoleto.");
	////Um teste na humildade pra ver se consigo avan�ar
	//auto camera = renderer->GetActiveCamera();
	//std::array<double, 3> newPos, newFocus;
	//CalculateCameraOffscreenMovement(camera, 1.0, 1.0, newPos, newFocus);
	//std::array<double, 3> viewDirectionVector, focalPoint, position;
	//camera->SetFocalPoint(focalPoint.data());
	//camera->SetPosition(position.data());
	//renderWindow->Rend er(); //Tenho que ver se isso aqui vai passar pelo ponto onde eu pego o reslice.
	//auto shifter = NewVTK(vtkImageShiftScale);
	//shifter->SetInputData(resultadoDoReslice);
	//shifter->SetShift(-scalarOffset);
	//auto flipperY = NewVTK(vtkImageFlip);
	//flipperY->SetInputConnection(shifter->GetOutputPort());
	//flipperY->SetFilteredAxis(1);
	//auto flipperX = NewVTK(vtkImageFlip);
	//flipperX->SetInputConnection(flipperY->GetOutputPort());
	//flipperX->SetFilteredAxis(0);
	//boost::posix_time::ptime current_date_microseconds = boost::posix_time::microsec_clock::local_time();
	//long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
	//std::string filename = "C:\\medivolume_src\\resliceCubico\\dump\\" + boost::lexical_cast<std::string>(milliseconds) + ".vti";
	//vtkSmartPointer<vtkXMLImageDataWriter> debugsave = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	//debugsave->SetFileName(filename.c_str());
	//debugsave->SetInputConnection(flipperX->GetOutputPort());
	//debugsave->BreakOnError();
	//debugsave->Write();
}

void ResliceCubico::SetOperacoesDeMouse(int qualBotao, int qualOperacao) {
	style->SetOperacao(qualBotao, qualOperacao);
}
void ResliceCubico::Reset() {
	renderer->ResetCamera();
	//Tem que resetar a posi��o da c�mera pra posi��o inicial fornecida
	//SetPosicao(posicaoInicial);
	renderer->GetActiveCamera()->DeepCopy(initialCamera);
	ip->SetColorWindow(initialW);
	ip->SetColorLevel(initialL + scalarOffset);
	//Tem que resetar a orienta��o
	//Tem que resetar o window/level


}
int ResliceCubico::MouseMove(HWND wnd, UINT nFlags, int X, int Y) {
	if (renderWindowInteractor)
		return renderWindowInteractor->OnMouseMove(wnd, nFlags, X, Y);
	else
		return 0;
}
void ResliceCubico::SetCallbackDeReslice(FNCallbackDoDicomReslice c) {
	onResliceCallback = c;
}
bool ResliceCubico::IsThisExam(const char* idExame, const char* idSerie) {
	std::string se = idExame;
	std::string ss = idSerie;
	if (exame == se && serie == ss)
		return true;
	else
		return false;
}

std::array<double, 3> ResliceCubico::GetCenterBySlice(int numSlice) {
	////Vers�o nova.
	//std::array<double, 3> resultado;
	////colunas 0028,0011
	////linhas 0028,0010
	////image position = 0020 0032 x/y/z
	//resultado[0] = colunas / 2 * spacingX + imagePositionX;
	//resultado[1] = linhas / 2 * spacingY + imagePositionY;
	//resultado[2] = imagePositionZ + numSlice * spacingZ;
	//return resultado;

	//Vers�o antiga.
	std::array<double, 3> physicalOrigin, spacing;
	std::array<int, 3> dimensions;
	auto img = imagem->GetOutput();
	img->GetSpacing(spacing.data());
	img->GetOrigin(physicalOrigin.data());
	img->GetDimensions(dimensions.data());
	std::array<double, 3> resultado;
	imagem->GetOutput()->GetCenter(resultado.data());
	resultado[2] = 0;
	std::array<double, 3> zVec = { {0,0,1} };
	zVec = physicalOrigin + double(numSlice-1/*+1*/) * spacing[2] * zVec;
	zVec[0] = 0;
	zVec[1] = 0;
	return resultado + zVec;
}

void ResliceCubico::SetCorDasLetrasDeOrientacao(BYTE r, BYTE g, BYTE b) {
	renderPass->SetCorDaOrientacao(r, g, b);
}

void ResliceCubico::SetCorDasLetrasDeInformacao(BYTE r, BYTE g, BYTE b) {
	renderPass->SetCorDaInformacao(r, g, b);
}