#include "stdafx.h"
#include "RenderPassLetraDeOrientacao.h"
#include "myStyle.h"


class ResliceCubico  {
public:
	enum Funcao { MIP, MinP, Composite };
	enum Interpolacao { Nearest, Linear, Cubica };

private:
	bool canRender;
	vtkSmartPointer<vtkWin32OpenGLRenderWindow> renderWindow;
	vtkSmartPointer<vtkWin32RenderWindowInteractor> renderWindowInteractor;
	//O callback do reslice, que manda pro delphi o resultado do reslice.
	FNCallbackDoDicomReslice onResliceCallback;
	//A identifica��o dessa imagem. � usado para ter mais de uma imagem aberta ao mesmo tempo em telas
	//diferentes.
	std::string exame, serie;
	vtkSmartPointer<myLetraRenderPass> letraRenderPass;
	//Metadata da imagem em forma de uma tabela de strings, necess�ria pra saber que rota��es fazer pra lidar com
	//HFS, FFS e afins
	std::map < std::string, std::string> metadata;
	//Espessura da fatia em mm.
	double slabThicknessInMM;
	//Quao fun��o de renderizacao e qual tipo de interpola��o deve ser usada?
	Funcao tipoFuncao;
	Interpolacao tipoInterpolacao;
	//Se true o SetUp j� rodou e construiu o sistema
	bool thingsAreSet;
	vtkSmartPointer<vtkImageData> resultadoDoReslice;
	vtkSmartPointer<vtkMatrix4x4> resliceAxes;
	//O mapper
	vtkSmartPointer<vtkImageResliceMapper> im;
	//As propriedades da imagem
	vtkSmartPointer<vtkImageProperty> ip;
	//O actor da imagem
	vtkSmartPointer<vtkImageSlice> ia;
	//O interactor style necess�rio
	vtkSmartPointer<myStyle> style;
	//O actor do cubo
	vtkSmartPointer<vtkAssembly> cubeActor;
	//a imagem usada no reslice
	vtkSmartPointer<vtkImageImport> imagem;
	vtkSmartPointer<vtkRenderer> renderer;
	//Serve para garantir que o centro do cubo esteja sempre no focus da c�mera.
	class RepositionCubeObserver :public vtkCommand {
	public:
		vtkCamera *camera;
		vtkProp3D *cubeActor;
	private:
		RepositionCubeObserver();
	public:
		static RepositionCubeObserver* New() { return new RepositionCubeObserver(); }
		void Execute(vtkObject *caller, unsigned long ev, void *calldata);
	};
	//Serve para capturar o imageData do reslice para envio para o delphi
	class CaptureCurrentSliceObserver :public vtkCommand {
	public:
		vtkImageResliceMapper* resliceMapper;
		ResliceCubico *outer;
	private:
		CaptureCurrentSliceObserver();
	public:
		static CaptureCurrentSliceObserver* New() { return new CaptureCurrentSliceObserver(); }
		void Execute(vtkObject *caller, unsigned long ev, void *calldata);
	};
	//Controla a renderiza��o pra fazer com que cubo apare�a por cima da imagem mesmo os
	//dois estando na mesma camada, algo que percebi ser necess�rio pra fazer o reslice funcionar.
	class RenderPassDoResliceCubico : public vtkRenderPass {
	private:
		vtkSmartPointer<vtkCameraPass> cameraPass;
		vtkSmartPointer<vtkClearZPass> clearZPass;
		vtkSmartPointer<vtkLightsPass> lightsPass;
		vtkSmartPointer<vtkOpaquePass> opaquePass;
		vtkSmartPointer<vtkTranslucentPass> translucentPass;
		vtkSmartPointer<vtkVolumetricPass> volumetricPass;
		vtkSmartPointer<vtkOverlayPass> overlayPass;
		vtkSmartPointer<myLetraRenderPass> letrasPass;
		RenderPassDoResliceCubico();
	public:
		void SetCorDaOrientacao(BYTE r, BYTE g, BYTE b) {
			letrasPass->SetCorDaOrientacao(r, g, b);
		}
		void SetCorDaInformacao(BYTE r, BYTE g, BYTE b) {
			letrasPass->SetCorDaInformacao(r, g, b);
		}
		vtkProp3D *cube;
		vtkImageSlice *image;
		static RenderPassDoResliceCubico* New() { return new RenderPassDoResliceCubico(); }

		vtkSmartPointer<vtkImageImport> importScreenBufferToVTK(int screenWidth, int screenHeight, BYTE* bufferComImagem);

		void Render(const vtkRenderState *s) override;
		void SetEspessura(float e) { letrasPass->SetEspessura(e); }
		void SetWL(float w, float l) { letrasPass->SetWL(w, l); }
	};
	vtkSmartPointer<RenderPassDoResliceCubico> renderPass;

	void ApplyFuncaoInterpolacaoAndThickness();
	///Se o renderer e a imagem tiverem sido informados, monta tudo necess�rio pro
	///reslice ocorrer. O reslice s� estar� est�vel depois que esse m�todo terminar
	///de executar.
	void SetUp();
	int deslocamentoDaFatiaDoResliceOffline;
	std::array<double, 3> posicaoInicial;
	short scalarOffset;
	vtkSmartPointer<vtkCamera> initialCamera;
	bool hasSetInitialWL;
	double initialW, initialL;
public:
	std::array<double, 3> GetCenterBySlice(int numSlice);
	ImageDataToDelphi GetReslice( int fatia);
	void TesteOffscreenReslice();
	void Desligar();
	void Ligar(HWND handle);
	ResliceCubico(const char* idExame, const char* idSerie);
	void SetSlabThicknessInMM(double mm);
	void SetFuncao(Funcao f);
	void SetInterpolacao(Interpolacao i);
	///Seta a imagem a ser usada no reslice e tenta montar o sistema de reslice.
	///O sistema s� ser� montado se o renderer e a imagem tiverem sido informados.
	void SetImage(vtkImageImport* img, std::map<std::string, std::string> metadata, short scalarOffset);
	///Seta o renderer a ser usado no reslice. Um renderer e sua renderWindow que esteja sendo
	///usados pelo reslice n�o devem ser usados para mais nada porque o renderPass e o interaction
	///style ser�o modificados para o reslice cubico funcionar e n�o servir�o para outras coisas.
	void SetRenderer(vtkRenderer* renderer);
	void SetPosicao();
	///Reposiciona o centro do reslice. S� deve ser invocado depois o reslice cubico estiver
	///est�vel, ou seja, depois que SetImage, e SetRenderer tiverem sido invocados.
	void SetPosicao(std::array<double, 3> newPos);
	void SetPosicaoInicial(std::array<double, 3> pos) {
		posicaoInicial = pos;
		SetPosicao(pos);
		initialCamera = vtkSmartPointer<vtkCamera>::New();
		initialCamera->DeepCopy(renderer->GetActiveCamera());
	}

	void Destroy();

	ImageDataToDelphi GetFatia(double spacing, double espessura, int fatia);
	///Cria o renderer, a renderwindow e o renderwindow interactor no HWND dado, que
	///pode ser qqer componente delphi com HWND, tipo tform ou tpanel (provavelmente ser�
	///num tpanel, que � o que o vincenzo usa).
	void CreateRenderer(HWND handle);
	///Serve para destruir o renderer+renderWindow+renderWindowInteractor que renderiza
	///no HWNW fornceido no CreateRenderer, ou seja, pra destruir o exibidor na tela. Isso
	///� usado no processo de desligamento do cubo, pra passer pra offscreen rendering.
	void DestroyRenderer();
	///Se o sistema de renderiza��o estiver destruido isso aqui cria o offscreen renderer,
	///que ser� usado pra gerar as fatias pro mediworks no m�doto de avan�ar/recuar na imagem.
	void CreateOffScreenRenderer(vtkSmartPointer<vtkCamera> oldCamere);
	///Quando o cubo for reativado o renderer offscreen tem que ser destruido pra que o renderer
	///onscreen seja recriado em seu lugar e as liga��es necess�rias para o funcionamento do
	///cubo na tela feitas.
	void DestroyOffScreenRenderer();

	void Resize(int w, int h);

	void Render();

	void SetWL(double ww, double wl);

	int MouseMove(HWND wnd, UINT nFlags, int X, int Y);
	int LMouseDown(HWND wnd, UINT nFlags, int X, int Y);
	int LMouseUp(HWND wnd, UINT nFlags, int X, int Y);
	int MMouseDown(HWND wnd, UINT nFlags, int X, int Y);
	int MMouseUp(HWND wnd, UINT nFlags, int X, int Y);
	int RMouseDown(HWND wnd, UINT nFlags, int X, int Y);
	int RMouseUp(HWND wnd, UINT nFlags, int X, int Y);
	void SetOperacoesDeMouse(int qualBotao, int qualOperacao);
	void Reset();
	void SetCallbackDeReslice(FNCallbackDoDicomReslice c);
	bool IsThisExam(const char* idExame, const char* idSerie);

	void SetCorDasLetrasDeOrientacao(BYTE r, BYTE g, BYTE b);
	void SetCorDasLetrasDeInformacao(BYTE r, BYTE g, BYTE b);
};