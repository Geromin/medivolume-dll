#include "stdafx.h"
#include "myStyle.h"


vtkStandardNewMacro(myStyle);

//----------------------------------------------------------------------------
myStyle::myStyle()
{
	codigoOperacaoDosBotoes[0] = VTKIS_NONE;
	codigoOperacaoDosBotoes[1] = VTKIS_NONE;
	codigoOperacaoDosBotoes[2] = VTKIS_NONE;

	this->WindowLevelStartPosition[0] = 0;
	this->WindowLevelStartPosition[1] = 0;

	this->WindowLevelCurrentPosition[0] = 0;
	this->WindowLevelCurrentPosition[1] = 0;

	this->WindowLevelInitial[0] = 1.0; // Window
	this->WindowLevelInitial[1] = 0.5; // Level

	this->CurrentImageProperty = nullptr;
	this->CurrentImageNumber = -1;

	this->InteractionMode = VTKIS_IMAGE2D;

	this->XViewRightVector[0] = 0;
	this->XViewRightVector[1] = 1;
	this->XViewRightVector[2] = 0;

	this->XViewUpVector[0] = 0;
	this->XViewUpVector[1] = 0;
	this->XViewUpVector[2] = -1;

	this->YViewRightVector[0] = 1;
	this->YViewRightVector[1] = 0;
	this->YViewRightVector[2] = 0;

	this->YViewUpVector[0] = 0;
	this->YViewUpVector[1] = 0;
	this->YViewUpVector[2] = -1;

	this->ZViewRightVector[0] = 1;
	this->ZViewRightVector[1] = 0;
	this->ZViewRightVector[2] = 0;

	this->ZViewUpVector[0] = 0;
	this->ZViewUpVector[1] = 1;
	this->ZViewUpVector[2] = 0;
}

//----------------------------------------------------------------------------
myStyle::~myStyle()
{
	if (this->CurrentImageProperty)
	{
		this->CurrentImageProperty->Delete();
	}
}

//----------------------------------------------------------------------------
void myStyle::StartWindowLevel()
{
	if (this->State != VTKIS_NONE)
	{
		return;
	}
	this->StartState(VTKIS_WINDOW_LEVEL);

	// Get the last (the topmost) image
	this->SetCurrentImageNumber(this->CurrentImageNumber);

	if (this->HandleObservers &&
		this->HasObserver(vtkCommand::StartWindowLevelEvent))
	{
		this->InvokeEvent(vtkCommand::StartWindowLevelEvent, this);
	}
	else
	{
		if (this->CurrentImageProperty)
		{
			vtkImageProperty *property = this->CurrentImageProperty;
			this->WindowLevelInitial[0] = property->GetColorWindow();
			this->WindowLevelInitial[1] = property->GetColorLevel();
		}
	}
}

//----------------------------------------------------------------------------
void myStyle::EndWindowLevel()
{
	if (this->State != VTKIS_WINDOW_LEVEL)
	{
		return;
	}
	if (this->HandleObservers)
	{
		this->InvokeEvent(vtkCommand::EndWindowLevelEvent, this);
	}
	this->StopState();
}

//----------------------------------------------------------------------------
void myStyle::StartPick()
{
	if (this->State != VTKIS_NONE)
	{
		return;
	}
	this->StartState(VTKIS_PICK);
	if (this->HandleObservers)
	{
		this->InvokeEvent(vtkCommand::StartPickEvent, this);
	}
}

//----------------------------------------------------------------------------
void myStyle::EndPick()
{
	if (this->State != VTKIS_PICK)
	{
		return;
	}
	if (this->HandleObservers)
	{
		this->InvokeEvent(vtkCommand::EndPickEvent, this);
	}
	this->StopState();
}

//----------------------------------------------------------------------------
void myStyle::StartSlice()
{
	if (this->State != VTKIS_NONE)
	{
		return;
	}
	this->StartState(VTKIS_SLICE);
}

//----------------------------------------------------------------------------
void myStyle::EndSlice()
{
	if (this->State != VTKIS_SLICE)
	{
		return;
	}
	this->StopState();
}

//----------------------------------------------------------------------------
void myStyle::OnMouseMove()
{
	int x = this->Interactor->GetEventPosition()[0];
	int y = this->Interactor->GetEventPosition()[1];

	switch (this->State)
	{
	case VTKIS_WINDOW_LEVEL:
		this->FindPokedRenderer(x, y);
		this->WindowLevel();
		this->InvokeEvent(vtkCommand::InteractionEvent, nullptr);
		break;

	case VTKIS_PICK:
		this->FindPokedRenderer(x, y);
		this->Pick();
		this->InvokeEvent(vtkCommand::InteractionEvent, nullptr);
		break;

	case VTKIS_SLICE:
		this->FindPokedRenderer(x, y);
		this->Slice();
		this->InvokeEvent(vtkCommand::InteractionEvent, nullptr);
		break;
	}

	// Call parent to handle all other states and perform additional work

	this->Superclass::OnMouseMove();
}
//----------------------------------------------------------------------------
void myStyle::StartMouseOperation(int qualBotao, int x, int y) {
	switch (codigoOperacaoDosBotoes[qualBotao]) {
	case VTKIS_NONE:
		return;
		break;
	case VTKIS_ROTATE:
		this->StartRotate();
		break;
	case VTKIS_PAN:
		this->StartPan();
		break;
	case VTKIS_SPIN:
		this->StartSpin();
		break;
	case VTKIS_DOLLY:
		this->StartSlice();
		break;
	case VTKIS_ZOOM:
		this->StartDolly();
		break;
	default:
		this->WindowLevelStartPosition[0] = x;
		this->WindowLevelStartPosition[1] = y;
		this->StartWindowLevel();
		break;
	}
}
//-----------------------------------------------------
void myStyle::EndMouseOperation(int qualBotao) {
	switch (codigoOperacaoDosBotoes[qualBotao]) {
	case VTKIS_NONE:
		return;
		break;
	case VTKIS_ROTATE:
		this->EndRotate();
		break;
	case VTKIS_PAN:
		this->EndPan();
		break;
	case VTKIS_SPIN:
		this->EndSpin();
		break;
	case VTKIS_DOLLY:
		this->EndSlice();
		break;
	case VTKIS_ZOOM:
		this->EndDolly();
		break;
	default:
		this->EndWindowLevel();
		break;
	}
}

void myStyle::OnLeftButtonDown()
{
	int x = this->Interactor->GetEventPosition()[0];
	int y = this->Interactor->GetEventPosition()[1];

	this->FindPokedRenderer(x, y);
	if (this->CurrentRenderer == nullptr)
	{
		return;
	}
	
	this->GrabFocus(this->EventCallbackCommand);
	//Escolha da opera��o de acordo com o escolhido pelo usu�rio:
	StartMouseOperation(0, x,y);
}

//----------------------------------------------------------------------------
void myStyle::OnLeftButtonUp()
{
	//Escolha da opera��o de acordo com o escolhido pelo usu�rio:
	EndMouseOperation(0);
	this->ReleaseFocus();
 
	// Call parent to handle all other states and perform additional work

	this->Superclass::OnLeftButtonUp();
}

//----------------------------------------------------------------------------
void myStyle::OnMiddleButtonDown()
{
	int x = this->Interactor->GetEventPosition()[0];
	int y = this->Interactor->GetEventPosition()[1];

	this->FindPokedRenderer(x, y);
	if (this->CurrentRenderer == nullptr)
	{
		return;
	}

	this->GrabFocus(this->EventCallbackCommand);
	//Escolha da opera��o de acordo com o escolhido pelo usu�rio:
	StartMouseOperation(1, x, y);
}

void myStyle::Pan()
{
	if (this->CurrentRenderer == NULL)
	{
		return;
	}

	vtkRenderWindowInteractor *rwi = this->Interactor;

	double viewFocus[4], focalDepth, viewPoint[3];
	double newPickPoint[4], oldPickPoint[4], motionVector[3];

	// Calculate the focal depth since we'll be using it a lot

	vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
	camera->GetFocalPoint(viewFocus);
	this->ComputeWorldToDisplay(viewFocus[0], viewFocus[1], viewFocus[2],
		viewFocus);
	focalDepth = viewFocus[2];

	this->ComputeDisplayToWorld(rwi->GetEventPosition()[0],
		rwi->GetEventPosition()[1],
		focalDepth,
		newPickPoint);

	// Has to recalc old mouse point since the viewport has moved,
	// so can't move it outside the loop

	this->ComputeDisplayToWorld(rwi->GetLastEventPosition()[0],
		rwi->GetLastEventPosition()[1],
		focalDepth,
		oldPickPoint);

	// Camera motion is reversed

	motionVector[0] = oldPickPoint[0] - newPickPoint[0];
	motionVector[1] = oldPickPoint[1] - newPickPoint[1];
	motionVector[2] = oldPickPoint[2] - newPickPoint[2];

	camera->GetFocalPoint(viewFocus);
	camera->GetPosition(viewPoint);
	camera->SetFocalPoint(-motionVector[0] + viewFocus[0],
		-motionVector[1] + viewFocus[1],
		-motionVector[2] + viewFocus[2]);

	camera->SetPosition(-motionVector[0] + viewPoint[0],
		-motionVector[1] + viewPoint[1],
		-motionVector[2] + viewPoint[2]);

	if (rwi->GetLightFollowCamera())
	{
		this->CurrentRenderer->UpdateLightsGeometryToFollowCamera();
	}

	rwi->Render();
}


//----------------------------------------------------------------------------
void myStyle::OnMiddleButtonUp()
{
	//Escolha da opera��o de acordo com o escolhido pelo usu�rio:
	EndMouseOperation(1);
	this->ReleaseFocus();

	// Call parent to handle all other states and perform additional work
	this->Superclass::OnMiddleButtonUp();
}

//----------------------------------------------------------------------------
void myStyle::OnRightButtonDown()
{
	int x = this->Interactor->GetEventPosition()[0];
	int y = this->Interactor->GetEventPosition()[1];

	this->FindPokedRenderer(x, y);
	if (this->CurrentRenderer == nullptr)
	{
		return;
	}

	this->GrabFocus(this->EventCallbackCommand);
	//Escolha da opera��o de acordo com o escolhido pelo usu�rio:
	StartMouseOperation(2, x, y);
}

//----------------------------------------------------------------------------
void myStyle::OnRightButtonUp()
{
	//Escolha da opera��o de acordo com o escolhido pelo usu�rio:
	EndMouseOperation(2);
	this->ReleaseFocus();

	// Call parent to handle all other states and perform additional work
	this->Superclass::OnMiddleButtonUp();
}

//----------------------------------------------------------------------------
void myStyle::OnChar()
{
	vtkRenderWindowInteractor *rwi = this->Interactor;

	switch (rwi->GetKeyCode())
	{
	case 'f':
	case 'F':
	{
		this->AnimState = VTKIS_ANIM_ON;
		vtkAssemblyPath *path = nullptr;
		this->FindPokedRenderer(rwi->GetEventPosition()[0],
			rwi->GetEventPosition()[1]);
		rwi->GetPicker()->Pick(rwi->GetEventPosition()[0],
			rwi->GetEventPosition()[1], 0.0,
			this->CurrentRenderer);
		vtkAbstractPropPicker *picker;
		if ((picker = vtkAbstractPropPicker::SafeDownCast(rwi->GetPicker())))
		{
			path = picker->GetPath();
		}
		if (path != nullptr)
		{
			rwi->FlyToImage(this->CurrentRenderer, picker->GetPickPosition());
		}
		this->AnimState = VTKIS_ANIM_OFF;
		break;
	}

	case 'r':
	case 'R':
		// Allow either shift/ctrl to trigger the usual 'r' binding
		// otherwise trigger reset window level event
		if (rwi->GetShiftKey() || rwi->GetControlKey())
		{
			this->Superclass::OnChar();
		}
		else if (this->HandleObservers &&
			this->HasObserver(vtkCommand::ResetWindowLevelEvent))
		{
			this->InvokeEvent(vtkCommand::ResetWindowLevelEvent, this);
		}
		else if (this->CurrentImageProperty)
		{
			vtkImageProperty *property = this->CurrentImageProperty;
			property->SetColorWindow(this->WindowLevelInitial[0]);
			property->SetColorLevel(this->WindowLevelInitial[1]);
			this->Interactor->Render();
		}
		break;

	case 'x':
	case 'X':
	{
		this->SetImageOrientation(this->XViewRightVector, this->XViewUpVector);
		this->Interactor->Render();
	}
	break;

	case 'y':
	case 'Y':
	{
		this->SetImageOrientation(this->YViewRightVector, this->YViewUpVector);
		this->Interactor->Render();
	}
	break;

	case 'z':
	case 'Z':
	{
		this->SetImageOrientation(this->ZViewRightVector, this->ZViewUpVector);
		this->Interactor->Render();
	}
	break;

	default:
		this->Superclass::OnChar();
		break;
	}
}

//----------------------------------------------------------------------------
void myStyle::WindowLevel()
{
	vtkRenderWindowInteractor *rwi = this->Interactor;

	this->WindowLevelCurrentPosition[0] = rwi->GetEventPosition()[0];
	this->WindowLevelCurrentPosition[1] = rwi->GetEventPosition()[1];

	if (this->HandleObservers &&
		this->HasObserver(vtkCommand::WindowLevelEvent))
	{
		this->InvokeEvent(vtkCommand::WindowLevelEvent, this);
	}
	else if (this->CurrentImageProperty)
	{
		int *size = this->CurrentRenderer->GetSize();

		double window = this->WindowLevelInitial[0];
		double level = this->WindowLevelInitial[1];

		// Compute normalized delta

		double dx = (this->WindowLevelCurrentPosition[0] -
			this->WindowLevelStartPosition[0]) * 4.0 / size[0];
		double dy = (this->WindowLevelStartPosition[1] -
			this->WindowLevelCurrentPosition[1]) * 4.0 / size[1];

		// Scale by current values

		if (fabs(window) > 0.01)
		{
			dx = dx * window;
		}
		else
		{
			dx = dx * (window < 0 ? -0.01 : 0.01);
		}
		if (fabs(level) > 0.01)
		{
			dy = dy * level;
		}
		else
		{
			dy = dy * (level < 0 ? -0.01 : 0.01);
		}

		// Abs so that direction does not flip

		if (window < 0.0)
		{
			dx = -1 * dx;
		}
		if (level < 0.0)
		{
			dy = -1 * dy;
		}

		// Compute new window level

		double newWindow = dx + window;
		double newLevel = level - dy;

		if (newWindow < 0.01)
		{
			newWindow = 0.01;
		}

		this->CurrentImageProperty->SetColorWindow(newWindow);
		this->CurrentImageProperty->SetColorLevel(newLevel);

		this->Interactor->Render();
	}
}

//----------------------------------------------------------------------------
void myStyle::Pick()
{
	this->InvokeEvent(vtkCommand::PickEvent, this);
}

//----------------------------------------------------------------------------
void myStyle::Slice()
{
	if (this->CurrentRenderer == nullptr)
	{
		return;
	}

	vtkRenderWindowInteractor *rwi = this->Interactor;
	int dy = rwi->GetEventPosition()[1] - rwi->GetLastEventPosition()[1];

	vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
	double *range = camera->GetClippingRange();
	double distance = camera->GetDistance();

	// scale the interaction by the height of the viewport
	double viewportHeight = 0.0;
	if (camera->GetParallelProjection())
	{
		viewportHeight = camera->GetParallelScale();
	}
	else
	{
		double angle = vtkMath::RadiansFromDegrees(camera->GetViewAngle());
		viewportHeight = 2.0*distance*tan(0.5*angle);
	}

	int *size = this->CurrentRenderer->GetSize();
	double delta = dy*viewportHeight / size[1];
	distance += delta;

	// clamp the distance to the clipping range
	if (distance < range[0])
	{
		distance = range[0] + viewportHeight*1e-3;
	}
	if (distance > range[1])
	{
		distance = range[1] - viewportHeight*1e-3;
	}
	camera->SetDistance(distance);

	rwi->Render();
}

//----------------------------------------------------------------------------
void myStyle::SetImageOrientation(
	const double leftToRight[3], const double viewUp[3])
{
	if (this->CurrentRenderer)
	{
		// the cross product points out of the screen
		double vector[3];
		vtkMath::Cross(leftToRight, viewUp, vector);
		double focus[3];
		vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
		camera->GetFocalPoint(focus);
		double d = camera->GetDistance();
		camera->SetPosition(focus[0] + d*vector[0],
			focus[1] + d*vector[1],
			focus[2] + d*vector[2]);
		camera->SetFocalPoint(focus);
		camera->SetViewUp(viewUp);
	}
}

//----------------------------------------------------------------------------
// This is a way of dealing with images as if they were layers.
// It looks through the renderer's list of props and sets the
// interactor ivars from the Nth image that it finds.  You can
// also use negative numbers, i.e. -1 will return the last image,
// -2 will return the second-to-last image, etc.
void myStyle::SetCurrentImageNumber(int i)
{
	this->CurrentImageNumber = i;

	if (!this->CurrentRenderer)
	{
		return;
	}

	vtkPropCollection *props = this->CurrentRenderer->GetViewProps();
	vtkProp *prop = nullptr;
	vtkAssemblyPath *path;
	vtkImageSlice *imageProp = nullptr;
	vtkCollectionSimpleIterator pit;

	for (int k = 0; k < 2; k++)
	{
		int j = 0;
		for (props->InitTraversal(pit); (prop = props->GetNextProp(pit)); )
		{
			bool foundImageProp = false;
			for (prop->InitPathTraversal(); (path = prop->GetNextPath()); )
			{
				vtkProp *tryProp = path->GetLastNode()->GetViewProp();
				imageProp = vtkImageSlice::SafeDownCast(tryProp);
				if (imageProp)
				{
					if (j == i && imageProp->GetPickable())
					{
						foundImageProp = true;
						break;
					}
					imageProp = nullptr;
					j++;
				}
			}
			if (foundImageProp)
			{
				break;
			}
		}
		if (i < 0)
		{
			i += j;
		}
	}

	vtkImageProperty *property = nullptr;
	if (imageProp)
	{
		property = imageProp->GetProperty();
	}

	if (property != this->CurrentImageProperty)
	{
		if (this->CurrentImageProperty)
		{
			this->CurrentImageProperty->Delete();
		}

		this->CurrentImageProperty = property;

		if (this->CurrentImageProperty)
		{
			this->CurrentImageProperty->Register(this);
		}
	}
}

//----------------------------------------------------------------------------
void myStyle::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);

	os << indent << "Window Level Current Position: ("
		<< this->WindowLevelCurrentPosition[0] << ", "
		<< this->WindowLevelCurrentPosition[1] << ")\n";

	os << indent << "Window Level Start Position: ("
		<< this->WindowLevelStartPosition[0] << ", "
		<< this->WindowLevelStartPosition[1] << ")\n";

	os << indent << "Interaction Mode: ";
	if (this->InteractionMode == VTKIS_IMAGE2D)
	{
		os << "Image2D\n";
	}
	else if (this->InteractionMode == VTKIS_IMAGE3D)
	{
		os << "Image3D\n";
	}
	else if (this->InteractionMode == VTKIS_IMAGE_SLICING)
	{
		os << "ImageSlicing\n";
	}
	else
	{
		os << "Unknown\n";
	}

	os << indent << "X View Right Vector: ("
		<< this->XViewRightVector[0] << ", "
		<< this->XViewRightVector[1] << ", "
		<< this->XViewRightVector[2] << ")\n";

	os << indent << "X View Up Vector: ("
		<< this->XViewUpVector[0] << ", "
		<< this->XViewUpVector[1] << ", "
		<< this->XViewUpVector[2] << ")\n";

	os << indent << "Y View Right Vector: ("
		<< this->YViewRightVector[0] << ", "
		<< this->YViewRightVector[1] << ", "
		<< this->YViewRightVector[2] << ")\n";

	os << indent << "Y View Up Vector: ("
		<< this->YViewUpVector[0] << ", "
		<< this->YViewUpVector[1] << ", "
		<< this->YViewUpVector[2] << ")\n";

	os << indent << "Z View Right Vector: ("
		<< this->ZViewRightVector[0] << ", "
		<< this->ZViewRightVector[1] << ", "
		<< this->ZViewRightVector[2] << ")\n";

	os << indent << "Z View Up Vector: ("
		<< this->ZViewUpVector[0] << ", "
		<< this->ZViewUpVector[1] << ", "
		<< this->ZViewUpVector[2] << ")\n";
}


void myStyle::SetOperacao(int idBotao, int idOperacao) {
	codigoOperacaoDosBotoes[idBotao] = idOperacao;
}